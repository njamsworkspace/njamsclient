package com.w3p.im.iib.mon.monitor.data.groovy;

import static org.junit.Assert.*

import org.junit.After
import org.junit.AfterClass
import org.junit.Before
import org.junit.BeforeClass
import org.junit.Test

import com.w3p.im.iib.mon.monitor.data.MonitoringEventSummary
import com.w3p.im.iib.mon.monitor.data.NodeData

class TestCreateMonitoringEventSummary {

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

//	@Test
	public void testCreateSummaryWithPayloadAndData() {
		
				
	assertTrue(true) 
		
	}
	
//	@Test
	public void testCreateSummarytWithoutPayloadAndData() {
		
		MonitoringEventSummary summary = new MonitoringEventSummary("IB9NODE", "PagerMesageFlows", "TextMessenger")
		summary.correlationId = "04959914-c387-4464-8a2c-9993753fe780-22"
		summary.start = "2017-03-20T17:15:10.788Z"
		summary.end = "2017-03-20T17:15:10.791Z"
//		summary.duration = 3
		NodeData nodeData1 = new NodeData("TEXTMESSENGER")
		nodeData1.payloadOut = "PD94bWwgdmVyc2lvbj0iMS4wIj8+DQo8UGFnZXI+DQoJPFRleHQ+SGVsbG8gV29ybGQgNjwvVGV4dD4NCjwvUGFnZXI+"
		nodeData1.applicationData = null 
		NodeData nodeData2 = new NodeData("Mapping")
		nodeData2.payloadOut = "PD94bWwgdmVyc2lvbj0iMS4wIj8+PFBhZ2VyPjxUZXh0PkhlbGxvIFdvcmxkIDYgUG93ZXJlZCBieSBJQk0uPC9UZXh0PjwvUGFnZXI+"
		nodeData2.applicationData = null
		NodeData nodeData3 = new NodeData("Mapping")
		nodeData3.payloadOut = ""
		nodeData3.applicationData = null
		summary.executionPath = [nodeData1, nodeData2, nodeData3] as List<String>
		
		CreateMonitoringEventSummary monSummary = new CreateMonitoringEventSummary()
		String summaryAsXML = monSummary.createAsXML(summary) 
		
		assert summaryAsXML != null
//		println summaryAsXML
		assertTrue(summaryAsXML.contains("<integrationNode>IB9NODE</integrationNode>"))
		assertTrue(summaryAsXML.contains("<duration>3</duration>"))		
		assertTrue(summaryAsXML.contains("<executionPath><node name='TEXTMESSENGER'>"))
	}

// Date.parse("yyyy-MM-dd'T'HH:mm:ss.SSSX", "2017-03-20T17:15:10.788Z")
}
