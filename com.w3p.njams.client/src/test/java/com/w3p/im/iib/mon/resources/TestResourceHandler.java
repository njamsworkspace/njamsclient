package com.w3p.im.iib.mon.resources;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

public class TestResourceHandler {

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testGetNLSResourceString() {
		String result = ResourceHandler.getNLSResource("Test");
		assertTrue(result.equals("Test"));
	}

	@Test
	public void testGetNLSResourceStringStringArray() {
		String result = ResourceHandler.getNLSResource("Variable", new String[] {"789", "123"});
		assertTrue(result.equals("abcd123xyz789"));
	}

}
