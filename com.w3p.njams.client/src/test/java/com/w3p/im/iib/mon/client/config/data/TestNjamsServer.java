package com.w3p.im.iib.mon.client.config.data;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class TestNjamsServer {

	private static final String ENDPOINT = "ingest.integrationmatters.com";
	private static final String CLIENT_PRIVATE_KEY = "e08740c608-private.pem.key";
	private static final String CLIENT_INSTANCE_ID = "e08740c608-instanceId";
	private static final String CLIENT_CERT = "e08740c608-certificate.pem";
	private static final String API_KEY = "e08740c608-api.key";
	private static final String PROPS_LOC = "certs";
	private static final String SERVER_TYPE = "CLOUD";

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testCanCreateNjamsServer() {
		NjamsServer njamsServer = new NjamsServer();
		assertNotNull(njamsServer);
	}
	
	@Test
	public void testCanPopulateNjamsServer() {
		NjamsServer njamsServer = new NjamsServer();
		JmsServer js = new JmsServer();
		CloudServer cs = new CloudServer();
		
		njamsServer.setServerType(SERVER_TYPE);
		njamsServer.setSubflowDrillDown(true);
		njamsServer.setJmsConfig(js);
		njamsServer.setCloudConfig(cs);
		
		assertEquals(SERVER_TYPE, njamsServer.getServerType());
		assertTrue(njamsServer.isSubflowDrillDown());
		assertEquals(js, njamsServer.getJmsConfig());
		assertEquals(cs, njamsServer.getCloudConfig());
	}
	
	
	@Test
	public void testCanCreateJmsServer() {
		JmsServer jmsServer = new JmsServer();
		assertNotNull(jmsServer);
	}
	
	@Test
	public void testCanPopulateJmsServer() {
		User jmsUser = new User("njams", "njams");
		User jndiUser = new User("njams", "njams");
		
		Jndi jndi = new Jndi();
		jndi.setUser(jndiUser);
		
		JmsServer jmsServer = new JmsServer();
		assertNotNull(jmsServer);
		
		jmsServer.setConnectionFactory("ConnectionFactory");
		jmsServer.setDestination("njams");
		jmsServer.setDestinationCommands("cmds.dest");
		jmsServer.setJmsUser(jmsUser);
		jmsServer.setJndi(jndi);
		jmsServer.setInitialContextFactory("org.apache.activemq.jndi.ActiveMQInitialContextFactory");
		jmsServer.setProviderURL("tcp://localhost:61616");
		jmsServer.setUseSSL(false);
		
		assertTrue(jmsServer.getConnectionFactory().equals("ConnectionFactory"));
		assertTrue(jmsServer.getDestination().equals("njams"));
		assertTrue(jmsServer.getDestinationCommands().equals("cmds.dest"));
		assertTrue(jmsServer.getInitialContextFactory().equals("org.apache.activemq.jndi.ActiveMQInitialContextFactory"));
		assertTrue(jmsServer.getProviderURL().equals("tcp://localhost:61616"));
		
		assertTrue(jmsServer.getJmsUser().equals(jmsUser));
		assertTrue(jmsServer.getJmsUser().getUserName().equals("njams"));
		assertTrue(jmsServer.getJmsUser().getPasswordInClear().equals("njams"));
		assertTrue(jmsServer.getJmsUser().getPasswordEncoded().equals("njams"));
		
		assertTrue(jmsServer.getJndi().equals(jndi));
		assertTrue(jmsServer.getJndi().getUser().equals(jndiUser));
		assertTrue(jmsServer.getJndi().getUser().getUserName().equals("njams"));
		assertTrue(jmsServer.getJndi().getUser().getPasswordInClear().equals("njams"));
		assertTrue(jmsServer.getJndi().getUser().getPasswordEncoded().equals("njams"));
		
		
		// njams base64 encoded = bmphbXM=
		jmsUser.setPassword("{bmphbXM=}");
		assertTrue(jmsServer.getJmsUser().getPasswordInClear().equals("njams"));
		assertTrue(jmsServer.getJmsUser().getPasswordEncoded().equals("bmphbXM="));
		
		jndiUser.setPassword("{bmphbXM=}");
		assertTrue(jmsServer.getJndi().getUser().getPasswordInClear().equals("njams"));
		assertTrue(jmsServer.getJndi().getUser().getPasswordEncoded().equals("bmphbXM="));
	}

	@Test
	public void testCanPopulateCloudServer() {
		CloudServer cs = new CloudServer();
		cs.setPropertiesLoc(PROPS_LOC);
		
		cs.setApiKey(API_KEY);
		cs.setClientCertificate(CLIENT_CERT);
		cs.setClientInstanceId(CLIENT_INSTANCE_ID);
		cs.setClientPrivateKey(CLIENT_PRIVATE_KEY);
		cs.setEndpoint(ENDPOINT);
		
		String propsLoc = cs.getPropertiesLoc();
		assertEquals(PROPS_LOC.concat("/"), propsLoc);
		assertEquals(propsLoc.concat(API_KEY), cs.getApiKey());
		assertEquals(propsLoc.concat(CLIENT_CERT), cs.getClientCertificate());
		assertEquals(propsLoc.concat(CLIENT_INSTANCE_ID), cs.getClientInstanceId());
		assertEquals(propsLoc.concat(CLIENT_PRIVATE_KEY), cs.getClientPrivateKey());
		assertEquals(ENDPOINT, cs.getEndpoint());
	}
}
