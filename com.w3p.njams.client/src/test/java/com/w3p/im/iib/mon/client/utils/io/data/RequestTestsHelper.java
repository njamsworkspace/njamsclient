package com.w3p.im.iib.mon.client.utils.io.data;

public class RequestTestsHelper {
	

	public RequestTestsHelper() {
	}
	
	public IWriteQueueRequest createRequestDataOutForIB10QMGR() {
		ConnectionData connData = createConnectionDataForIB10QMGR("NJAMS.UTILS.TEST.OUT");
		IWriteQueueRequest req = new WriteQueueRequest();
		req.setConnectionData(connData);		
				
		return req;
	}
	
	public IReadQueueRequest createRequestDataInForIB10QMGR() {
		ConnectionData connData = createConnectionDataForIB10QMGR("NJAMS.UTILS.TEST.IN");
		IReadQueueRequest req = new ReadQueueRequest();
		req.setConnectionData(connData);		
		return req;
	}

	public ConnectionData createConnectionDataForIB10QMGR(String queueName) {
		return createConnectionDataForIB10QMGR(queueName, false);
	}
	
	public ConnectionData createConnectionDataForIB10QMGR(String queueName, boolean useCredentials) {
		ConnectionData connData = new ConnectionData();
		connData.setQmgr("IB10QMGR")
		.setQueue(queueName)
		.setHost("localhost")
		.setPort(3414)
		.setServerChan("DEV.APP.SVRCONN");
		if (useCredentials) {
			connData.setUserid("njams")
			.setPassword("njams");
		}
		return connData;
	}


}
