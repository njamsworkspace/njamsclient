package com.w3p.im.iib.mon.client.utils.io.readers;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.io.File;
import java.io.IOException;
import java.nio.file.DirectoryStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.util.ArrayList;
import java.util.List;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.ClassRule;
import org.junit.Test;
import org.junit.rules.TemporaryFolder;

import com.w3p.im.iib.mon.client.utils.io.data.IReadFileRequest;
import com.w3p.im.iib.mon.client.utils.io.data.ReadFileRequest;
import com.w3p.im.iib.mon.client.utils.io.data.Response;

public class TestFilesReader {
	
	private static final Path inputFolderPath = Paths.get("src","test","resources", "inputFolder");
//	private RequestTestsHelper helper = new RequestTestsHelper();

	@ClassRule
	public static TemporaryFolder tempInputFolder = new TemporaryFolder();
	@ClassRule
	public static TemporaryFolder emptyInputFolder = new TemporaryFolder();
	@ClassRule
	public static TemporaryFolder fileInputFolder = new TemporaryFolder();

	@BeforeClass
	public static void copyTestFiles() throws IOException {
		// Copy test files to a jUnit temp folder
		DirectoryStream<Path> filesList = Files.newDirectoryStream(inputFolderPath); // List the source files
		for (Path inFilePath : filesList) {
			String fileName = inFilePath.toFile().getName();
			File tempFile = tempInputFolder.newFile(fileName);
			Path tempFilePath = Paths.get(tempFile.getAbsolutePath());
			Files.copy(inFilePath, tempFilePath, StandardCopyOption.REPLACE_EXISTING, StandardCopyOption.COPY_ATTRIBUTES);
		}
		
		// Create a file in fileInputFolder for testing 'folder not folder'
		fileInputFolder.newFile("InputFolder");
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testCanCreateFilesReader() {
		FileReader fr = new FileReader();
		assertNotNull(fr);
	}

	@Test
	public void testCanReadFilesFromFolder() throws IOException {
		Response resp = new Response();
		IReadFileRequest req = new ReadFileRequest();
		String inputFolder = tempInputFolder.getRoot().getAbsolutePath();
		req.setSource(inputFolder);
		req.setNamePattern("*.xml");
		
		FileReader fr = new FileReader();
		fr.init(req, resp);
		FileContent fc = (FileContent)fr.readAll(req, resp);
		fr.close();
		
		assertTrue(resp.isSuccess());
		assertEquals(10, fc.getSize());
		assertEquals(10, resp.getFilesToBeRead());
		assertEquals(10, resp.getFilesRead());
		assertEquals(0, resp.getFilesDeleted());
		assertEquals(0, resp.getFilesInError());
		assertFalse(resp.getAllMessages().isPresent());
		// Verify the file has been read
		assertTrue(fc.getContentOfFirstItem().startsWith("<wmb:event"));
	}

	@Test
	public void testCanHandleFileDoesNotExistWhenGettingFileContentsAsString() throws IOException {
		Response resp = new Response();
		IReadFileRequest req = new ReadFileRequest();
		Path errorPath = Paths.get("missing");
		
		req.setSource(errorPath.toString());
		req.setNamePattern("*.xml");
		FileReader fr = new FileReader();
		fr.init(req, resp);
		fr.close();

		assertFalse(resp.isSuccess());
		assertTrue(resp.getAllMessages().isPresent());
		assertTrue(resp.getAllMessages().get().equals("ERROR: Input folder 'missing' does not exist.\r\n"));
	}
	
//	private List<Path> getListOfFilesForTesting() throws IOException {
//		List<Path> filesForTesting = new ArrayList<>();
//		String inputFolder = tempInputFolder.getRoot().getAbsolutePath();
//		Path inputFolderPath = Paths.get(inputFolder);
//		try (DirectoryStream<Path> stream = Files.newDirectoryStream(inputFolderPath)) {
//			for (Path file : stream) {
//				filesForTesting.add(file);
//			}
//		}
//		return filesForTesting;
//	}
}
