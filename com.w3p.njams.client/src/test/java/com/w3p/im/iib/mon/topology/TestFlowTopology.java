package com.w3p.im.iib.mon.topology;

import static org.junit.Assert.assertNotNull;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import com.w3p.api.config.proxy.BrokerConnectionParameters;
import com.w3p.api.config.proxy.BrokerProxy;
import com.w3p.api.config.proxy.ConfigManagerProxyLoggedException;
import com.w3p.api.config.proxy.ConfigManagerProxyPropertyNotInitializedException;
import com.w3p.api.config.proxy.IntegrationNodeConnectionParameters;
import com.w3p.im.iib.mon.exceptions.internal.IIBConnectionException;
import com.w3p.im.iib.mon.exceptions.internal.IIBTopologyCreationException;
import com.w3p.im.iib.mon.monitor.data.IntNodeConnectionObject;
import com.w3p.im.iib.mon.monitor.data.MonitoredIntegrationNode;
import com.w3p.im.iib.mon.monitor.data.MonitoredIntegrationServer;
import com.w3p.im.iib.mon.monitor.data.MonitoredObject;
import com.w3p.im.iib.mon.topology.data.TopologyRequest;
import com.w3p.im.iib.mon.topology.data.TopologyResponse;

public class TestFlowTopology {
	
	private static String intNodeName = "nJAMS";
	private static String intSvrName = "nJAMSDemo";
	private static String bkrHostname = "iibdemo";
	private static int bkrAdminPort = 4414;
	private static BrokerProxy intNode = null;
	
	private static MonitoredObject<?> monitoringScope;

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		monitoringScope = determineScopeForMonitoring();
		IntNodeConnectionObject bkrConn = connectToRemoteBroker();
		intNode = bkrConn.getIntNode(); //BrokerProxy.getLocalInstance(integrationNodeName);
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

//	@Test
//	public void testCanCreateFlowTopology() {
//		TopologyCreator topology = new TopologyCreator();
//		assertNotNull("Flow topology is null", topology);		
//	}
	

	@Test
	public void testCanGetTopology() throws IIBTopologyCreationException {
		TopologyCreator topologyCreator = new TopologyCreator();
//		TopologyResponse resp0 = topology.init(intNode);
//		assertNotNull("Response for init() is null", resp0);
//		assertTrue(resp0.isSuccess());
		TopologyRequest request = new TopologyRequest(intNode, monitoringScope, "intSvrName"); //TODO handle int svr name
		TopologyResponse response = topologyCreator.createTopology(request);
		assertNotNull("Topology is null", response.getTopology());
//		System.out.println("Topology: "+resp.getTopology());

	}
	
	private static IntNodeConnectionObject connectToRemoteBroker() throws IIBConnectionException, ConfigManagerProxyLoggedException, ConfigManagerProxyPropertyNotInitializedException {
		IntNodeConnectionObject bkrConn = new IntNodeConnectionObject();
		bkrConn.setIpaddress(bkrHostname);//"localhost");
		bkrConn.setPort(bkrAdminPort);
		String userName =  null; //iibConfig.getIibUser().getUserName();
		String password  = null; //iibConfig.getIibUser().getPasswordInClear();
		boolean bUseSSL = false;
		BrokerConnectionParameters bcp = new IntegrationNodeConnectionParameters(bkrConn.getIpaddress(), bkrConn.getPort(), userName, password, bUseSSL); 
		// The next statement is THE most important one in this method. When writing a
		// Message Broker Administration application, this is the one you NEED to call. It starts up the connection to the broker.
		bkrConn.setIntNode(BrokerProxy.getInstance(bcp));
		bkrConn.setIntNodeName(bkrConn.getIntNode().getName());
		bkrConn.setLocal(false);
		return bkrConn;
	}

	private static MonitoredObject<?> determineScopeForMonitoring() {
		MonitoredObject<?> intNode = new MonitoredIntegrationNode(intNodeName);	
		MonitoredIntegrationServer mis = new MonitoredIntegrationServer(intSvrName);
		intNode.addMonitoredChildObject(mis);

		return intNode;		
	}

}
