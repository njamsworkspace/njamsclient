package com.w3p.im.iib.mon.client.config.data;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import com.ibm.broker.config.common.Base64;

public class TestIIBConfig {

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testCanCreateIIBConfig() {
		IIBConfig config = new IIBConfig();
//		config.setHost("host").setMqQueueMgr("qmgr").setIntNode("intNode");
		assertNotNull(config);
		assertEquals("host", config.getHost());
//		assertEquals("qmgr", config.getMqQueueMgr());
		assertEquals("intNode", config.getIntNode());
	}

	@Test
	public void testGetUnencodedPassword() {
		IIBConfig config = new IIBConfig();
//		config.setHost("host").setMqQueueMgr("qmgr").setIntNode("intNode");
		config.getIibUser().setPassword("password");
		assertEquals("password", config.getIibUser().getPasswordInClear());
	}
	
	@Test
	public void testGetPasswordInClear() {
		IIBConfig config = new IIBConfig();
//		config.setHost("host").setMqQueueMgr("qmgr").setIntNode("intNode");
		config.getIibUser().setPassword("{"+Base64.encode("password".getBytes())+"}");
		assertEquals("password", config.getIibUser().getPasswordInClear());
	}
	
	@Test
	public void testGetUnencodedPasswordInClear() {
		IIBConfig config = new IIBConfig();
//		config.setHost("host").setMqQueueMgr("qmgr").setIntNode("intNode");
		config.getIibUser().setPassword("password");
		assertEquals("password", config.getIibUser().getPasswordInClear());
	}

	@Test
	public void testGetPasswordEncoded() {
		String encodedPwd = Base64.encode("password".getBytes());
		IIBConfig config = new IIBConfig();
//		config.setHost("host").setMqQueueMgr("qmgr").setIntNode("intNode");
		config.getIibUser().setPassword("{"+Base64.encode("password".getBytes())+"}");
		assertEquals(encodedPwd, config.getIibUser().getPasswordEncoded());
	}

}
