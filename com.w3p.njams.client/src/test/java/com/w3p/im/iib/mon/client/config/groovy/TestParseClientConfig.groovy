package com.w3p.im.iib.mon.client.config.groovy

import static org.junit.Assert.*

import java.security.PublicKey

import org.junit.After
import org.junit.AfterClass
import org.junit.Before
import org.junit.BeforeClass
import org.junit.Rule
import org.junit.Test

import com.w3p.im.iib.mon.client.config.data.ClientConfig
import com.w3p.im.iib.mon.client.config.data.IbmMqConfig

class TestParseClientConfig {
		
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}


	@Test
	public void canParseClientConfigJMSNoMQTT() {
		
		def clientProperties = '''<?xml version="1.0" encoding="UTF-8"?>
<njamsClient xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:noNamespaceSchemaLocation="file:///E:/data/W3P/IntegrationMatters/code/njamsClientProperties.xsd">
  <Type>IIB-10</Type>
  <ClientMode>PROD</ClientMode> <!-- PROD or DEV - PROD = events from MQ queue, DEV = events using pub/sub -->		
  <Connections>
	<IibEventsSource>queue://QUEUE_MGR/QUEUE</IibEventsSource>
	<IibEventsProblemsQueue>queue://QUEUE_MGR/PROBLEMS_QUEUE</IibEventsProblemsQueue>  <!-- Destination for a set of Event msgs when there is an error. Required for PROD mode -->	
    <IbmMQ>
      <JmsEvents>
        <JMSProviderURL>file:%cd%\\jms\\JNDI_Remote</JMSProviderURL>
        <JMSInitialContextFactory>com.sun.jndi.fscontext.RefFSContextFactory</JMSInitialContextFactory>
        <JMSConnectionFactory>JMSCLNTCF</JMSConnectionFactory>
		<JMSMQUser name="JohnO" password="Pswd"/> <!-- Set both empty for local MQ connection -->
		<JMSClientId>testApps#</JMSClientId>
      </JmsEvents>
    </IbmMQ>
	<MQTT> <!-- Optional -->
		<ClientId>njams#</ClientId>
		<Host>localhost</Host>
		<Port>11883</Port>
		<KeepAliveInterval>30</KeepAliveInterval> <!-- seconds -->
		<QoS>0</QoS> <!-- Quality of Service - values: 0, 1, 2 -->
		<MQTTUser name="JohnO" password="Pswd"/> <!-- Only if the IIB MQTT Broker requires authentication -->
	</MQTT>
    <NjamsServer>
		<ServerType>JMS</ServerType> <!-- JMS = on-prem server, CLOUD = nJAMS cloud server -->
		<SubflowDrillDown>true</SubflowDrillDown> <!-- allow drill-down to subflows in nJAMS server -->
      <JmsServer>
        <JmsUser name="njams" password="njams"/>
        <JmsDestination>njams</JmsDestination>
		<JmsDestinationCommands>dest.cmds</JmsDestinationCommands> <!-- *** -->
        <JMSProviderURL>tcp://localhost:61616</JMSProviderURL>
        <JMSInitialContextFactory>org.apache.activemq.jndi.ActiveMQInitialContextFactory</JMSInitialContextFactory>
        <JMSConnectionFactory>ConnectionFactory</JMSConnectionFactory>
        <UseSSL>false</UseSSL>
        <!-- UseJndi>true</UseJndi -->
        <JNDI>
          <JndiUser name="njams" password="njams"/>
        </JNDI>
      </JmsServer>
      <CloudServer>
		<PropertiesLoc>certs</PropertiesLoc>
		<Endpoint>ingest.integrationmatters.com</Endpoint>
		<API_Key>e08740c608-api.key</API_Key>
		<ClientInstanceId>e08740c608-instanceId</ClientInstanceId>
		<ClientCertificate>e08740c608-certificate.pem</ClientCertificate> 
		<ClientPrivateKey>e08740c608-private.pem.key</ClientPrivateKey>
	  </CloudServer>
    </NjamsServer>
    <IIB>
      <ConnectionType>remote</ConnectionType>
      <Host>JOT420</Host>
      <AdminPort>4414</AdminPort>
      <IntegrationNode name="W3P_NJAMS_DEMO"/>
	  <IIBUser name="IibUser" password="{SWliUHN3ZA==}"/> <!-- 'IibPswd' Base64 encoded -->
    </IIB>
  </Connections>
  <Monitoring>
	<ConsumerThreads>2</ConsumerThreads> <!-- Number of Client threads when reading Event messages from a queue -->	
    <Profile>
      <Directory>%cd%\\monitoringProfiles</Directory>
      <ApplyIfChanged>true</ApplyIfChanged>
	  <TimeToWait>60</TimeToWait> <!-- in seconds, when applying remote Monitoring Profiles -->
    </Profile>
    <IntegrationServers>
      <IntegrationServer name="3rdPartyApps" includeSchemaName="false" includeName="false">
        <Applications>
          <Application name="AddressLookup" includeSchemaName="false">
            <Correlations>
              <GlobalCorrelationIds>
                <Node type="ComIbmMQGetNode" value="$Root/MQMD/MsgId"/>
                <Node type="ComIbmWSInputNode" value="$Root/XMLNSC/PO:purchaseOrder/PO:customerID">
                  <Namespaces>
                    <Namespace prefix="PO" value="http://www.ibm.com/ProcessOrder"/>
                  </Namespaces>
                </Node>
              </GlobalCorrelationIds>
				<ApplicationDataQuery>
					<Query type="ComIbmMQGetNode">
						<Content name="MsgId" xpath="$Root/MQMD/MsgId"/>
					</Query>
					<Query type="ComIbmMQOutputNode">
						<Content name="MsgId" xpath="$LocalEnvironment/WrittenDestination/MQ/DestinationData[1]/msgId"/>
					</Query>
					<Query type="ComIbmMQReply">
						<Content name="MsgId" xpath="$LocalEnvironment/WrittenDestination/MQ/DestinationData[1]/msgId"/>
						<Content name="CorrelId" xpath="$LocalEnvironment/WrittenDestination/MQ/DestinationData[1]/correlId"/>									
					</Query> 
				</ApplicationDataQuery>
            </Correlations>
            <MsgFlow name="*" labelNodeStartsGroup="true">
              <Correlations>
                <GlobalCorrelationIds>
                  <Node type="ComIbmMQInputNode" value="$Root/MQMD/MsgId"/>
                  <Node type="ComIbmMQGetNode" value="$Root/MQMD/MsgId"/>
                  <Node type="ComIbmSOAPInputNode" value="$Root/SOAP/Body/NS1:AddressLookupRequest/NS1:MessageBody/NS5:PostCode">
                    <Namespaces>
                      <Namespace prefix="NS1" value="http://www.petsathome.co.uk/messages/AddressLookup"/>
                      <Namespace prefix="NS5" value="http://www.petsathome.co.uk/types/Address"/>
                    </Namespaces>
                  </Node>
                  <Node type="ComIbmWSInputNode" value="$Root/XMLNSC/PO:purchaseOrder/PO:customerID">
                    <Namespaces>
                      <Namespace prefix="PO" value="http://www.ibm.com/ProcessOrder"/>
                    </Namespaces>
                  </Node>
                </GlobalCorrelationIds>
                <ApplicationData>
                  <MsgId>
                    <MQPut queryText="$LocalEnvironment/WrittenDestination/MQ/DestinationData[1]/msgId"/>
                    <MQGet queryText="$Root/MQMD/MsgId"/>
                  </MsgId>
                </ApplicationData>
              </Correlations>
            </MsgFlow>
            <MsgFlow name="mmm" includePayload="false"/>
            <SubFlow name="*"/>
            <SubFlow name="sss" includePayload="false"/>
          </Application>
        </Applications>
        <SharedLibraries>
          <Library name="shl-1" includeSchemaName="false">
            <SubFlow name="*"/>
            <SubFlow name="shl-sss" labelNodeStartsGroup="true"/>
          </Library>
        </SharedLibraries>
        <StaticLibraries>
         <Library name="stl-1" includeSchemaName="false">
		    <Correlations/>
            <MsgFlow name="stl-mf*">
              <Correlations/>              
            </MsgFlow>
            <MsgFlow name="stl-mf2" includePayload="false"/>
            <SubFlow name="stl-sf1"/>
            <SubFlow name="stl-sf2" labelNodeStartsGroup="true"/>
		  </Library>
        </StaticLibraries>
		<RestAPIs>
		  <RestAPI name="api-1" includeSchemaName="true">
			<Correlations/>
			<MsgFlow name="api-mf*">
				<Correlations/>
			</MsgFlow>
			<MsgFlow name="api-mf2" includePayload="false"/>
			<SubFlow name="*"/>
			<SubFlow name="api-sf2" includePayload="false"/>			
		  </RestAPI>
		</RestAPIs>
      </IntegrationServer>
			<IntegrationServer name="appTesting" includeSchemaName="true" includeName="false">
				<MsgFlow name=""/>
				<SubFlow name=""/>
				<Applications>
					<Application name="HTTPInputApplication" includeSchemaName="false">
						<MsgFlow name="*"/>
						<SubFlow name="*"/>
					</Application>
				</Applications>
				<SharedLibraries>
					<Library name="" includeSchemaName="false">
						<SubFlow name="shl-sss"/>
					</Library>
				</SharedLibraries>
				<StaticLibraries>
					<Library name="" includeSchemaName="false">
						<MsgFlow name="stl-mmm"/>
						<SubFlow name="stl-sss"/>
						<SubFlow name=""/>
					</Library>
				</StaticLibraries>
				<RestAPIs>
					<RestAPI name="" includeSchemaName="false">
						<MsgFlow name=""/>
						<SubFlow name=""/>
					</RestAPI>
				</RestAPIs>
			</IntegrationServer>
	</IntegrationServers>
  </Monitoring>
</njamsClient>
'''
		ClientConfig config = new ParseClientConfig().parse(clientProperties);
		assertNotNull(config)
		assertEquals("IIB-10", config.type)
		assertEquals("PROD", config.clientMode)
		// Connections
		//   MQ		
		assertNotNull(config.connections.ibmMqConfig)
		assertEquals("file:%cd%\\jms\\JNDI_Remote", config.connections.ibmMqConfig.jmsEvents.providerURL)		
		assertEquals("queue://QUEUE_MGR/QUEUE", config.connections.iibEventsSource)
		assertEquals("queue://QUEUE_MGR/PROBLEMS_QUEUE", config.connections.iibEventsProblemsQueue)
		assertEquals("com.sun.jndi.fscontext.RefFSContextFactory", config.connections.ibmMqConfig.jmsEvents.initialContextFactory)
		assertEquals("JMSCLNTCF", config.connections.ibmMqConfig.jmsEvents.connectionFactory)
		assertEquals("JohnO", config.connections.ibmMqConfig.jmsEvents.jmsMqUser.userName)
		assertEquals("Pswd", config.connections.ibmMqConfig.jmsEvents.jmsMqUser.passwordInClear)
		assertEquals("testApps#", config.connections.ibmMqConfig.jmsEvents.jmsClientId)
		// 	 MQTT
		assertFalse(config.connections.iibEventsSource.equals("MQTT"))
		assertEquals("njams#", config.connections.mqttConfig.clientId)
		assertEquals("localhost", config.connections.mqttConfig.host)		
		assertEquals(11883, config.connections.mqttConfig.port)
		assertEquals(30, config.connections.mqttConfig.keepAliveInterval)
		assertEquals(0, config.connections.mqttConfig.qos)
		assertNotNull(config.connections.mqttConfig.MQTTUser)
		assertEquals("JohnO", config.connections.mqttConfig.MQTTUser.userName)
		assertEquals("Pswd", config.connections.mqttConfig.MQTTUser.passwordInClear)
		//   nJAMS server
		assertNotNull(config.connections.serverConfig)
		assertEquals("JMS", config.connections.serverConfig.serverType);
		assertTrue(config.connections.serverConfig.subflowDrillDown)
		//     JMS server
		assertEquals("njams", config.connections.serverConfig.jmsConfig.jmsUser.userName)
		assertEquals("njams", config.connections.serverConfig.jmsConfig.jmsUser.password)
		assertEquals("njams", config.connections.serverConfig.jmsConfig.destination)		
		assertEquals("dest.cmds", config.connections.serverConfig.jmsConfig.destinationCommands)
		assertEquals("tcp://localhost:61616", config.connections.serverConfig.jmsConfig.providerURL)
		assertEquals("org.apache.activemq.jndi.ActiveMQInitialContextFactory", config.connections.serverConfig.jmsConfig.initialContextFactory)
		assertEquals("ConnectionFactory", config.connections.serverConfig.jmsConfig.connectionFactory)
		assertFalse(config.connections.serverConfig.jmsConfig.useSSL)
		assertEquals("njams", config.connections.serverConfig.jmsConfig.jndi.user.userName)
		assertEquals("njams", config.connections.serverConfig.jmsConfig.jndi.user.password)
		//   Cloud server
		assertEquals("certs/", config.connections.serverConfig.cloudConfig.propertiesLoc)
		assertEquals("ingest.integrationmatters.com", config.connections.serverConfig.cloudConfig.endpoint)
		assertEquals("certs/e08740c608-api.key", config.connections.serverConfig.cloudConfig.apiKey)
		assertEquals("certs/e08740c608-instanceId", config.connections.serverConfig.cloudConfig.clientInstanceId)
		assertEquals("certs/e08740c608-certificate.pem", config.connections.serverConfig.cloudConfig.clientCertificate)
		assertEquals("certs/e08740c608-private.pem.key", config.connections.serverConfig.cloudConfig.clientPrivateKey)
		//   IIB
		assertNotNull(config.connections.iibConfig)
		assertEquals("remote", config.connections.iibConfig.connectionType)
		assertEquals("JOT420", config.connections.iibConfig.host)
		assertEquals(4414, config.connections.iibConfig.adminPort)
		assertEquals("W3P_NJAMS_DEMO", config.connections.iibConfig.intNode)
		assertEquals("IibUser", config.connections.iibConfig.iibUser.userName)
		assertEquals("{SWliUHN3ZA==}", config.connections.iibConfig.iibUser.password) // pswd is Base64 encoded
		assertEquals("IibPswd", config.connections.iibConfig.iibUser.getPasswordInClear())
		assertEquals("SWliUHN3ZA==", config.connections.iibConfig.iibUser.getPasswordEncoded())
		
		// Monitoring
		//  Profile
		assertEquals(2, config.monitoring.consumerThreads)
		assertNotNull(config.monitoring.profile)
		assertEquals("%cd%\\monitoringProfiles", config.monitoring.profile.directory)
		assertTrue(config.monitoring.profile.applyIfUnchanged)
		assertEquals(60, config.monitoring.profile.timeToWait)
		//  Integration Servers
		assertNotNull(config.monitoring.integrationServers)
		assertEquals(2, config.monitoring.integrationServers.size())
		assertFalse(config.monitoring.integrationServers[0].includeName)
		assertFalse(config.monitoring.integrationServers[0].includeSchemaName)
		assertFalse(config.monitoring.integrationServers[1].includeName)
		assertTrue(config.monitoring.integrationServers[1].includeSchemaName)
		//  Applications
		assertNotNull(config.monitoring.integrationServers[0].applications)
		assertEquals(1, config.monitoring.integrationServers[0].applications.size())
		assertEquals("AddressLookup", config.monitoring.integrationServers[0].applications[0].name)
		assertFalse(config.monitoring.integrationServers[0].applications[0].includeSchemaName)
		
		//    Correlations
		assertNotNull(config.monitoring.integrationServers[0].applications[0].correlations)
		//      GlobalCorrelationId / Node
		assertNotNull(config.monitoring.integrationServers[0].applications[0].correlations.globalCorrelationId)
		assertEquals(2, config.monitoring.integrationServers[0].applications[0].correlations.globalCorrelationId.nodes.size())
		assertEquals("ComIbmMQGetNode", config.monitoring.integrationServers[0].applications[0].correlations.globalCorrelationId.nodes[0].type)
		assertEquals("\$Root/MQMD/MsgId", config.monitoring.integrationServers[0].applications[0].correlations.globalCorrelationId.nodes[0].xpath) // xpath is 'value' in x ml
		assertEquals("ComIbmWSInputNode", config.monitoring.integrationServers[0].applications[0].correlations.globalCorrelationId.nodes[1].type)
		assertEquals("\$Root/XMLNSC/PO:purchaseOrder/PO:customerID", config.monitoring.integrationServers[0].applications[0].correlations.globalCorrelationId.nodes[1].xpath)
		//      GlobalCorrelationId / Node / Namespaces
		assertEquals(0, config.monitoring.integrationServers[0].applications[0].correlations.globalCorrelationId.nodes[0].namespaces.size())
		assertNotNull(config.monitoring.integrationServers[0].applications[0].correlations.globalCorrelationId.nodes[1].namespaces)
		assertEquals(1, config.monitoring.integrationServers[0].applications[0].correlations.globalCorrelationId.nodes[1].namespaces.size())
		assertEquals("PO", config.monitoring.integrationServers[0].applications[0].correlations.globalCorrelationId.nodes[1].namespaces[0].prefix)
		assertEquals("http://www.ibm.com/ProcessOrder", config.monitoring.integrationServers[0].applications[0].correlations.globalCorrelationId.nodes[1].namespaces[0].namespace) //  namespace is 'value' in xml
		//      ApplicationData
		assertNotNull(config.monitoring.integrationServers[0].applications[0].correlations.applicationDataQuery)
		//      ApplicationData / Query *** these look more complex than the xml - I must have designed it this way for ease of processing! ***
		assertEquals(3, config.monitoring.integrationServers[0].applications[0].correlations.applicationDataQuery.nodeTypeToQueryText.size())
		Map<String, List<Map<String, String>>>  queryMap = config.monitoring.integrationServers[0].applications[0].correlations.applicationDataQuery.nodeTypeToQueryText
		assertNotNull(queryMap)
/*
		println queryMap
		[
		 ComIbmMQReply:[
		  [
			MsgId:$LocalEnvironment/WrittenDestination/MQ/DestinationData[1]/msgId, 
			CorrelId:$LocalEnvironment/WrittenDestination/MQ/DestinationData[1]/correlId
			]
		  ], 
		 ComIbmMQGetNode:[
		  [
		    MsgId:$Root/MQMD/MsgId]
		  ], 
		 ComIbmMQOutputNode:[
		   [
		     MsgId:$LocalEnvironment/WrittenDestination/MQ/DestinationData[1]/msgId]
		   ]
		  ]
 */
		assertEquals("\$Root/MQMD/MsgId", queryMap.ComIbmMQGetNode[0].MsgId)
		assertEquals("\$LocalEnvironment/WrittenDestination/MQ/DestinationData[1]/msgId", queryMap.ComIbmMQOutputNode[0].MsgId)
		assertEquals("\$LocalEnvironment/WrittenDestination/MQ/DestinationData[1]/msgId", queryMap.ComIbmMQReply[0].MsgId)
		assertEquals("\$LocalEnvironment/WrittenDestination/MQ/DestinationData[1]/correlId", queryMap.ComIbmMQReply[0].CorrelId)		
		
		//    Msgflows
		assertNotNull(config.monitoring.integrationServers[0].applications[0].msgFlows)
		assertEquals("*", config.monitoring.integrationServers[0].applications[0].msgFlows[0].name)
		assertTrue(config.monitoring.integrationServers[0].applications[0].msgFlows[0].labelNodeStartsGroup)
		assertTrue(config.monitoring.integrationServers[0].applications[0].msgFlows[0].includePayload)
		
		assertEquals("mmm", config.monitoring.integrationServers[0].applications[0].msgFlows[1].name)
		assertFalse(config.monitoring.integrationServers[0].applications[0].msgFlows[1].labelNodeStartsGroup)
		assertFalse(config.monitoring.integrationServers[0].applications[0].msgFlows[1].includePayload)
		//    Subflows
		assertEquals(2, config.monitoring.integrationServers[0].applications[0]?.subFlows.size())
		assertEquals("*", config.monitoring.integrationServers[0].applications[0].subFlows[0].name)
		assertFalse(config.monitoring.integrationServers[0].applications[0].subFlows[0].labelNodeStartsGroup)
		assertTrue(config.monitoring.integrationServers[0].applications[0].subFlows[0].includePayload)
		assertEquals("sss", config.monitoring.integrationServers[0].applications[0].subFlows[1].name)
		assertFalse(config.monitoring.integrationServers[0].applications[0].subFlows[1].labelNodeStartsGroup)
		assertFalse(config.monitoring.integrationServers[0].applications[0].subFlows[1].includePayload)
		
		// Shared Libraries (SharedLibrary is subclass of Library)		
		assertNotNull(config.monitoring.integrationServers[0].sharedLibs)
		assertEquals("shl-1", config.monitoring.integrationServers[0].sharedLibs[0].name)
		assertFalse(config.monitoring.integrationServers[0].sharedLibs[0].includeSchemaName)
		//    Subflows
		assertEquals(2, config.monitoring.integrationServers[0].sharedLibs[0].subFlows.size())
		assertEquals("*", config.monitoring.integrationServers[0].sharedLibs[0].subFlows[0].name)
		assertTrue(config.monitoring.integrationServers[0].sharedLibs[0].subFlows[0].includePayload)
		assertFalse(config.monitoring.integrationServers[0].sharedLibs[0].subFlows[0].labelNodeStartsGroup)
		assertEquals("shl-sss", config.monitoring.integrationServers[0].sharedLibs[0].subFlows[1].name)
		assertTrue(config.monitoring.integrationServers[0].sharedLibs[0].subFlows[1].includePayload)
		assertTrue(config.monitoring.integrationServers[0].sharedLibs[0].subFlows[1].labelNodeStartsGroup)
		
		// Static Libraries (StaticLibrary is subclass of Library)
		assertNotNull(config.monitoring.integrationServers[0].staticLibs)
		assertEquals("stl-1", config.monitoring.integrationServers[0].staticLibs[0].name)
		assertFalse(config.monitoring.integrationServers[0].staticLibs[0].includeSchemaName)
		//    Correlations
		assertNotNull(config.monitoring.integrationServers[0].staticLibs[0].correlations)
		assertNotNull(config.monitoring.integrationServers[0].staticLibs[0].correlations.globalCorrelationId)
		//    Msgflows
		assertNotNull(config.monitoring.integrationServers[0].staticLibs[0].msgFlows)
		assertEquals(2, config.monitoring.integrationServers[0].staticLibs[0].msgFlows.size())
		assertEquals("stl-mf*", config.monitoring.integrationServers[0].staticLibs[0].msgFlows[0].name)
		assertFalse(config.monitoring.integrationServers[0].staticLibs[0].msgFlows[0].labelNodeStartsGroup)
		assertTrue(config.monitoring.integrationServers[0].staticLibs[0].msgFlows[0].includePayload)		
		assertEquals("stl-mf2", config.monitoring.integrationServers[0].staticLibs[0].msgFlows[1].name)
		assertFalse(config.monitoring.integrationServers[0].staticLibs[0].msgFlows[1].labelNodeStartsGroup)
		assertFalse(config.monitoring.integrationServers[0].staticLibs[0].msgFlows[1].includePayload)
		//    MsgFlows[0] / Correlations
		assertNotNull(config.monitoring.integrationServers[0].staticLibs[0].msgFlows[0].correlations)
		assertNotNull(config.monitoring.integrationServers[0].staticLibs[0].msgFlows[0].correlations.globalCorrelationId)
		assertNotNull(config.monitoring.integrationServers[0].staticLibs[0].msgFlows[0].correlations.globalCorrelationId.nodes)
		assertNull(config.monitoring.integrationServers[0].staticLibs[0].msgFlows[0].correlations.globalCorrelationId.nodes[0])
		assertNotNull(config.monitoring.integrationServers[0].staticLibs[0].msgFlows[1].correlations)
		assertNotNull(config.monitoring.integrationServers[0].staticLibs[0].msgFlows[1].correlations.globalCorrelationId)
		//    Subflows
		assertEquals(2, config.monitoring.integrationServers[0].staticLibs[0].subFlows.size())
		assertEquals("stl-sf1", config.monitoring.integrationServers[0].staticLibs[0].subFlows[0].name)
		assertTrue(config.monitoring.integrationServers[0].staticLibs[0].subFlows[0].includePayload)
		assertFalse(config.monitoring.integrationServers[0].staticLibs[0].subFlows[0].labelNodeStartsGroup)
		assertEquals("stl-sf2", config.monitoring.integrationServers[0].staticLibs[0].subFlows[1].name)
		assertTrue(config.monitoring.integrationServers[0].staticLibs[0].subFlows[1].includePayload)
		assertTrue(config.monitoring.integrationServers[0].staticLibs[0].subFlows[1].labelNodeStartsGroup)
		
		//  Rest APIs
		assertNotNull(config.monitoring.integrationServers[0].restAPIs)
		assertEquals("api-1", config.monitoring.integrationServers[0].restAPIs[0].name)
		assertTrue(config.monitoring.integrationServers[0].restAPIs[0].includeSchemaName)
		//    Correlations
		assertNotNull(config.monitoring.integrationServers[0].restAPIs[0].correlations)
		assertNotNull(config.monitoring.integrationServers[0].restAPIs[0].correlations.globalCorrelationId)
		//    Msgflows
		assertNotNull(config.monitoring.integrationServers[0].restAPIs[0].msgFlows)
		assertEquals(2, config.monitoring.integrationServers[0].restAPIs[0].msgFlows.size())
		assertEquals("api-mf*", config.monitoring.integrationServers[0].restAPIs[0].msgFlows[0].name)
		assertFalse(config.monitoring.integrationServers[0].restAPIs[0].msgFlows[0].labelNodeStartsGroup)
		assertTrue(config.monitoring.integrationServers[0].restAPIs[0].msgFlows[0].includePayload)
		assertEquals("api-mf2", config.monitoring.integrationServers[0].restAPIs[0].msgFlows[1].name)
		assertFalse(config.monitoring.integrationServers[0].restAPIs[0].msgFlows[1].labelNodeStartsGroup)
		assertFalse(config.monitoring.integrationServers[0].restAPIs[0].msgFlows[1].includePayload)
		//    MsgFlows[0] / Correlations
		assertNotNull(config.monitoring.integrationServers[0].restAPIs[0].msgFlows[0].correlations)
		assertNotNull(config.monitoring.integrationServers[0].restAPIs[0].msgFlows[0].correlations.globalCorrelationId)
		assertNotNull(config.monitoring.integrationServers[0].restAPIs[0].msgFlows[0].correlations.globalCorrelationId.nodes)
		assertNull(config.monitoring.integrationServers[0].restAPIs[0].msgFlows[0].correlations.globalCorrelationId.nodes[0])
		assertNotNull(config.monitoring.integrationServers[0].restAPIs[0].msgFlows[1].correlations)
		assertNotNull(config.monitoring.integrationServers[0].restAPIs[0].msgFlows[1].correlations.globalCorrelationId)
		//    Subflows
		assertEquals(2, config.monitoring.integrationServers[0].restAPIs[0].subFlows.size())
		assertEquals("*", config.monitoring.integrationServers[0].restAPIs[0].subFlows[0].name)
		assertTrue(config.monitoring.integrationServers[0].restAPIs[0].subFlows[0].includePayload)
		assertFalse(config.monitoring.integrationServers[0].restAPIs[0].subFlows[0].labelNodeStartsGroup)
		assertEquals("api-sf2", config.monitoring.integrationServers[0].restAPIs[0].subFlows[1].name)
		assertFalse(config.monitoring.integrationServers[0].restAPIs[0].subFlows[1].includePayload)
		assertFalse(config.monitoring.integrationServers[0].restAPIs[0].subFlows[1].labelNodeStartsGroup)
		
	}
}
