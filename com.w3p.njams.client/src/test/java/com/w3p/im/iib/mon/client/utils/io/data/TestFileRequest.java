package com.w3p.im.iib.mon.client.utils.io.data;

import static com.w3p.im.iib.mon.client.constants.IClientConstants.DEFAULT_NAME_PATTERN;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TemporaryFolder;

public class TestFileRequest {
	
	private static final String content = "content";
	private static final String namePattern = "pattern_%s.txt";
	
	@Rule
	public TemporaryFolder folder = new TemporaryFolder();

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	// NOTE: IFileRequest doesn't add any fields to IRequest - is present for symmetry with IQueueRequest
	public void testCanCreateRequestWithNamePattern() {
		IFileRequest req = new FileRequest();
		req.setContent(content);
		req.setNamePattern(namePattern);
		req.addOption("optionA", Boolean.TRUE);

		assertNotNull(req);
		assertTrue(req.getContent().equals(content));
		assertTrue(req.getNamePattern().equals(namePattern));
		assertTrue(req.getOption("optionA").isPresent());
		assertTrue((Boolean)req.getOption("optionA").get());
	}
	
	@Test
	public void testCanCreateRequestWithDefaultNamePattern() {
		IFileRequest req = new FileRequest();
		req.setContent(content);
		req.addOption("optionA", Integer.MAX_VALUE);

		assertNotNull(req);
		assertTrue(req.getContent().equals(content));
		assertTrue(req.getNamePattern().equals(DEFAULT_NAME_PATTERN));
		assertTrue(req.getOption("optionA").isPresent());
		assertTrue((Integer)req.getOption("optionA").get() == Integer.MAX_VALUE);
	}
}
