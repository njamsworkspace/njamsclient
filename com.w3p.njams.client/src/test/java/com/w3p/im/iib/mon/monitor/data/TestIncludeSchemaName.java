package com.w3p.im.iib.mon.monitor.data;

import static org.junit.Assert.*;

import org.junit.Test;

public class TestIncludeSchemaName {

	@Test
	public void testCanAddandRetrieveEntries() {
		IncludeSchemaName icn = new IncludeSchemaName();
		icn.add("Flow1", true);
		icn.add("Flow2", false);
		icn.add("Flow3", true);
		
		assertTrue(icn.isIncludeSchemaName("Flow1"));
		assertFalse(icn.isIncludeSchemaName("Flow2"));
		assertTrue(icn.isIncludeSchemaName("Flow3"));		
		assertFalse(icn.isIncludeSchemaName("FlowX"));
	}
}
