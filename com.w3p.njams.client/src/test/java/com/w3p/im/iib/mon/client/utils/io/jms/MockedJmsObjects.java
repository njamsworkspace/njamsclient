package com.w3p.im.iib.mon.client.utils.io.jms;

import javax.jms.Connection;
import javax.jms.Destination;
import javax.jms.MessageProducer;
import javax.jms.Queue;
import javax.jms.Session;
import javax.jms.TextMessage;

import com.ibm.msg.client.jms.JmsConnectionFactory;
import com.ibm.msg.client.jms.JmsFactoryFactory;

public class MockedJmsObjects {
	private JmsConnectionFactory jmsConnectionFactory;
	private JmsFactoryFactory jmsFactoryFactory;
	private Connection jmsConnection;
	private Session jmsSession;
	private Destination jmsDestination;
	private Queue jmsQueue;
	private TextMessage message;
	private MessageProducer producer;
	private JmsFactoryFactoryWrapper jmsFactoryFactoryWrapper;
	 

	public MockedJmsObjects() {
	}

	public JmsConnectionFactory getJmsConnectionFactory() {
		return jmsConnectionFactory;
	}
	public void setJmsConnectionFactory(JmsConnectionFactory jmsConnectionFactory) {
		this.jmsConnectionFactory = jmsConnectionFactory;
	}
	public JmsFactoryFactory getJmsFactoryFactory() {
		return jmsFactoryFactory;
	}
	public void setJmsFactoryFactory(JmsFactoryFactory jmsFactoryFactory) {
		this.jmsFactoryFactory = jmsFactoryFactory;
	}
	public Connection getJmsConnnection() {
		return jmsConnection;
	}
	public void setJmsConnnection(Connection jmsConnection) {
		this.jmsConnection = jmsConnection;
	}
	public Session getJmsSession() {
		return jmsSession;
	}

	public void setJmsSession(Session jmsSession) {
		this.jmsSession = jmsSession;
	}

	public Queue getJmsQueue() {
		return jmsQueue;
	}
	public void setJmsQueue(Queue jmsQueue) {
		this.jmsQueue = jmsQueue;
	}
	public Destination getJmsDestination() {
		return jmsDestination;
	}

	public void setJmsDestination(Destination jmsDestination) {
		this.jmsDestination = jmsDestination;
	}

	public MessageProducer getProducer() {
		return producer;
	}
	public void setProducer(MessageProducer producer) {
		this.producer = producer;
	}
	public JmsFactoryFactoryWrapper getJmsFactoryFactoryWrapper() {
		return jmsFactoryFactoryWrapper;
	}
	public void setJmsFactoryFactoryWrapper(JmsFactoryFactoryWrapper jmsFactoryFactoryWrapper) {
		this.jmsFactoryFactoryWrapper = jmsFactoryFactoryWrapper;
	}

	public TextMessage getMessage() {
		return message;
	}

	public void setMessage(TextMessage message) {
		this.message = message;
	}
}
