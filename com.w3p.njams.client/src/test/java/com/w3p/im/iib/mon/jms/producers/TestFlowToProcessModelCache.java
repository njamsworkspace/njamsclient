package com.w3p.im.iib.mon.jms.producers;

import static org.junit.Assert.*;
import static org.mockito.Mockito.*;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.Arrays;
import java.util.List;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.im.njams.sdk.common.Path;
import com.im.njams.sdk.model.ProcessModel;

public class TestFlowToProcessModelCache {

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testCanCreateFlowToProcessModelCache() {
		FlowToProcessModelCache cache = new FlowToProcessModelCache();
		assertNotNull(cache);		
	}

	@Test
	public void testEmptyPathCacheReturnsNull() {
		FlowToProcessModelCache cache = new FlowToProcessModelCache();
		List<String> emptyList = cache.getPathForFlowAsList("flowName");
		assertTrue(emptyList.isEmpty());
	}
	
	@Test
	public void testPathCacheIsEmpty() {
		FlowToProcessModelCache cache = new FlowToProcessModelCache();
		assertTrue(cache.isEmpty());
	}
	
	@Test
	public void testPathCacheNotEmpty() {
		FlowToProcessModelCache cache = new FlowToProcessModelCache();
		cache.addPathForFlow("Flow1", Arrays.asList("App1", "Flow1"));
		assertFalse(cache.isEmpty());
	}
	
	@Test
	public void testPathNotFoundReturnsNull() {
		FlowToProcessModelCache cache = new FlowToProcessModelCache();
		cache.addPathForFlow("Flow1", Arrays.asList("App1", "Flow1"));
		List<String> notFound = cache.getPathForFlowAsList("flow1");
		assertTrue(notFound.isEmpty());
	}
	
	
	@Test
	public void testCanAddPathForFlow() {
		FlowToProcessModelCache cache = new FlowToProcessModelCache();
		cache.addPathForFlow("flow1", Arrays.asList("App1", "Flow1"));
		List<String> flowList = cache.getPathForFlowAsList("flow1");
		assertFalse(flowList.isEmpty());
		assertEquals(2, flowList.size());
		String app = flowList.get(0);
		String flow = flowList.get(1);
		assertNotNull(app);
		assertNotNull(flow);
		
		assertEquals("App1", app);
		assertEquals("Flow1", flow);

		// Subflow
		cache.addPathForFlow("flow1.subflow1", Arrays.asList("App1", "Subflow1")); // FIXME The nodeLabel will always contain [App|Lib|api].MsgFlow.[Subflow..] need to crearte key wityh App|Lib
		List<String> subflowList = cache.getPathForFlowAsList("flow1.subflow1");
		assertFalse(subflowList.isEmpty());
		assertEquals(2, subflowList.size());
		assertEquals("App1", subflowList.get(0));
		assertEquals("Subflow1", subflowList.get(1));
	}
	
	@Test
	public void testCanSerialiseTheCache() throws IOException, ClassNotFoundException {
		FlowToProcessModelCache cache = new FlowToProcessModelCache();
		cache.addPathForFlow("flow1", Arrays.asList("App1", "Flow1"));
		
		List<String> flowList = cache.getPathForFlowAsList("flow1");
		assertFalse(flowList.isEmpty());
		assertEquals(2, flowList.size());
		String app = flowList.get(0);
		String flow = flowList.get(1);
		assertNotNull(app);
		assertNotNull(flow);		
		assertEquals("App1", app);
		assertEquals("Flow1", flow);

		// Subflow
		cache.addPathForFlow("flow1.subflow1", Arrays.asList("App1", "Subflow1")); // FIXME The nodeLabel will always contain [App|Lib|api].MsgFlow.[Subflow..] need to crearte key wityh App|Lib
		
		List<String> subflowList = cache.getPathForFlowAsList("flow1.subflow1");
		assertFalse(subflowList.isEmpty());
		assertEquals(2, subflowList.size());
		assertEquals("App1", subflowList.get(0));
		assertEquals("Subflow1", subflowList.get(1));
		
		// Serialise
		FileOutputStream fileOutputStream
	      = new FileOutputStream("cache.txt");
	    ObjectOutputStream objectOutputStream 
	      = new ObjectOutputStream(fileOutputStream);
	    objectOutputStream.writeObject(cache);
	    objectOutputStream.flush();
	    objectOutputStream.close();
	    
	    FileInputStream fileInputStream
	      = new FileInputStream("cache.txt");
	    ObjectInputStream objectInputStream
	      = new ObjectInputStream(fileInputStream);
	    FlowToProcessModelCache cache2 = (FlowToProcessModelCache) objectInputStream.readObject();
	    objectInputStream.close(); 
	 
		List<String> flowList2 = cache2.getPathForFlowAsList("flow1");
		assertFalse(flowList2.isEmpty());
		assertEquals(2, flowList2.size());
		String app2 = flowList2.get(0);
		String flow2 = flowList2.get(1);
		assertNotNull(app2);
		assertNotNull(flow2);
		assertEquals("App1", app2);
		assertEquals("Flow1", flow2);
		
		List<String> subflowList2 = cache2.getPathForFlowAsList("flow1.subflow1");
		assertFalse(subflowList2.isEmpty());
		assertEquals(2, subflowList2.size());
		assertEquals("App1", subflowList2.get(0));
		assertEquals("Subflow1", subflowList2.get(1));
		
	}

}
