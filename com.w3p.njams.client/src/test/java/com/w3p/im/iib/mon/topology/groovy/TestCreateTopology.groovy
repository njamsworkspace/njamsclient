package com.w3p.im.iib.mon.topology.groovy;

import org.junit.After
import org.junit.AfterClass
import org.junit.Before
import org.junit.BeforeClass
import org.junit.Test

import com.ibm.broker.config.proxy.ApplicationProxy
import com.ibm.broker.config.proxy.BrokerProxy
import com.ibm.broker.config.proxy.ConfigManagerProxyLoggedException
import com.ibm.broker.config.proxy.ConfigManagerProxyPropertyNotInitializedException
import com.ibm.broker.config.proxy.ExecutionGroupProxy
import com.ibm.broker.config.proxy.MessageFlowProxy
import com.ibm.broker.config.proxy.MessageFlowProxy.Node
import com.ibm.broker.config.proxy.MessageFlowProxy.NodeConnection
import com.w3p.im.iib.mon.topology.model.Application
import com.w3p.im.iib.mon.topology.model.FlowNode
import com.w3p.im.iib.mon.topology.model.FlowNodeConnection
import com.w3p.im.iib.mon.topology.model.IntegrationServer
import com.w3p.im.iib.mon.topology.model.MessageFlow

class TestCreateTopology {

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

//	@Test
	public void testTopology() {
		/*
		 * Q&D test to exercisw the Groovy XML builder, by getting data from the running Broker
		 * 
		 * TODO write code to build the data structure to be used.
		 */
		BrokerProxy intNode = connectToIntegrationNode("IB9NODE")
		List<IntegrationServer> iServerList = getTreeStructureForIntegrationNode(intNode);
		CreateTopology topology = new CreateTopology()
		String topologyAsXML = topology.createAsXML(iServerList, intNode)
		assert topologyAsXML != null
		println "Topology: $topologyAsXML"
//		println summaryAsXML
//		assertTrue(summaryAsXML.contains("<integrationNode>IB9NODE</integrationNode>"))
//		assertTrue(summaryAsXML.contains("<duration>3</duration>"))		
//		assertTrue(summaryAsXML.contains("<executionPath><node name='TEXTMESSENGER'>"))
	}

	private BrokerProxy connectToIntegrationNode(String integrationNodeName) {
		BrokerProxy intNode = null;
		try {
			intNode = BrokerProxy.getLocalInstance(integrationNodeName);
		} catch (ConfigManagerProxyLoggedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return intNode;
	}

/*		
	private List<IntegrationServer> getTopology() {
		
	
		List<IntegrationServer> iServerList = getTreeStructureForIntegrationNode();
		
		//TODO TODO refactor and possibly simplify using generices and/or class hierarchy of Application, RestApi etc
		// DeployedObjectGroupProxy is super class to: ApplicationProxy, ExecutionGroupProxy, StaticLibrary Proxy
		// ApplicationProxy is super for RestApiProxy
		//
		// FlowProxy is super to MessageFlowProxy, SubFlowProxy
		//
		
		for (IntegrationServer iSvr : iServerList) {
System.out.println("TopologyCreator/getTopology() - Int Server name: "+iSvr.getName());
			List<Application> appList = iSvr.getApplications();
			for (Application app : appList) {
				List<MessageFlow> msgFlowList = app.getMessageFlows();
				for (MessageFlow messageFlow : msgFlowList) {
					List<FlowNode> nodeList = messageFlow.getFlowNodes();
					for (FlowNode flowNode : nodeList) {
						Map<String, FlowNode> nodeTopology = new LinkedHashMap<>();
						nodeTopology.put(flowNode.getName(), flowNode);
//							String json = new JsonUtil().convertTopologyMapToJson(nodeTopology);
//							topology.put(flowNode.getName(), json);
					}
				}
			}
			
			List<RestApi> apiList = iSvr.getRestApis();
			for (RestApi api : apiList) {
				List<MessageFlow> msgFlowList = api.getMessageFlows();
				for (MessageFlow messageFlow : msgFlowList) {
					List<FlowNode> nodeList = messageFlow.getFlowNodes();
					for (FlowNode flowNode : nodeList) {
						Map<String, FlowNode> nodeTopology = new LinkedHashMap<>();
						nodeTopology.put(flowNode.getName(), flowNode);
//							String json = new JsonUtil().convertTopologyMapToJson(nodeTopology);
//							topology.put(flowNode.getName(), json);
					}
				}
			}

			List<MessageFlow> msgFlowList = iSvr.getMessageFlows();
			for (MessageFlow messageFlow : msgFlowList) {
				List<FlowNode> nodeList = messageFlow.getFlowNodes();
				for (FlowNode flowNode : nodeList) {
					Map<String, FlowNode> nodeTopology = new LinkedHashMap<>();
					nodeTopology.put(flowNode.getName(), flowNode);
//						String json = new JsonUtil().convertTopologyMapToJson(nodeTopology);
//						topology.put(flowNode.getName(), json);
				}
			}

		}
//
//		String topologyAsXML = new CreateTopology().createAsXML(iServerList, intNode);
////			StringBuilder topologyBuilder = new StringBuilder();
////			Collection<String> FlowNames = topology.keySet();
////			for (String flowName : FlowNames) {
////				topologyBuilder.append(topology.get(flowName));
////			}
//		response.setTopology(topologyAsXML);
//		return response;
		return iServerList;
	}
*/	

/**
 * @return a list of Integration Servers (Execution Groups), Applications and RestApis for this Integration Node (Broker)
 * 	that defines the Flow Node topology in the contained Message Fkows.
 */
private List<IntegrationServer> getTreeStructureForIntegrationNode(BrokerProxy intNode) {
	// Get list of Integration Servers (Execution Groups)
	List<IntegrationServer> iServerList = new ArrayList<>();
	try {
		Enumeration<ExecutionGroupProxy> iservers = intNode.getExecutionGroups(new Properties()); // Empty Props = all Int Servers / Execution Groups
		while (iservers.hasMoreElements()) {
			ExecutionGroupProxy iServer = (ExecutionGroupProxy) iservers.nextElement();
			IntegrationServer is = new IntegrationServer(iServer.getName(), iServer.isRunning());
			List<Application> apps = getApplications(iServer);
			is.setApplications(apps);
// IIB v10		is.setRestApis(restApis);
			List<MessageFlow> msgFlows = getMessageFlows(iServer);
			is.setMessageFlows(msgFlows);
			iServerList.add(is);
		}
	} catch (ConfigManagerProxyPropertyNotInitializedException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
	return iServerList;
}
/*
*   ===> New in IIB 10
*
private List<RestApi> getRestApis(ExecutionGroupProxy iServer) {
	// Get a list of all REST APIs and their MessageFlows
	List<RestApi> restApiList = new ArrayList<>();
	try {
		Enumeration<RestApiProxy> apiProxies = iServer.getRestApis(new Properties());
		while (apiProxies.hasMoreElements()) {
			RestApiProxy apiProxy = (RestApiProxy) apiProxies.nextElement();
System.out.println("TopologyCreator/getRestApis() - REST API name: "+apiProxy.getName());
			RestApi restApi = new RestApi(apiProxy.getName(), apiProxy.isRunning());
			List<MessageFlow> msgFlows = getMessageFlows(apiProxy);
			restApi.setMessageFlows(msgFlows);
			restApiList.add(restApi);
		}
	} catch (ConfigManagerProxyPropertyNotInitializedException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
	return restApiList;
}
*/
private List<Application> getApplications(ExecutionGroupProxy iServer) {
	// Get a list of all Applications and their MessageFlows
	List<Application> appList = new ArrayList<>();
	try {
		Enumeration<ApplicationProxy> appProxies = iServer.getApplications(new Properties());
		while (appProxies.hasMoreElements()) {
			ApplicationProxy appProxy = (ApplicationProxy) appProxies.nextElement();
System.out.println("TopologyCreator/getApplications() - REST API name: "+appProxy.getName());
			Application app = new Application(appProxy.getName(), appProxy.isRunning());
			List<MessageFlow> msgFlows = getMessageFlows(appProxy);
			app.setMessageFlows(msgFlows);
			appList.add(app);
		}
	} catch (ConfigManagerProxyPropertyNotInitializedException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
	 return appList;
}
/*
*   ==> Only valid in IIB 10
*
private List<MessageFlow> getMessageFlows(RestApiProxy api) {
	// Get a list of all Message Flows
	List<MessageFlow> flowList = new ArrayList<>();
	try {
		Enumeration<MessageFlowProxy> msgFlowProxies = api.getMessageFlows(new Properties());
		while (msgFlowProxies.hasMoreElements()) {
			MessageFlowProxy flowProxy = (MessageFlowProxy) msgFlowProxies.nextElement();
			MessageFlow flow = new MessageFlow(flowProxy.getName(), flowProxy.isRunning());
			flow.setFlowNodes(getFlowNodes(flowProxy));
			flowList.add(flow);
		}
	} catch (ConfigManagerProxyPropertyNotInitializedException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
	 return flowList;
}
*/

private List<MessageFlow> getMessageFlows(ApplicationProxy app) {
	// Get a list of all Message Flows
	List<MessageFlow> flowList = new ArrayList<>();
	try {
		Enumeration<MessageFlowProxy> flowProxies = app.getMessageFlows(new Properties());
		while (flowProxies.hasMoreElements()) {
			MessageFlowProxy flowProxy = (MessageFlowProxy) flowProxies.nextElement();
			MessageFlow flow = new MessageFlow(flowProxy.getName(), flowProxy.isRunning());
			flow.setFlowNodes(getFlowNodes(flowProxy));
			flowList.add(flow);
		}
	} catch (ConfigManagerProxyPropertyNotInitializedException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
	 return flowList;
}

private List<MessageFlow> getMessageFlows(ExecutionGroupProxy iServer) {
	// Get a list of all Message Flows
	List<MessageFlow> flowList = new ArrayList<>();
	try {
		Enumeration<MessageFlowProxy> flowProxies = iServer.getMessageFlows(new Properties());
		while (flowProxies.hasMoreElements()) {
			MessageFlowProxy flowProxy = (MessageFlowProxy) flowProxies.nextElement();
			MessageFlow flow = new MessageFlow(flowProxy.getName(), flowProxy.isRunning());
			flow.setFlowNodes(getFlowNodes(flowProxy));
			flowList.add(flow);
		}
	} catch (ConfigManagerProxyPropertyNotInitializedException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
	 return flowList;
}

private List<FlowNode> getFlowNodes(MessageFlowProxy flowProxy) {
	List<FlowNode> flowNodes = new ArrayList<>();
	try {
		// TODO Extract to method: Enumerations remember their position - so after first use, there are no more elements
		//		Extract to a List which can be re-used.
		List<NodeConnection> connectionsList = new ArrayList<>();
		Enumeration<NodeConnection> connections = flowProxy.getNodeConnections();
		while (connections.hasMoreElements()) {
			MessageFlowProxy.NodeConnection connection = (MessageFlowProxy.NodeConnection) connections.nextElement();
			connectionsList.add(connection);
		}
		
		Enumeration<Node> nodeProxies = flowProxy.getNodes();
		while (nodeProxies.hasMoreElements()) {
			MessageFlowProxy.Node node = (MessageFlowProxy.Node) nodeProxies.nextElement();
			FlowNode flowNode = new FlowNode(node.getName(), node.getType());
			List<FlowNodeConnection> targetNodes = getFlowNodeConnections(node.getName(), connectionsList);
			flowNode.setFlowNodeConnections(targetNodes);
			flowNodes.add(flowNode);
		}
	} catch (ConfigManagerProxyPropertyNotInitializedException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
	return flowNodes;
}

private List<FlowNodeConnection> getFlowNodeConnections(String nodeName, List<NodeConnection> connectionsList) { //MessageFlowProxy flowProxy) {
	List<FlowNodeConnection> targetNodes = new ArrayList<>();
	for (NodeConnection nc : connectionsList) {
		String sourceNode = nc.getSourceNode().getName();
		if (sourceNode.startsWith(nodeName)) {
			FlowNodeConnection fnc = new FlowNodeConnection(nc.getSourceNode().getName(), nc.getSourceOutputTerminal(),
					nc.getTargetNode().getName(), nc.getTargetInputTerminal());
			targetNodes.add(fnc);
		}
	}
	return targetNodes;
}

}
