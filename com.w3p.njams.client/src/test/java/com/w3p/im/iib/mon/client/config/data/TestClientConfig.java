package com.w3p.im.iib.mon.client.config.data;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class TestClientConfig {

	private static final String IIB_VERSION = "IIB-10";
	private static final String PROD = "PROD";

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testCanCreateClientConfig() {
		ClientConfig cf = new ClientConfig();
		assertNotNull(cf);
	}
	
	@Test
	public void testCanPopulateClientConfig() {
		ClientConfig cf = new ClientConfig();
		assertNotNull(cf);
		cf.setClientHome(System.getProperty("user.dir"));
		cf.setClientMode(PROD);
		Connections connections = new Connections();
		cf.setConnections(connections);
		Monitoring monitoring = new Monitoring();
		cf.setMonitoring(monitoring);
		cf.setType(IIB_VERSION);
		
		String userDir = cf.getClientHome();
		assertEquals(userDir, cf.getClientHome());
		assertEquals(PROD, cf.getClientMode());
		assertEquals(connections, cf.getConnections());
		assertEquals(monitoring, cf.getMonitoring());
		assertEquals(IIB_VERSION, cf.getType());
	}

}
