package com.w3p.im.iib.mon.client.utils.io.data;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class TestConnectionData {

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testCanCreateConnectionData() {
		ConnectionData connData = createConnectionData();
		
		assertNotNull(connData);
		assertEquals("localhost", connData.getHost());
		assertEquals(3414, connData.getPort());
		assertEquals("DEV.APP.SVRCONN", connData.getServerChan());
		assertEquals("IB10QMGR", connData.getQmgr());
		assertEquals("NJAMS_FILES", connData.getQueue());
		assertEquals("njams", connData.getUserid());
		assertEquals("njams", connData.getPassword());
	}
	
	private ConnectionData createConnectionData() {
		ConnectionData cd = new ConnectionData();
		cd.setHost("localhost")
		.setPort(3414)
		.setServerChan("DEV.APP.SVRCONN")
		.setQmgr("IB10QMGR")
		.setQueue("NJAMS_FILES")
		.setUserid("njams")
		.setPassword("njams");
		return cd;
	}

}
