package com.w3p.im.iib.mon.client.utils.io.data;

import static com.w3p.im.iib.mon.client.constants.IClientConstants.DEFAULT_NAME_PATTERN;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TemporaryFolder;

public class TestWriteFileRequest {
	
	private static final String content = "content";
	private static final String namePattern = "pattern_%s.txt";
	
	@Rule
	public TemporaryFolder folder = new TemporaryFolder();

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testCanCreateRequestWithSpecificOptions() {
		IWriteFileRequest req = new WriteFileRequest();
		req.setContent(content)
		.setNamePattern(namePattern)
		.addOption("optionA", Boolean.TRUE);
		req.setClearFolderIfNotEmpty(true)
		.setCreateFolderIfNotExists(true)
		.setReplaceFilesIfExist(true)
		.setDestination("targetFolder");

		assertNotNull(req);
		assertTrue(req.getContent().equals(content));
		assertTrue(req.getNamePattern().equals(namePattern));
		assertTrue(req.getOption("optionA").isPresent());
		assertTrue((Boolean)req.getOption("optionA").get());
		
		assertTrue(req.isClearFolderIfNotEmpty());
		assertTrue(req.isCreateFolderIfNotExists());
		assertTrue(req.isReplaceFilesIfExist());
		assertEquals("targetFolder", req.getDestination());
	}
	
	@Test
	public void testCanCreateRequestWithDefaultOptions() {
		IWriteFileRequest req = new WriteFileRequest();
		req.setContent(content);
		req.addOption("optionA", Integer.MAX_VALUE);
		req.setDestination("targetFolder");

		assertNotNull(req);
		assertTrue(req.getContent().equals(content));
		assertTrue(req.getNamePattern().equals(DEFAULT_NAME_PATTERN));
		assertTrue(req.getOption("optionA").isPresent());
		assertTrue((Integer)req.getOption("optionA").get() == Integer.MAX_VALUE);
		
		assertFalse(req.isClearFolderIfNotEmpty());
		assertFalse(req.isCreateFolderIfNotExists());
		assertFalse(req.isReplaceFilesIfExist());
		assertEquals("targetFolder", req.getDestination());
	}
}
