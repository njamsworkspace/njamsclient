package com.w3p.im.iib.mon.monitor.data.groovy;

import static org.junit.Assert.*

import com.w3p.im.iib.mon.monitor.data.MonitorEventsRequestForQueues
import com.w3p.im.iib.mon.monitor.data.MonitoringEvent
import javax.management.InstanceOfQueryExp

import org.junit.After
import org.junit.AfterClass
import org.junit.Before
import org.junit.BeforeClass
import org.junit.Test

class TestParseMonitoringEvent {

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	
	@Test
	public void testParseMonitoringRequestWithPayloadAndDateTimeMicrosecs() {
	// It looks as though IIB 10 publishes all creation time with microsec values 
		
		def event = '''
<wmb:event xmlns:wmb="http://www.ibm.com/xmlns/prod/websphere/messagebroker/6.1.0/monitoring/event">
 <wmb:eventPointData>
  <wmb:eventData wmb:productVersion="10008" wmb:eventSchemaVersion="6.1.0.3" wmb:eventSourceAddress="HTTP Input.transaction.Start">
   <wmb:eventIdentity wmb:eventName="HTTP Input.TransactionStart"/>
   <wmb:eventSequence wmb:creationTime="2017-07-24T13:46:46.335829Z" wmb:counter="1"/>
   <wmb:eventCorrelation wmb:localTransactionId="9109ac5b-5b2e-49fa-9e2f-a287031b4ed0-4" wmb:parentTransactionId="" wmb:globalTransactionId="414d512049423130514d47522020202029e170592012d668"/>
  </wmb:eventData>
  <wmb:messageFlowData>
   <wmb:broker wmb:name="PETS_AT_HOME" wmb:UUID="47ea0eeb-dc5d-47c5-a565-0bb5ec322503"/>
   <wmb:executionGroup wmb:name="njamsTesting" wmb:UUID="64030c8c-4287-4fff-b510-2cd5beb8f155"/>
   <wmb:messageFlow wmb:uniqueFlowName="PETS_AT_HOME.njamsTesting.HTTPInputApplication.HTTPInputMessageFlow" wmb:name="HTTPInputMessageFlow" wmb:UUID="7190a944-cf3f-46e6-90e5-22001ea669a4" wmb:threadId="11608"/>
   <wmb:node wmb:nodeLabel="HTTP Input" wmb:nodeType="ComIbmWSInputNode"/>
  </wmb:messageFlowData>
 </wmb:eventPointData>
 <wmb:bitstreamData>
  <wmb:bitstream wmb:encoding="base64Binary">ew0KIklucHV0RmllbGQxIjoiSGVsbG8iLA0KIklucHV0RmllbGQyIjoibkpBTVMgQUEiDQp9</wmb:bitstream>
 </wmb:bitstreamData>
</wmb:event>
		'''
				
		MonitoringEvent result = new ParseMonitoringEvent().parse(event)
		assert result != null
		assertEquals("HTTP Input.TransactionStart", result.eventName)
		assertEquals(result.creationTime.toString(),  "2017-07-24T13:46:46.335829")
//		assertEquals(1500904006336, result.creationTimeMilliSecs)
//		Date creationTime = new Date(result.creationTimeMilliSecs)
//		def year = creationTime.calendarDate.year
//		def month = creationTime.calendarDate.month
//		def day = creationTime.calendarDate.dayOfMonth
//		assertEquals("${year}${'-'}${month}${'-'}${day}".toString(), "2017-7-24")
		assertNull(result.getExceptionList())
		// for added methods
		assertTrue(result.isFromMsgFlow())
		assertFalse(result.isFromSubFlow())
		assertEquals("HTTP Input", result.getLocalNodeLabel())
		assertNull(result.getSubflowName())
	}
		
	@Test
	public void testParseMonitoringRequestWithMissingMilliseconds() {
	// received an event in which the msec must have been '000' and they were not present: creationTime="2017-04-18T14:38:55Z"
		
		def event = '''
<wmb:event xmlns:wmb="http://www.ibm.com/xmlns/prod/websphere/messagebroker/6.1.0/monitoring/event">
 <wmb:eventPointData>
  <wmb:eventData wmb:productVersion="9007" wmb:eventSchemaVersion="6.1.0.3" wmb:eventSourceAddress="TEXTMESSENGER.transaction.Start">
   <wmb:eventIdentity wmb:eventName="TEXTMESSENGER.FlowStart"/>
   <wmb:eventSequence wmb:creationTime="2017-04-18T14:38:55Z" wmb:counter="1"/>
   <wmb:eventCorrelation wmb:localTransactionId="04959914-c387-4464-8a2c-9993753fe780-22" wmb:parentTransactionId="" wmb:globalTransactionId="414d5120494239514d475220202020202881ad582170c606"/>
  </wmb:eventData>
  <wmb:messageFlowData>
   <wmb:broker wmb:name="IB9NODE" wmb:UUID="caec250c-73c8-49e1-9cda-4091c7a70655"/>
   <wmb:executionGroup wmb:name="PagerExecutionGroup" wmb:UUID="5ecf6193-5901-0000-0080-c313a9bf8971"/>
   <wmb:messageFlow wmb:uniqueFlowName="IB9NODE.PagerExecutionGroup.TextMessenger" wmb:name="TextMessenger" wmb:UUID="581f026c-5a01-0000-0080-b1a9b00a3c7f" wmb:threadId="13108"/>
   <wmb:node wmb:nodeLabel="TEXTMESSENGER" wmb:nodeType="ComIbmMQInputNode" wmb:detail="TEXTMESSENGER"/>
  </wmb:messageFlowData>
 </wmb:eventPointData>
</wmb:event>
		'''
				
		MonitoringEvent result = new ParseMonitoringEvent().parse(event)
		assert result != null
		assertEquals("TEXTMESSENGER.FlowStart", result.eventName)
		assertEquals(result.creationTime.toString(), "2017-04-18T14:38:55")
//		assertEquals(1492526335000, result.creationTimeMilliSecs)
//		Date creationTime = new Date(result.creationTimeMilliSecs)
//		def year = creationTime.calendarDate.year
//		def month = creationTime.calendarDate.month
//		def day = creationTime.calendarDate.dayOfMonth
//		assertEquals("${year}${'-'}${month}${'-'}${day}".toString(), "2017-4-18")
		assertNull(result.getExceptionList())		
	}
	
	@Test
		public void testParseMonitoringRequestWithTime999522() {
		// received an event in which the msec must have been '000' and they were not present: creationTime="2017-04-18T14:38:55Z"
			
			def event = '''
	<wmb:event xmlns:wmb="http://www.ibm.com/xmlns/prod/websphere/messagebroker/6.1.0/monitoring/event">
		<wmb:eventPointData>
			<wmb:eventData wmb:productVersion="10008" wmb:eventSchemaVersion="6.1.0.3" wmb:eventSourceAddress="SF_ProcessAddressLookupResponses.Try Catch.terminal.in">
				<wmb:eventIdentity wmb:eventName="Try Catch.InTerminal"/>
				<wmb:eventSequence wmb:creationTime="2017-11-02T20:44:37.999522Z" wmb:counter="15"/>
				<wmb:eventCorrelation wmb:localTransactionId="5260002b-4140-44e5-ab3f-e48517988bd2-10" wmb:parentTransactionId="" wmb:globalTransactionId=""/>
			</wmb:eventData>
			<wmb:messageFlowData>
				<wmb:broker wmb:name="PAHIIBUATN2_inuaib02" wmb:UUID="14a66dcc-4be3-4928-89e7-99934f339e92"/>
				<wmb:executionGroup wmb:name="3RD_PARTY_APPLICATIONS" wmb:UUID="a818972d-f452-411d-a61a-c6622c8cc7ca"/>
				<wmb:messageFlow wmb:uniqueFlowName="PAHIIBUATN2_inuaib02.3RD_PARTY_APPLICATIONS.AddressLookup.com.petsathome.integration.addresslookup.MF_AddressLookup" wmb:name="com.petsathome.integration.addresslookup.MF_AddressLookup" wmb:UUID="92c29fa2-06f3-43b1-a852-b9c7b82020dc" wmb:threadId="1716"/>
				<wmb:node wmb:nodeLabel="SF_ProcessAddressLookupResponses.Try Catch" wmb:nodeType="ComIbmTryCatchNode" wmb:terminal="in"/>
			</wmb:messageFlowData>
		</wmb:eventPointData>
	</wmb:event>
			'''
					
			MonitoringEvent result = new ParseMonitoringEvent().parse(event)
			assert result != null
			assertEquals("Try Catch.InTerminal", result.eventName)
			assertEquals(result.creationTime.toString(), "2017-11-02T20:44:37.999522")
//			assertEquals(1509655477999, result.creationTimeMilliSecs)
//			Date creationTime = new Date(result.creationTimeMilliSecs)
//			def year = creationTime.calendarDate.year
//			def month = creationTime.calendarDate.month
//			def day = creationTime.calendarDate.dayOfMonth
//			assertEquals("${year}${'-'}${month}${'-'}${day}".toString(), "2017-11-2")
			assertNull(result.getExceptionList())
		}

	@Test
	public void testParseMonitoringRequestWithoutPayloadAndDataAndDateTimeMillisecs() {
		def event = '''
<wmb:event xmlns:wmb="http://www.ibm.com/xmlns/prod/websphere/messagebroker/6.1.0/monitoring/event">
 <wmb:eventPointData>
  <wmb:eventData wmb:productVersion="9007" wmb:eventSchemaVersion="6.1.0.3" wmb:eventSourceAddress="TEXTMESSENGER.transaction.Start">
   <wmb:eventIdentity wmb:eventName="TEXTMESSENGER.FlowStart"/>
   <wmb:eventSequence wmb:creationTime="2017-03-20T17:15:10.788Z" wmb:counter="1"/>
   <wmb:eventCorrelation wmb:localTransactionId="04959914-c387-4464-8a2c-9993753fe780-22" wmb:parentTransactionId="" wmb:globalTransactionId="414d5120494239514d475220202020202881ad582170c606"/>
  </wmb:eventData>
  <wmb:messageFlowData>
   <wmb:broker wmb:name="IB9NODE" wmb:UUID="caec250c-73c8-49e1-9cda-4091c7a70655"/>
   <wmb:executionGroup wmb:name="PagerExecutionGroup" wmb:UUID="5ecf6193-5901-0000-0080-c313a9bf8971"/>
   <wmb:messageFlow wmb:uniqueFlowName="IB9NODE.PagerExecutionGroup.TextMessenger" wmb:name="TextMessenger" wmb:UUID="581f026c-5a01-0000-0080-b1a9b00a3c7f" wmb:threadId="13108"/>
   <wmb:node wmb:nodeLabel="TEXTMESSENGER" wmb:nodeType="ComIbmMQInputNode" wmb:detail="TEXTMESSENGER"/>
  </wmb:messageFlowData>
 </wmb:eventPointData>
</wmb:event>
		'''

				
		MonitoringEvent result = new ParseMonitoringEvent().parse(event)
		assert result != null
		assertEquals("TEXTMESSENGER.FlowStart", result.eventName)
		assertEquals("", result.bodyIn)
		assertEquals(null, result.applicationData)
		assertEquals(result.creationTime.toString(), "2017-03-20T17:15:10.788")
//		assertEquals(1490030110788, result.creationTimeMilliSecs)
//		Date creationTime = new Date(result.creationTimeMilliSecs)
//		def year = creationTime.calendarDate.year
//		def month = creationTime.calendarDate.month
//		def day = creationTime.calendarDate.dayOfMonth
//		assertEquals("${year}${'-'}${month}${'-'}${day}".toString(), "2017-3-20")
		assertNull(result.getExceptionList())		
	}
	
	
//	@Test
	public void testParseMonitoringRequestWithInvalidCreationTimeLength() {
		
		def event = '''
<wmb:event xmlns:wmb="http://www.ibm.com/xmlns/prod/websphere/messagebroker/6.1.0/monitoring/event">
 <wmb:eventPointData>
  <wmb:eventData wmb:productVersion="10008" wmb:eventSchemaVersion="6.1.0.3" wmb:eventSourceAddress="HTTP Input.transaction.Start">
   <wmb:eventIdentity wmb:eventName="HTTP Input.TransactionStart"/>
   <wmb:eventSequence wmb:creationTime="2017-07-24T13:46:46.33Z" wmb:counter="1"/>
   <wmb:eventCorrelation wmb:localTransactionId="9109ac5b-5b2e-49fa-9e2f-a287031b4ed0-4" wmb:parentTransactionId="" wmb:globalTransactionId="414d512049423130514d47522020202029e170592012d668"/>
  </wmb:eventData>
  <wmb:messageFlowData>
   <wmb:broker wmb:name="PETS_AT_HOME" wmb:UUID="47ea0eeb-dc5d-47c5-a565-0bb5ec322503"/>
   <wmb:executionGroup wmb:name="njamsTesting" wmb:UUID="64030c8c-4287-4fff-b510-2cd5beb8f155"/>
   <wmb:messageFlow wmb:uniqueFlowName="PETS_AT_HOME.njamsTesting.HTTPInputApplication.HTTPInputMessageFlow" wmb:name="HTTPInputMessageFlow" wmb:UUID="7190a944-cf3f-46e6-90e5-22001ea669a4" wmb:threadId="11608"/>
   <wmb:node wmb:nodeLabel="HTTP Input" wmb:nodeType="ComIbmWSInputNode"/>
  </wmb:messageFlowData>
 </wmb:eventPointData>
 <wmb:bitstreamData>
  <wmb:bitstream wmb:encoding="base64Binary">ew0KIklucHV0RmllbGQxIjoiSGVsbG8iLA0KIklucHV0RmllbGQyIjoibkpBTVMgQUEiDQp9</wmb:bitstream>
 </wmb:bitstreamData>
</wmb:event>
		'''
		try {
			MonitoringEvent result = new ParseMonitoringEvent().parse(event)
			fail('Expecting an exceptiom to be thrown for invalid Creation Time length')
		} catch (Exception e ) {
			assertTrue(e instanceof GroovyRuntimeException)
			assertEquals('Event Creation Time has an invalid length: 23, valid values are: 20, 24, 27', e.getMessage())
		}

	}

	@Test
	public void testParseMonitoringRequestWithExceptionList() {
		def event = '''
<wmb:event xmlns:wmb="http://www.ibm.com/xmlns/prod/websphere/messagebroker/6.1.0/monitoring/event">
 <wmb:eventPointData>
  <wmb:eventData wmb:productVersion="10008" wmb:eventSchemaVersion="6.1.0.3" wmb:eventSourceAddress="HTTP Input.terminal.catch">
   <wmb:eventIdentity wmb:eventName="HTTP Input.CatchTerminal"/>
   <wmb:eventSequence wmb:creationTime="2017-07-01T07:22:32.094396Z" wmb:counter="4"/>
   <wmb:eventCorrelation wmb:localTransactionId="56aa9d96-0f45-4b55-a59b-d0bd6cb73c70-1" wmb:parentTransactionId="" wmb:globalTransactionId="414d512049423130514d475220202020f72c4a592058dc05"/>
  </wmb:eventData>
  <wmb:messageFlowData>
   <wmb:broker wmb:name="PETS_AT_HOME" wmb:UUID="47ea0eeb-dc5d-47c5-a565-0bb5ec322503"/>
   <wmb:executionGroup wmb:name="njamsTesting" wmb:UUID="64030c8c-4287-4fff-b510-2cd5beb8f155"/>
   <wmb:messageFlow wmb:uniqueFlowName="PETS_AT_HOME.njamsTesting.PetsAtHome.PaH_AdapterFlows.AF01_HTTP_Input" wmb:name="PaH_AdapterFlows.AF01_HTTP_Input" wmb:UUID="64c81170-503e-4a85-b575-58918891d944" wmb:threadId="18596"/>
   <wmb:node wmb:nodeLabel="HTTP Input" wmb:nodeType="ComIbmWSInputNode" wmb:terminal="catch"/>
  </wmb:messageFlowData>
 </wmb:eventPointData>
 <wmb:applicationData xmlns="">
  <wmb:complexContent wmb:elementName="ExceptionList">
   <ExceptionList>
    <RecoverableException>
     <File>F:\\build\\S1000_slot1\\S1000_P\\src\\DataFlowEngine\\SQLNodeLibrary\\ImbComputeNode.cpp</File>
     <Line>515</Line>
     <Function>ImbComputeNode::evaluate</Function>
     <Type>ComIbmComputeNode</Type>
     <Name>PaH_AdapterFlows/AF01_HTTP_Input#FCMComposite_1_5</Name>
     <Label>PaH_AdapterFlows.AF01_HTTP_Input.BuildMessage</Label>
     <Catalog>BIPmsgs</Catalog>
     <Severity>3</Severity>
     <Number>2230</Number>
     <Text>Caught exception and rethrowing</Text>
     <Insert>
      <Type>14</Type>
      <Text>PaH_AdapterFlows.AF01_HTTP_Input.BuildMessage</Text>
     </Insert>
     <RecoverableException>
      <File>F:\\build\\S1000_slot1\\S1000_P\\src\\DataFlowEngine\\ImbRdl\\ImbRdlStatementGroup.cpp</File>
      <Line>792</Line>
      <Function>SqlStatementGroup::execute</Function>
      <Type></Type>
      <Name></Name>
      <Label></Label>
      <Catalog>BIPmsgs</Catalog>
      <Severity>3</Severity>
      <Number>2488</Number>
      <Text>Error detected, rethrowing</Text>
      <Insert>
       <Type>5</Type>
       <Text>PaH_AdapterFlows.AF01_HTTP_Input.Main</Text>
      </Insert>
      <Insert>
       <Type>5</Type>
       <Text>32.3</Text>
      </Insert>
      <Insert>
       <Type>5</Type>
       <Text>SET OutputRoot.MQRFH2.usr.ApplicationId = getApplicationID(InputRoot, NULL, TRUE);</Text>
      </Insert>
      <RecoverableException>
       <File>F:\\build\\S1000_slot1\\S1000_P\\src\\DataFlowEngine\\ImbRdl\\ImbRdlRoutine.cpp</File>
       <Line>1326</Line>
       <Function>SqlRoutine::invoke</Function>
       <Type></Type>
       <Name></Name>
       <Label></Label>
       <Catalog>BIPmsgs</Catalog>
       <Severity>3</Severity>
       <Number>2934</Number>
       <Text>Error occured in procedure</Text>
       <Insert>
        <Type>5</Type>
        <Text>getApplicationId</Text>
       </Insert>
       <RecoverableException>
        <File>F:\\build\\S1000_slot1\\S1000_P\\src\\DataFlowEngine\\ImbRdl\\ImbRdlStatementGroup.cpp</File>
        <Line>792</Line>
        <Function>SqlStatementGroup::execute</Function>
        <Type></Type>
        <Name></Name>
        <Label></Label>
        <Catalog>BIPmsgs</Catalog>
        <Severity>3</Severity>
        <Number>2488</Number>
        <Text>Error detected, rethrowing</Text>
        <Insert>
         <Type>5</Type>
         <Text>PaH_FunctionLib.getApplicationId</Text>
        </Insert>
        <Insert>
         <Type>5</Type>
         <Text>22.5</Text>
        </Insert>
        <Insert>
         <Type>5</Type>
         <Text>THROW EXCEPTION CATALOG 'BIPmsgs' MESSAGE 3011 VALUES('Bad Request: Could not find ApplicationId in MQRFH2 or Message Body');</Text>
        </Insert>
        <UserException>
         <File>F:\\build\\S1000_slot1\\S1000_P\\src\\DataFlowEngine\\ImbRdl\\ImbRdlThrowExceptionStatements.cpp</File>
         <Line>230</Line>
         <Function>SqlThrowExceptionStatement::execute</Function>
         <Type>ComIbmComputeNode</Type>
         <Name>PaH_AdapterFlows/AF01_HTTP_Input#FCMComposite_1_5</Name>
         <Label>PaH_AdapterFlows.AF01_HTTP_Input.BuildMessage</Label>
         <Catalog>BIPmsgs</Catalog>
         <Severity>1</Severity>
         <Number>3011</Number>
         <Text>User generated exception</Text>
         <Insert>
          <Type>5</Type>
          <Text>Bad Request: Could not find ApplicationId in MQRFH2 or Message Body</Text>
         </Insert>
         <Insert>
          <Type>14</Type>
          <Text>PaH_AdapterFlows.AF01_HTTP_Input.BuildMessage</Text>
         </Insert>
        </UserException>
       </RecoverableException>
      </RecoverableException>
     </RecoverableException>
    </RecoverableException>
   </ExceptionList>
  </wmb:complexContent>
 </wmb:applicationData>
 <wmb:bitstreamData>
  <wmb:bitstream wmb:encoding="base64Binary">PHNvYXBlbnY6RW52ZWxvcGUgeG1sbnM6c29hcGVudj0iaHR0cDovL3NjaGVtYXMueG1sc29hcC5vcmcvc29hcC9lbnZlbG9wZS8iIHhtbG5zOmFkZD0iaHR0cDovL3d3dy5wZXRzYXRob21lLmNvLnVrL21lc3NhZ2VzL0FkZHJlc3NMb29rdXAiIHhtbG5zOm1lcz0iaHR0cDovL3d3dy5wZXRzYXRob21lLmNvLnVrL3R5cGVzL01lc3NhZ2VIZWFkZXIiIHhtbG5zOmJhc2U9Imh0dHA6Ly93d3cucGV0c2F0aG9tZS5jby51ay90eXBlcy9CYXNlIiB4bWxuczpleGM9Imh0dHA6Ly93d3cucGV0c2F0aG9tZS5jby51ay90eXBlcy9FeGNlcHRpb24iIHhtbG5zOmFkZDE9Imh0dHA6Ly93d3cucGV0c2F0aG9tZS5jby51ay90eXBlcy9BZGRyZXNzIj4NCiAgIDxzb2FwZW52OkhlYWRlci8+DQogICA8c29hcGVudjpCb2R5Pg0KICAgICAgPGFkZDpBZGRyZXNzTG9va3VwUmVxdWVzdD4NCiAgICAgICAgIDxhZGQ6TWVzc2FnZUhlYWRlcj4NCiAgICAgICAgICAgIDxtZXM6SGVhZGVyUm91dGluZz4NCiAgICAgICAgICAgICAgIDxtZXM6QnJhbmQ+UGV0cyBBdCBIb21lPC9tZXM6QnJhbmQ+DQogICAgICAgICAgICAgICA8bWVzOkNoYW5uZWwvPg0KICAgICAgICAgICAgICAgPG1lczpSZXF1ZXN0U3RhZ2U+PC9tZXM6UmVxdWVzdFN0YWdlPg0KICAgICAgICAgICAgPC9tZXM6SGVhZGVyUm91dGluZz4NCiAgICAgICAgICAgIDxtZXM6SGVhZGVyRGF0YT4NCiAgICAgICAgICAgICAgIDxtZXM6Q2xpZW50UmVmZXJlbmNlPlRFU1Q8L21lczpDbGllbnRSZWZlcmVuY2U+DQogICAgICAgICAgICAgICA8bWVzOlN1Ym1pc3Npb25EYXRlVGltZT4yMDE2LTA3LTEzVDEyOjAwOjEyLjg3OTQwODErMDE6MDA8L21lczpTdWJtaXNzaW9uRGF0ZVRpbWU+DQogICAgICAgICAgICAgICA8bWVzOklzRXhjZXB0aW9uPmZhbHNlPC9tZXM6SXNFeGNlcHRpb24+DQogICAgICAgICAgICA8L21lczpIZWFkZXJEYXRhPg0KICAgICAgICAgPC9hZGQ6TWVzc2FnZUhlYWRlcj4NCiAgICAgICAgIDxhZGQ6TWVzc2FnZUJvZHk+DQogICAgICAgICAgICA8YWRkMTpDb3VudHJ5PlVLPC9hZGQxOkNvdW50cnk+DQogICAgICAgICAgICA8YWRkMTpQb3N0Q29kZT5BQjEgMEFBPC9hZGQxOlBvc3RDb2RlPg0KICAgICAgICAgICAgPGFkZDE6VHlwZT5DdXJyZW50PC9hZGQxOlR5cGU+DQogICAgICAgICAgICA8YWRkMTpBZGRyZXNzQ2hlY2tlZD50cnVlPC9hZGQxOkFkZHJlc3NDaGVja2VkPg0KICAgICAgICAgICAgPGFkZDE6SXNCdXNpbmVzc0FkZHJlc3M+ZmFsc2U8L2FkZDE6SXNCdXNpbmVzc0FkZHJlc3M+DQogICAgICAgICA8L2FkZDpNZXNzYWdlQm9keT4NCiAgICAgIDwvYWRkOkFkZHJlc3NMb29rdXBSZXF1ZXN0Pg0KICAgPC9zb2FwZW52OkJvZHk+DQo8L3NvYXBlbnY6RW52ZWxvcGU+</wmb:bitstream>
 </wmb:bitstreamData>
</wmb:event>
		'''
				
		MonitoringEvent result = new ParseMonitoringEvent().parse(event)
		assert result != null
		assertEquals("HTTP Input.CatchTerminal", result.eventName)
		assertNotNull(result.bodyIn)
		assertNotNull(result.applicationData)
		assertNotNull(result.getExceptionList())
		assertTrue(result.getExceptionList().startsWith("<![CDATA[<ExceptionList>"))
		
	}

	@Test
	public void testParseMonitoringRequestWithMsgId() {
		def event = '''
<wmb:event xmlns:wmb="http://www.ibm.com/xmlns/prod/websphere/messagebroker/6.1.0/monitoring/event">
 <wmb:eventPointData>
  <wmb:eventData wmb:productVersion="10008" wmb:eventSchemaVersion="6.1.0.3" wmb:eventSourceAddress="SF_StoreRequest.ADDRESSLOOKUP.REQ.STORE.terminal.in">
   <wmb:eventIdentity wmb:eventName="ADDRESSLOOKUP.REQ.STORE.InTerminal"/>
   <wmb:eventSequence wmb:creationTime="2018-01-12T17:49:19.893456Z" wmb:counter="7"/>
   <wmb:eventCorrelation wmb:localTransactionId="e6441449-7de8-46d5-a929-883a6cf8e6f9-2443" wmb:parentTransactionId="" wmb:globalTransactionId="AB13 0AA"/>
  </wmb:eventData>
  <wmb:messageFlowData>
   <wmb:broker wmb:name="PAHIIBUATN2_inuaib02" wmb:UUID="14a66dcc-4be3-4928-89e7-99934f339e92"/>
   <wmb:executionGroup wmb:name="3RD_PARTY_APPLICATIONS" wmb:UUID="a818972d-f452-411d-a61a-c6622c8cc7ca"/>
   <wmb:messageFlow wmb:uniqueFlowName="PAHIIBUATN2_inuaib02.3RD_PARTY_APPLICATIONS.AddressLookup.com.petsathome.integration.addresslookup.MF_AddressLookup" wmb:name="com.petsat
	 home.integration.addresslookup.MF_AddressLookup" wmb:UUID="92c29fa2-06f3-43b1-a852-b9c7b82020dc" wmb:threadId="1884"/>
   <wmb:node wmb:nodeLabel="SF_StoreRequest.ADDRESSLOOKUP.REQ.STORE" wmb:nodeType="ComIbmMQOutputNode" wmb:terminal="in"/>
  </wmb:messageFlowData>
 </wmb:eventPointData>
 <wmb:applicationData xmlns="">
  <wmb:simpleContent wmb:name="MsgId" wmb:value="414d512049423130514d475220202020bb20035a20bc0440" wmb:dataType="hexBinary"/>
 </wmb:applicationData>
 <wmb:bitstreamData/>
</wmb:event>
		'''
				
		MonitoringEvent result = new ParseMonitoringEvent().parse(event)
		assert result != null
		assertEquals("ADDRESSLOOKUP.REQ.STORE.InTerminal", result.eventName)
//		assertNotNull(result.bodyIn)
		assertNotNull(result.msgId)
		assertEquals("414d512049423130514d475220202020bb20035a20bc0440", result.msgId)
//		assertNotNull(result.getExceptionList())
		
	}

	@Test
	public void testParseMonitoringRequestWithMsgIdAndCorrelId() {
		def event = '''
<wmb:event xmlns:wmb="http://www.ibm.com/xmlns/prod/websphere/messagebroker/6.1.0/monitoring/event">
 <wmb:eventPointData>
  <wmb:eventData wmb:productVersion="10008" wmb:eventSchemaVersion="6.1.0.3" wmb:eventSourceAddress="SF_StoreRequest.ADDRESSLOOKUP.REQ.STORE.terminal.in">
   <wmb:eventIdentity wmb:eventName="ADDRESSLOOKUP.REQ.STORE.InTerminal"/>
   <wmb:eventSequence wmb:creationTime="2018-01-12T17:49:19.893456Z" wmb:counter="7"/>
   <wmb:eventCorrelation wmb:localTransactionId="e6441449-7de8-46d5-a929-883a6cf8e6f9-2443" wmb:parentTransactionId="" wmb:globalTransactionId="AB13 0AA"/>
  </wmb:eventData>
  <wmb:messageFlowData>
   <wmb:broker wmb:name="PAHIIBUATN2_inuaib02" wmb:UUID="14a66dcc-4be3-4928-89e7-99934f339e92"/>
   <wmb:executionGroup wmb:name="3RD_PARTY_APPLICATIONS" wmb:UUID="a818972d-f452-411d-a61a-c6622c8cc7ca"/>
   <wmb:messageFlow wmb:uniqueFlowName="PAHIIBUATN2_inuaib02.3RD_PARTY_APPLICATIONS.AddressLookup.com.petsathome.integration.addresslookup.MF_AddressLookup" wmb:name="com.petsat
	 home.integration.addresslookup.MF_AddressLookup" wmb:UUID="92c29fa2-06f3-43b1-a852-b9c7b82020dc" wmb:threadId="1884"/>
   <wmb:node wmb:nodeLabel="SF_StoreRequest.ADDRESSLOOKUP.REQ.STORE" wmb:nodeType="ComIbmMQOutputNode" wmb:terminal="in"/>
  </wmb:messageFlowData>
 </wmb:eventPointData>
 <wmb:applicationData xmlns="">
  <wmb:simpleContent wmb:name="MsgId" wmb:value="414d512049423130514d475220202020bb20035a20bc0440" wmb:dataType="hexBinary"/>
  <wmb:simpleContent wmb:name="CorrelId" wmb:value="414d5120514d55414942303220202020cb3a675a2188e16d" wmb:dataType="hexBinary"/>		
 </wmb:applicationData>
 <wmb:bitstreamData/>
</wmb:event>
		'''
				
		MonitoringEvent result = new ParseMonitoringEvent().parse(event)
		assert result != null
		assertEquals("ADDRESSLOOKUP.REQ.STORE.InTerminal", result.eventName)
//		assertNotNull(result.bodyIn)
		assertNotNull(result.msgId)
		assertEquals("414d512049423130514d475220202020bb20035a20bc0440", result.msgId)
		assertEquals("414d5120514d55414942303220202020cb3a675a2188e16d", result.correlId)
//		assertNotNull(result.getExceptionList())
		
	}
	
	@Test
	public void testParseMonitoringRequestWithRestRequest() {
		def event = '''
<wmb:event xmlns:wmb="http://www.ibm.com/xmlns/prod/websphere/messagebroker/6.1.0/monitoring/event">
 <wmb:eventPointData>
  <wmb:eventData wmb:productVersion="10008" wmb:eventSchemaVersion="6.1.0.3" wmb:eventSourceAddress="HTTP Input.transaction.Start">
   <wmb:eventIdentity wmb:eventName="HTTP Input.TransactionStart"/>
   <wmb:eventSequence wmb:creationTime="2017-07-26T08:06:22.457151Z" wmb:counter="1"/>
   <wmb:eventCorrelation wmb:localTransactionId="cc837a61-85d3-490e-bec6-8fc9fc88e318-1" wmb:parentTransactionId="" wmb:globalTransactionId="414d512049423130514d47522020202029e170592001ea6b"/>
  </wmb:eventData>
  <wmb:messageFlowData>
   <wmb:broker wmb:name="PETS_AT_HOME" wmb:UUID="47ea0eeb-dc5d-47c5-a565-0bb5ec322503"/>
   <wmb:executionGroup wmb:name="njamsTesting" wmb:UUID="64030c8c-4287-4fff-b510-2cd5beb8f155"/>
   <wmb:messageFlow wmb:uniqueFlowName="PETS_AT_HOME.njamsTesting.EmployeeService_REST.gen.EmployeeService_REST" wmb:name="gen.EmployeeService_REST" wmb:UUID="c3ed582d-5e56-41c8-b37f-0dbb2de1ea75" wmb:threadId="6864"/>
   <wmb:node wmb:nodeLabel="HTTP Input" wmb:nodeType="ComIbmWSInputNode"/>
  </wmb:messageFlowData>
 </wmb:eventPointData>
 <wmb:applicationData xmlns="">
  <wmb:complexContent wmb:elementName="Input">
   <Input>
    <Method>GET</Method>
    <Operation>getEmployee</Operation>
    <Path>/TestWebApp/resources/employees/000010</Path>
    <URI>http://localhost:7080/TestWebApp/resources/employees/000010</URI>
    <Parameters>
     <employeeNumber>000010</employeeNumber>
    </Parameters>
   </Input>
  </wmb:complexContent>
 </wmb:applicationData>
</wmb:event>
		'''
				
		MonitoringEvent result = new ParseMonitoringEvent().parse(event)
		assert result != null
		assertEquals("HTTP Input.TransactionStart", result.eventName)
		assertNotNull(result.bodyIn)
		assertNull(result.exceptionList)
		assertNotNull(result.applicationData)
		assertTrue(result.applicationData.startsWith("<wmb:complexContent xmlns:wmb=\"http://www.ibm.com/xmlns/prod/websphere/messagebroker/6.1.0/monitoring/event\" wmb:elementName=\"Input\">"))
		
	}
	
	@Test
	public void testParseMonitoringRequestWithMsgflowHasSubflow() {
	// When a subflow is invoked from a Msgflow, 'nodeLabel' - {subflow-name}.{node-name} 
	// The subflow name for the node is the one before the '.'	
		def event = '''
<wmb:event xmlns:wmb="http://www.ibm.com/xmlns/prod/websphere/messagebroker/6.1.0/monitoring/event">
 <wmb:eventPointData>
  <wmb:eventData wmb:productVersion="10008" wmb:eventSchemaVersion="6.1.0.3" wmb:eventSourceAddress="SF_TraceWithSubflow.Trace Q.terminal.in">
   <wmb:eventIdentity wmb:eventName="Trace Q.InTerminal"/>
   <wmb:eventSequence wmb:creationTime="2017-08-11T09:58:40.067004Z" wmb:counter="3"/>
   <wmb:eventCorrelation wmb:localTransactionId="5e713f8c-92f9-4941-98a9-816dc832bd47-1" wmb:parentTransactionId="" wmb:globalTransactionId="414d512049423130514d4752202020209d778d592000b10a"/>
  </wmb:eventData>
  <wmb:messageFlowData>
   <wmb:broker wmb:name="PETS_AT_HOME" wmb:UUID="47ea0eeb-dc5d-47c5-a565-0bb5ec322503"/>
   <wmb:executionGroup wmb:name="njamsTesting" wmb:UUID="64030c8c-4287-4fff-b510-2cd5beb8f155"/>
   <wmb:messageFlow wmb:uniqueFlowName="PETS_AT_HOME.njamsTesting.HTTPInputApplication.HTTPInputWithSubflowSubflow" wmb:name="HTTPInputWithSubflowSubflow" wmb:UUID="cf734320-abaa-4178-afb5-1ecbdc19e1d3" wmb:threadId="14352"/>
   <wmb:node wmb:nodeLabel="SF_TraceWithSubflow.Trace Q" wmb:nodeType="ComIbmTraceNode" wmb:terminal="in"/>
  </wmb:messageFlowData>
 </wmb:eventPointData>
 <wmb:bitstreamData>
  <wmb:bitstream wmb:encoding="base64Binary">PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0iVVRGLTgiPz4NCjxTdXJmV2F0Y2g+DQogPFRpbWVEYXRlPjE4LjA0LjE3IDE4OjE3OjUwPC9UaW1lRGF0ZT4NCiA8QnJlYWs+QmFja2Rvb3IgUGlwZTwvQnJlYWs+DQogPFdhdmVzPjI8L1dhdmVzPg0KIDxXaW5kPlNpZGUtb2Zmc2hvcmU8L1dpbmQ+DQo8L1N1cmZXYXRjaD4=</wmb:bitstream>
 </wmb:bitstreamData>
</wmb:event>
		'''
				
		MonitoringEvent result = new ParseMonitoringEvent().parse(event)
		assert result != null
		assertEquals("Trace Q.InTerminal", result.eventName)
		assertEquals(result.creationTime.toString(), "2017-08-11T09:58:40.067004")
//		assertEquals(1502445520067, result.creationTimeMilliSecs)
//		Date creationTime = new Date(result.creationTimeMilliSecs)
//		def year = creationTime.calendarDate.year
//		def month = creationTime.calendarDate.month
//		def day = creationTime.calendarDate.dayOfMonth
//		assertEquals("${year}${'-'}${month}${'-'}${day}".toString(), "2017-8-11")
		assertNull(result.getExceptionList())
		// for added methods
		assertFalse(result.isFromMsgFlow())
		assertTrue(result.isFromSubFlow())
		assertEquals("Trace Q", result.getLocalNodeLabel())
		assertEquals("SF_TraceWithSubflow", result.getSubflowName())
	}
	
	@Test
	public void testParseMonitoringRequestWithSubflowHasSubflow() {
	// When a subflow is invoked from a Subflow, 'nodeLabel' - {parent-subflow-name}.{this-subflow}.{node-name}
	// The subflow name for the node is the one before the '.' i.e {this-subflow}
		def event = '''
<wmb:event xmlns:wmb="http://www.ibm.com/xmlns/prod/websphere/messagebroker/6.1.0/monitoring/event">
 <wmb:eventPointData>
  <wmb:eventData wmb:productVersion="10008" wmb:eventSchemaVersion="6.1.0.3" wmb:eventSourceAddress="SF_TraceWithSubflow.SF_Trace.Trace A.terminal.in">
   <wmb:eventIdentity wmb:eventName="Trace A.InTerminal"/>
   <wmb:eventSequence wmb:creationTime="2017-08-11T09:58:40.067741Z" wmb:counter="4"/>
   <wmb:eventCorrelation wmb:localTransactionId="5e713f8c-92f9-4941-98a9-816dc832bd47-1" wmb:parentTransactionId="" wmb:globalTransactionId="414d512049423130514d4752202020209d778d592000b10a"/>
  </wmb:eventData>
  <wmb:messageFlowData>
   <wmb:broker wmb:name="PETS_AT_HOME" wmb:UUID="47ea0eeb-dc5d-47c5-a565-0bb5ec322503"/>
   <wmb:executionGroup wmb:name="njamsTesting" wmb:UUID="64030c8c-4287-4fff-b510-2cd5beb8f155"/>
   <wmb:messageFlow wmb:uniqueFlowName="PETS_AT_HOME.njamsTesting.HTTPInputApplication.HTTPInputWithSubflowSubflow" wmb:name="HTTPInputWithSubflowSubflow" wmb:UUID="cf734320-abaa-4178-afb5-1ecbdc19e1d3" wmb:threadId="14352"/>
   <wmb:node wmb:nodeLabel="SF_TraceWithSubflow.SF_Trace.Trace A" wmb:nodeType="ComIbmTraceNode" wmb:terminal="in"/>
  </wmb:messageFlowData>
 </wmb:eventPointData>
 <wmb:bitstreamData>
  <wmb:bitstream wmb:encoding="base64Binary">PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0iVVRGLTgiPz4NCjxTdXJmV2F0Y2g+DQogPFRpbWVEYXRlPjE4LjA0LjE3IDE4OjE3OjUwPC9UaW1lRGF0ZT4NCiA8QnJlYWs+QmFja2Rvb3IgUGlwZTwvQnJlYWs+DQogPFdhdmVzPjI8L1dhdmVzPg0KIDxXaW5kPlNpZGUtb2Zmc2hvcmU8L1dpbmQ+DQo8L1N1cmZXYXRjaD4=</wmb:bitstream>
 </wmb:bitstreamData>
</wmb:event>
		'''
				
		MonitoringEvent result = new ParseMonitoringEvent().parse(event)
		assert result != null
		assertEquals("Trace A.InTerminal", result.eventName)
		assertEquals(result.creationTime.toString(), "2017-08-11T09:58:40.067741")
//		assertEquals(1502445520068, result.creationTimeMilliSecs)
//		Date creationTime = new Date(result.creationTimeMilliSecs)
//		def year = creationTime.calendarDate.year
//		def month = creationTime.calendarDate.month
//		def day = creationTime.calendarDate.dayOfMonth
//		assertEquals("${year}${'-'}${month}${'-'}${day}".toString(), "2017-8-11")
		assertNull(result.getExceptionList())
		// for added methods
		assertFalse(result.isFromMsgFlow())
		assertTrue(result.isFromSubFlow())
		assertEquals("Trace A", result.getLocalNodeLabel())
		assertEquals("SF_Trace", result.getSubflowName())
	}
	
	@Test
	public void testCanDetectSubflowInvokingSubflow() {
	// When a subflow is invoked from a Subflow, 'nodeLabel' - {parent-subflow-name}.{this-subflow}.{node-name}
	// The subflow name for the node is the one before the '.' i.e {this-subflow}
		def previousEvent = '''
<wmb:event xmlns:wmb="http://www.ibm.com/xmlns/prod/websphere/messagebroker/6.1.0/monitoring/event">
 <wmb:eventPointData>
  <wmb:eventData wmb:productVersion="10008" wmb:eventSchemaVersion="6.1.0.3" wmb:eventSourceAddress="SF_TraceWithSubflow.Trace Q.terminal.in">
   <wmb:eventIdentity wmb:eventName="Trace Q.InTerminal"/>
   <wmb:eventSequence wmb:creationTime="2017-08-11T09:58:40.067004Z" wmb:counter="3"/>
   <wmb:eventCorrelation wmb:localTransactionId="5e713f8c-92f9-4941-98a9-816dc832bd47-1" wmb:parentTransactionId="" wmb:globalTransactionId="414d512049423130514d4752202020209d778d592000b10a"/>
  </wmb:eventData>
  <wmb:messageFlowData>
   <wmb:broker wmb:name="PETS_AT_HOME" wmb:UUID="47ea0eeb-dc5d-47c5-a565-0bb5ec322503"/>
   <wmb:executionGroup wmb:name="njamsTesting" wmb:UUID="64030c8c-4287-4fff-b510-2cd5beb8f155"/>
   <wmb:messageFlow wmb:uniqueFlowName="PETS_AT_HOME.njamsTesting.HTTPInputApplication.HTTPInputWithSubflowSubflow" wmb:name="HTTPInputWithSubflowSubflow" wmb:UUID="cf734320-abaa-4178-afb5-1ecbdc19e1d3" wmb:threadId="14352"/>
   <wmb:node wmb:nodeLabel="SF_TraceWithSubflow.Trace Q" wmb:nodeType="ComIbmTraceNode" wmb:terminal="in"/>
  </wmb:messageFlowData>
 </wmb:eventPointData>
 <wmb:bitstreamData>
  <wmb:bitstream wmb:encoding="base64Binary">PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0iVVRGLTgiPz4NCjxTdXJmV2F0Y2g+DQogPFRpbWVEYXRlPjE4LjA0LjE3IDE4OjE3OjUwPC9UaW1lRGF0ZT4NCiA8QnJlYWs+QmFja2Rvb3IgUGlwZTwvQnJlYWs+DQogPFdhdmVzPjI8L1dhdmVzPg0KIDxXaW5kPlNpZGUtb2Zmc2hvcmU8L1dpbmQ+DQo8L1N1cmZXYXRjaD4=</wmb:bitstream>
 </wmb:bitstreamData>
</wmb:event>
		'''		
		
		def currentEvent = '''
<wmb:event xmlns:wmb="http://www.ibm.com/xmlns/prod/websphere/messagebroker/6.1.0/monitoring/event">
 <wmb:eventPointData>
  <wmb:eventData wmb:productVersion="10008" wmb:eventSchemaVersion="6.1.0.3" wmb:eventSourceAddress="SF_TraceWithSubflow.SF_Trace.Trace A.terminal.in">
   <wmb:eventIdentity wmb:eventName="Trace A.InTerminal"/>
   <wmb:eventSequence wmb:creationTime="2017-08-11T09:58:40.067741Z" wmb:counter="4"/>
   <wmb:eventCorrelation wmb:localTransactionId="5e713f8c-92f9-4941-98a9-816dc832bd47-1" wmb:parentTransactionId="" wmb:globalTransactionId="414d512049423130514d4752202020209d778d592000b10a"/>
  </wmb:eventData>
  <wmb:messageFlowData>
   <wmb:broker wmb:name="PETS_AT_HOME" wmb:UUID="47ea0eeb-dc5d-47c5-a565-0bb5ec322503"/>
   <wmb:executionGroup wmb:name="njamsTesting" wmb:UUID="64030c8c-4287-4fff-b510-2cd5beb8f155"/>
   <wmb:messageFlow wmb:uniqueFlowName="PETS_AT_HOME.njamsTesting.HTTPInputApplication.HTTPInputWithSubflowSubflow" wmb:name="HTTPInputWithSubflowSubflow" wmb:UUID="cf734320-abaa-4178-afb5-1ecbdc19e1d3" wmb:threadId="14352"/>
   <wmb:node wmb:nodeLabel="SF_TraceWithSubflow.SF_Trace.Trace A" wmb:nodeType="ComIbmTraceNode" wmb:terminal="in"/>
  </wmb:messageFlowData>
 </wmb:eventPointData>
 <wmb:bitstreamData>
  <wmb:bitstream wmb:encoding="base64Binary">PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0iVVRGLTgiPz4NCjxTdXJmV2F0Y2g+DQogPFRpbWVEYXRlPjE4LjA0LjE3IDE4OjE3OjUwPC9UaW1lRGF0ZT4NCiA8QnJlYWs+QmFja2Rvb3IgUGlwZTwvQnJlYWs+DQogPFdhdmVzPjI8L1dhdmVzPg0KIDxXaW5kPlNpZGUtb2Zmc2hvcmU8L1dpbmQ+DQo8L1N1cmZXYXRjaD4=</wmb:bitstream>
 </wmb:bitstreamData>
</wmb:event>
		'''
				
		MonitoringEvent previous = new ParseMonitoringEvent().parse(previousEvent) // Invoking subflow
		MonitoringEvent current = new ParseMonitoringEvent().parse(currentEvent) // Invoked subflow
		assertTrue(current.isChildOfSubflow(previous))
	}
	
	@Test
	public void testCanDetectSubflowReturningToInvokingSubflow() {
	// When a subflow is invoked from a Subflow, 'nodeLabel' - {parent-subflow-name}.{this-subflow}.{node-name}
	// The subflow name for the node is the one before the '.' i.e {this-subflow}
		def currentEvent = '''
<wmb:event xmlns:wmb="http://www.ibm.com/xmlns/prod/websphere/messagebroker/6.1.0/monitoring/event">
 <wmb:eventPointData>
  <wmb:eventData wmb:productVersion="10008" wmb:eventSchemaVersion="6.1.0.3" wmb:eventSourceAddress="SF_TraceWithSubflow.Trace Q.terminal.in">
   <wmb:eventIdentity wmb:eventName="Trace Q.InTerminal"/>
   <wmb:eventSequence wmb:creationTime="2017-08-11T09:58:40.067004Z" wmb:counter="3"/>
   <wmb:eventCorrelation wmb:localTransactionId="5e713f8c-92f9-4941-98a9-816dc832bd47-1" wmb:parentTransactionId="" wmb:globalTransactionId="414d512049423130514d4752202020209d778d592000b10a"/>
  </wmb:eventData>
  <wmb:messageFlowData>
   <wmb:broker wmb:name="PETS_AT_HOME" wmb:UUID="47ea0eeb-dc5d-47c5-a565-0bb5ec322503"/>
   <wmb:executionGroup wmb:name="njamsTesting" wmb:UUID="64030c8c-4287-4fff-b510-2cd5beb8f155"/>
   <wmb:messageFlow wmb:uniqueFlowName="PETS_AT_HOME.njamsTesting.HTTPInputApplication.HTTPInputWithSubflowSubflow" wmb:name="HTTPInputWithSubflowSubflow" wmb:UUID="cf734320-abaa-4178-afb5-1ecbdc19e1d3" wmb:threadId="14352"/>
   <wmb:node wmb:nodeLabel="SF_TraceWithSubflow.Trace Q" wmb:nodeType="ComIbmTraceNode" wmb:terminal="in"/>
  </wmb:messageFlowData>
 </wmb:eventPointData>
 <wmb:bitstreamData>
  <wmb:bitstream wmb:encoding="base64Binary">PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0iVVRGLTgiPz4NCjxTdXJmV2F0Y2g+DQogPFRpbWVEYXRlPjE4LjA0LjE3IDE4OjE3OjUwPC9UaW1lRGF0ZT4NCiA8QnJlYWs+QmFja2Rvb3IgUGlwZTwvQnJlYWs+DQogPFdhdmVzPjI8L1dhdmVzPg0KIDxXaW5kPlNpZGUtb2Zmc2hvcmU8L1dpbmQ+DQo8L1N1cmZXYXRjaD4=</wmb:bitstream>
 </wmb:bitstreamData>
</wmb:event>
		'''		
		
		def previousEvent = '''
<wmb:event xmlns:wmb="http://www.ibm.com/xmlns/prod/websphere/messagebroker/6.1.0/monitoring/event">
 <wmb:eventPointData>
  <wmb:eventData wmb:productVersion="10008" wmb:eventSchemaVersion="6.1.0.3" wmb:eventSourceAddress="SF_TraceWithSubflow.SF_Trace.Trace A.terminal.in">
   <wmb:eventIdentity wmb:eventName="Trace A.InTerminal"/>
   <wmb:eventSequence wmb:creationTime="2017-08-11T09:58:40.067741Z" wmb:counter="4"/>
   <wmb:eventCorrelation wmb:localTransactionId="5e713f8c-92f9-4941-98a9-816dc832bd47-1" wmb:parentTransactionId="" wmb:globalTransactionId="414d512049423130514d4752202020209d778d592000b10a"/>
  </wmb:eventData>
  <wmb:messageFlowData>
   <wmb:broker wmb:name="PETS_AT_HOME" wmb:UUID="47ea0eeb-dc5d-47c5-a565-0bb5ec322503"/>
   <wmb:executionGroup wmb:name="njamsTesting" wmb:UUID="64030c8c-4287-4fff-b510-2cd5beb8f155"/>
   <wmb:messageFlow wmb:uniqueFlowName="PETS_AT_HOME.njamsTesting.HTTPInputApplication.HTTPInputWithSubflowSubflow" wmb:name="HTTPInputWithSubflowSubflow" wmb:UUID="cf734320-abaa-4178-afb5-1ecbdc19e1d3" wmb:threadId="14352"/>
   <wmb:node wmb:nodeLabel="SF_TraceWithSubflow.SF_Trace.Trace A" wmb:nodeType="ComIbmTraceNode" wmb:terminal="in"/>
  </wmb:messageFlowData>
 </wmb:eventPointData>
 <wmb:bitstreamData>
  <wmb:bitstream wmb:encoding="base64Binary">PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0iVVRGLTgiPz4NCjxTdXJmV2F0Y2g+DQogPFRpbWVEYXRlPjE4LjA0LjE3IDE4OjE3OjUwPC9UaW1lRGF0ZT4NCiA8QnJlYWs+QmFja2Rvb3IgUGlwZTwvQnJlYWs+DQogPFdhdmVzPjI8L1dhdmVzPg0KIDxXaW5kPlNpZGUtb2Zmc2hvcmU8L1dpbmQ+DQo8L1N1cmZXYXRjaD4=</wmb:bitstream>
 </wmb:bitstreamData>
</wmb:event>
		'''
				
		MonitoringEvent current = new ParseMonitoringEvent().parse(currentEvent) // Invoking subflow
		MonitoringEvent previous = new ParseMonitoringEvent().parse(previousEvent) // Invoked subflow
		
		assertTrue(previous.isChildOfSubflow(current))
	}
	
	@Test
	public void testCanParseDatesWhichRoundToHundreds() {
		// The 'creationTime' values in the following events, will be processewd as millisecs and rounded up and down to "2017-08-16T10:17:39.800Z"
		// If the conversion uses Double's round(3) method, the traoiling 0s are dropped, resulting in an invalid Date format: "2017-08-16T10:17:39.8Z"
		// The parsing logic was changed do manipulations as Strings
		
		def roundUp = '''
<wmb:event
	xmlns:wmb="http://www.ibm.com/xmlns/prod/websphere/messagebroker/6.1.0/monitoring/event">
	<wmb:eventPointData>
		<wmb:eventData wmb:productVersion="10008"
			wmb:eventSchemaVersion="6.1.0.3"
			wmb:eventSourceAddress="SF_InvokeReply.SF_HTTPReply.Trace FF.terminal.in">
			<wmb:eventIdentity wmb:eventName="Trace FF.InTerminal" />
			<wmb:eventSequence wmb:creationTime="2017-08-16T10:17:39.799771Z"
				wmb:counter="5" />
			<wmb:eventCorrelation wmb:localTransactionId="da664ae8-29ce-4770-839d-9279ee42438d-4"
				wmb:parentTransactionId=""
				wmb:globalTransactionId="414d512049423130514d4752202020209d778d59203e6224" />
		</wmb:eventData>
		<wmb:messageFlowData>
			<wmb:broker wmb:name="PETS_AT_HOME"
				wmb:UUID="47ea0eeb-dc5d-47c5-a565-0bb5ec322503" />
			<wmb:executionGroup wmb:name="njamsTesting"
				wmb:UUID="64030c8c-4287-4fff-b510-2cd5beb8f155" />
			<wmb:messageFlow
				wmb:uniqueFlowName="PETS_AT_HOME.njamsTesting.HTTPInputApplication.HTTPInputWithReplySubflow"
				wmb:name="HTTPInputWithReplySubflow" wmb:UUID="bd444ca9-ac84-4962-8dfb-e97b31893cc3"
				wmb:threadId="7772" />
			<wmb:node wmb:nodeLabel="SF_InvokeReply.SF_HTTPReply.Trace FF"
				wmb:nodeType="ComIbmTraceNode" wmb:terminal="in" />
		</wmb:messageFlowData>
	</wmb:eventPointData>
	<wmb:bitstreamData>
		<wmb:bitstream wmb:encoding="base64Binary">PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0iVVRGLTgiPz4NCjxTdXJmV2F0Y2g+DQogPFRpbWVEYXRlPjE4LjA0LjE3IDE4OjE3OjUwPC9UaW1lRGF0ZT4NCiA8QnJlYWs+QmFja2Rvb3IgUGlwZTwvQnJlYWs+DQogPFdhdmVzPjE8L1dhdmVzPg0KIDxXaW5kPlNpZGUtb2Zmc2hvcmU8L1dpbmQ+DQo8L1N1cmZXYXRjaD4=
		</wmb:bitstream>
	</wmb:bitstreamData>
</wmb:event>
		'''		
		
		def roundDown = '''
<wmb:event
	xmlns:wmb="http://www.ibm.com/xmlns/prod/websphere/messagebroker/6.1.0/monitoring/event">
	<wmb:eventPointData>
		<wmb:eventData wmb:productVersion="10008"
			wmb:eventSchemaVersion="6.1.0.3"
			wmb:eventSourceAddress="SF_InvokeReply.SF_HTTPReply.HTTP Reply.terminal.in">
			<wmb:eventIdentity wmb:eventName="HTTP Reply.InTerminal" />
			<wmb:eventSequence wmb:creationTime="2017-08-16T10:17:39.800355Z"
				wmb:counter="6" />
			<wmb:eventCorrelation wmb:localTransactionId="da664ae8-29ce-4770-839d-9279ee42438d-4"
				wmb:parentTransactionId=""
				wmb:globalTransactionId="414d512049423130514d4752202020209d778d59203e6224" />
		</wmb:eventData>
		<wmb:messageFlowData>
			<wmb:broker wmb:name="PETS_AT_HOME"
				wmb:UUID="47ea0eeb-dc5d-47c5-a565-0bb5ec322503" />
			<wmb:executionGroup wmb:name="njamsTesting"
				wmb:UUID="64030c8c-4287-4fff-b510-2cd5beb8f155" />
			<wmb:messageFlow
				wmb:uniqueFlowName="PETS_AT_HOME.njamsTesting.HTTPInputApplication.HTTPInputWithReplySubflow"
				wmb:name="HTTPInputWithReplySubflow" wmb:UUID="bd444ca9-ac84-4962-8dfb-e97b31893cc3"
				wmb:threadId="7772" />
			<wmb:node wmb:nodeLabel="SF_InvokeReply.SF_HTTPReply.HTTP Reply"
				wmb:nodeType="ComIbmWSReplyNode" wmb:terminal="in" />
		</wmb:messageFlowData>
	</wmb:eventPointData>
	<wmb:bitstreamData>
		<wmb:bitstream wmb:encoding="base64Binary">PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0iVVRGLTgiPz4NCjxTdXJmV2F0Y2g+DQogPFRpbWVEYXRlPjE4LjA0LjE3IDE4OjE3OjUwPC9UaW1lRGF0ZT4NCiA8QnJlYWs+QmFja2Rvb3IgUGlwZTwvQnJlYWs+DQogPFdhdmVzPjE8L1dhdmVzPg0KIDxXaW5kPlNpZGUtb2Zmc2hvcmU8L1dpbmQ+DQo8L1N1cmZXYXRjaD4=
		</wmb:bitstream>
	</wmb:bitstreamData>
</wmb:event>
		'''

		def leadingZeros = '''
<wmb:event xmlns:wmb="http://www.ibm.com/xmlns/prod/websphere/messagebroker/6.1.0/monitoring/event">
 <wmb:eventPointData>
  <wmb:eventData wmb:productVersion="10008" wmb:eventSchemaVersion="6.1.0.3" wmb:eventSourceAddress="SF_Handle_Exception.FAILURE.terminal.in">
   <wmb:eventIdentity wmb:eventName="FAILURE.InTerminal"/>
   <wmb:eventSequence wmb:creationTime="2017-08-21T09:33:32.008799Z" wmb:counter="5"/>
   <wmb:eventCorrelation wmb:localTransactionId="b0e4cc1f-1060-478a-afe7-65fc524f4527-6" wmb:parentTransactionId="" wmb:globalTransactionId="414d512049423130514d4752202020209d778d59203a6831"/>
  </wmb:eventData>
  <wmb:messageFlowData>
   <wmb:broker wmb:name="PETS_AT_HOME" wmb:UUID="47ea0eeb-dc5d-47c5-a565-0bb5ec322503"/>
   <wmb:executionGroup wmb:name="njamsTesting" wmb:UUID="64030c8c-4287-4fff-b510-2cd5beb8f155"/>
   <wmb:messageFlow wmb:uniqueFlowName="PETS_AT_HOME.njamsTesting.PetsAtHome.PaH_AdapterFlows.AF01_HTTP_Input" wmb:name="PaH_AdapterFlows.AF01_HTTP_Input" wmb:UUID="64c81170-503e-4a85-b575-58918891d944" wmb:threadId="2960"/>
   <wmb:node wmb:nodeLabel="SF_Handle_Exception.FAILURE" wmb:nodeType="ComIbmMQOutputNode" wmb:terminal="in"/>
  </wmb:messageFlowData>
 </wmb:eventPointData>
 <wmb:bitstreamData>
  <wmb:bitstream wmb:encoding="base64Binary">PEVYQ0VQVElPTj48RVhDRVBUSU9OX0RFVEFJTFM+PFRJTUVTVEFNUD4yMDE3LTA4LTIxIDEwOjMzOjMyLjAwNzExNDwvVElNRVNUQU1QPjxDT05TVU1FUl9JRD45OTk5PC9DT05TVU1FUl9JRD48U0VSVklDRV9JRD45OTk5PC9TRVJWSUNFX0lEPjxBREFQVEVSX1RZUEU+OTk5OTwvQURBUFRFUl9UWVBFPjxFUlJPUl9DT0RFPjUwMDQ8L0VSUk9SX0NPREU+PEVYQ0VQVElPTl9URVhUPiZsdDtFWENFUFRJT05fREVUQUlMUyZndDsmbHQ7SUlCX0VYQ0VQVElPTl9URVhUJmd0O0FuIFhNTCBwYXJzaW5nIGVycm9yIGhhcyBvY2N1cnJlZCB3aGlsZSBwYXJzaW5nIHRoZSBYTUwgZG9jdW1lbnQmbHQ7L0lJQl9FWENFUFRJT05fVEVYVCZndDsmbHQ7RVhDRVBUSU9OX0ZMT1cmZ3Q7UGFIX0FkYXB0ZXJGbG93cy5BRjAxX0hUVFBfSW5wdXQmbHQ7L0VYQ0VQVElPTl9GTE9XJmd0OyZsdDtFWENFUFRJT05fTk9ERSZndDtIVFRQIElucHV0Jmx0Oy9FWENFUFRJT05fTk9ERSZndDsmbHQ7RVJST1JfVEVYVCZndDsxNTUwJmx0Oy9FUlJPUl9URVhUJmd0OyZsdDtFUlJPUl9URVhUJmd0OzImbHQ7L0VSUk9SX1RFWFQmZ3Q7Jmx0O0VSUk9SX1RFWFQmZ3Q7MTEmbHQ7L0VSUk9SX1RFWFQmZ3Q7Jmx0O0VSUk9SX1RFWFQmZ3Q7NDImbHQ7L0VSUk9SX1RFWFQmZ3Q7Jmx0O0VSUk9SX1RFWFQmZ3Q7VGhlIG5hbWVzcGFjZSBwcmVmaXggJmFtcDtxdW90O2lpYiZhbXA7cXVvdDsgd2FzIG5vdCBkZWNsYXJlZC4mbHQ7L0VSUk9SX1RFWFQmZ3Q7Jmx0O0VSUk9SX1RFWFQmZ3Q7L1Jvb3QvWE1MTlNDL3todHRwOi8vc2NoZW1hcy54bWxzb2FwLm9yZy9zb2FwL2VudmVsb3BlL306RW52ZWxvcGUve2h0dHA6Ly9zY2hlbWFzLnhtbHNvYXAub3JnL3NvYXAvZW52ZWxvcGUvfTpCb2R5L3todHRwOi8vd3d3LnBldHNhdGhvbWUuY28udWsvbWVzc2FnZXMvQWRkcmVzc0xvb2t1cH06QWRkcmVzc0xvb2t1cFJlcXVlc3Qve2h0dHA6Ly93d3cucGV0c2F0aG9tZS5jby51ay9tZXNzYWdlcy9BZGRyZXNzTG9va3VwfTpNZXNzYWdlSGVhZGVyL3todHRwOi8vd3d3LnBldHNhdGhvbWUuY28udWsvdHlwZXMvTWVzc2FnZUhlYWRlcn06SGVhZGVyUm91dGluZyZsdDsvRVJST1JfVEVYVCZndDsmbHQ7L0VYQ0VQVElPTl9ERVRBSUxTJmd0OzwvRVhDRVBUSU9OX1RFWFQ+PC9FWENFUFRJT05fREVUQUlMUz48Q09ORklHVVJBVElPTj4mbHQ7SUlCQ09ORklHVVJBVElPTiZndDtDb25maWd1cmF0aW9uIERldGFpbHMgd2VyZSBub3QgYXZhaWxhYmxlIHdoZW4gdGhlIGVycm9yIG9jY3VyZWQmbHQ7L0lJQkNPTkZJR1VSQVRJT04mZ3Q7PC9DT05GSUdVUkFUSU9OPjxNRVNTQUdFPiZsdDtzb2FwZW52OkVudmVsb3BlIHhtbG5zOnNvYXBlbnY9JnF1b3Q7aHR0cDovL3NjaGVtYXMueG1sc29hcC5vcmcvc29hcC9lbnZlbG9wZS8mcXVvdDsgeG1sbnM6YWRkPSZxdW90O2h0dHA6Ly93d3cucGV0c2F0aG9tZS5jby51ay9tZXNzYWdlcy9BZGRyZXNzTG9va3VwJnF1b3Q7IHhtbG5zOm1lcz0mcXVvdDtodHRwOi8vd3d3LnBldHNhdGhvbWUuY28udWsvdHlwZXMvTWVzc2FnZUhlYWRlciZxdW90OyB4bWxuczpiYXNlPSZxdW90O2h0dHA6Ly93d3cucGV0c2F0aG9tZS5jby51ay90eXBlcy9CYXNlJnF1b3Q7IHhtbG5zOmV4Yz0mcXVvdDtodHRwOi8vd3d3LnBldHNhdGhvbWUuY28udWsvdHlwZXMvRXhjZXB0aW9uJnF1b3Q7IHhtbG5zOmFkZDE9JnF1b3Q7aHR0cDovL3d3dy5wZXRzYXRob21lLmNvLnVrL3R5cGVzL0FkZHJlc3MmcXVvdDsmZ3Q7Jmx0O3NvYXBlbnY6SGVhZGVyLyZndDsmbHQ7c29hcGVudjpCb2R5Jmd0OyZsdDthZGQ6QWRkcmVzc0xvb2t1cFJlcXVlc3QmZ3Q7Jmx0O2FkZDpNZXNzYWdlSGVhZGVyJmd0OyZsdDsvYWRkOk1lc3NhZ2VIZWFkZXImZ3Q7Jmx0Oy9hZGQ6QWRkcmVzc0xvb2t1cFJlcXVlc3QmZ3Q7Jmx0Oy9zb2FwZW52OkJvZHkmZ3Q7Jmx0Oy9zb2FwZW52OkVudmVsb3BlJmd0OzwvTUVTU0FHRT48L0VYQ0VQVElPTj4=</wmb:bitstream>
 </wmb:bitstreamData>
</wmb:event>
		'''
						
		MonitoringEvent roundedUp = new ParseMonitoringEvent().parse(roundUp)
		MonitoringEvent roundedDown = new ParseMonitoringEvent().parse(roundDown)		
// No rounding with LocalDateTime		assertEquals(roundedUp.creationTime.toString(), roundedDown.creationTime.toString())

		// If the next test works, then leading 0s in the creation date have been preserved
		MonitoringEvent result = new ParseMonitoringEvent().parse(leadingZeros)
		assertNotNull(result.creationTime)
		assertEquals(result.creationTime.toString(), "2017-08-21T09:33:32.008799")
	}
	
}
