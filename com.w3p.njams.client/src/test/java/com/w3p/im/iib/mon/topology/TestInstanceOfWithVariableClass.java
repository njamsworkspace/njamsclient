package com.w3p.im.iib.mon.topology;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.io.FileInputStream;
import java.lang.reflect.InvocationTargetException;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;
import java.util.Set;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.w3p.im.iib.mon.topology.model.Application;
import com.w3p.im.iib.mon.topology.model.IntegrationServer;
import com.w3p.im.iib.mon.topology.model.MessageFlow;

public class TestInstanceOfWithVariableClass {
	
//	@Mock
//	MessageFlowProxy mfp;
//	
//	@Rule public MockitoRule mockitoRule = MockitoJUnit.rule();
	
	private Map<Class<?>, Class<?>> proxyToNjams = new HashMap<Class<?>, Class<?>>();

	@Before
	public void setUp() throws Exception {
		Properties props = new Properties();
		props.load(new FileInputStream("files/test.properties"));
		Set<Object> keys = props.keySet();
		for (Object key : keys) {
			if ( ((String)key).startsWith("proxy#") ) {
				String proxyClass = ((String)key).split("#")[1];
				proxyToNjams.put(Class.forName(proxyClass), Class.forName((String) props.get(key)));
			}
		}		

	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void test() throws Exception {
//		String mfp = mock(String.class);
		Object result =  testInstanceOf(new Integer(3));
		assertTrue(result instanceof MessageFlow);
		assertEquals("My Name", ((MessageFlow)result).getName());
		result = testInstanceOf(new Double(3.1));
		assertTrue(result instanceof IntegrationServer);
		assertEquals("My Name", ((IntegrationServer)result).getName());
		result = testInstanceOf(new String("string"));
		assertTrue(result instanceof Application);
		assertEquals("My Name", ((Application)result).getName());
		result = testInstanceOf(new Boolean("True"));
		assertEquals("No matching class found for: Boolean", (String)result);
		
		
				
	}
	
	private Object testInstanceOf(Object o ) throws ClassNotFoundException, NoSuchMethodException, SecurityException, InstantiationException, IllegalAccessException, IllegalArgumentException, InvocationTargetException {
		Object result = null;
			if (proxyToNjams.containsKey(o.getClass())) {
				Class<?> topologyObject = Class.forName(proxyToNjams.get(o.getClass()).getName());
				result = topologyObject.asSubclass(topologyObject).getConstructor(String.class).newInstance("My Name");				
			} else {
				result = new String("No matching class found for: "+o.getClass().getSimpleName());
			}			
		return result;
	}

}
