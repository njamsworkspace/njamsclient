package com.w3p.im.iib.mon.jms.consumers;

import static com.w3p.im.iib.mon.client.constants.IClientConstants.UTF_8;

import static org.junit.Assert.*;

import java.nio.charset.Charset;

import org.apache.commons.lang3.StringUtils;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import jakarta.xml.bind.DatatypeConverter;

public class TestMonitorEventsConsumer {
	private static final Charset UTF8_CHARSET = Charset.forName(UTF_8);

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testConvertUTF2Ascii() {
		String result = decodeUTF8("313233546573740000000000000000000000000000000000");
		assertTrue(true);
	}
	
	private static String decodeUTF8(String utfInput) {
		String stripped = StringUtils.strip(utfInput, "0");
		byte[] bytes = DatatypeConverter.parseHexBinary(StringUtils.strip(utfInput, "0"));
		String result = new String(DatatypeConverter.parseHexBinary(StringUtils.strip(utfInput, "0")), UTF8_CHARSET); 
	    return result;
	}
	

}
