package com.w3p.im.iib.mon.client.utils.io.writers;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.LinkOption;
import java.nio.file.Path;
import java.nio.file.Paths;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.ClassRule;
import org.junit.Test;
import org.junit.rules.TemporaryFolder;

import com.w3p.im.iib.mon.client.utils.io.data.IWriteFileRequest;
import com.w3p.im.iib.mon.client.utils.io.data.Response;
import com.w3p.im.iib.mon.client.utils.io.data.WriteFileRequest;

public class TestFileWriter {
	
	@ClassRule
	public static TemporaryFolder tempOutputFolder = new TemporaryFolder();
	@ClassRule
	public static TemporaryFolder emptyOutputFolder = new TemporaryFolder();
	@ClassRule
	public static TemporaryFolder fileOutputFolder = new TemporaryFolder();


	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
		Path pathToBeDeleted = tempOutputFolder.getRoot().toPath().resolve(Paths.get("newFolder"));	
		if (!pathToBeDeleted.toFile().exists()) {
			return;
		}
		
//	    Files.walk(pathToBeDeleted)
//	      .sorted(Comparator.reverseOrder())
//	      .map(Path::toFile)
//	      .forEach(File::delete);
		
		File folderToBeDeleted = new File(pathToBeDeleted.toString());  
		File[] allContents = folderToBeDeleted.listFiles();
	    if (allContents != null) {
	        for (File file : allContents) {
	            file.delete();
	        }
	    }
	    folderToBeDeleted.delete();
	    
	    // Delete any files in temp folder
		File[] allFiles = tempOutputFolder.getRoot().listFiles();
	    if (allFiles != null) {
	        for (File file : allFiles) {
	            file.delete();
	        }
	    }
	}
	
	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	    // Delete any files in temp folder
		File[] allFiles = tempOutputFolder.getRoot().listFiles();
	    if (allFiles != null) {
	        for (File file : allFiles) {
	            file.delete();
	        }
	    }
		tempOutputFolder.getRoot().delete();
	}

	@Test
	public void testCanCreateFilesWriter() {
		FileWriter fw = new FileWriter();
		assertNotNull(fw);
	}

	@Test
	public void testCanInitFilesWriter() throws Exception {
		Response resp = new Response();
		File outFolder = tempOutputFolder.newFolder("newFolder");
		
		try (FileWriter fw = new FileWriter()) {			
			IWriteFileRequest req = new WriteFileRequest();
			req.setDestination(outFolder.getCanonicalPath())
			.setNamePattern("%s.txt");
			req.setCreateFolderIfNotExists(true)
			.setClearFolderIfNotEmpty(true)
			.setReplaceFilesIfExist(false); // this is the default
			fw.init(req, resp);
		} catch (Exception e) {
			throw e;
		}
		
		assertEquals(Response.INFO, resp.getStatus());
		assertTrue(resp.isSuccess());
//		String allMessages = resp.getAllMessages().get();
		assertFalse(resp.getAllMessages().isPresent());
	}

	@Test
	public void testCanCreateFolderIfNotExistsIsTrue() throws IOException {
		Response resp = new Response();
		File rootFolder = tempOutputFolder.getRoot();
		File outFolder = new File(rootFolder, "newFolder");
		
		try (FileWriter fw = new FileWriter()) {
			IWriteFileRequest req = new WriteFileRequest();
			req.setDestination(outFolder.getCanonicalPath());
			req.setNamePattern("%s.txt");
			req.setCreateFolderIfNotExists(true);
			req.setClearFolderIfNotEmpty(true);
			fw.init(req, resp);
		} catch (Exception e) {
			throw e;
		}
		
		assertEquals(Response.INFO, resp.getStatus());
		assertTrue(resp.isSuccess());
		assertTrue(resp.getAllMessages().isPresent());
		String allMessages = resp.getAllMessages().get();
		assertTrue(allMessages.startsWith("INFO: Created folder"));
		
	}
	
	@Test
	public void testCanHandleCreateFolderIfNotExistsIsFalse() throws IOException {
		Response resp = new Response();
		File rootFolder = tempOutputFolder.getRoot();
		File outFolder = new File(rootFolder, "newFolder");
		
		try (FileWriter fw = new FileWriter()) {
			IWriteFileRequest req = new WriteFileRequest();
			req.setDestination(outFolder.getCanonicalPath());
			req.setNamePattern("%s.txt");
			req.setCreateFolderIfNotExists(false);
			req.setClearFolderIfNotEmpty(true);
			fw.init(req, resp);
		} catch (Exception e) {
			throw e;
		}
		
		assertEquals(Response.ERROR, resp.getStatus());
		assertFalse(resp.isSuccess());
		assertTrue(resp.getAllMessages().isPresent());
		String errMsg = String.format("ERROR: Folder '%s' does not exist. Pease create it or auto-create it by using the 'createFolderIfNotExists' option.\r\n", outFolder.getCanonicalFile());
		String allMessages = resp.getAllMessages().get();
		assertEquals(allMessages, errMsg);
	}
	
	@Test
	public void testCanClearFolderIfNotEmptyIsTrue() throws IOException {
		Response resp = new Response();
		// Sometimes this file is not being deleted from the temp folder!!
		boolean exists = Files.exists(Paths.get(tempOutputFolder.getRoot().getCanonicalPath(), "test.txt"), LinkOption.NOFOLLOW_LINKS);
		if (!exists) {
			tempOutputFolder.newFile("test.txt");  // Populate the temp folder
		}
		
		try (FileWriter fw = new FileWriter()) {
			IWriteFileRequest req = new WriteFileRequest();
			req.setDestination(tempOutputFolder.getRoot().getCanonicalPath());
			req.setNamePattern("%s.txt");
			req.setCreateFolderIfNotExists(false);
			req.setClearFolderIfNotEmpty(true);
			fw.init(req, resp);
		} catch (Exception e) {
			throw e;
		}
		
		assertEquals(Response.INFO, resp.getStatus());
		assertTrue(resp.isSuccess());
		assertTrue(resp.getAllMessages().isPresent());
		String msg = String.format("INFO: Cleared existing files in target folder '%s'.\r\n", tempOutputFolder.getRoot().getCanonicalFile());
		String allMessages = resp.getAllMessages().get();
		assertEquals(allMessages, msg);
		assertTrue(tempOutputFolder.getRoot().listFiles().length == 0); // folder should be empty
	}
	
	@Test
	public void testCanHandleClearFolderIfNotEmptyIsFalse() throws IOException {
		Response resp = new Response();
		// Sometimes this file is not being deleted from the temp folder!!
		boolean exists = Files.exists(Paths.get(tempOutputFolder.getRoot().getCanonicalPath(), "test.txt"), LinkOption.NOFOLLOW_LINKS);
		if (!exists) {
			tempOutputFolder.newFile("test.txt");  // Populate the temp folder
		}
		
		try (FileWriter fw = new FileWriter()) {
			IWriteFileRequest req = new WriteFileRequest();
			req.setDestination(tempOutputFolder.getRoot().getCanonicalPath());
			req.setNamePattern("%s.txt");
			req.setCreateFolderIfNotExists(false);
			req.setClearFolderIfNotEmpty(false);
			fw.init(req, resp);
		} catch (Exception e) {
			throw e;
		}
		
		assertEquals(Response.INFO, resp.getStatus());
		assertTrue(resp.isSuccess());
		assertTrue(resp.getAllMessages().isPresent());
		String msg = String.format("INFO: Folder '%s' is not empty - new files will be added.\r\n", tempOutputFolder.getRoot().getCanonicalFile());
		String allMessages = resp.getAllMessages().get();
		assertEquals(allMessages, msg);
		assertFalse(tempOutputFolder.getRoot().listFiles().length == 0); // folder should not be empty
	}
	
	@Test
	public void testCanWriteFileToEmptyFolder() throws IOException {
		Response resp = new Response();
		File rootFolder = tempOutputFolder.getRoot();
		File outFolder = new File(rootFolder, "newFolder");
		String message = "This is a test";	
		
		try (FileWriter fw = new FileWriter()) {
			IWriteFileRequest req = new WriteFileRequest();
			req.setDestination(outFolder.getCanonicalPath());
			req.setNamePattern("%s.txt");
			req.setCreateFolderIfNotExists(true);
			req.setClearFolderIfNotEmpty(true);
			fw.init(req, resp);
			fw.write("test", message, resp);				
		} catch (Exception e) {
			throw e;
		}
		
		assertEquals(Response.INFO, resp.getStatus());
		assertTrue(resp.isSuccess());
		assertTrue(resp.getAllMessages().isPresent());
		String msgInit = String.format("INFO: Created folder '%s'.", outFolder.getCanonicalFile());
		String msgWrite = String.format("INFO: File '%s' has been created.", outFolder.getCanonicalFile()+"\\test.txt");
		String allMessages = resp.getAllMessages().get();
		assertTrue(allMessages.contains(msgInit));		
		assertTrue(allMessages.contains(msgWrite));
		assertTrue(outFolder.listFiles().length == 1);
	}
	
	@Test
	public void testCanWriteFileToNonEmptyFolder() throws IOException {
		Response resp = new Response();
		File outFolder = tempOutputFolder.getRoot();
		// Sometimes this file is not being deleted from the temp folder!!
		boolean exists = Files.exists(Paths.get(tempOutputFolder.getRoot().getCanonicalPath(), "firstTest.txt"), LinkOption.NOFOLLOW_LINKS);
		if (!exists) {
			tempOutputFolder.newFile("firstTest.txt");  // Populate the temp folder
		}
		
		String message = "This is a test";	
		
		try (FileWriter fw = new FileWriter()) {
			IWriteFileRequest req = new WriteFileRequest();
			req.setDestination(outFolder.getCanonicalPath());
			req.setNamePattern("%s.txt");
			req.setCreateFolderIfNotExists(true);
			req.setClearFolderIfNotEmpty(false);
			fw.init(req, resp);
			fw.write("test", message, resp);				
		} catch (Exception e) {
			throw e;
		}
		
		assertEquals(Response.INFO, resp.getStatus());
		assertTrue(resp.isSuccess());
		assertTrue(resp.getAllMessages().isPresent());
		String msgInit = String.format("INFO: Folder '%s' is not empty - new files will be added.", outFolder.getCanonicalFile());
		String msgWrite = String.format("INFO: File '%s' has been created.", outFolder.getCanonicalFile()+"\\test.txt");
		String allMessages = resp.getAllMessages().get();
		assertTrue(allMessages.contains(msgInit));
		assertTrue(allMessages.contains(msgWrite));
		assertTrue(outFolder.listFiles().length == 2);
	}
	
		@Test
		public void testCanClearFolderAndWriteFile() throws IOException {
			Response resp = new Response();
			File outFolder = tempOutputFolder.getRoot();
			// Sometimes this file is not being deleted from the temp folder!!
			boolean exists = Files.exists(Paths.get(tempOutputFolder.getRoot().getCanonicalPath(), "firstTest.txt"), LinkOption.NOFOLLOW_LINKS);
			if (!exists) {
				tempOutputFolder.newFile("firstTest.txt");  // Populate the temp folder
			}

			String message = "This is a test";	
			
			try (FileWriter fw = new FileWriter()) {
				IWriteFileRequest req = new WriteFileRequest();
				req.setDestination(outFolder.getCanonicalPath());
				req.setNamePattern("%s.txt");
				req.setCreateFolderIfNotExists(true);
				req.setClearFolderIfNotEmpty(true);
				fw.init(req, resp);
				fw.write("test", message, resp);				
			} catch (Exception e) {
				throw e;
			}
			
			assertEquals(Response.INFO, resp.getStatus());
			assertTrue(resp.isSuccess());
			assertTrue(resp.getAllMessages().isPresent());
			String msgInit = String.format("INFO: Cleared existing files in target folder '%s'.", outFolder.getCanonicalFile());
			String msgWrite = String.format("INFO: File '%s' has been created.", outFolder.getCanonicalFile()+"\\test.txt");
			String allMessages = resp.getAllMessages().get();
			assertTrue(allMessages.contains(msgInit));
			assertTrue(allMessages.contains(msgWrite));
			assertTrue(outFolder.listFiles().length == 1);
		}

	@Test
	public void testCanReplaceFileInFolder() throws IOException {
		Response resp = new Response();
		// Sometimes this file is not being deleted from the temp folder!!
		boolean exists = Files.exists(Paths.get(tempOutputFolder.getRoot().getCanonicalPath(), "test.txt"), LinkOption.NOFOLLOW_LINKS);
		if (!exists) {
			tempOutputFolder.newFile("test.txt");  // Populate the temp folder
		}
		String message = "This has replaced the contents of the test file.";	
		
		try (FileWriter fw = new FileWriter()) {
			IWriteFileRequest req = new WriteFileRequest();
			req.setDestination(tempOutputFolder.getRoot().getCanonicalPath());
			req.setNamePattern("%s.txt");
			req.setCreateFolderIfNotExists(true);
			req.setClearFolderIfNotEmpty(false);
			req.setReplaceFilesIfExist(true); // replace the content
			fw.init(req, resp);
			fw.write("test", message, resp);				
		} catch (Exception e) {
			throw e;
		}
		
		assertEquals(Response.INFO, resp.getStatus());
		assertTrue(resp.isSuccess());
		assertTrue(resp.getAllMessages().isPresent());
		String msgInit = String.format("INFO: Folder '%s' is not empty - new files will be added.", tempOutputFolder.getRoot().getCanonicalPath());
		String msgWrite = String.format("INFO: Content of file '%s' has been replaced.", tempOutputFolder.getRoot().getCanonicalPath()+"\\test.txt");
		String allMessages = resp.getAllMessages().get();
		assertTrue(allMessages.contains(msgInit));
		assertTrue(allMessages.contains(msgWrite));
		assertTrue(tempOutputFolder.getRoot().listFiles().length == 1);
	}

	@Test
	public void testCanHandleFileInFolderNotReplaced() throws IOException {
		Response resp = new Response();
		// Sometimes this file is not being deleted from the temp folder!!
		boolean exists = Files.exists(Paths.get(tempOutputFolder.getRoot().getCanonicalPath(), "test.txt"), LinkOption.NOFOLLOW_LINKS);
		if (!exists) {
			tempOutputFolder.newFile("test.txt");  // Populate the temp folder
		}
		String message = "This has replaced the contents of the test file.";	
		
		try (FileWriter fw = new FileWriter()) {
			IWriteFileRequest req = new WriteFileRequest();
			req.setDestination(tempOutputFolder.getRoot().getCanonicalPath());
			req.setNamePattern("%s.txt");
			req.setCreateFolderIfNotExists(true);
			req.setClearFolderIfNotEmpty(false);
			req.setReplaceFilesIfExist(false); // do not replace the content
			fw.init(req, resp);
			fw.write("test", message, resp);				
		} catch (Exception e) {
			throw e;
		}
		
		assertEquals(Response.WARN, resp.getStatus());
		assertFalse(resp.isSuccess());
		assertTrue(resp.getAllMessages().isPresent());
		String msgInit = String.format("INFO: Folder '%s' is not empty - new files will be added.", tempOutputFolder.getRoot().getCanonicalPath());
		String msgWrite = String.format("WARN: The file '%s' exists. It has not been changed because the option 'replace file if exists' was not set to 'true'.",
				tempOutputFolder.getRoot().getCanonicalPath()+"\\test.txt");
		String allMessages = resp.getAllMessages().get();
		assertTrue(allMessages.contains(msgInit));
		assertTrue(allMessages.contains(msgWrite));
		
		assertTrue(tempOutputFolder.getRoot().listFiles().length == 1);
	}
}
