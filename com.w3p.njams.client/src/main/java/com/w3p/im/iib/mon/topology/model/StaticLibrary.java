package com.w3p.im.iib.mon.topology.model;

import java.util.List;

import com.w3p.api.config.proxy.ConfigManagerProxyPropertyNotInitializedException;
import com.w3p.api.config.proxy.StaticLibraryProxy;

public class StaticLibrary extends AbstractTopology {
	
	private final StaticLibraryProxy slp;	
	

	public StaticLibrary(StaticLibraryProxy slp) {
		this.slp = slp;
		try {
			super.name = slp.getName();
		} catch (ConfigManagerProxyPropertyNotInitializedException e) {
			super.name = null;
			e.printStackTrace();
		}
	}

	public StaticLibraryProxy getStaticLibraryProxy() {
		return slp;
	}
	
	
	@Override
	public <T extends AbstractTopology> void addTopologyItem(T topologyItem) {
		if (topologyItem instanceof MessageFlow) {
			messageFlows.add((MessageFlow) topologyItem);
		} else if (topologyItem instanceof SubFlow) {
			subFlows.add((SubFlow) topologyItem);
		}
	}
	
	/* (non-Javadoc)
	 * @see com.w3p.im.iib.mon.topology.model.AbstractTopology#isRunning()
	 * @return = true ->  // isRunning() is not defined for a StaticLibrary
	 */
	@Override
	public boolean isRunning() {
		return true;
	}

	public List<MessageFlow> getMessageFlows() {
		return messageFlows;
	}
	protected void setMessageFlows(List<MessageFlow> messageFlows) {
		this.messageFlows.clear();
		this.messageFlows.addAll(messageFlows);
	}
	
	@Override
	public String toString() {
		return "Static Library [name=" + name + "]";
	}

}
