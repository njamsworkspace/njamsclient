package com.w3p.im.iib.mon.client.config.data;

import java.io.File;

import org.apache.commons.lang3.StringUtils;

public class JmsEvents {
	
	private static final String CURRENT_DIRECTORY = "cd";
	private String clientHome;
	private String  providerURL;
	private String  initialContextFactory;
	private String  connectionFactory;
	private String jmsClientId;
	private User jmsMqUser;


	public void setClientHome(String clientHome) {
		this.clientHome = clientHome;
	}

	public String getProviderURL() {
        // Look for a single EnvVar and resolve
        if (StringUtils.countMatches(providerURL, "%") == 2) {
        	String envVar = StringUtils.substringBetween(providerURL, "%");
        	String sysVar;
        	if (envVar.toLowerCase().equals(CURRENT_DIRECTORY)) {
        		// Current working dir
        		sysVar = clientHome;
        	} else {
        		sysVar = System.getenv(envVar);
        	}
        	sysVar = StringUtils.strip(sysVar, File.pathSeparator);
        	providerURL = StringUtils.replace(providerURL, "%"+envVar+"%", sysVar);            	
        } 
		return providerURL;
	}

	public void setProviderURL(String providerURL) {
		this.providerURL = providerURL;
	}

	public String getInitialContextFactory() {
		return initialContextFactory;
	}

	public void setInitialContextFactory(String initialContextFactory) {
		this.initialContextFactory = initialContextFactory;
	}

	public String getConnectionFactory() {
		return connectionFactory;
	}

	public void setConnectionFactory(String connectionFactory) {
		this.connectionFactory = connectionFactory;
	}

	public String getJmsClientId() {
		return jmsClientId;
	}

	public void setJmsClientId(String jmsClientId) {
		this.jmsClientId = jmsClientId;
	}

	public User getJmsMqUser() {
		return jmsMqUser;
	}

	public void setJmsMqUser(User jmsMqUser) {
		this.jmsMqUser = jmsMqUser;
	}

	@Override
	public String toString() {
		return "JmsEvents [providerURL=" + providerURL + ", connectionFactory=" + connectionFactory + ", jmsClientId="
				+ jmsClientId + "]";
	}
}
