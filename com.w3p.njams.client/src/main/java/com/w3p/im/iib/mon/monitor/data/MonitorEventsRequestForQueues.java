package com.w3p.im.iib.mon.monitor.data;

import static com.w3p.im.iib.mon.client.constants.IClientConstants.JMS_TOPIC;
import static com.w3p.im.iib.mon.client.constants.IClientConstants.MQTT_TOPIC;
import static com.w3p.im.iib.mon.client.constants.IClientConstants.QUEUE;

import java.util.Map;

import com.im.njams.sdk.Njams;
import com.w3p.im.iib.mon.jms.producers.FlowToProcessModelCache;
import com.w3p.im.iib.mon.jms.service.MqJmsConnectionProperties;
import com.w3p.im.iib.mon.mqtt.service.MqttConnectionProperties;

public class MonitorEventsRequestForQueues extends MonitorEventsRequest {

//	protected final Njams njamsClient;
	private final Map<String, Njams> njamsClients; // k=intSvrName, v=njamsClient for intSvr
	private Map<String, FlowToProcessModelCache> flowToProcessModelCaches; // k=intSvrName, v=FlowToProcessModelCache for intSvr
	
//	protected final String integrationNodeName;
//	protected final String integrationServerName;
//	private String flowName;
	 
//	private MqJmsConnectionProperties mqJmsConnectionProperties;
//	private MqttConnectionProperties mqttConnectionProperties;
//	private String eventsSource;
	private String iibEventsProblemsQueue;
//	private int consumerThreads;
	
	
//	private boolean tracingEnabled;
	
	
	public MonitorEventsRequestForQueues(Map<String, Njams> njamsClients, String integrationNodeName) { //Njams njamsClient, String integrationNodeName, String integrationServerName) {
		super(integrationNodeName);
		this.njamsClients = njamsClients; // = njamsClient;
	}
	
	public Map<String, Njams> getNjamsClients() {
		return njamsClients;
	}

	public Map<String, FlowToProcessModelCache> getFlowToProcessModelCaches() {
		return flowToProcessModelCaches;
	}

	public MonitorEventsRequestForQueues setFlowToProcessModelCaches(Map<String, FlowToProcessModelCache> flowToProcessModelCaches) {
		this.flowToProcessModelCaches = flowToProcessModelCaches;
		return this;
	}
	
//	public String getIntegrationNodeName() {
//		return integrationNodeName;
//	}

//	public String getIntegrationServerName() {
//		return integrationServerName;
//	}

//	public String getFlowName() {
//		return flowName;
//	}

//	public MonitorEventsRequest setFlowName(String flowName) {
//		this.flowName = flowName;
//		return this;
//	}
	
//	public MqJmsConnectionProperties getMqJmsConnectionProperties() {
//		return mqJmsConnectionProperties;
//	}

//	public MonitorEventsRequest setMqJmsConnectionProperties(MqJmsConnectionProperties mqJmsConnectionProperties) {
//		this.mqJmsConnectionProperties = mqJmsConnectionProperties;
//		return this;
//	}

//	public MqttConnectionProperties getMqttConnectionProperties() {
//		return mqttConnectionProperties;
//	}

//	public MonitorEventsRequest setMqttConnectionProperties(MqttConnectionProperties mqttConnectionProperties) {
//		this.mqttConnectionProperties = mqttConnectionProperties;
//		return this;
//	}

//	public String getEventsSource() {
//		return eventsSource;
//	}
//
//	public MonitorEventsRequest setEventsSource(String eventsSource) {
//		this.eventsSource = eventsSource;
//		return this;
//	}

//	public boolean isUseMQTT() {
//		return eventsSource.equals(MQTT_TOPIC);
//	}
//
//	public boolean isUseTopic() {
//		return eventsSource.equals(JMS_TOPIC);
//	}
//
//	public boolean isUseQueue() {
//		return eventsSource.startsWith(QUEUE);
//	}
//
//	public boolean isTracingEnabled() {
//		return tracingEnabled;
//	}

//	public MonitorEventsRequest setTracingEnabled(boolean tracingEnabled) {
//		this.tracingEnabled = tracingEnabled;
//		return this;
//	}

	public String getIibEventsProblemsQueue() {
		return iibEventsProblemsQueue;
	}

	public MonitorEventsRequestForQueues setIibEventsProblemsQueue(String iibEventsProblemsQueue) {
		this.iibEventsProblemsQueue = iibEventsProblemsQueue;
		return this;
	}

//	public int getConsumerThreads() {
//		return consumerThreads;
//	}
//
//	public MonitorEventsRequestForQueues setConsumerThreads(int consumerThreads) {
//		this.consumerThreads = consumerThreads;
//		return this;
//	}

	@Override
	public String toString() {
		return "MonitorEventsRequestForQueues [integrationNodeName=" + integrationNodeName + ", eventsSource=" + eventsSource +"]";
	}

}
