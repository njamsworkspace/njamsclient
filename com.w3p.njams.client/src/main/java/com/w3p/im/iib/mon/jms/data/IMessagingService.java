package com.w3p.im.iib.mon.jms.data;

import com.w3p.im.iib.mon.exceptions.internal.MessagingException;

/**
 * @author John Ormerod
 *
 */
public interface IMessagingService {

	public void createConnection(String dest) throws MessagingException;
	
	public void close();
}
