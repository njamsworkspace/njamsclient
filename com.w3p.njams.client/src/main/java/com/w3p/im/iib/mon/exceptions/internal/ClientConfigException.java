package com.w3p.im.iib.mon.exceptions.internal;

import com.w3p.im.iib.mon.exceptions.InternalException;

public class ClientConfigException extends InternalException {


	private static final long serialVersionUID = 1L;

	public ClientConfigException() {
		super();
	}

	public ClientConfigException(String msg, Object... values) {
		super(msg, values);
	}
}
