package com.w3p.im.iib.mon.client.config.data;

public class JmsServer {
	
	private User jmsUser;
	private Jndi jndi;
	private String destination;
	private String destinationCommands;
	private String providerURL;
	private String initialContextFactory;
	private String connectionFactory;
	private boolean  useSSL;
		
	
	public User getJmsUser() {
		return jmsUser;
	}
	public void setJmsUser(User jmsUser) {
		this.jmsUser = jmsUser;
	}
	public Jndi getJndi() {
		return jndi;
	}
	public void setJndi(Jndi jndi) {
		this.jndi = jndi;
	}
	public String getDestination() {
		return destination;
	}
	public void setDestination(String destination) {
		this.destination = destination;
	}
	public String getDestinationCommands() {
		return destinationCommands;
	}
	public void setDestinationCommands(String destinationCommands) {
		this.destinationCommands = destinationCommands;
	}
	public String getProviderURL() {
		return providerURL;
	}
	public void setProviderURL(String providerURL) {
		this.providerURL = providerURL;
	}
	public String getInitialContextFactory() {
		return initialContextFactory;
	}
	public void setInitialContextFactory(String providerContextFactory) {
		this.initialContextFactory = providerContextFactory;
	}
	public String getConnectionFactory() {
		return connectionFactory;
	}
	public void setConnectionFactory(String connectionFactory) {
		this.connectionFactory = connectionFactory;
	}
	public boolean isUseSSL() {
		return useSSL;
	}
	public void setUseSSL(boolean useSSL) {
		this.useSSL = useSSL;
	}

	@Override
	public String toString() {
		return "JmsServer [destination=" + destination + "]";
	}
}
