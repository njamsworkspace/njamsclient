package com.w3p.im.iib.mon.client.utils.serialize;

import static com.w3p.im.iib.mon.client.constants.IClientConstants.*;
import java.util.Collection;

import com.im.njams.sdk.Njams;
import com.im.njams.sdk.common.IdUtil;
import com.im.njams.sdk.model.ActivityModel;
import com.im.njams.sdk.model.GroupModel;
import com.im.njams.sdk.model.ProcessModel;
import com.im.njams.sdk.model.SubProcessActivityModel;
import com.im.njams.sdk.model.TransitionModel;
import com.w3p.im.iib.mon.client.model.data.ActivityModelData;
import com.w3p.im.iib.mon.client.model.data.GroupModelData;
import com.w3p.im.iib.mon.client.model.data.ProcessModelData;
import com.w3p.im.iib.mon.client.model.data.SubProcessActivityModelData;
import com.w3p.im.iib.mon.client.model.data.TransitionModelData;

public class ProcessModelSerializer extends AbstractSerializer {

	private final String outputFolder;

	public ProcessModelSerializer(String outputFolder) {
		this.outputFolder = outputFolder;
	}
	
	public void exportAllProcessModels(Njams njamsClient) {
		Collection<ProcessModel> pms = njamsClient.getProcessModels();
		for (ProcessModel pm : pms) {
			try {
				ProcessModelData pmd = serialise(pm);
				write(pmd, outputFolder,pm.getName()+".pmd");
			} catch (Exception e) {
				e.printStackTrace(); // TODO dort this out
			}
		}
	}

	private ProcessModelData serialise(ProcessModel pm) {
		ProcessModelData pmd = createProcessModelData(pm);
		return pmd;
	}

	private ProcessModelData createProcessModelData(ProcessModel pm) {
		ProcessModelData pmd = new ProcessModelData(pm.getPath());
		pmd.setStarter(pm.isStarter());
		pmd.setSvg(pm.getSerializableProcessModel().getSvg());
		pmd.setProperty(INCLUDE_SCHEMA_NAME, pm.getProperty(INCLUDE_SCHEMA_NAME) == null ? false : pm.getProperty(INCLUDE_SCHEMA_NAME));
		pmd.setProperty(LABEL_NODE_STARTS_GROUP, pm.getProperty(LABEL_NODE_STARTS_GROUP) == null ? false : pm.getProperty(LABEL_NODE_STARTS_GROUP));
		pmd.setProperty(INCLUDE_SERVER_NAME, pm.getProperty(INCLUDE_SERVER_NAME) == null ? false : pm.getProperty(INCLUDE_SERVER_NAME));
		
		pm.getActivityModels().forEach(am -> {
			if (SubProcessActivityModel.class.isAssignableFrom(am.getClass())) {
				pmd.addActivityModel(createSubProcessActivityModelData(pmd, (SubProcessActivityModel)am));
			} else if (GroupModel.class.isAssignableFrom(am.getClass())) {
				pmd.addActivityModel(createGroupModelData(pmd, (GroupModel)am));
			} else {
				pmd.addActivityModel(createActivityModelData(pmd, am));
			}
		});

		pm.getActivityModels().forEach(am -> {
			// create the incoming outgoing for all AMDs driven by am.getId()
			ActivityModelData amd = pmd.getActivityModel(am.getId());
			am.getPredecessors().forEach(p -> 
			amd.addIncomingTransition(createTransitionModelData(pm, pmd, p, am))); 
			am.getSuccessors().forEach(s -> 
			amd.addOutgoingTransition(createTransitionModelData(pm, pmd, am, s)));
		});

		return pmd;}

	private ActivityModelData createActivityModelData(ProcessModelData pmd, ActivityModel am) {
		ActivityModelData amd = new ActivityModelData(pmd, am.getId(), am.getName(), am.getType());
		if (am.getParent() != null) {
			String parentId = am.getParent().getId();
			GroupModelData gmd = (GroupModelData) pmd.getActivityModel(parentId);
			amd.setParent(gmd);
		}
		amd.setConfig(am.getConfig());
		amd.setMapping(am.getMapping());
		amd.setStarter(am.isStarter());
		amd.setX(am.getX());
		amd.setY(am.getY());

		return amd;
	}

	private SubProcessActivityModelData createSubProcessActivityModelData(ProcessModelData pmd, SubProcessActivityModel spam) {
		SubProcessActivityModelData spamd = pmd.createSubProcessModelData(spam.getId(), spam.getName(), spam.getType());
		// spamd.setSubProcess(subprocess); // Can't be set by the topology producer
		spamd.setSubProcessName(spam.getSubProcessName());
		spamd.setSubProcessPath(spam.getSubProcessPath().toString());
		spamd.setConfig(spam.getConfig());
		spamd.setMapping(spam.getMapping());
		spamd.setStarter(spam.isStarter());
		spamd.setX(spam.getX());
		spamd.setY(spam.getY());

		return spamd;
	}

	private GroupModelData createGroupModelData(ProcessModelData pmd, GroupModel gm) {
		GroupModelData gmd = new GroupModelData(pmd, gm.getId(), gm.getName(), gm.getType());
		gmd.setConfig(gm.getConfig());
		gmd.setHeight(gm.getHeight());
		gmd.setMapping(gm.getMapping());
		gmd.setStarter(gm.isStarter());
		gmd.setWidth(gm.getWidth());
		gmd.setX(gm.getX());
		gmd.setY(gm.getY());

		return gmd;
	}

	private TransitionModelData createTransitionModelData(ProcessModel pm, ProcessModelData pmd, ActivityModel from,
			ActivityModel to) {
		String fromActivityModelId = from.getId();
		String toActivityModelId = to.getId();
		String transitionModelId = IdUtil.getTransitionModelId(fromActivityModelId, toActivityModelId);

		TransitionModelData tmd = pmd.getTransitionModel(transitionModelId);
		if (tmd == null) {
			TransitionModel tm = pm.getTransition(transitionModelId);
			tmd = new TransitionModelData(pmd, transitionModelId, transitionModelId);
			// this from the PMD?
			tmd.setFromActivity(pmd.getActivityModel(fromActivityModelId));
			tmd.setToActivity(pmd.getActivityModel(toActivityModelId));
			tmd.setConfig(tm.getConfig());
			if (tm.getParent() != null) {
				String parentId = tm.getParent().getId();
				GroupModelData gmd = (GroupModelData) pmd.getActivityModel(parentId);
				tmd.setParent(gmd);
			}
			pmd.addTransition(tmd);
		}
		return tmd;
	}

}
