package com.w3p.im.iib.mon.client.config.data;

public class RestApi  extends DeployableObject { //implements IRunnableObject { // ConfigObject {

	public RestApi(String name, boolean includeSchemaName) {
		super(name, includeSchemaName);
	}

	@Override
	public String toString() {
		return "RestApi [name=" + getName() + "]";
	}
}
