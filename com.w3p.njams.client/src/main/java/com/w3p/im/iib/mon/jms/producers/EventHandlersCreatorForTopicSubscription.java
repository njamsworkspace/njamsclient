package com.w3p.im.iib.mon.jms.producers;

import static com.w3p.im.iib.mon.client.constants.IClientConstants.EVENT_TOPIC_PATTERN;
import static com.w3p.im.iib.mon.client.constants.IClientConstants.EVENT_TOPIC_PATTERN_MQTT;

import java.text.MessageFormat;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.w3p.im.iib.mon.topology.model.Service;
import com.w3p.im.iib.mon.event.processors.MonitorEventMessagesHandler;
import com.w3p.im.iib.mon.exceptions.internal.CreateMonitoringEventHandlerException;
import com.w3p.im.iib.mon.monitor.data.EventHandlingForTopicRequest;
import com.w3p.im.iib.mon.monitor.data.EventHandlingRequest;
import com.w3p.im.iib.mon.monitor.data.MonitorEventsRequestForTopics;
import com.w3p.im.iib.mon.topology.model.Application;
import com.w3p.im.iib.mon.topology.model.IntegrationServer;
import com.w3p.im.iib.mon.topology.model.MessageFlow;
import com.w3p.im.iib.mon.topology.model.RestApi;
import com.w3p.im.iib.mon.topology.model.SharedLibrary;
import com.w3p.im.iib.mon.topology.model.StaticLibrary;
import com.w3p.im.iib.mon.topology.model.TopologyObject;

public class EventHandlersCreatorForTopicSubscription extends EventHandlersCreator {	
	private static final Logger logger = LoggerFactory.getLogger(EventHandlersCreatorForTopicSubscription.class);
	
	private final IntegrationServer is;

	
	public EventHandlersCreatorForTopicSubscription(EventHandlingForTopicRequest topicRequest) {
		super((EventHandlingRequest)topicRequest);
		TopologyObject topology = topicRequest.getTopologyObject();
		IntegrationServer is = topology.getIntegrationServer(topicRequest.getIntSvrName());		
		this.is = is;
	}

	@Override
	public void createEventsHandlers() throws CreateMonitoringEventHandlerException {
		logger.trace("#createEventHandlers: entry");

		// When getting events from  topic subscriptions
		for (Application app : is.getApplications()) {
			for (MessageFlow mf : app.getMessageFlows()) {
				MonitorEventsRequestForTopics mer = createMonitorEventsRequest(is, app.getName(), mf, request);
				createEventMessagesHandler(mer);
			}
		}
		for (RestApi api : is.getRestApis()) {
			for (MessageFlow mf : api.getMessageFlows()) {
				MonitorEventsRequestForTopics mer = createMonitorEventsRequest(is, api.getName(), mf, request);
				createEventMessagesHandler(mer);
			}
		}
		for (Service service : is.getServices()) {
			for (MessageFlow mf : service.getMessageFlows()) {
				MonitorEventsRequestForTopics mer = createMonitorEventsRequest(is, service.getName(), mf, request);
				createEventMessagesHandler(mer);
			}
		}
/*	Libraries only produce monitoring events in the context of the flow that is using them	
		for (SharedLibrary lib : is.getSharedLibraries()) {
			for (MessageFlow mf : lib.getMessageFlows()) {
				MonitorEventsRequestForTopics mer = createMonitorEventsRequest(is, lib.getName(), mf, request);
				createEventMessagesHandler(mer);
			}
		}
		for (StaticLibrary lib : is.getStaticLibraries()) {
			for (MessageFlow mf : lib.getMessageFlows()) {
				MonitorEventsRequestForTopics mer = createMonitorEventsRequest(is, lib.getName(), mf, request);
				createEventMessagesHandler(mer);
			}
		}
*/		
		logger.info("\n\n*+*+*+*\n   The nJAMS Client 4 IIB has started and will process monitoring event messages published to MQ or MQTT topics from Int Server '{}'.\n*+*+*+*\n", is.getName());
		logger.trace("#createEventHandlers: exit");
	}

	private MonitorEventsRequestForTopics createMonitorEventsRequest(IntegrationServer is, String parentName, MessageFlow mf, EventHandlingRequest request) throws CreateMonitoringEventHandlerException {
		EventHandlingForTopicRequest topicRequest = (EventHandlingForTopicRequest)request;
		
		MonitorEventsRequestForTopics mer = new MonitorEventsRequestForTopics(topicRequest.getNjamsClient(),
				topicRequest.getIntNodeName(), topicRequest.getIntSvrName(), parentName); // parentName added for ACE
		mer.setMqttConnectionProperties(topicRequest.getMqttConnectionProperties())		
		.setFlowToProcessModelCache(topicRequest.getFlowToProcessModelCache())
		.setEventsSource(topicRequest.getEventsSource())
		.setMqJmsConnectionProperties(topicRequest.getMqJmsConnectionProperties())
		.setConsumerThreads(topicRequest.getConsumerThreads())
		.setTracingEnabled(false) // FIXME find out if ever used and where to set it from - IS IT NJAMS SDK4?
		.setFlowName(mf.getNameWithSchema());  // Have to subscribe using schema+name TODO
		
		return mer;
	}
	
	private void createEventMessagesHandler(MonitorEventsRequestForTopics mer) throws CreateMonitoringEventHandlerException {
		MonitorEventMessagesHandler monitorEventMessagesHandler = new MonitorEventMessagesHandler(mer);
		String topicString = formatTopicString(mer);
		monitorEventMessagesHandler.handleMessagesForEventsSource(topicString);
	}

	private String formatTopicString(MonitorEventsRequestForTopics mer) {
		String topicString = null;
		if (mer.isUseMQTT()) {
			topicString = MessageFormat.format(EVENT_TOPIC_PATTERN_MQTT, mer.getIntegrationNodeName(), mer.getIntegrationServerName(), mer.getFlowName());
		} else if (mer.isUseTopic()) { // $SYS/Broker/{0}/Monitoring/{1}/{2}/{3} FIXME need to handle IIB and ACE - *NOTE* ACE is publishng events in IIB format
			topicString = MessageFormat.format(EVENT_TOPIC_PATTERN, mer.getIntegrationNodeName(), mer.getIntegrationServerName(), mer.getFlowName()); // , mer.getParentName()
		}		
		logger.info("#createEventMessagesHandler(): Events source = '{}'.", topicString);
		return topicString;
	}
}
