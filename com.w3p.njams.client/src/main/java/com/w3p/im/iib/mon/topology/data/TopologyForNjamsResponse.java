package com.w3p.im.iib.mon.topology.data;

import com.w3p.im.iib.mon.jms.producers.FlowToProcessModelCache;

public class TopologyForNjamsResponse {	
	
	private boolean success = true;
	private String message;
	private FlowToProcessModelCache flowToProcessModelCache;
	

	public String getMessage() {
		if (message == null) {
			message = "";
		}
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public boolean isSuccess() {
		return success;
	}

	public void setSuccess(boolean success) {
		this.success = success;
	}

	public FlowToProcessModelCache getFlowToProcessModelCache() {
		return flowToProcessModelCache;
	}

	public void setFlowToProcessModelCache(FlowToProcessModelCache flowToProcessModelCache) {
		this.flowToProcessModelCache = flowToProcessModelCache;
	}
}
