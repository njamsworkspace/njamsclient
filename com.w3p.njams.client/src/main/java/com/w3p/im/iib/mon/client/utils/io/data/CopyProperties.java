package com.w3p.im.iib.mon.client.utils.io.data;

import java.nio.file.Path;

public class CopyProperties {
	
	protected Path filePath;
	protected String msgId;
	protected int hashcodeInputFile;
	protected int hashCodeFromMessage;
	
	public CopyProperties(Path filePath) {
		this.filePath = filePath;
	}

	public Path getFilePath() {
		return filePath;
	}

	public String getMsgId() {
		return msgId;
	}

	public void setMsgId(String msgId) {
		this.msgId = msgId;
	}

	public int getHashcodeInputFile() {
		return hashcodeInputFile;
	}

	public void setHashcodeInputFile(int hashcodeInputFile) {
		this.hashcodeInputFile = hashcodeInputFile;
	}

	public int getHashCodeFromMessage() {
		return hashCodeFromMessage;
	}

	public void setHashCodeFromMessage(int hashCodeFromMQ) {
		this.hashCodeFromMessage = hashCodeFromMQ;
	}

	@Override
	public String toString() {
		return "CopyProperties [filePath=" + filePath + ", msgId=" + msgId + ", hashcodeInputFile=" + hashcodeInputFile
				+ ", hashCodeFromMessage=" + hashCodeFromMessage + "]";
	}
}
