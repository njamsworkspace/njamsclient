package com.w3p.im.iib.mon.client.utils.io.exceptions;

public class MessagingException extends IOUtilsException {

	private static final long serialVersionUID = 1L;
	private String msg;

	public MessagingException() {
		super();
	}

	public MessagingException(String arg, Throwable cause) {
		super(cause);
		setMsg(arg, cause.getMessage());

	}

	public MessagingException(String arg) {
		setMsg(arg);
	}

	@Override
	public String getMessage() {
		return msg;
	}
	
	private void setMsg(String arg) {
		this.msg = String.format("An MQ/JMS error occured. Message is '%s'.", arg); 
	}

	
	private void setMsg(String arg, String cause) {
		this.msg = String.format("An MQ/JMS error occured. Message is '%s'.\n  Cause is '%s'.", arg, cause); 
	}

}
