package com.w3p.im.iib.mon.jms.data;

import java.util.Map;

import javax.jms.BytesMessage;
import javax.jms.Connection;
import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.TextMessage;
import javax.naming.Context;

import com.w3p.im.iib.mon.jms.service.MessagingService;

public interface IMessageProducer {
	
	/**
	 * @param context
	 * @param connection
	 * @param dest
	 * @param isTransacted
	 * @throws JMSException
	 */
	void createMessageProducer(Context context, Connection connection, String dest, boolean isTransacted) throws JMSException;
	
	BytesMessage createBytesMessage(String msgContent) throws JMSException;
	
	TextMessage createTextMessage(String msgContent) throws JMSException;
	
	Message setMessageProperties(Message message, Map<String, String> msgProperties) throws JMSException;
	
	void closeSession() throws JMSException;
}
