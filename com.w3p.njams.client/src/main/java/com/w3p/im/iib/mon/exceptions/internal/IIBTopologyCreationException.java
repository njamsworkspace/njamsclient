package com.w3p.im.iib.mon.exceptions.internal;

import com.w3p.im.iib.mon.exceptions.InternalException;

public class IIBTopologyCreationException extends InternalException {


	private static final long serialVersionUID = 1L;

	public IIBTopologyCreationException() {
		super();
	}

	public IIBTopologyCreationException(String msg, Object... values) {
		super(msg, values);
	}
}
