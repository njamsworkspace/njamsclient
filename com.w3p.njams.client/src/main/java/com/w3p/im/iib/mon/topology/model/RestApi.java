package com.w3p.im.iib.mon.topology.model;

import com.w3p.api.config.proxy.RestApiProxy;


public class RestApi extends Deployable { //AbstractTopology {
	
	
	public RestApi(RestApiProxy apip) {
		super(apip);
	}

	@Override
	public String toString() {
		return "Rest Api [name=" + name + "]";
	}
}
