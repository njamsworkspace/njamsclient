package com.w3p.im.iib.mon.client.model.data;

import static java.util.stream.Collectors.toList;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

public class GroupModelData extends ActivityModelData {

	private static final long serialVersionUID = 1L;
	
    private final Map<String, ActivityModelData> childActivities = new LinkedHashMap<>();
    private final Map<String, TransitionModelData> childTransitions = new LinkedHashMap<>();
    private final Map<String, ActivityModelData> startActivities = new LinkedHashMap<>();

    private int width;
    private int height;

	public GroupModelData(ProcessModelData processModelData) {
		super(processModelData);
	}

	public GroupModelData(ProcessModelData processModelData, String id, String name, String type) {
		super(processModelData, id, name, type);
	}

	public void addChildActivity(ActivityModelData childActivity) {
		ActivityModelData amd = childActivities.get(childActivity.getId());
		if (amd == null) {
			childActivities.put(childActivity.getId(), childActivity);
			childActivity.setParent(this);
		}
//		if (childActivity.getType().equals("groupStartType")) {
//			addStartActivity(childActivity);
//		}
	}

	public List<ActivityModelData> getChildActivities() {
		return Collections.unmodifiableList(new ArrayList<>(childActivities.values()));
	}

	//    public void addChildGroup(final String groupModelId, final String groupName, final String groupType) {
	public void addChildGroup (GroupModelData group) {
		addChildActivity(group);
	}

	public Collection<GroupModelData> getChildGroups() {
	    return this.childActivities.values().stream()
	            .filter(ActivityModelData::isGroup)
	            .map(GroupModelData.class::cast)
	            .collect(toList());
	}
	
	public void addChildSubProcess(SubProcessActivityModelData subProcess) {
		addChildActivity(subProcess);
	}
			

	public void addChildTransition(TransitionModelData childTransition) {
		TransitionModelData tmd = childTransitions.get(childTransition.getId());
		if (tmd == null) {
            childTransitions.put(childTransition.getId(), childTransition);
            childTransition.setParent(this);
        }			
	}
	public List<TransitionModelData> getChildTransitions() {
		return Collections.unmodifiableList(new ArrayList<>(childTransitions.values()));
	}

    public void addStartActivity(ActivityModelData activity) {
        if (!activity.isStarter()) {
            activity.setStarter(true);
        }
        startActivities.put(activity.getId(), activity);
    }
    
	public List<ActivityModelData> getStartActivities() {
		return Collections.unmodifiableList(new ArrayList<>(startActivities.values()));
	}

	public int getWidth() {
		return width;
	}

	public void setWidth(int width) {
		this.width = width;
	}

	public int getHeight() {
		return height;
	}

	public void setHeight(int height) {
		this.height = height;
	}

}
