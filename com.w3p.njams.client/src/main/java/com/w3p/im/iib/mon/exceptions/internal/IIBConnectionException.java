package com.w3p.im.iib.mon.exceptions.internal;

import com.w3p.im.iib.mon.exceptions.InternalException;

public class IIBConnectionException extends InternalException {


	private static final long serialVersionUID = 1L;

	public IIBConnectionException() {
		super();
	}

	public IIBConnectionException(String msg, Object... values) {
		super(msg, values);
	}
}
