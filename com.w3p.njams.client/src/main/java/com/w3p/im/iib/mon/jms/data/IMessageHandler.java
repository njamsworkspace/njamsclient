package com.w3p.im.iib.mon.jms.data;


import com.w3p.im.iib.mon.exceptions.ApplicationException;
import com.w3p.im.iib.mon.exceptions.internal.CreateMonitoringEventHandlerException;



public interface IMessageHandler { //<T extends MonitorRequest> {
	
	/**
	 * This is the callback method for processing messages that arrive in the onMessage method
	 * of the invoked class
	 * @throws ApplicationException 
	 * @throws CreateMonitoringEventHandlerException 
	 * 
	 */
	public void handleMessagesForEventsSource(String topic)  throws CreateMonitoringEventHandlerException;
}
