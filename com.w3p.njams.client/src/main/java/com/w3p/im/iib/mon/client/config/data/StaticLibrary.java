package com.w3p.im.iib.mon.client.config.data;

import java.util.List;

public class StaticLibrary extends Library {

	public StaticLibrary(String name, boolean includeSchemaName) {
		super(name, includeSchemaName);
	}

	/*
	 * Ensure all msgflows are monitored by inserting an '*'
	 */
	public List<MsgFlow> getMsgFlows() {
		if (msgFlows.isEmpty()) {
			MsgFlow mf = new MsgFlow("*", isIncludeSchemaName(), isIncludeSchemaName());
			msgFlows.add(mf);
		}
		return msgFlows;
	}

	/*
	 * Ensure all subflows are monitored by inserting an '*'
	 */
	public List<SubFlow> getSubFlows() {
		if (subFlows.isEmpty()) {
			SubFlow sf = new SubFlow("*", isIncludeSchemaName(), isIncludeSchemaName());
			subFlows.add(sf);
		}
		return subFlows;
	}

	@Override
	public String toString() {
		return "StaticLibrary [name=" + getName() + "]";
	}	
}
