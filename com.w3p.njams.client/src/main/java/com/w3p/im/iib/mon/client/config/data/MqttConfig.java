package com.w3p.im.iib.mon.client.config.data;

public class MqttConfig {
	
	private String clientId;
	private String host;
	private int port;
	private int keepAliveInterval;
	private int qos;
	private User MQTTUser;
	
	
	public String getClientId() {
		return clientId;
	}
	public void setClientId(String clientId) {
		this.clientId = clientId;
	}
	public String getHost() {
		return host;
	}
	public void setHost(String host) {
		this.host = host;
	}
	public int getPort() {
		return port;
	}
	public void setPort(int port) {
		this.port = port;
	}
	public int getKeepAliveInterval() {
		return keepAliveInterval;
	}
	public void setKeepAliveInterval(int keepAliveInterval) {
		this.keepAliveInterval = keepAliveInterval;
	}
	public int getQos() {
		return qos;
	}
	public void setQos(int qos) {
		this.qos = qos;
	}
	public User getMQTTUser() {
		return MQTTUser;
	}
	public void setMQTTUser(User mQTTUser) {
		MQTTUser = mQTTUser;
	}
	@Override
	public String toString() {
		return "MqttConfig [clientId=" + clientId + ", host=" + host + ", port=" + port + "]";
	}
	



}
