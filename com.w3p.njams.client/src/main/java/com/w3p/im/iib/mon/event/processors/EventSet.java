package com.w3p.im.iib.mon.event.processors;

import java.util.ArrayList;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import org.apache.commons.lang3.StringUtils;

import com.w3p.im.iib.mon.jms.consumers.MonitorEventsConsumer;


public class EventSet {
	
	private MonitorEventsConsumer eventsConsumer;
	
	// For timeout handling
	private Timer timer;
	private int waitTime; // seconds to wait 
	
	protected boolean isComplete;
	
	private String localTxnId;
	String eventSourceAddress;
	private boolean txnEndReceived;
	private int maxCounter = 0;
	private List<String> monEvents = new ArrayList<>();
	
	
	public EventSet(String localTxnId) {
		this(localTxnId, 20); // Default the waiting time for all events to be received to 20 secs
	}
	public EventSet(String localTxnId, int waitTime) {
		this.localTxnId = localTxnId;
        timer = new Timer();
        this.waitTime = waitTime;
        timer.schedule(new TimeoutTask(), waitTime*1000);
	}
	
	public boolean isNew() {
		return monEvents.isEmpty();
	}
	
	public void addEvent(String eventAsXML) {
	
		monEvents.add(eventAsXML);
		String counter = StringUtils.substringBetween(eventAsXML, "wmb:counter=\"", "\"/>");
		eventSourceAddress = StringUtils.substringBetween(eventAsXML, " wmb:eventSourceAddress=\"", "\">");
		if (eventSourceAddress.endsWith("transaction.End")) {
			txnEndReceived = true;
		}
//		String msgflowPart = StringUtils.substringAfter(eventAsXML, "wmb:messageFlow wmb:uniqueFlowName");
//		String msgflowName = StringUtils.substringBetween(msgflowPart, "wmb:name=\"", "\" wmb:UUID=");
//		String localTranId = StringUtils.substringBetween(eventAsXML, "wmb:localTransactionId=\"", "\" wmb:parentTransactionId");
//		System.out.println("MonitorEventsConsumer#addEvent()");
//		System.out.println("  "+counter+"_"+msgflowName+"_"+localTranId+"\"");
		
		int iCounter = Integer.parseInt(counter);
		if (iCounter > maxCounter) {
			maxCounter = iCounter;
		}

		if (txnEndReceived && maxCounter == monEvents.size()) {
			// all events should have been received unless maxCounter = 1
			// Call back to the onMessageHandler to process this event set.
			eventsConsumer.sendEventsToNjams(localTxnId, this);
			isComplete = true;
			timer.cancel();
		}
	}

	public List<String> getMonEvents() {
		return monEvents;
	}
	
	public void clear() {
		monEvents.clear();
		localTxnId = null;
		isComplete = false;
		maxCounter = 0;
	}
	
	public void registerHandler(final MonitorEventsConsumer eventsConsumer) {
		if (this.eventsConsumer == null) {
			this.eventsConsumer = eventsConsumer;
		}
	}

    class TimeoutTask extends TimerTask {
        public void run() {            
            eventsConsumer.sendEventsToNjams(localTxnId, EventSet.this);
			isComplete = true;
			System.err.println(String.format("Message events for local txn id '%s' were incomplete after waiting for %d seconds. they were sent to nJAMS in this state", localTxnId, waitTime));
            timer.cancel(); //Terminate the timer thread
        }
    }
}
