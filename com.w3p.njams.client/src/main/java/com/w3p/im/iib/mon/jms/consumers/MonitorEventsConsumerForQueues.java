package com.w3p.im.iib.mon.jms.consumers;

import java.util.concurrent.LinkedBlockingDeque;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

import com.w3p.im.iib.mon.monitor.data.MonitorEventsRequest;
import com.w3p.im.iib.mon.monitor.data.MonitorEventsRequestForQueues;

public class MonitorEventsConsumerForQueues extends MonitorEventsConsumer {

	public MonitorEventsConsumerForQueues(MonitorEventsRequestForQueues request) {
		super(request);
		super.njamsClients = request.getNjamsClients();
		super.flowToProcessModelCaches = request.getFlowToProcessModelCaches();
		int consumerThreads = request.getConsumerThreads();
		super.consumeEventsExecutor = new ThreadPoolExecutor(consumerThreads, consumerThreads, 1000, TimeUnit.MILLISECONDS, 
			new LinkedBlockingDeque<Runnable>()) {}; 
	}

}
