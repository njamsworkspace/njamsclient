package com.w3p.im.iib.mon.monitor.data;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.StringUtils;

import com.w3p.api.config.proxy.ApplicationProxy;
import com.w3p.api.config.proxy.ExecutionGroupProxy;
import com.w3p.api.config.proxy.MessageFlowProxy;
import com.w3p.api.config.proxy.RestApiProxy;
import com.w3p.api.config.proxy.ServiceProxy;
import com.w3p.api.config.proxy.SharedLibraryProxy;
import com.w3p.api.config.proxy.StaticLibraryProxy;
import com.w3p.api.config.proxy.SubFlowProxy;


public class MonitoredObject<M extends MonitoredObject<?>>  {
	protected final static String DOT = ".";
	protected String name;	
	protected boolean includeSchemaName = false;
	protected boolean includePayload;
	protected boolean labelNodeStartsGroup;
	protected final List<MonitoredObject<?>> monitoredIntegrationServers = new ArrayList<>();
	protected final List<MonitoredObject<?>> monitoredMessageFlows = new ArrayList<>();
	protected final List<MonitoredObject<?>> monitoredSubFlows = new ArrayList<>();
	protected final List<MonitoredObject<?>> monitoredApplications = new ArrayList<>();
	protected final List<MonitoredObject<?>> monitoredRestApis = new ArrayList<>();
	protected final List<MonitoredObject<?>> monitoredServices = new ArrayList<>();
	protected final List<MonitoredObject<?>> monitoredSharedibraries = new ArrayList<>();
	protected final List<MonitoredObject<?>> monitoredStaticLibraries = new ArrayList<>();
		
	
	public MonitoredObject(String name) {
		this.name = name;
	}
	
	public String getName() {
		return name;
	}
	

	public void addMonitoredChildObject(MonitoredObject<?> mo) {
		if (mo instanceof MonitoredMessageFlow) { 
			if (!monitoredMessageFlows.contains(mo)) { monitoredMessageFlows.add(mo);}		
		} else if (mo instanceof MonitoredSubFlow) {
			if (!monitoredSubFlows.contains(mo)) {monitoredSubFlows.add(mo);}		
		}else if (mo instanceof MonitoredIntegrationServer) {
			if (!monitoredIntegrationServers.contains(mo)) {monitoredIntegrationServers.add(mo);}		
		} else if (mo instanceof MonitoredApplication) {
			if (!monitoredApplications.contains(mo)) {monitoredApplications.add(mo);}		
		} else if (mo instanceof MonitoredRestApi) {
			if (!monitoredRestApis.contains(mo)) {monitoredRestApis.add(mo);}		
		} else if (mo instanceof MonitoredService) {
			if (!monitoredServices.contains(mo)) {monitoredServices.add(mo);}		
		} else if (mo instanceof MonitoredSharedLibrary) {
			if (!monitoredSharedibraries.contains(mo)) {monitoredSharedibraries.add(mo);}		
		} else if (mo instanceof MonitoredStaticLibrary) {
			if (!monitoredStaticLibraries.contains(mo)) {monitoredStaticLibraries.add(mo);}		
		}
	}
	
	public  List<MonitoredObject<?>> getMonitoredChildObjects(Class<?> ao) {
		List<MonitoredObject<?>> mos = null;
		if (ao.isAssignableFrom(MessageFlowProxy.class)) {
			mos = monitoredMessageFlows;
		} else if (ao.isAssignableFrom(SubFlowProxy.class)) {
			mos = monitoredSubFlows;
		} else if (ao.isAssignableFrom(ExecutionGroupProxy.class)) {
			mos = monitoredIntegrationServers;
		} else if (ao.isAssignableFrom(ApplicationProxy.class)) {
			mos = monitoredApplications;
		} else if (ao.isAssignableFrom(RestApiProxy.class)) {
			mos = monitoredRestApis;
		} else if (ao.isAssignableFrom(ServiceProxy.class)) {
			mos = monitoredServices;
		} else if (ao.isAssignableFrom(SharedLibraryProxy.class)) {
			mos = monitoredSharedibraries;
		} else if (ao.isAssignableFrom(StaticLibraryProxy.class)) {
			mos = monitoredStaticLibraries;
		}		
		return mos;
	}

	public MonitoredObject<?> getMonitoredChildObjectByName(Class<?> ao, String name) {
		MonitoredObject<?> monitoredObject = null;

		if (ao.isAssignableFrom(MessageFlowProxy.class)) {
			if (!includeSchemaName && StringUtils.contains(name, DOT)) {
				String flowName = StringUtils.substringAfterLast(name, DOT);
				if (!flowName.equals("*")) {
					name = flowName; // Preservere the schema name to match 'all msgflows in schema'
				}
			}
			for (MonitoredObject<?> mo : monitoredMessageFlows) {
				if (mo.getName().equals(name)) { // || mo.getName().equals("*")) {
					// Supports named msgflows having specific attributes with all other msgflows having different attributes
					monitoredObject = mo;
					return monitoredObject;
				}
			}
			for (MonitoredObject<?> mo : monitoredMessageFlows) {
				if (mo.getName().equals("*")) {
					return mo;
				}
			}
		} else if (ao.isAssignableFrom(SubFlowProxy.class)) {
			if (!includeSchemaName && StringUtils.contains(name, DOT)) {
				name = StringUtils.substringAfterLast(name, DOT);
				String flowName = StringUtils.substringAfterLast(name, DOT);
				if (!flowName.equals("*")) {
					name = flowName; // Preservere the schema name to match 'all msgflows in schema'
				}
			}
			for (MonitoredObject<?> mo : monitoredSubFlows) {				
				if (mo.getName().equals(name)) { // || mo.getName().equals("*")) {
					// Supports named subflows having specific attributes with all other subflows having different attributes
					monitoredObject = mo;
					return monitoredObject;
				}
			}
			for (MonitoredObject<?> mo : monitoredSubFlows) {
				if (mo.getName().equals("*")) {
					return mo;
				}
			}
		} else if (ao.isAssignableFrom(ExecutionGroupProxy.class)) {
			monitoredObject = findNamedMonitoredObject(name, monitoredObject,  monitoredIntegrationServers);
		} else if (ao.isAssignableFrom(ApplicationProxy.class)) {
			monitoredObject = findNamedMonitoredObject(name, monitoredObject,  monitoredApplications);
		} else if (ao.isAssignableFrom(RestApiProxy.class)) {
			monitoredObject = findNamedMonitoredObject(name, monitoredObject,  monitoredRestApis);
		} else if (ao.isAssignableFrom(ServiceProxy.class)) {
			monitoredObject = findNamedMonitoredObject(name, monitoredObject,  monitoredServices);
		} else if (ao.isAssignableFrom(SharedLibraryProxy.class)) {
			monitoredObject = findNamedMonitoredObject(name, monitoredObject,  monitoredSharedibraries);
		} else if (ao.isAssignableFrom(StaticLibraryProxy.class)) {
			monitoredObject = findNamedMonitoredObject(name, monitoredObject,  monitoredStaticLibraries);
		}		
		return monitoredObject;
	}

	private MonitoredObject<?> findNamedMonitoredObject(String name, MonitoredObject<?> monitoredObject, List<MonitoredObject<?>> mos) {
		if (!mos.isEmpty()) {
			for (MonitoredObject<?> mo : mos) {
				if (mo.getName().equals("*") || mo.getName().equals(name)) { // FIXME should we test for name=* if the list fails? Porbably not
					monitoredObject = mo;
					break; // FIXME return mo
				}
			}
		}
		return monitoredObject;
	}

	/**
	 * 'isMonitorAllObjects' checks for a name=* || the name argument is in the list of objects to be monitored
	 * @param ao
	 * @param name
	 * @return
	 */
	public boolean isSomethingToMonitor(Class<?> ao, String name) {
		return isMonitorAllObjects(ao) || getMonitoredChildObjectByName(ao, name)!= null;
	}
	
	/**
	 * @param ao an AdministeredObject (proxy)
	 * @return depends on the actual subclass of the 'mo' parameter
	 */
	public boolean isMonitorAllObjects(Class<?> ao) {
		boolean result = false;
		if (ao.isAssignableFrom(MessageFlowProxy.class)) { 
			result = (monitoredMessageFlows.size() == 1 && monitoredMessageFlows.get(0).getName().equals("*")) ? true : false;
		} else if (ao.isAssignableFrom(SubFlowProxy.class)) {
			result = (monitoredSubFlows.size() == 1 && monitoredSubFlows.get(0).getName().equals("*")) ? true : false;
		} else if (ao.isAssignableFrom(ApplicationProxy.class)) {
			result = (monitoredApplications.size() == 1 && monitoredApplications.get(0).getName().equals("*")) ? true : false;
		} else if (ao.isAssignableFrom(ExecutionGroupProxy.class)) {
			result = (monitoredIntegrationServers.size() == 1 && monitoredIntegrationServers.get(0).getName().equals("*")) ? true : false;
		} else if (ao.isAssignableFrom(RestApiProxy.class)) {
			result = (monitoredRestApis.size() == 1 && monitoredRestApis.get(0).getName().equals("*")) ? true : false;
		} else if (ao.isAssignableFrom(ServiceProxy.class)) {
			result = (monitoredServices.size() == 1 && monitoredServices.get(0).getName().equals("*")) ? true : false;
		} else if (ao.isAssignableFrom(SharedLibraryProxy.class)) {
			result = (monitoredSharedibraries.size() == 1 && monitoredSharedibraries.get(0).getName().equals("*")) ? true : false;
		} else if (ao.isAssignableFrom(StaticLibraryProxy.class)) {
			result = (monitoredStaticLibraries.size() == 1 && monitoredStaticLibraries.get(0).getName().equals("*")) ? true : false;
		}		
		return result;
	}

	public boolean isIncludeSchemaName() {
		return includeSchemaName;
	}

	public void setIncludeSchemaName(boolean includeSchemaNames) {
		this.includeSchemaName = includeSchemaNames;
	}

	public boolean isIncludePayload() {
		return includePayload;
	}

	public void setIncludePayload(boolean includePayload) {
		this.includePayload = includePayload;
	}

	public boolean isLabelNodeStartsGroup() {
		return labelNodeStartsGroup;
	}

	public void setLabelNodeStartsGroup(boolean labelNodeStartsGroup) {
		this.labelNodeStartsGroup = labelNodeStartsGroup;
	}

	@Override
	public String toString() {
		return "MonitoredObject [name=" + name + "]"; //(child == null ? null : child.getName()) + "]";
	}

}
