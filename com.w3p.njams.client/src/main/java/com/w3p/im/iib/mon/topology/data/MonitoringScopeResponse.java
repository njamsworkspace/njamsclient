package com.w3p.im.iib.mon.topology.data;

import com.w3p.im.iib.mon.monitor.data.MonitoredObject;

public class MonitoringScopeResponse {
	
	private boolean success;
	private int retCode;
	private String message;
	private MonitoredObject<MonitoredObject<?>> monitoringScope;

	
	public MonitoringScopeResponse() {
		success = true;
		message = "";
	}
	
	public String getIntNodeName() {
		return monitoringScope.getName();
	}
	
	public boolean isSuccess() {
		return success;
	}

	public void setSuccess(boolean success) {
		this.success = success;
	}

	public int getRetCode() {
		return retCode;
	}

	public void setRetCode(int retCode) {
		this.retCode = retCode;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public MonitoredObject<MonitoredObject<?>> getMonitoringScope() {
		return monitoringScope;
	}

	public void setMonitoringScope(MonitoredObject<MonitoredObject<?>> monitoringScope) {
		this.monitoringScope = monitoringScope;
	}

	@Override
	public String toString() {
		return "MonitoringScopeResponse [intNodeName=" + getIntNodeName() + "]";
	}

}
