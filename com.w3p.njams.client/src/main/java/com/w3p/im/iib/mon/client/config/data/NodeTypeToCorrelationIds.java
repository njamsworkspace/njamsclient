package com.w3p.im.iib.mon.client.config.data;

import java.util.HashMap;
import java.util.Map;

public class NodeTypeToCorrelationIds {
	
	private final Map<String, String> nodeTypeToCorrelationIds = new HashMap<>();

	public Map<String, String> getNodeTypeToCorrelationIdsMap () {
		return nodeTypeToCorrelationIds;
	}

	@Override
	public String toString() {
		return "NodeTypeToCorrelationIds [nodeTypeToCorrelationIds=" + nodeTypeToCorrelationIds + "]";
	}
}
