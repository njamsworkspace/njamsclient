package com.w3p.im.iib.mon.client.utils.io.readers;

import java.io.IOException;
import java.nio.file.DirectoryStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.w3p.im.iib.mon.client.utils.io.data.IReadFileRequest;
import com.w3p.im.iib.mon.client.utils.io.data.Response;

public class FindFilesToRead {	
	private static final Logger log = LoggerFactory.getLogger(FindFilesToRead.class);

	public FindFilesToRead() {
	}

	public FilesToRead find(IReadFileRequest req, Response resp) {		
		FilesToRead filesToRead = new FilesToRead();

		Path inputFolder = verifyFilesCanBeCopied(req.getSource(), resp);
		if (!resp.isSuccess()) {
			return filesToRead;
		}

		// Read files from the folder and store in a List of file properties
		Path currentFile = inputFolder; // For exception reporting - it needs to be init with a valid value
		// 	If no file name pattern is provided, then get all files.
		String fileNamePattern = req.getNamePattern();
		// See: https://examples.javacodegeeks.com/core-java/nio/java-nio-iterate-files-directory/			
		try (DirectoryStream<Path> stream = Files.newDirectoryStream(inputFolder, fileNamePattern)) {
			for (Path file : stream) {
				if (!Files.isDirectory(file) ) { // exclude directories
					currentFile = file;				
					filesToRead.addFile(file);
				}
			}
			resp.setFilesToBeRead(filesToRead.getFiles().size());
		} catch (IOException e) {
			String reason = currentFile == null? 
					String.format("Error starting to read files rom the input folder: '%s'. See log.", req.getSource()) :
						String.format("Error reading file '%s' from the input folder: '%s'. See log.", req.getSource(), currentFile.toFile().getName(), e.toString());	
			log.error(reason, e);
			resp.setMessage(currentFile.toFile().getName(), reason);
		}

		return filesToRead;
	}

	private Path verifyFilesCanBeCopied(String source, Response resp) {
		Path inputFolder = Paths.get(source);	

		// Check that the input folder exists.
		if (!Files.exists(inputFolder)) {
			String reason = String.format("Input folder '%s' does not exist.", inputFolder.toString());
			log.error(reason);
			resp.setMessage(Response.ERROR, reason);
		}
		if (!resp.isSuccess()) {
			return inputFolder;
		}

		// Check that input folder is a folder
		if (!Files.isDirectory(inputFolder) ) {
			String reason = String.format("Input folder '%s' is not a directory.", inputFolder.toString());
			log.error(reason);
			resp.setMessage(Response.ERROR, reason);
		}
		if (!resp.isSuccess()) {
			return inputFolder;
		}

		// Check for an empty folder
		if (inputFolder.toFile().listFiles().length == 0) {
			String reason = String.format("Input folder '%s' is empty. No files to read.", source);
			log.warn(reason);
			resp.setMessage(Response.WARN, reason);
		}
		return inputFolder;
	}
}
