package com.w3p.im.iib.mon.jms.service;

import javax.jms.JMSException;

import com.w3p.im.iib.mon.exceptions.internal.MessagingException;
import com.w3p.im.iib.mon.jms.data.IMessageProducer;
import com.w3p.im.iib.mon.jms.data.IMessagingServiceProducer;

public class MessagingServiceProducer extends MessagingService implements IMessagingServiceProducer {

	protected IMessageProducer msgProducer;
	protected MessagingServiceFactory messagingServiceFactory;
	

	public MessagingServiceProducer(MqJmsConnectionProperties connectionProperties) {
		super(connectionProperties);
	}

	@Override
	public synchronized IMessageProducer addMessageProducer(String dest, IMessageProducer msgProducer) throws MessagingException, JMSException {
		return addMessageProducer(dest, msgProducer, false);
	}

	@Override
	public synchronized IMessageProducer addMessageProducer(String dest, IMessageProducer msgProducer, boolean isTransacted) throws MessagingException, JMSException {
		this.msgProducer= msgProducer; 
		createConnection(dest);
		msgProducer.createMessageProducer(context, connection, dest, isTransacted);
		return msgProducer;
	}

	@Override
	public synchronized IMessageProducer getMessageProducer() {
		return msgProducer;
	}
	
	public synchronized void setMessagingServiceFactory(MessagingServiceFactory messagingServiceFactory) {
		this.messagingServiceFactory = messagingServiceFactory;
	}

	public synchronized MessagingServiceFactory getMessagingServiceFactory() {
		return messagingServiceFactory;
	}
}
