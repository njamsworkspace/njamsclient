package com.w3p.im.iib.mon.topology.data;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.w3p.api.config.proxy.BrokerProxy;
import com.w3p.api.config.proxy.ConfigManagerProxyPropertyNotInitializedException;
//import com.ibm.broker.config.proxy.BrokerProxy;
//import com.ibm.broker.config.proxy.ConfigManagerProxyPropertyNotInitializedException;
import com.w3p.im.iib.mon.client.config.data.IntegrationServer;

public class MonitoringScopeRequest {
	private static final Logger logger = LoggerFactory.getLogger( MonitoringScopeRequest.class);
	
	private final BrokerProxy integrationNode;
	private final IntegrationServer intSvr;
	private String intNodeName;
	

	public MonitoringScopeRequest(BrokerProxy integrationNode, IntegrationServer intSvr) {
		this.integrationNode = integrationNode;
		this.intSvr = intSvr;
	}

	public BrokerProxy getIntegrationNode() {
		return integrationNode;
	}
	
	public String getIntegrationNodeName() {
		if (intNodeName == null) {
			try {
				intNodeName =  integrationNode.getName();
			} catch (ConfigManagerProxyPropertyNotInitializedException e) {
				// Shouldn't ever happen if we have got this far... should it?
				logger.error(e.toString(), e);			
			}
		}
		return intNodeName;
	}

	public IntegrationServer getIntSvr() {
		return intSvr;
	}	
}
