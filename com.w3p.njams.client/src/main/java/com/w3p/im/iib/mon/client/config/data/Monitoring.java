package com.w3p.im.iib.mon.client.config.data;

import java.util.ArrayList;
import java.util.List;

public class Monitoring {
	
	private int consumerThreads;
	private Profile profile;
	private final List<IntegrationServer> integrationServers = new ArrayList<>();
	
	
	public int getConsumerThreads() {
		return consumerThreads;
	}
	public void setConsumerThreads(int consumerThreads) {
		this.consumerThreads = consumerThreads;
	}
	public Profile getProfile() {
		return profile;
	}
	public void setProfile(Profile profile) {
		this.profile = profile;
	}
	public List<IntegrationServer> getIntegrationServers() {
		return integrationServers;
	}
	public IntegrationServer getIntegrationServer(String name) {
		for (IntegrationServer integrationServer : integrationServers) {
			if (integrationServer.getName().equals(name)) {
				return integrationServer;
			}
		}
		return null;
	}
	@Override
	public String toString() {
		return "Monitoring [integrationServers=" + integrationServers + "]";
	}
}
