package com.w3p.im.iib.mon.client.config.data;

public class IbmMqConfig {
	
	private JmsEvents jmsEvents;

	public JmsEvents getJmsMQ() {
		return jmsEvents;
	}

	public void setJmsMQ(JmsEvents jmsEvents) {
		this.jmsEvents = jmsEvents;
	}

	@Override
	public String toString() {
		return "IbmMqConfig [jmsEvents=" + jmsEvents + "]";
	}
}
