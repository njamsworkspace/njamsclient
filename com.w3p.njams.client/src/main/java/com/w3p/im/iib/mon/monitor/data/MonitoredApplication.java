package com.w3p.im.iib.mon.monitor.data;

public class MonitoredApplication extends MonitoredObject<MonitoredObject<?>> {
	
	public MonitoredApplication(String name) {
		super(name);
	}

	@Override
	public String toString() {
		return "Monitored Application [name=" + name + "]";
	}
}
