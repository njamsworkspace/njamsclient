package com.w3p.im.iib.mon.monitor.data;

import static com.w3p.im.iib.mon.client.constants.IClientConstants.DOT;
import static com.w3p.im.iib.mon.client.constants.IClientConstants.GROUP_NAME_PREFIX;
import static com.w3p.im.iib.mon.client.constants.IClientConstants.INCLUDE_SCHEMA_NAME;
import static com.w3p.im.iib.mon.client.constants.IClientConstants.INCLUDE_SERVER_NAME;
import static com.w3p.im.iib.mon.client.constants.IClientConstants.LABEL_NODE;
import static com.w3p.im.iib.mon.client.constants.IClientConstants.LABEL_NODE_STARTS_GROUP;
import static com.w3p.im.iib.mon.client.constants.IClientConstants.REGEX_DOT;
import static com.w3p.im.iib.mon.client.constants.IClientConstants.SPACE;
import static com.w3p.im.iib.mon.client.constants.IClientConstants.UNDERSCORE;

import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZoneOffset;
import java.time.ZonedDateTime;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;

import com.im.njams.sdk.model.ActivityModel;
import com.im.njams.sdk.model.GroupModel;
import com.im.njams.sdk.model.ProcessModel;

/**
 * @author JohnO
 *
 */
public class MonitoringEvent {
	
	protected String eventName;
	protected LocalDateTime creationTime;
	protected long creationTimeMillis = 0L;
	protected int counter;
	protected String localTransactionId;
	protected String globalTransactionId;
	protected String intSvrName;
	protected String messageFlow;     // wmb:messageFlow'.@'wmb:name' [schema-name.] msg/sub Flow name
	protected String baseMessageFlow; // messageFlow name, without a schema name
	protected String uniqueflowName;  // IntNode.IntSvr.App|Lib|Api.FlowName
	protected String nodeLabel;       // Msgflow: nodeLabel || Subflow: MsgFlow[...SubFlow].nodeLabel Note: nodeLabel can contain a '.'
	protected String nodeType;
	protected Map<String, String> applicationData; 
	protected String exceptionList; //CDATA
	protected String msgId; // MQMD.MsgId (input) or LocalEnvironment/WrittenDestination/MQ/DestinationData[1]/msgId (output) 
	protected String correlId; // LocalEnvironment/WrittenDestination/MQ/DestinationData[1]/correlId (MQ Reply = incoming MsgId)
	
	protected String bodyIn;
	protected String bodyOut;	
	
	protected String localNodeLabel; // This is the Node Label without any SubFlowName prefix
	protected String localNodeLabelAsId; // localNodeLabel to lower case with spaces = underscore
	protected String subflowName; // Prefix on NodeLabel when this is an event from a SubFlow
	protected GroupModel groupModel; 
	protected String groupModelId;
	protected String processFlowPath;    // MsgflowName[...SubflowName] = the EventSourceAddress minus the node name. Used to find the nJAMS Path for ProcessModel 
	protected String flowParentName;
	protected ProcessModel processModel;  // for this event
	protected ActivityModel activityModel;  // for this event 
	protected boolean includeSchemaName; // If false, then omit the schema prefix of the name
	protected boolean includeIntSvrName; // If false, then omit the integration server part of the name
	protected boolean labelNodeStartsGroup; // If false, do not treat a Label node as the start of a Group
	
	protected int flowLevel = 0;    // The subflow level ofr this message. 1 = msgflow, 2 = fist-level subflow, etc.
	
	protected StringBuilder toString;

	
	public String getEventName() {
		return eventName;
	}
	public void setEventName(String eventName) {
		this.eventName = eventName;
	}
	public LocalDateTime getCreationTime() {
		return creationTime;
	}
	/**
	 * Set using the creationTime from the XML event:  "YYY-MM-DDThh:mm:ss.nnnnnnZ"
	 * @param creationTime
	 */
	public void setCreationTime(String creationTime) {
		String creationTimeNoZone = creationTime.substring(0, creationTime.length() - 1);
		this.creationTime = LocalDateTime.parse(creationTimeNoZone);
		this.creationTimeMillis = this.creationTime.toEpochSecond(ZoneOffset.UTC);
	}
	
	/**
	 * Set using LocalDateTime
	 * @param creationTime
	 */
	public MonitoringEvent setCreationTime(LocalDateTime creationTime) {
		this.creationTime = creationTime;
		ZonedDateTime zdt = ZonedDateTime.of(creationTime, ZoneId.systemDefault());
		this.creationTimeMillis = zdt.toInstant().toEpochMilli();
		return this;
	}

	public long getCreationTimeMillis() {
		return creationTimeMillis;
	}
	public int getCounter() {
		return counter;
	}
	public void setCounter(int counter) {
		this.counter = counter;
	}
	public String getLocalTransactionId() {
		return localTransactionId;
	}
	public void setLocalTransactionId(String localTransactionId) {
		this.localTransactionId = localTransactionId;
	}
	public String getGlobalTransactionId() {
		return globalTransactionId;
	}
	public void setGlobalTransactionId(String globalTransactionId) {
		this.globalTransactionId = globalTransactionId;
	}
	public String getIntSvrName() {
		return intSvrName;
	}
	public void setIntSvrName(String executionGroup) {
		this.intSvrName = executionGroup;
	}
	public String getBaseMessageFlow() {
		// Remove any Broker Schema values for the Flow
		if (baseMessageFlow == null) {
			if (!includeSchemaName && StringUtils.contains(messageFlow, DOT)) {
				baseMessageFlow = StringUtils.substringAfterLast(messageFlow, DOT);
			} else {
				baseMessageFlow =  messageFlow;
			}
		}
		return baseMessageFlow;
	}
	public String getBaseMessageFlowAsId() {
		return getBaseMessageFlow().toLowerCase().replace(SPACE, UNDERSCORE);
	}
	public String getMessageFlow() {
		return messageFlow;
	}
	public void setMessageFlow(String messageFlow) {
		this.messageFlow = messageFlow;
	}
	public String getUniqueflowName() {
		return uniqueflowName;
	}
	public void setUniqueflowName(String uniqueflowName) {
		this.uniqueflowName = uniqueflowName;
	}
	public String getNodeLabel() {
		return nodeLabel;
	}
	public  MonitoringEvent setNodeLabel(String nodeLabel) {
		this.nodeLabel = nodeLabel;
		// nodeLabel = {parent-subflow-name}.{this-subflow}.{node-name} - can be 'n' levels of subflow names & node-name can have form aaa.bbb.ccc
		// eventName = {node.name}.{terminal-name}
		this.localNodeLabel = StringUtils.substringBeforeLast(this.eventName, DOT); // If no '.' in name, then name is unchanged
		// If event is from a Msgflow, then localNodeLabel = nodeLabel
		if (!nodeLabel.equals(localNodeLabel)) {  // Node is in a subflow
			String subflowPath = StringUtils.substringBefore(nodeLabel, DOT+this.localNodeLabel); // Node-name might have '.' in it, so remove before split on '.'
			// Subflow name is the substring between the last DOT and the previous DOT
			String[] subflowParts = subflowPath.split(REGEX_DOT); // Need escape char as this is a regex and not a string
			this.subflowName = subflowParts[subflowParts.length-1]; //subflowParts.length > 0 ? subflowParts[subflowParts.length-1] : null;
		}
		return this;
	}
	public String getLocalNodeLabel() {
		return localNodeLabel;
	}
	/**
	 * The local node label as lower case, with spaces replaced by underscores. Used to get an ActivityModel from a ProceswModel
	 * @return
	 */
	public String getLocalNodeLabelAsId() {
		if (localNodeLabelAsId == null) {
			localNodeLabelAsId = localNodeLabel.toLowerCase().replace(SPACE, UNDERSCORE);
		}
		return localNodeLabelAsId;
	}
	public String getNodeType() {
		return nodeType;
	}
	public void setNodeType(String nodeType) {
		this.nodeType = nodeType;
	}
	/**
	 * This returns the IIB flow path used as input to FlowtoProcessModel for finding the nJAMS Path to the actual (sub)fow used.
	 * 
	 * @return
	 */
	public String getProcessFlowPath() {
		if (processFlowPath == null) {
			processFlowPath = nodeLabel.equals(localNodeLabel) 
				? messageFlow   // MsgflowName
				: messageFlow   // = +StringUtils.substringAfter(uniqueflowName, StringUtils.substringAfter(intSvrName+DOT, DOT))
				+ DOT
				+ StringUtils.substringBefore(nodeLabel, DOT
						+ StringUtils.substringBefore(eventName, DOT)); // Node name
		}
		return processFlowPath;
	}
	public String getSubflowName() {
		return subflowName;
	}
//	public MonitoringEvent setSubflowName(String subflowName) { 
//		this.subflowName = subflowName; // This is set as part of setNodeLabel() 
//		return this;
//	}
	/**
	 * <b>Note<\b>: This is only valid for an event from a Label node that starts a Group
	 * @return
	 */
	public GroupModel getGroupModel() {
		if (groupModel == null) {
			if (nodeType.equals(LABEL_NODE) && labelNodeStartsGroup) {
				groupModel = processModel.getGroup(getGroupModelId());
			}
		}
		return groupModel;
	}

	public String getGroupModelId() {
		if (groupModelId == null) {
			groupModelId = (GROUP_NAME_PREFIX+localNodeLabel).toLowerCase().replace(SPACE, UNDERSCORE);
		}
		return groupModelId;
	}

	public String getFlowParentName() {
		if (flowParentName == null) {
			if (isFromSubFlow()) {
				List<String> subflowPathParts = Arrays.asList(nodeLabel.split(REGEX_DOT));
				StringBuilder sb = new StringBuilder(messageFlow);
				int partMax = subflowPathParts.size() - 2; // Ignore the final 2 elements, which are this subflow and the node
				for (int i = 0; i < partMax ; i++) { 
					sb.append(DOT).append(subflowPathParts.get(i));						
				}
				this.flowParentName = sb.toString(); //messageFlow+DOT+subflowPathParts.get(thisPart-1);

			} else {
				flowParentName = getBaseMessageFlow();
			}
		}
		return flowParentName;
	}
	public Map<String, String> getApplicationData() {
		return applicationData;
	}
	public void setApplicationData(Map<String, String> applicationData) {
		this.applicationData = applicationData;
	}
	public String getBodyIn() {
		return bodyIn;
	}
	public void setBodyIn(String body) {
		this.bodyIn = body;
	}
	public String getBodyOut() {
		return bodyOut;
	}
	public void setBodyOut(String bodyOut) {
		this.bodyOut = bodyOut;
	}
	public String getExceptionList() {
		return exceptionList;
	}
	public void setExceptionList(String exceptionList) {
		this.exceptionList = exceptionList;
	}

	public String getMsgId() {
		return msgId;
	}
	public void setMsgId(String msgId) {
		this.msgId = msgId;
	}
	/**
	 * This is extracted from LocalEnvironment/WrittenDestination/MQ/DestinationData[1]/correlId 
	 * For an MQ Reply node, it holds the value of the current MsgId
	 * 
	 * @return
	 */
	public String getCorrelId() {
		return correlId;
	}
	public void setCorrelId(String correlId) {
		this.correlId = correlId;
	}
	public ProcessModel getProcessModel() {
		return processModel;
	}
	public String getProcessModelName() {
		return processModel.getName(); //processModelName;
	}
	public String getProcessModelNameAsId() {
		return getProcessModelName().toLowerCase().replace(SPACE, UNDERSCORE);
	}
	public MonitoringEvent setProcessModel(ProcessModel processModel) {
		this.processModel = processModel;
		return this;
	}
	public ActivityModel getActivityModel() {
		if (activityModel == null) {
			activityModel = processModel.getActivity(getLocalNodeLabelAsId()); // Assumes that ProcessModel must have been set
		}
		return activityModel;
	}
	public boolean isIncludeSchemaName() {
		if (includeSchemaName) {
			includeSchemaName = (Boolean)processModel.getProperty(INCLUDE_SERVER_NAME); // usually 'false', so double-check if it's 'true'
		}
		return includeSchemaName;
	}
	public void setIncludeSchemaName(boolean includeSchemaNames) {
		this.includeSchemaName = includeSchemaNames;
	}
	public boolean isIncludeIntSvrName() {
		if (includeIntSvrName) {
			includeIntSvrName = (Boolean)processModel.getProperty(INCLUDE_SERVER_NAME); //  usually 'false', so double-check if it's 'true'
		}
		return includeIntSvrName;
	}
	public void setIncludeIntSvrName(boolean includeIntSvrName) {
		this.includeIntSvrName = includeIntSvrName;
	}
	public boolean isLabelNodeStartsGroup() {
		if (!labelNodeStartsGroup) {
			labelNodeStartsGroup = (Boolean)processModel.getProperty(LABEL_NODE_STARTS_GROUP); // 'false' is default, ProcessModel can override
		}
		return labelNodeStartsGroup;
	}
//	public void setLabelNodeStartsGroup(boolean labelNodeStartsGroup) {
//		this.labelNodeStartsGroup = labelNodeStartsGroup;
//	}
	public boolean isFromSubFlow() {
		return subflowName != null;
	}
	public boolean isFromMsgFlow() {
		return subflowName == null;
	}
	/**
	 * Checks if the current event is from a node that is a successor to the previous node
	 *  
	 * @param previousEvent
	 * @return
	 */
	public boolean isSuccessorToPreviousEvent(MonitoringEvent previousEvent) {
		ActivityModel currentActivity = getActivityModel();
		boolean isSuccessor = false;
		if (currentActivity != null) {
			isSuccessor = currentActivity.getPredecessors().stream()  //containsKey(previousEvent.getLocalNodeLabel());
				.anyMatch( am -> am.getName().equals(previousEvent.getLocalNodeLabel()));	
		} 

		return isSuccessor;
	}
	
	/**
	 * <b>Note</b>: this event can be from > 1 subflow level from the 'event' arg<br>
	 * <br
	 * Use to determine if this subflow (sf_b) is an immediate child of the flow the published the event (sf_a).<br>
	 * When this is true:<br>
	 *   Previous Event will have nodeLabel = sf_b.sf_a.node<br> 
	 *   This event will have nodeLabel = sf_a.node<br>
	 * Test that 'sf_b.sf_a.node' contains 'sf_a.'<br>
	 *   
	 * @param event
	 * @return
	 */
	public boolean isChildOfSubflow(MonitoringEvent event) {
		if (event.isFromMsgFlow()) {
			return true;
		}
		if (this.isFromMsgFlow() && event.isFromSubFlow()) {
			return false;
		}
		// Note: use removeEnd() in case the the node and subflow have the same name
//		String eventLocalNodeLabel = event.getLocalNodeLabel(); => intermediate variables used for debugging.
//		String eventNodeLabel = event.getNodeLabel();
//		String nodeLabelRemoved = StringUtils.removeEnd(localNodeLabel, eventNodeLabel); // Removes a substring only if it is at the end of a source string, otherwise returns the source string
//		boolean isIt = nodeLabel.contains(nodeLabelRemoved);
		return nodeLabel.contains(StringUtils.removeEnd(event.getNodeLabel(), event.getLocalNodeLabel()));		
	}
	
	@Override
	public String toString() {
		toString = new StringBuilder("\nUniqueFlowName:\n ").append(uniqueflowName).append("\n node label:").append(nodeLabel).append(", #:").append(counter);
		return toString.toString();
	}
	public int getFlowLevel() {
		if (flowLevel == 0) {
			if (isFromSubFlow()) {
				String nodeName = StringUtils.substringBeforeLast(this.eventName, DOT); // value before '.terminal-name'				
				String[] subflowParts = StringUtils.substringBefore(nodeLabel, DOT+nodeName).split(REGEX_DOT);
				flowLevel = subflowParts.length + 1;
			} else {
				flowLevel = 1;
			}
		}
		return flowLevel;
	}
	

}
