package com.w3p.im.iib.mon.topology.model;

import com.w3p.api.config.proxy.ServiceProxy;

public class Service extends Deployable { //AbstractTopology {
	
	public Service(ServiceProxy sp) {
		super(sp);
	}
	
	@Override
	public String toString() {
		return "Service [name=" + name + "]";
	}
}
