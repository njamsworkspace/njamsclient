package com.w3p.im.iib.mon.jms.producers;

import static com.w3p.im.iib.mon.client.constants.IClientConstants.*;

import java.io.Serializable;
import java.util.Hashtable;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.w3p.im.iib.mon.event.processors.FlowToProcessModelCacheRun;


public class FlowToProcessModelCache implements Serializable {
	
	private static final long serialVersionUID = 1L;

	private static final Logger logger = LoggerFactory.getLogger(FlowToProcessModelCache.class);
	
//	private final Hashtable<Path, ProcessModel> allProcessModels = new Hashtable<>();	
//	private final Hashtable<String, ProcessModel> flowToProcessModel = new Hashtable<>(); 
	private final Hashtable<String, List<String>> flowToPath = new Hashtable<>();   // msg/subFlow name : PM Path as List
	private final Hashtable<String, String> flowToName = new Hashtable<>(); // msg/sub Flow name : PM Name
	
	private FlowToProcessModelCacheRun flowToProcessModelCacheRun;


	/**
	 * Used for <b>subflows</b> where the ProcessModel is not available at creation time.
	 * <br>It will found on first use at runtime and cached.
	 * <br>If in a static lib, its parent name must be concatenated to the flow name 
	 * @param flowName
	 * @param processPathParts
	 * @param isStaticLib
	 */
	public void addPathForFlow(String flowName, List<String> processPathParts) {
		flowToPath.put(flowName, processPathParts);
		if (processPathParts.size() == 2) {
			flowToName.put(flowName, processPathParts.get(processPathParts.size()-1));
		} else { // Static Lib used
			List<String> nameParts = processPathParts.subList(1, 3); // processPathParts.size()));			
			flowToName.put(flowName, nameParts.get(0).concat(".").concat(nameParts.get(1))); // TODO Check that 2-levels only for static ib
		}
	}

	public List<String> getPathForFlowAsList(String flowName) {
		return flowToPath.get(flowName);
	}
	
	public Hashtable<String, List<String>> getFlowToPath() {
		return flowToPath;
	}

	public boolean isEmpty() {
		return flowToPath.isEmpty();
	}

	public FlowToProcessModelCacheRun getFlowToProcessModelCacheRun() {
		return flowToProcessModelCacheRun;
	}

	public void setFlowToProcessModelCacheRun(FlowToProcessModelCacheRun flowToProcessModelCacheRun) {
		this.flowToProcessModelCacheRun = flowToProcessModelCacheRun;
	}

}
