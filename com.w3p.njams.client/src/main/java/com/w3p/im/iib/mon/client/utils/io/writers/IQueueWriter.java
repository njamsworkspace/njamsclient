package com.w3p.im.iib.mon.client.utils.io.writers;

import com.w3p.im.iib.mon.client.utils.io.data.IWriteQueueRequest;
import com.w3p.im.iib.mon.client.utils.io.data.Response;

public interface IQueueWriter extends IWriter {
	
	void init(IWriteQueueRequest req, Response resp);

}
