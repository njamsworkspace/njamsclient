package com.w3p.im.iib.mon.topology.model;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.w3p.api.config.proxy.ConfigManagerProxyPropertyNotInitializedException;
import com.w3p.api.config.proxy.FlowProxy;

public class Flow extends AbstractTopology {	
	private static final Logger logger = LoggerFactory.getLogger(Flow.class);
	
	protected final FlowProxy fp;
	
	protected boolean includePayload;
	protected boolean labelNodeStartsGroup;
	protected final List<FlowNode> flowNodes = new ArrayList<>();
	protected final Set<String> nodeIconNames = new HashSet<>(); // The names of the IIB Node types used in the topology  
	protected String flowName; // value depends on 'includeSchemaName'
	
	protected 	String nameInFlow; // The 'external' name used for the subflow node in its parent msgflow / subflow . Used in Monitoring Profile creation
	
	protected FlowNode inputNode; // Assumed to be the non-Label node without an input connection
	protected final List<FlowNode> startNodes = new ArrayList<>();
	
	
	public Flow(FlowProxy fp) {
		this.fp = fp;
		try {
			super.name = fp.getName();
		} catch (ConfigManagerProxyPropertyNotInitializedException e) {
			super.name = null;
			e.printStackTrace(); // TODO can we avoid throwing exception in a constructor ADD a populate() or init() kind of method.
		}
		// Remove any Broker Schema values for the Flow
		if (!includeSchemaName && StringUtils.contains(name, DOT)) {
			flowName = StringUtils.substringAfterLast(name, DOT);
		} else {
			flowName = name;
		}
	}

	public boolean isIncludePayload() {
		return includePayload;
	}

	public void setIncludePayload(boolean includePayload) {
		this.includePayload = includePayload;
	}

	public boolean isLabelNodeStartsGroup() {
		return labelNodeStartsGroup;
	}

	public void setLabelNodeStartsGroup(boolean labelNodeStartsGroup) {
		this.labelNodeStartsGroup = labelNodeStartsGroup;
	}

	public FlowProxy getFlowProxy() {
		return fp;  //TODO use FlowProxy and cast?
	}

	@Override
	public String getName() {
		if (includeSchemaName) {
			return super.name;
		}
		return flowName;
	}
	
	@Override
	public <T extends AbstractTopology> void addTopologyItem(T topologyItem) {
		// TODO use for subflows?		
	}

	/**
	 * This is to be used when subscribing to published events.<br> It returns schema1.schema2.flowName
	 * @return
	 */
	public String getNameWithSchema() {
		return super.name;
	}

	public List<FlowNode> getFlowNodes() {
		return flowNodes;
	}
	
	public void setFlowNodes(List<FlowNode> messageFlows) {
		this.flowNodes.clear();
		this.flowNodes.addAll(messageFlows);
	}
	
	public FlowNode getInputNode() {
		// This has to assume that the input node is the non-Label node without an input
		if (inputNode == null) {
			List<FlowNode> startNodes = getStartNodes();
			for (FlowNode flowNode : startNodes) {
				if (!flowNode.getType().equals("ComIbmLabelNode")) {
					inputNode = flowNode;
					return inputNode;
				}
			}
		}
		return inputNode;
	}

	public List<FlowNode> getStartNodes() {
		// These will consist of: 
		// The actual input node:
		// 	For a msg flow: the one that is not a target of a FlowNodeConnection
		// 	For a sub flow: the one that is the target of the InputNode
		// Plus any Label nodes:  these will become successors to the Start node in the nJAMS Process Model 
		if (startNodes.isEmpty()) {
			List<String> targetNodeNames = getConnectionTargetNames(); //FIXME if target-nodes is empty, return empty start nodes.
			if (!targetNodeNames.isEmpty()) {
			for (FlowNode flowNode : flowNodes) {
				if (flowNode.getType().equals("InputNode")) { // Note: only applies to subflows
					List<FlowNodeConnection> flowNodeConnections = flowNode.getFlowNodeConnections();
					for (FlowNodeConnection fnc : flowNodeConnections) {
						// An InputNode should only have 1 connection, but...
						FlowNode inputNode = getNodeByName(fnc.getTargetNode());
						startNodes.add(inputNode);
					}
				} else if (!targetNodeNames.contains(flowNode.getName())) {
					startNodes.add(flowNode);
				}
			}
			} else {
				logger.warn(flowName+" does not have any connections between nodes - its Process Model will not be created.");
			}
		}
		return startNodes;
	}
	

	/**
	 * This is only for use in GenerateMonitoringProfiles.groovy.<br>
	 * In effect, it provides temporary storage for when a subflow has a different name in a flow, from its underlying name
	 * @return
	 */
	public String getNameInFlow() {
		return nameInFlow;
	}

	/**
	 * This is only for use in GenerateMonitoringProfiles.groovy.<br>
	 * In effect, it provides temporary storage for when a subflow has a different name in a flow, from its underlying name 
	 * @param nameInFlow
	 */
	public void setNameInFlow(String nameInFlow) {
		this.nameInFlow = nameInFlow;
	}
	
	protected List<String> getConnectionTargetNames() {
		List<String> targetNodeNames = new ArrayList<>();
		for (FlowNode flowNode : flowNodes) {
			for (FlowNodeConnection conn : flowNode.getFlowNodeConnections()) {
				if (!targetNodeNames.contains(conn.getTargetNode())) {
					targetNodeNames.add(conn.getTargetNode()); // Don't create duplicates
				}
			}
		}
		return targetNodeNames;
	}
	
	public FlowNode getNodeByName(String name) {
		if (flowNodes == null) {
			return null;
		} else {
			for (FlowNode flowNode : flowNodes) {
				if (flowNode.getName().equals(name)) {
					return flowNode;
				}
			}
		}
		return null; // Not in list
	}

	public Set<String> getNodeIconNames() {
		if (nodeIconNames.isEmpty()) {
			for (FlowNode flowNode : flowNodes) {				
				nodeIconNames.add(flowNode.getType());
			}
		}
		return nodeIconNames;
	}

	@Override
	public boolean isRunning() {
		return true; //Not relevant for Flow as not  method in FlowProxy
	}

	@Override
	public String toString() {
		return "MessageFlow [name=" + flowName + "]";
	}
}
