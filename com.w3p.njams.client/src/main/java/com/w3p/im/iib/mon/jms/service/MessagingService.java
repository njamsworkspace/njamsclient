package com.w3p.im.iib.mon.jms.service;

import java.util.Hashtable;

import javax.jms.Connection;
import javax.jms.ConnectionFactory;
import javax.jms.JMSException;
import javax.naming.Context;
import javax.naming.NamingException;
import javax.naming.directory.InitialDirContext;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.w3p.im.iib.mon.client.config.data.User;
import com.w3p.im.iib.mon.exceptions.internal.MessagingException;
import com.w3p.im.iib.mon.jms.data.IMessagingService;




/**
 * @author JOrmerod
 *
 */
public class MessagingService implements IMessagingService {
	protected static final Logger logger = LoggerFactory.getLogger(MessagingService.class);

	// Initial Context Factory for JNDI 
	private final String INITIAL_CONTEXT_FACTORY; // = "com.sun.jndi.fscontext.RefFSContextFactory";
	private final String JMS_CONNECTION_FACTORY;
	private final String PROVIDER_URL; // Folder location of .bindings file for MQ, tcp address for ActiveMQ 
	private final User JMS_USER;
	private final String JMS_CLIENT_ID; // required for subscriptions
	
	
	protected Context context; // java naming context
	// JMS Connection is thread-safe and can be shared - unlike Session which must be single-threaded
	protected Connection connection;


	public MessagingService(MqJmsConnectionProperties connectionProperties) {
		logger.trace("enter");
		JMS_CONNECTION_FACTORY = connectionProperties.getMqJmsConnectionFactory();
		INITIAL_CONTEXT_FACTORY = connectionProperties.getMqJmsInitialContextFactory();
		PROVIDER_URL = connectionProperties.getMqJmsProviderUrl();
		JMS_USER = connectionProperties.getMqJmsUser();
		JMS_CLIENT_ID = connectionProperties.getJmsClientId();
		logger.trace("exit");
	}

	/**
	 * @throws MessagingException
	 */
	public void createConnection(String dest) throws MessagingException {
		logger.trace("enter");
		if (connection == null){
			String jmsClientId = "";
			try {				
				if (JMS_CLIENT_ID != null && !JMS_CLIENT_ID.isEmpty()) {
					jmsClientId = JMS_CLIENT_ID+dest;
				}
				logger.info("Creating connection for User: name={}, clientId={}.", JMS_USER.getUserName(), jmsClientId);
				Hashtable<String, String> environment = new Hashtable<String, String>();
				environment.put(Context.INITIAL_CONTEXT_FACTORY, INITIAL_CONTEXT_FACTORY);
				environment.put(Context.PROVIDER_URL, PROVIDER_URL);
				context = new InitialDirContext(environment);
				// Lookup the connection factory
				ConnectionFactory cf = (ConnectionFactory)context.lookup(JMS_CONNECTION_FACTORY);
				// Create JMS objects
				connection = cf.createConnection(JMS_USER.getUserName(), JMS_USER.getPasswordInClear());
				if (jmsClientId != null && !jmsClientId.isEmpty()) {
					connection.setClientID(jmsClientId);
				}
				logger.info("Connection created for User: name={}, clientId={}.", JMS_USER.getUserName(), jmsClientId);
			} catch (NamingException e) {
				throw new MessagingException("JNDI naming error for JMS ConnectionFactory: ''{0}''. Cause: ''{1}''.", JMS_CONNECTION_FACTORY, e.toString(), e);
			} catch (JMSException e) {
				throw new MessagingException("JMS exception thrown. Cause: ''{0}''. ClientId=''{1}''.", e.toString(), jmsClientId, e);
			} catch (Exception e) {
				throw new MessagingException("Unhandled exception thrown. Cause: ''{0}''.", e.toString(), e);
			}
		}
		logger.trace("exit");
	}

	/**
	 * 
	 */
	public void close() {
		logger.trace("enter");
		try {
			connection.close();
			connection = null;
		} catch (JMSException e) {
			// Log the error, can't do much about it
//			logger.error((new UserMsg("I003E", new String[]{e.getMessage()}, UserMsg.SEVERITY_ERROR)).toString(), e);
			logger.error(e.toString(), e);
		}
		logger.trace("exit");
	}
}
