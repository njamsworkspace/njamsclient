package com.w3p.im.iib.mon.jms.producers;

import static com.w3p.im.iib.mon.client.constants.IClientConstants.DOT;
import static com.w3p.im.iib.mon.client.constants.IClientConstants.DOT_PNG;
import static com.w3p.im.iib.mon.client.constants.IClientConstants.END_ACTIVITY_MODEL_ID;
import static com.w3p.im.iib.mon.client.constants.IClientConstants.END_ACTIVITY_NAME;
import static com.w3p.im.iib.mon.client.constants.IClientConstants.END_ACTIVITY_TYPE;
import static com.w3p.im.iib.mon.client.constants.IClientConstants.GROUP_END_MODEL_ID;
import static com.w3p.im.iib.mon.client.constants.IClientConstants.GROUP_END_NAME;
import static com.w3p.im.iib.mon.client.constants.IClientConstants.GROUP_END_TYPE;
import static com.w3p.im.iib.mon.client.constants.IClientConstants.GROUP_NAME_PREFIX;
import static com.w3p.im.iib.mon.client.constants.IClientConstants.GROUP_START_MODEL_ID;
import static com.w3p.im.iib.mon.client.constants.IClientConstants.GROUP_START_NAME;
import static com.w3p.im.iib.mon.client.constants.IClientConstants.GROUP_START_TYPE;
import static com.w3p.im.iib.mon.client.constants.IClientConstants.GROUP_TYPE;
import static com.w3p.im.iib.mon.client.constants.IClientConstants.GT;
import static com.w3p.im.iib.mon.client.constants.IClientConstants.IMAGES;
import static com.w3p.im.iib.mon.client.constants.IClientConstants.INCLUDE_SCHEMA_NAME;
import static com.w3p.im.iib.mon.client.constants.IClientConstants.INCLUDE_SERVER_NAME;
import static com.w3p.im.iib.mon.client.constants.IClientConstants.LABEL_NODE;
import static com.w3p.im.iib.mon.client.constants.IClientConstants.LABEL_NODE_STARTS_GROUP;
import static com.w3p.im.iib.mon.client.constants.IClientConstants.MQ_OUTPUT_NODE;
import static com.w3p.im.iib.mon.client.constants.IClientConstants.OUTPUT_NODE;
import static com.w3p.im.iib.mon.client.constants.IClientConstants.PUB_SUB_NODE;
import static com.w3p.im.iib.mon.client.constants.IClientConstants.ROUTE_TO_LABEL_NODE;
import static com.w3p.im.iib.mon.client.constants.IClientConstants.START_ACTIVITY_MODEL_ID;
import static com.w3p.im.iib.mon.client.constants.IClientConstants.START_ACTIVITY_NAME;
import static com.w3p.im.iib.mon.client.constants.IClientConstants.START_ACTIVITY_TYPE;
import static com.w3p.im.iib.mon.client.constants.IClientConstants.STEP_TYPE;
import static com.w3p.im.iib.mon.client.constants.IClientConstants.SUBPROCESS;
import static com.w3p.im.iib.mon.client.constants.IClientConstants.SUB_FLOW_NODE;

import java.io.File;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.im.njams.sdk.Njams;
import com.im.njams.sdk.model.ActivityModel;
import com.im.njams.sdk.model.GroupModel;
import com.im.njams.sdk.model.ProcessModel;
import com.im.njams.sdk.model.SubProcessActivityModel;
import com.im.njams.sdk.model.image.FileImageSupplier;
import com.w3p.api.config.proxy.ConfigManagerProxyPropertyNotInitializedException;
import com.w3p.api.config.proxy.StaticLibraryProxy;
import com.w3p.im.iib.mon.exceptions.internal.NJAMSConnectionException;
import com.w3p.im.iib.mon.exceptions.internal.NJAMSTopologyCreationException;
import com.w3p.im.iib.mon.jms.producers.layout.IIBLayouter;
//import com.w3p.im.iib.mon.topology.changes.ChangeListenerFactory;
import com.w3p.im.iib.mon.topology.data.TopologyForNjamsRequest;
import com.w3p.im.iib.mon.topology.data.TopologyForNjamsResponse;
import com.w3p.im.iib.mon.topology.model.AbstractTopology;
import com.w3p.im.iib.mon.topology.model.Application;
import com.w3p.im.iib.mon.topology.model.Flow;
import com.w3p.im.iib.mon.topology.model.FlowNode;
import com.w3p.im.iib.mon.topology.model.IntegrationServer;
import com.w3p.im.iib.mon.topology.model.MessageFlow;
import com.w3p.im.iib.mon.topology.model.RestApi;
import com.w3p.im.iib.mon.topology.model.Service;
import com.w3p.im.iib.mon.topology.model.SharedLibrary;
import com.w3p.im.iib.mon.topology.model.StaticLibrary;
import com.w3p.im.iib.mon.topology.model.SubFlow;
import com.w3p.im.iib.mon.topology.model.TopologyObject;

public class TopologyProducerForNjams {
	private static final Logger logger = LoggerFactory.getLogger(TopologyProducerForNjams.class);
	
	private final TopologyForNjamsRequest request;

	// Avoid the creation of duplicate 'transitionToProcessEnd()' from 'end' nodes 
	private final Map<String, Boolean> nodeTransitionedToProcessEnd = new HashMap<String, Boolean>();
	private final List<String> processedTransitions = new ArrayList<>(); // Avoid re-processing a transition

	

	public TopologyProducerForNjams(TopologyForNjamsRequest request) {
		this.request = request;		
	}

	public TopologyForNjamsResponse createProcessModels() throws NJAMSTopologyCreationException, NJAMSConnectionException  {		
		logger.trace("enter");
		try {
			TopologyForNjamsResponse response = new TopologyForNjamsResponse();		
			TopologyObject topology = request.getTopologyObject();
			IntegrationServer intServer = topology.getIntegrationServer(request.getIntServerName());

			if (intServer != null) {
				String intSvrName = intServer.getName();
				intServer.setIncludeSvrName(request.isIncludeIntSvrName());
				// Cache the Process Model PATH for each Flow => this will be passed to the Event handling, to avoid overhead of discovering the PM for a (sub)flow				
				FlowToProcessModelCache flowToProcessModelCache = new FlowToProcessModelCache();

				Njams njamsClient = request.getNjamsClient(); 

				IIBLayouter layouter = new IIBLayouter(150, 100, 0, 0); // TODO - externalise in config
				njamsClient.setProcessModelLayouter(layouter);

				addIconsForNodesUsed(request.getClientHome(), intServer, njamsClient); //, project);
				StringBuilder pathToIntSvr = new StringBuilder(); // FIXME for IIB and ACE - not used - see SDK1 client
				if (request.isIncludeIntSvrName()) {
					pathToIntSvr.append(intSvrName).append(GT);
				}

				for (SharedLibrary lib : intServer.getSharedLibraries()) {
					addFlowTopologyToProcessModel(njamsClient, intServer, lib, flowToProcessModelCache);
				}
				
				for (Application app : intServer.getApplications()) {
					addFlowTopologyToProcessModel(njamsClient, intServer, app, flowToProcessModelCache);
					for (StaticLibrary lib : app.getStaticLibraries()) {
						addFlowTopologyToProcessModel(njamsClient, intServer, lib, flowToProcessModelCache);
					}
				}
				for (RestApi api : intServer.getRestApis()) {
					addFlowTopologyToProcessModel(njamsClient, intServer, api, flowToProcessModelCache);
					for (StaticLibrary lib : api.getStaticLibraries()) {
						addFlowTopologyToProcessModel(njamsClient, intServer, lib, flowToProcessModelCache);
					}
				}
				for (Service service : intServer.getServices()) {
					addFlowTopologyToProcessModel(njamsClient, intServer, service, flowToProcessModelCache);
					for (StaticLibrary lib : service.getStaticLibraries()) {
						addFlowTopologyToProcessModel(njamsClient, intServer, lib, flowToProcessModelCache);
					}
				}

				if (!flowToProcessModelCache.isEmpty()) {
					response.setFlowToProcessModelCache(flowToProcessModelCache);
					response.setMessage(String.format("Process Models for Integration Server '%s' were created.", intSvrName));
					response.setSuccess(true);
				} else {
					response.setMessage(String.format("No message flows were requested for '%s'. Monitoring was not established.", intSvrName));
					response.setSuccess(false);
				}
			} else {
				// Report 'nothing requested'			
				response.setMessage(String.format("No integration servers were in the request. Monitoring was not established."));
				response.setSuccess(false);
				logger.warn(response.getMessage());			
			}
			logger.trace("exit");
			return response;
		} catch (Exception e) {
			// Catch any possible exception thrown by IIB's ApplicationApi or nJAMS' SDK
			if (e.getClass().isAssignableFrom(NJAMSConnectionException.class)) {
				// This has caught the exception thrown above
				throw (NJAMSConnectionException)e;
			}			
			throw new NJAMSTopologyCreationException("Error when creating the nJAMS Process Model. Cause is ''{0}''.", e.toString(), e);
		}
	}
	
	private void addFlowTopologyToProcessModel(Njams njamsClient, IntegrationServer intServer, AbstractTopology topologyItem, FlowToProcessModelCache flowToProcessModelCache)
			throws NJAMSTopologyCreationException, ConfigManagerProxyPropertyNotInitializedException {	
		
		for (MessageFlow mf : topologyItem.getMessageFlows()) {
			ProcessModel pm = createFlowTopology(njamsClient, intServer, topologyItem, mf, flowToProcessModelCache);
			setProperties(pm, mf, intServer); 
			// Add any subflows in the context of its parent flow - getting the name of the underlying subflow
			addAnySubflowsToFlowProcessModelCache(mf, pm, topologyItem, flowToProcessModelCache); // This adds the *subflow node* (not the subflow itself) with name of actual subflow (not local name)
		}
		for (SubFlow sf : topologyItem.getSubFlows()) {
			if (!sf.getStartNodes().isEmpty()) {  // Do not add subflows without any nodes
				ProcessModel pm = createFlowTopology(njamsClient, intServer, topologyItem, sf, flowToProcessModelCache);
				setProperties(pm, sf, intServer);
				// Add any subflows in the context of its parent subflow
				addAnySubflowsToFlowProcessModelCache(sf, pm, topologyItem, flowToProcessModelCache);
			}
		}
	}


	/**
	 * Pass the msgflow and subflow options as specified in the startup config xml file, as Process Model properties
	 * @param pm
	 * @param flow
	 * @param intServer 
	 */
	private void setProperties(ProcessModel pm, Flow flow, IntegrationServer intServer) {
		pm.setProperty(INCLUDE_SCHEMA_NAME, flow.isIncludeSchemaName()); //String.valueOf(flow.isIncludeSchemaName()));		
		pm.setProperty(LABEL_NODE_STARTS_GROUP, flow.isLabelNodeStartsGroup()); //String.valueOf(flow.isLabelNodeStartsGroup()));
		pm.setProperty(INCLUDE_SERVER_NAME, intServer.isIncludeSvrName()); //String.valueOf(intServer.isIncludeSvrName()));
	}


	/**
	 * @param mf
	 * @param pm
	 * @param flowProcessModelCache
	 */
	private void addAnySubflowsToFlowProcessModelCache(Flow flow, ProcessModel pm, AbstractTopology topologyItem, FlowToProcessModelCache flowToProcessModelCache) {
		// Save the name of the underlying subflow (not the same as its 'local' name in the enclosing flow
		List<String> clientPathAsList = pm.getNjams().getClientPath().getParts();
		for (ActivityModel am : pm.getActivityModels()) {
			if (am.getType().equals(SUB_FLOW_NODE)) { //SubProcessActivityModel.class.isAssignableFrom(am.getClass())) {
				// remove ClientPath from full Path.
				List<String> subProcessPathAsList = ((SubProcessActivityModel)am).getSubProcessPath().getParts(); // Full Monty
				subProcessPathAsList.removeAll(clientPathAsList);
				String subFlowPath = flow.getName()+DOT+am.getName();
				flowToProcessModelCache.addPathForFlow(subFlowPath, subProcessPathAsList);
			}
		}
	}
	
	private ProcessModel createFlowTopology(Njams njamsClient, IntegrationServer intServer, AbstractTopology topologyItem, Flow flow, FlowToProcessModelCache flowToProcessModelCache)
		throws NJAMSTopologyCreationException, ConfigManagerProxyPropertyNotInitializedException {
		
		String deployableParent = null;
		if (topologyItem instanceof StaticLibrary) {
			StaticLibraryProxy slp = ((StaticLibrary) topologyItem).getStaticLibraryProxy();
			String uri = slp.getUri(); // e.g /apiv2/servers/njamsDev/applications/TestStaticLibApp/libraries/TestStaticLib
			String preLibrary = StringUtils.substringBefore(uri, "/libraries"); // e.g /apiv2/servers/njamsDev/applications/TestStaticLibApp
			deployableParent = StringUtils.substringAfterLast(preLibrary, "/"); // e.g TestStaticLibApp
		}
	
		
		ProcessModel pm = null;
		List<String> processPathList = null;
		if (deployableParent == null) {
			processPathList = Arrays.asList(topologyItem.getName(), flow.getName());
		} else { // Static Lib
			processPathList = Arrays.asList(deployableParent, topologyItem.getName(), flow.getName());
		}
		com.im.njams.sdk.common.Path processPath = new com.im.njams.sdk.common.Path(processPathList);

		try {
			nodeTransitionedToProcessEnd.clear();
			processedTransitions.clear();			

			FlowNode startNode = flow.getInputNode();
			if (startNode != null) {
				logger.info("#createFlowTopology() - start creation of ProcessModel named: "+processPath.toString());			
				pm = njamsClient.createProcess(processPath); // This concatenates the processPath with the (fixed) clientPath
				ActivityModel startModel = pm.createActivity(START_ACTIVITY_MODEL_ID, START_ACTIVITY_NAME, START_ACTIVITY_TYPE);
				startModel.setStarter(true);
				flowToProcessModelCache.addPathForFlow(flow.getName(), processPathList);

				// Create the Process model starting from the input node
				createNextActivity(njamsClient, startModel, startNode, intServer, topologyItem, flow, false); // This calls itself recursively
				List<FlowNode> startNodes = flow.getStartNodes();
				startNodes.remove(startNode); // This has been processed
				for (FlowNode labelNode : startNodes) {
					// If there are any 'orphaned' Label nodes, we need to add them to the Process model.
					createNextActivity(njamsClient, startModel, labelNode, intServer, topologyItem, flow, false);
				}
			} else {
				logger.warn(flow.getName()+"does not have any nodes - it will not be crearted as a Process Model");
			}
		} catch (Exception e) {
			String cause = e.toString() + ", Flow path: " + processPath.toString();
			throw new NJAMSTopologyCreationException("Error when creating the nJAMS Process Model. Cause is ''{0}''.", cause, e);
		}			

	
		return pm;
	}


	/**
	 * This uses the current IIB node to create the current Acitvity as a transition from the previous activity
	 *
	 * @param njamsClient
	 * @param previousActivity
	 * @param currentNode
	 * @param intServer
	 * @param topologyItem
	 * @param flow
	 * @param inGroup
	 */
	private void createNextActivity(Njams njamsClient, ActivityModel previousActivity, FlowNode currentNode, IntegrationServer intServer, AbstractTopology topologyItem, Flow flow, boolean inGroup) {

		logger.trace("+ Entry: createNextActivity. previousActivity: {}, currentNode: {}, topologyItem:{}, flow: {}, inGroup: {}",
				previousActivity.toString(), currentNode == null ? null : currentNode.toString(), topologyItem.toString(), flow.toString(), inGroup);
		ActivityModel currentActivity = null;				

		// A Label node and its successors* is assumed to form a Group, as it has the potential to be invoked in a loop.
		// 	It will only do so if 'isLabelNodeStartsGroup' = true in the Client Properties XML file
		if (currentNode.getType().equals(LABEL_NODE) && flow.isLabelNodeStartsGroup()) {
			currentActivity = createGroup(previousActivity, currentNode, processedTransitions);
			inGroup = true;
		} else if (inGroup) {
			if(doesCurrentNodeHaveMultiplePredecessors(currentNode, processedTransitions)) {
				previousActivity = handleCurrentNodeWithMultiplePredecessors(previousActivity, currentNode, processedTransitions, inGroup);
			} else if (currentNode.getType().equals(SUB_FLOW_NODE)) {  // Create the SubProcess Activity and its Path
				if (!processedTransitions.contains(createTransitionName(previousActivity, currentNode.getName()))) {
					currentActivity = createSubProcessActivity(njamsClient, intServer, topologyItem, previousActivity, currentNode);
				}
			} else {
				currentActivity = createTransition(previousActivity, currentNode);
			}
		} else if (currentNode.getType().equals(SUB_FLOW_NODE)) {  // Create the SubProcess Activity and its Path
			if (!processedTransitions.contains(createTransitionName(previousActivity, currentNode.getName()))) {
				currentActivity = createSubProcessActivity(njamsClient, intServer, topologyItem, previousActivity, currentNode);
			}
		}
		else { // Normal node i.e // Not a Label, not 'inGroup', not a Subflow node
			currentActivity = createTransition(previousActivity, currentNode); 
		}
		
		int xVal = currentNode.getX();
		int xRoundUp = (int)Math.ceil((double)xVal / 50d)*50;
		currentActivity.setX(xRoundUp);
		
		int yVal = currentNode.getY();
		int yRoundDown = (int)Math.floor((double)yVal / 50d)*50;
		
		currentActivity.setY(yRoundDown); // njams uses negative values to display down the page
		
		if (!currentNode.getFlowNodeConnections().isEmpty()) {  // Current node has target nodes
			for (int i = 0; i < currentNode.getFlowNodeConnections().size(); i++) {
				FlowNode targetNode = flow.getNodeByName(currentNode.getFlowNodeConnections().get(i).getTargetNode());				
				if (currentNode == targetNode) {
					handleRecursiveConnections(currentNode, flow, currentActivity, i);
				} else if (currentNode.getType().equals(PUB_SUB_NODE) && targetNode.getType().equals(MQ_OUTPUT_NODE)) {
					handleTransition2End(currentActivity);
				} else if (targetNode.getType().equals(OUTPUT_NODE)) {					 
					handleEndOfsubflow(currentActivity, inGroup);
				} else if (!processedTransitions.contains(createTransitionName(currentActivity, targetNode.getName()))) {
					createNextActivity(njamsClient, currentActivity, targetNode, intServer, topologyItem, flow, inGroup); // Recurse to create the next node
				}
			}
		} else { 
			// FlowNodeConnections is empty - end of the connections = end of this recursion			
			if (inGroup) {				
				handleEndOfGroup(currentNode, currentActivity);
				inGroup = false;				
			} else if (!currentNode.getType().equals(ROUTE_TO_LABEL_NODE)) {
				// A RouteToLabel node is the end of a sequence and will never transition to End
				handleTransition2End(currentActivity);
			}
		}
		logger.trace("+ Exit: createNextActivity. previousActivity: {}", previousActivity.toString()); // TODO move down
	}


	private void handleEndOfGroup(FlowNode currentNode, ActivityModel currentActivity) {
		// Check whether the Group has a successor node - if not, then transition to End.
		if (!processedTransitions.contains(createTransitionName(currentActivity, currentNode.getName()))) {			
			ActivityModel endGroupActivity = currentActivity.transitionTo(GROUP_END_MODEL_ID, GROUP_END_NAME, GROUP_END_TYPE);
			if (!currentNode.getType().equals(ROUTE_TO_LABEL_NODE)) {
				handleTransitionFromGroup2End(endGroupActivity);
			}
		}
	}

	private void handleEndOfsubflow(ActivityModel currentActivity, boolean inGroup) {
		// This is the OutputNode of a subflow - remove from njams topology as it doesn't produce monitoring events
		if (inGroup) {
			// Transition to the end of the group
			ActivityModel groupEndActivity = currentActivity.transitionTo(GROUP_END_MODEL_ID, GROUP_END_NAME, GROUP_END_TYPE);
			processedTransitions.add(createTransitionName(currentActivity, GROUP_END_NAME));
			// Transition from the group to the the end of the process
			groupEndActivity.getParent().transitionTo(END_ACTIVITY_MODEL_ID, END_ACTIVITY_NAME, END_ACTIVITY_TYPE);
			processedTransitions.add(createTransitionName(groupEndActivity, END_ACTIVITY_NAME));
		} else {
			handleTransition2End(currentActivity);
		}
	}

	/**
	 * Handle a recursive connection: current and target nodes are the same. 
	 * 
	 * @param currentNode
	 * @param flow
	 * @param currentActivity
	 * @param i
	 */
	private void handleRecursiveConnections(FlowNode currentNode, Flow flow, ActivityModel currentActivity, int i) {
		// For PoC assume that it's an output node, such as a FileOutputNode, and there is no connection from the Out terminal. Hence make a transition to End
		logger.info("Found a node with a recursive connection. Flow: '{}', Node: '{}', Source terminal: '{}', Target terminal '{}'",
				flow.getName(), currentNode.getName(), currentNode.getFlowNodeConnections().get(i).getSourceOutTerminal(), currentNode.getFlowNodeConnections().get(i).getTargetInTerminal());
		handleTransition2End(currentActivity);
	}


	private ActivityModel createSubProcessActivity(Njams njamsClient, IntegrationServer intServer, AbstractTopology topologyItem, ActivityModel previousActivity, FlowNode currentNode) {

		SubProcessActivityModel subProcessActivity = previousActivity.transitionToSubProcess(currentNode.getName()
			.replaceAll(" ", "_").toLowerCase(), currentNode.getName(), currentNode.getType());		
		processedTransitions.add(createTransitionName(previousActivity, currentNode.getName()));
		// Define the subProcess path for the SubProcess - excludes the Client Path
//		String clientPathAsString = njamsClient.getClientPath().toString();
		com.im.njams.sdk.common.Path subProcessPath = getNjamsPathForSubflow(njamsClient.getClientPath(), currentNode, false);     //clientPathAsString, currentNode, false);
		// Set the subprocess Process Model on the subProcessActivityModel
		((SubProcessActivityModel)subProcessActivity).setSubProcess(currentNode.getName(), subProcessPath); // Alternative signature

		return subProcessActivity;
	}


	private ActivityModel createTransition(ActivityModel previousActivity, FlowNode currentNode) {
		ActivityModel currentActivity;
		currentActivity = previousActivity.transitionTo(currentNode.getName().replaceAll(" ", "_").toLowerCase(), currentNode.getName(), currentNode.getType());
		processedTransitions.add(createTransitionName(previousActivity, currentNode.getName()));
		return currentActivity;
	}


	private ActivityModel createGroup(ActivityModel previousActivity, FlowNode currentNode, List<String> processedTransitions) {
		// The previousActivity will be the Start activity
		String groupName = GROUP_NAME_PREFIX+currentNode.getName();
		GroupModel gm = previousActivity.transitionToGroup(groupName.replaceAll(" ",  "_").toLowerCase(), groupName, GROUP_TYPE); // non-existent image!
		processedTransitions.add(createTransitionName(previousActivity, gm.getName()));
        ActivityModel groupStartModel = gm.createChildActivity(GROUP_START_MODEL_ID, GROUP_START_NAME, GROUP_START_TYPE);
        groupStartModel.setStarter(true);

        // Transition to the Label node starting the group
		ActivityModel labelNodeActivity = groupStartModel.transitionTo(currentNode.getName().replaceAll(" ", "_").toLowerCase(), currentNode.getName(), currentNode.getType()); 
        processedTransitions.add(createTransitionName(gm, currentNode.getName()));
		previousActivity = groupStartModel; // Note: note really mecessary
		
		return labelNodeActivity;
	}
	
	private boolean doesCurrentNodeHaveMultiplePredecessors(FlowNode currentNode, List<String> processedTransitions) {
		boolean nodeHasTransitionsToIt = false;
		String currentNodeName = currentNode.getName();
		
		for (String processedTransition : processedTransitions) {
			if (processedTransition.endsWith(currentNodeName)) { //contains(currentNodeName)) {
				nodeHasTransitionsToIt = true;
				break;
			}
		}
		
		return nodeHasTransitionsToIt;
	}


	private ActivityModel handleCurrentNodeWithMultiplePredecessors(ActivityModel previousActivity, FlowNode currentNode, List<String> processedTransitions, boolean inGroup) {		
		// We need to check that the current node has not been the target of another transition.
		// If it has, then it will not be considered as part of the Group. The Group should now end and transition to the current node

		ActivityModel endGroupActivityModel = previousActivity.transitionTo(GROUP_END_MODEL_ID, GROUP_END_NAME, GROUP_END_TYPE);
		processedTransitions.add(createTransitionName(previousActivity, endGroupActivityModel.getName()));
		ActivityModel currentActivity = endGroupActivityModel.getParent().transitionTo(currentNode.getName().replaceAll(" ",  "_").toLowerCase(), currentNode.getName(), currentNode.getType());
		processedTransitions.add(createTransitionName(currentActivity, endGroupActivityModel.getName()));
		previousActivity = endGroupActivityModel;
		inGroup = false;		

		return currentActivity;
		
	}

	private com.im.njams.sdk.common.Path getNjamsPathForSubflow(com.im.njams.sdk.common.Path clientPath, FlowNode currentNode, boolean includeSchemaName) {
		com.im.njams.sdk.common.Path sfPath = currentNode.getSubflowPath(includeSchemaName);
		com.im.njams.sdk.common.Path subProcessPath = clientPath.add(sfPath); 
				
		return subProcessPath;
	}


	private String createTransitionName(ActivityModel currentActivity, String targetNodeName) {
		return currentActivity.getName()+GT+targetNodeName;
	}

	private void handleTransition2End(ActivityModel currentActivity) {
		if (nodeTransitionedToProcessEnd.get(currentActivity.getName()) == null || !nodeTransitionedToProcessEnd.containsKey(currentActivity.getName())) { // Avoid another transition to end 
			currentActivity.transitionTo(END_ACTIVITY_MODEL_ID, END_ACTIVITY_NAME, END_ACTIVITY_TYPE); // currentActivity.getName()+"_end", "End", "endType");
			nodeTransitionedToProcessEnd.put(currentActivity.getName(), true);
		}
	}
	
	private void handleTransitionFromGroup2End(ActivityModel currentActivity) {
		if (nodeTransitionedToProcessEnd.get(currentActivity.getName()) == null || !nodeTransitionedToProcessEnd.containsKey(currentActivity.getName())) { // Avoid another transition to end 
			currentActivity.getParent().transitionTo(END_ACTIVITY_MODEL_ID, END_ACTIVITY_NAME, END_ACTIVITY_TYPE);
			nodeTransitionedToProcessEnd.put(currentActivity.getName(), true);
		}
	}


	private void addIconsForNodesUsed(String clientHome, IntegrationServer intServer, Njams njamsClient) { //, NjamsProject project) {
		Set<String> allIconNames = new HashSet<>(); // Only add each icon to the project once 
		allIconNames.add(SUBPROCESS); // A SubFlow node's type is changed to 'subprocess' when it can open the SubFlow
		// Add default images from the SDK
        njamsClient.addImage(START_ACTIVITY_TYPE, "images/njams_java_sdk_process_start.png");
        njamsClient.addImage(STEP_TYPE, "images/njams_java_sdk_process_step.png");
        njamsClient.addImage(END_ACTIVITY_TYPE, "images/njams_java_sdk_process_end.png");
        // Aadd Group images
        njamsClient.addImage(GROUP_START_TYPE, "images/njams_java_sdk_group_start.png");
        njamsClient.addImage(GROUP_END_TYPE, "images/njams_java_sdk_group_end.png");

		getUniqueNodeIconNames(allIconNames, intServer);
		for (Application app : intServer.getApplications()) {
			getUniqueNodeIconNames(allIconNames, app);
		}
		for (RestApi api : intServer.getRestApis()) {
			getUniqueNodeIconNames(allIconNames, api);
		}
		for (SharedLibrary lib : intServer.getSharedLibraries()) {
			getUniqueNodeIconNames(allIconNames, lib);
		}
		for (StaticLibrary lib : intServer.getStaticLibraries()) {
			getUniqueNodeIconNames(allIconNames, lib);
		}
        	
		for (String iconName : allIconNames) { // TODO Improve this
			Path imgLoc = Paths.get(clientHome, IMAGES, iconName+DOT_PNG);
			if (Files.exists(imgLoc)) {
				File image = new File(IMAGES, iconName+DOT_PNG);
		        FileImageSupplier fis = new FileImageSupplier(iconName, image);
		        njamsClient.addImage(fis);
			} else {
				logger.info("No icon file found for node type: {}", iconName);
			}
		}

	}

	private void getUniqueNodeIconNames(Set<String> allIconNames, AbstractTopology topologyItem) {
		for (MessageFlow mf : topologyItem.getMessageFlows()) {
			allIconNames.addAll(mf.getNodeIconNames());
			logger.debug("MsgFlow {} uses these node types: {}", mf.getName(), mf.getNodeIconNames().toArray());
		}
		for (SubFlow sf : topologyItem.getSubFlows()) {
			allIconNames.addAll(sf.getNodeIconNames());
			logger.debug("SubFlow {} uses these node types: {}", sf.getName(), sf.getNodeIconNames().toArray());
		}
	}
}
