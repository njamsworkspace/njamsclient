package com.w3p.im.iib.mon.exceptions;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


/**
 * @author JohnO
 *
 * Top-level exception for all exceptions that are seen as being internal to the
 * application, and whose details should be logged and hidden from end users
 * 
 */
public abstract class InternalException extends NjamsException implements ILogableException {
	private static final Logger logger = LoggerFactory.getLogger(InternalException.class);
	
	private static final long serialVersionUID = 1L;

	protected boolean logged; // Set true when exception has been logged

	/**
	 * 
	 */
	public InternalException() {
		super();
	}

	/**
	 * @param msg
	 * @param values - an array of values for the message variables, unless the final one is a Throwable (cause)
	 */
	public InternalException(String msg, Object...values) {
		super.formatMsg(msg, values);
		super.createErrorId();
		logMessage();
	}
	

	/**
	 * @return
	 */
	public boolean isLogged() {
		return logged;
	}

	/**
	 * Log the stack trace if severity level is equal or greater than the default
	 */
	public void logMessage() {
		logger.error(this.getErrorID()+": "+super.toString(), this);
		logged = true;
	}
}

