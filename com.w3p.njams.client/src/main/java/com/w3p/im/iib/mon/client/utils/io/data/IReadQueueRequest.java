package com.w3p.im.iib.mon.client.utils.io.data;

import java.util.Optional;

public interface IReadQueueRequest extends IQueueRequest {
	
	/**
	 * @return GET or BROWSE - defaults to BROWSE
	 */
	String getMode();
	
	/**
	 * @param mode = GET or BROWSE 
	 * @return
	 */
	IReadQueueRequest setMode(String mode);
	
	Optional<String> getMsgSelector();
	
	IReadQueueRequest setMsgSelector(String msgSelector);
}
