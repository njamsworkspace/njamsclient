package com.w3p.im.iib.mon.topology.model;

import com.w3p.api.config.proxy.MessageFlowProxy.Node;


public class SubflowNode extends FlowNode {

	public SubflowNode(Node node) {
		super(node);
	}
}
