package com.w3p.im.iib.mon.monitor.data;

import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;

public class ProcessMonitoringProfileResponse {
	
	Map<String, String> messages;
	boolean success = false;
	String  monitoringProfile;
	
	public ProcessMonitoringProfileResponse(String monitoringProfile) {
		this.monitoringProfile = monitoringProfile;
	}
	
	public String getMonitoringProfile() {
		return monitoringProfile;
	}
	public Map<String, String> getMessages() {
		if (messages == null) {
			messages = new LinkedHashMap<>();
		}
		return messages;
	}
	public boolean isSuccess() {
		return success;
	}
	public void setSuccess(boolean success) {
		this.success = success;
	}
	public String getAllMessages() {
		StringBuilder sb = new StringBuilder();
		Set<String> profiles = getMessages().keySet();
		for (String profile : profiles) {
			sb.append(profile).append(": ").append(messages.get(profile));		
		}
		return sb.toString();
	}
	@Override
	public String toString() {
		return "Monitoring Profile="+ monitoringProfile+", messages=" + messages;
	}
}
