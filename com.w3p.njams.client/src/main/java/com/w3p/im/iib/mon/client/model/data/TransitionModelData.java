package com.w3p.im.iib.mon.client.model.data;

import java.io.Serializable;

public class TransitionModelData implements Serializable {
	
	private static final long serialVersionUID = 1L;

	private final ProcessModelData processModel;

    private GroupModelData parent;

    // the from activity
    private ActivityModelData fromActivity;
    // the to activity
    private ActivityModelData toActivity;

    private final String id;
    private final String name;    
    private String config;


	public TransitionModelData(ProcessModelData processModel, String id, String name) {
		this.processModel = processModel;
		this.id = id;
		this.name = name;
	}

	public GroupModelData getParent() {
		return parent;
	}

	public TransitionModelData setParent(GroupModelData parent) {
		if (this.parent == null) {
            this.parent = parent;
            parent.addChildTransition(this);
        }
		return this;
	}

	public ActivityModelData getFromActivity() {
		return fromActivity;
	}

	public TransitionModelData setFromActivity(ActivityModelData fromActivity) {
		this.fromActivity = fromActivity;
		return this;
	}

	public ActivityModelData getToActivity() {
		return toActivity;
	}

	public TransitionModelData setToActivity(ActivityModelData toActivity) {
		this.toActivity = toActivity;
		return this;
	}

	public String getConfig() {
		return config;
	}

	public TransitionModelData setConfig(String config) {
		this.config = config;
		return this;
	}

	public ProcessModelData getProcessModel() {
		return processModel;
	}

	public String getId() {
		return id;
	}

	public String getName() {
		return name;
	}

	@Override
	public String toString() {
		return "TransitionModelData [processModel=" + processModel + ", id=" + id + "]";
	}
}
