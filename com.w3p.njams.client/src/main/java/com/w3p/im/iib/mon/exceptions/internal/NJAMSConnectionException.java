package com.w3p.im.iib.mon.exceptions.internal;

import com.w3p.im.iib.mon.exceptions.InternalException;

public class NJAMSConnectionException extends InternalException {


	private static final long serialVersionUID = 1L;

	public NJAMSConnectionException() {
		super();
	}

	public NJAMSConnectionException(String msg, Object... values) {
		super(msg, values);
	}
}
