package com.w3p.im.iib.mon.mqtt.service;

import org.eclipse.paho.client.mqttv3.IMqttToken;
import org.eclipse.paho.client.mqttv3.MqttClient;
import org.eclipse.paho.client.mqttv3.MqttConnectOptions;
import org.eclipse.paho.client.mqttv3.MqttException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.w3p.im.iib.mon.client.config.data.User;
import com.w3p.im.iib.mon.monitor.data.MonitorEventsRequestForQueues;
import com.w3p.im.iib.mon.monitor.data.MonitorEventsRequestForTopics;

public class MqttClientConsumer {
	private static final Logger logger = LoggerFactory.getLogger(MqttClientConsumer.class);
	
	private final MonitorEventsRequestForTopics request;
	
	private final String host;
	private final int port;
	private final int qos;	//Quality of Service: 0, 1, 2
	private final int keepAliveInterval;
	private final String clientId;
	private final User user;

	public MqttClientConsumer(MonitorEventsRequestForTopics request) {
		this.request = request;
		MqttConnectionProperties mqttConnectionProperties = request.getMqttConnectionProperties();
		this.host = mqttConnectionProperties.getHost();
		this.port = mqttConnectionProperties.getPort();
		this.qos = mqttConnectionProperties.getQos();
		this.keepAliveInterval = mqttConnectionProperties.getKeepAliveInterval();
		this.clientId = mqttConnectionProperties.getClientId();
		this.user = mqttConnectionProperties.getUser();		
	}
	
	public void consumeMsgsFromDestination(String dest) {
		MqttConnectOptions connOpts = setupClient();
		String serverUrl = String.format("tcp://%1$s:%2$d", host, port);
		logger.info("MQTT server URL: '{}'", serverUrl);
		MqttClient mqttClient = connectToBroker(serverUrl, clientId+dest, connOpts, dest);
		if (mqttClient.isConnected()) {
			// subscribe to a topic
			subscribeToMonEvent(mqttClient, dest, qos);
			logger.info("Successfuly subscribed to topic '{}'", dest);
		} else {
			// Connection failed
			logger.error("Failed to connect to MQTT Broker with server url: {}.", serverUrl);
		}
	}
	
	protected MqttConnectOptions setupClient() {		
		MqttConnectOptions connOpt = new MqttConnectOptions();
		connOpt.setCleanSession(true);
		connOpt.setKeepAliveInterval(keepAliveInterval);
		if (user.getUserName() != null && !user.getUserName().isEmpty() && user.getPasswordInClear() != null && !user.getPasswordInClear().isEmpty()) {
			connOpt.setUserName(user.getUserName());
			connOpt.setPassword(user.getPasswordInClear().toCharArray());
		}
		return connOpt;
	}

	protected MqttClient connectToBroker(String serverUrl, String clientId, MqttConnectOptions connOpts, String topic) {
		// Connect to Broker
		MqttClient mqttClient = null;
		try {
			mqttClient = new MqttClient(serverUrl, clientId);
			mqttClient.setCallback(new MqttEventsConsumer(mqttClient, connOpts, request, topic));
			IMqttToken token = mqttClient.connectWithResult(connOpts);
			if (token.getException() != null) {
				logger.error("Response is: exception = {}.", token.getException().toString()) ; 
			}
			logger.debug("Response from Broker connection: {}.",token.getResponse().toString());
			return mqttClient; //"Connected to " + serverUrl;
		} catch (MqttException e) {
			logger.error(e.toString(), e);
		}catch (Exception e) {
			logger.error(e.toString(), e);
		}
		return mqttClient; // "Failed to connect to " + serverUrl;
	}
	
	protected void subscribeToMonEvent(MqttClient mqttClient, String topic, int qos) {
		try {
//			MqttTopic mqttTopic = mqttClient.getTopic(topic); - validates the topic string - and rejects "#" !?
			mqttClient.subscribe(topic, qos);
		} catch (MqttException e) {
			logger.error(e.toString(), e);
		} catch (Exception e) {
			logger.error(e.toString(), e);
		}
	}
}
