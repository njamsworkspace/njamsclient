package com.w3p.im.iib.mon.client.config.data;

public class MsgFlow extends Flow {

	private GlobalCorrelationId globalCorrelationId;
	private ApplicationDataQuery applicationDataQuery;

	public MsgFlow(String name, boolean includePayload, boolean labelNodeStartsGroup) {
		super(name, includePayload, labelNodeStartsGroup);
	}

	public GlobalCorrelationId getGlobalCorrelationId() {
		return globalCorrelationId;
	}
	public void setGlobalCorrelationId(GlobalCorrelationId globalCorrelationId) {
		this.globalCorrelationId = globalCorrelationId;
	}

	public ApplicationDataQuery getApplicationDataQuery() {
		return applicationDataQuery;
	}
	public void setApplicationDataQuery(ApplicationDataQuery applicationDataQuery) {
		this.applicationDataQuery = applicationDataQuery;
	}

	@Override
	public String toString() {
		return "MsgFlow [name=" + name + "]";
	}
}
