package com.w3p.im.iib.mon.client.utils.serialize;

import com.w3p.im.iib.mon.jms.producers.FlowToProcessModelCache;

public class FlowToProcessModelCacheSerializer extends AbstractSerializer {


	public void export(String outputFolder, FlowToProcessModelCache cache) {
		try {				
			write(cache, outputFolder,"FlowToProcessModel.cache");
		} catch (Exception e) {
			e.printStackTrace(); // TODO sort this out
		}
	}

}
