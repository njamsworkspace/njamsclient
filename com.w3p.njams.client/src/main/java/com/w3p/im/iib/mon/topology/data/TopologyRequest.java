package com.w3p.im.iib.mon.topology.data;

import com.w3p.api.config.proxy.BrokerProxy;

import com.w3p.im.iib.mon.client.config.data.IntegrationServer;
import com.w3p.im.iib.mon.monitor.data.MonitoredObject;

public class TopologyRequest {
	
	private final BrokerProxy integrationNode;
	private final MonitoredObject<?> monitoringScope;
	private final String intSvrName;
	
	private int timeToWait; // Wait-time in seconds for sync calls to remote broker for applying Monitoring Profiles
	private String intNodeName;
	private String monProfileDir;
	private IntegrationServer integrationServer;
	
	
	private boolean applyProfileIfUnchanged; // 'true' = generate profile and apply it even if it has not changed (compared to current file)
	
	
	public TopologyRequest(BrokerProxy integrationNode, MonitoredObject<?> monitoringScope, String intSvrName) {
		this.integrationNode = integrationNode;
		this.monitoringScope =  monitoringScope;
		this.intSvrName = intSvrName;
	}

	public int getTimeToWait() {
		return timeToWait;
	}

	public TopologyRequest setTimeToWait(int timeToWait) {
		this.timeToWait = timeToWait;
		return this;
	}

	public BrokerProxy getIntegrationNode() {
		return integrationNode;
	}

	public MonitoredObject<?> getMonitoringScope() {
		return monitoringScope;
	}

	public String getIntSvrName() {
		return intSvrName;
	}

	public TopologyRequest setApplyProfileIfUnchanged(boolean applyProfileIfChanged) {
		this.applyProfileIfUnchanged = applyProfileIfChanged;
		return this;
	}

	public String getIntNodeName() {
		return intNodeName;
	}

	public TopologyRequest setIntNodeName(String intNodeName) {
		this.intNodeName = intNodeName;
		return this;
	}

	public String getMonProfileDir() {
		return monProfileDir;
	}

	public TopologyRequest setMonProfileDir(String monProfileDir) {
		this.monProfileDir = monProfileDir;
		return this;
	}

	public IntegrationServer getIntegrationServer() {
		return integrationServer;
	}

	public TopologyRequest setIntegrationServer(IntegrationServer intSvr) {
		this.integrationServer = intSvr;
		return this;
	}
}
