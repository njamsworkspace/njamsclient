package com.w3p.im.iib.mon.exceptions;

//import com.w3p.im.iib.mon.String.String;

/**
 * @author JohnO
 *
 * Top-level exception for all exceptions that represent application error conditions,
 * whose details should be logged and reported to the nJAMS Server.
 * 
 */
public abstract class ApplicationException extends NjamsException {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * 
	 */
	protected ApplicationException() {
		super();

	}

	/**
	 * @param msg
	 */
	protected ApplicationException(String msg, Object... values) {
		super.formatMsg(msg, values);
		super.createErrorId();
	}
}
