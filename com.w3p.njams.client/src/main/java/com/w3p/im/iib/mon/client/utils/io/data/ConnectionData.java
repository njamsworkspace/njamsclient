package com.w3p.im.iib.mon.client.utils.io.data;

import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

public class ConnectionData {
	
	protected String qmgr;
	protected String queue;
	protected String host; // hostname or ip address
	protected int port;
	protected String serverChan; // Channel name
	protected String userid;
	protected String password;
	
	protected final Map<String, Object> requiredVars; // these must not have a null or 0 value

	
	public ConnectionData() {
		requiredVars  = new HashMap<>();
	}
	public String getQmgr() {
		return qmgr;
	}
	public ConnectionData setQmgr(String qMgr) {
		this.qmgr = qMgr;
		return this;
	}
	public String getQueue() {
		return queue;
	}
	public ConnectionData setQueue(String queue) {
		this.queue = queue;
		return this;
	}
	public String getHost() {
		return host;
	}
	public ConnectionData setHost(String host) {
		this.host = host;
		return this;
	}
	public int getPort() {
		return port;
	}
	public ConnectionData setPort(int port) {
		this.port = port;
		return this;
	}
	public String getServerChan() {
		return serverChan;
	}
	public ConnectionData setServerChan(String serverChan) {
		this.serverChan = serverChan;
		return this;
	}
	public String getUserid() {
		return userid;
	}
	public ConnectionData setUserid(String userid) {
		this.userid = userid;
		return this;
	}
	public String getPassword() {
		return password;
	}
	public ConnectionData setPassword(String password) {
		this.password = password;
		return this;
	}
	
	/**
	 * Only run this after ConnectionData has been set.
	 * @return Optional 
	 */
	public Optional<Map<String, String>> isConnectionDataComplete() {		
		final Map<String, String> invalidValues = new HashMap<>();
		requiredVars.forEach((k, v) -> {
			// (At the time of coding!) It's safe to assume that a required variable is either a String or an int 
			if (v == null || (v.getClass().isAssignableFrom(String.class) && ((String)v).isEmpty())) {				
				invalidValues.put(k, "is missing - it must be specified");
			} else if (v.getClass().isAssignableFrom(Integer.class) && ((Integer)v) <= 0) {
				invalidValues.put(k, "must be specified and be greater than zero");
			}
		});

		return invalidValues.isEmpty() ? Optional.empty() : Optional.of(invalidValues);
	}
	
	@Override
	public String toString() {
		return "ConnectionData [qMgr=" + qmgr + ", queue=" + queue + ", host=" + host + ", port=" + port
				+ ", serverChan=" + serverChan + "]";
	}

}
