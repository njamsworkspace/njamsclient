package com.w3p.im.iib.mon.jms.consumers;

import com.im.njams.sdk.Njams;
import com.w3p.im.iib.mon.monitor.data.MonitorEventsRequest;
import com.w3p.im.iib.mon.monitor.data.MonitorEventsRequestForTopics;

public class MonitorEventsConsumerForTopics extends MonitorEventsConsumer {
	
	private final String intSvrName;	
	private final Njams njamsClient;

	public MonitorEventsConsumerForTopics(MonitorEventsRequestForTopics request) {
		super(request);
		this.intSvrName = request.getIntegrationServerName();
		this.njamsClient = request.getNjamsClient();
	}

}
