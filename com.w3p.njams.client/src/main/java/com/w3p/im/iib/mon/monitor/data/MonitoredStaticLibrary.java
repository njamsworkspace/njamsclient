package com.w3p.im.iib.mon.monitor.data;

public class MonitoredStaticLibrary extends MonitoredObject<MonitoredObject<?>> {
	

	public MonitoredStaticLibrary(String name) {
		super(name);
	}
	
	@Override
	public String toString() {
		return "Monitored Static Library [name=" + name + "]";
	}
}
