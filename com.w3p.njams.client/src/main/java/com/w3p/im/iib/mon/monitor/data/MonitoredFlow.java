package com.w3p.im.iib.mon.monitor.data;

public class MonitoredFlow extends MonitoredObject<MonitoredObject<?>> {
	
	protected boolean labelNodeStartsGroup;

	public MonitoredFlow(String name) {
		super(name);
	}
	
	public boolean isLabelNodeStartsGroup() {
		return labelNodeStartsGroup;
	}

	public void setLabelNodeStartsGroup(boolean labelNodeStartsGroup) {
		this.labelNodeStartsGroup = labelNodeStartsGroup;
	}

	@Override
	public String toString() {
		return "Monitored Message Flow [name=" + name + "]";
	}
}
