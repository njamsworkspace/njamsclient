package com.w3p.im.iib.mon.client.config.data;

public class Application extends DeployableObject {
	
	
	public Application(String name, boolean includeSchemaName) {
		super(name, includeSchemaName);
	}

	@Override
	public String toString() {
		return "Application [name=" + getName() + "]";
	}
}
