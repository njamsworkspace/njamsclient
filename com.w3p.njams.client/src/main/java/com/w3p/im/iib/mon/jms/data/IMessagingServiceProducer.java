package com.w3p.im.iib.mon.jms.data;

import javax.jms.JMSException;

import com.w3p.im.iib.mon.exceptions.internal.MessagingException;
import com.w3p.im.iib.mon.jms.service.MessagingServiceFactory;

/**
 * @author John Ormerod
 *
 */
public interface IMessagingServiceProducer extends IMessagingService {
	
	public IMessageProducer addMessageProducer(String dest, IMessageProducer msgProducer) throws MessagingException, JMSException;
	
	public IMessageProducer addMessageProducer(String dest, IMessageProducer msgProducer, boolean isTransacted) throws MessagingException, JMSException;
	
	public IMessageProducer getMessageProducer();
	
	void setMessagingServiceFactory(MessagingServiceFactory messagingServiceFactory);
	
	MessagingServiceFactory getMessagingServiceFactory();
}
