package com.w3p.im.iib.mon.client.config.data;

import java.io.File;

import org.apache.commons.lang3.StringUtils;

public class Profile {
	
	private static final String CURRENT_DIRECTORY = "cd";
	private String clientHome;
	private String directory;
	private int timeToWait;	
	private final boolean applyIfUnchanged;
	
	
	public Profile(String directory, boolean applyIfUnchanged) {
		this.directory = directory;
		this.applyIfUnchanged = applyIfUnchanged;
	}


	public Profile setClientHome(String clientHome) {
		this.clientHome = clientHome;
		return this;
	}


	public String getDirectory() {
        // Look for a single EnvVar and resolve 
        if (StringUtils.countMatches(directory, "%") == 2) {
        	String envVar = StringUtils.substringBetween(directory, "%");
        	String sysVar;
        	if (envVar.toLowerCase().equals(CURRENT_DIRECTORY)) {
        		// Current working dir
        		sysVar = clientHome;
        	} else {
        		sysVar = System.getenv(envVar);
        	}
        	sysVar = StringUtils.strip(sysVar, File.pathSeparator);
        	directory = StringUtils.replace(directory, "%"+envVar+"%", sysVar);            	
        } 
		return directory;
	}

	public int getTimeToWait() {
		return timeToWait;
	}
	public Profile setTimeToWait(int timeToWait) {
		this.timeToWait = timeToWait;
		return this;
	}

	public boolean isApplyIfUnchanged() {
		return applyIfUnchanged;
	}
	
}
