package com.w3p.im.iib.mon.client.utils.io.readers;

import static com.w3p.im.iib.mon.client.constants.IClientConstants.BROWSE;
import static com.w3p.im.iib.mon.client.constants.IClientConstants.GET;
import static com.w3p.im.iib.mon.client.constants.IClientConstants.NAME_USING_MSG_PROPERTIES;
import static com.w3p.im.iib.mon.client.constants.IClientConstants.UTF_8;

import java.io.Closeable;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Enumeration;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Optional;

import javax.jms.BytesMessage;
import javax.jms.JMSException;
import javax.jms.JMSRuntimeException;
import javax.jms.Message;
import javax.jms.MessageConsumer;
import javax.jms.MessageProducer;
import javax.jms.Queue;
import javax.jms.QueueBrowser;
import javax.jms.TextMessage;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ibm.msg.client.jms.DetailedInvalidSelectorRuntimeException;
import com.w3p.im.iib.mon.client.utils.io.data.IReadQueueRequest;
import com.w3p.im.iib.mon.client.utils.io.data.Response;
import com.w3p.im.iib.mon.client.utils.io.jms.JmsConnector;
import com.w3p.im.iib.mon.client.utils.io.jms.JmsFactoryFactoryWrapper;


public class QueueReader extends JmsConnector implements IQueueReader, Closeable {
	private static final Logger log = LoggerFactory.getLogger(QueueReader.class);
	
	protected MessageProducer producer;


	public QueueReader() {
	}

	@Override
	public void init(IReadQueueRequest req, Response resp) {
		try {
			super.init(req.getConnectionData());
		} catch (JMSException e) {
			String reason = String.format("Error configuring the JMS environment. Reason: '%s'.", e.toString());
			resp.setMessage(Response.ERROR, reason);
			log.error(reason, e);
		}
	}

	// For jUnit testing using jms mocks
	protected void init(IReadQueueRequest req, Response resp, JmsFactoryFactoryWrapper jffw) {
		try {
			super.init(req.getConnectionData(), jffw);
		} catch (JMSException e) {
			String reason = String.format("Error configuring the JMS environment. Reason: '%s'.", e.toString());
			resp.setMessage(Response.ERROR, reason);
			log.error(reason, e.toString(), e);
		}
	}
	
	
	
	
//	@SuppressWarnings("unchecked")
	@Override
	public IContent readAll(IReadQueueRequest req, Response resp) {   //browseMessagesFromQueue
		IContent queueContent = new QueueContent();
		String msgSelector = req.getMsgSelector().isPresent() ? req.getMsgSelector().get() : null;
		Enumeration<Message> messages = null;
		Queue queue = (Queue) jmsDest;
		QueueBrowser qBrowser = null;
		MessageConsumer msgConsumer = null;

		try {
			// FIXME add option to get queue depth
			// Get queue depth
			qBrowser = getQueueDepth(queue, msgSelector, resp);

			if (req.getMode().equals(BROWSE)) {
//				qBrowser = jmsSession.createBrowser(queue, msgSelector);
				// Process all mesages
				messages =  qBrowser.getEnumeration();
				while (messages.hasMoreElements()) {
					Message message = messages.nextElement();
					processMsg(req, message, queueContent, resp);
				}
			} else if (req.getMode().equals(GET)) {					
				// Process all messages
				msgConsumer = jmsSession.createConsumer(jmsDest, msgSelector);
				Message message  = msgConsumer.receive(1000L);				
				while (message != null) {
					processMsg(req, message, queueContent, resp);
				}
			}
		} catch (JMSException e) {
			String reason = String.format("Error creating a text version of the current JMS message - msg count is %d'. Reason: '%s'.", resp.getFilesWritten(), e.toString());
			log.error(reason, e);
			resp.setMessage("Messages reader", reason);
			resp.setFilesInError(resp.getFilesInError() + 1);
		} catch (DetailedInvalidSelectorRuntimeException e) {
			String reason = String.format("Error in the message selector's format - terminating run. Reason: '%s'.", e.toString());
			// log.error(reason, e);
			resp.setMessage("Messages reader", reason);
			resp.setFilesInError(resp.getFilesInError() + 1);	
		} catch (JMSRuntimeException e) {
			String reason = String.format("Error creating a text version of the current JMS message - msg count is %d'. Reason: '%s'.", resp.getFilesWritten(), e.toString());
			log.error(reason, e);
			resp.setMessage("Message writer", reason);
			resp.setFilesInError(resp.getFilesInError() + 1);
		} catch (Exception e) {
			// Catch any un-handled exception/
			String reason = String.format("Unhandled error creating a text version of the current JMS message - msg count is %d'. Reason: '%s'.", resp.getFilesWritten(), e.toString());
			log.error(reason, e);
			resp.setMessage("Messages writer", reason);
			resp.setFilesInError(resp.getFilesInError() + 1);
		} finally {
			try {
				if (jmsSession != null)	{jmsSession.close();} // Assume consumer has been auto-closed
				if (jmsConnection != null) {jmsConnection.close();}
				if (qBrowser != null) {qBrowser.close();}
				if (msgConsumer != null) {msgConsumer.close();}
			} catch (JMSException e) {
				// FIXME report in Response
			}				
		}
		return queueContent;
	}
	
	private void processMsg(IReadQueueRequest req, Message message, IContent queueContent, Response resp) throws UnsupportedEncodingException, JMSException {
		String name = null;
		String msgText = extractMessageText(resp, message);
		if (msgText != null) {
			Optional<Object> o = req.getOption(NAME_USING_MSG_PROPERTIES); 
			if (o.isPresent() ? (Boolean)o.get() : false) {
				Map<String, String> msgProperties = findUserProperties(message); // could be used to create file-name
				if (!msgProperties.isEmpty()) {
					StringBuffer sbBaseName = new StringBuffer();
					msgProperties.forEach((k, v) -> {
						sbBaseName.append(k).append("-").append(v).append("_");
					});
					String baseName = StringUtils.substringBeforeLast(sbBaseName.toString(), "_"); 
					name = createName(req.getNamePattern(), baseName);
				} else {
					name = createName(req.getNamePattern()); // will create a time-based name
				}
			} else {
				name = createName(req.getNamePattern());
			}
			queueContent.addContent(name, msgText);
		} else {
			// Report INFO empty msg read??
		}
	}

	private QueueBrowser getQueueDepth(Queue queue, String msgSelector, Response resp) throws JMSException {
		QueueBrowser qBrowser = null;
		int qDepth = 0;
		qBrowser = jmsSession.createBrowser(queue, msgSelector);
		for (@SuppressWarnings("unchecked")
		Enumeration<Message> msg = qBrowser.getEnumeration(); msg.hasMoreElements(); )	{
			qDepth++;
			msg.nextElement();
		}			
		resp.setFilesToBeRead(qDepth); // FIXME change 'Files' to 'Items'

		return qBrowser;
	}
	
	private String extractMessageText(Response resp, Message message) throws JMSException, UnsupportedEncodingException { //throws JMSException {
		String msgText = null;

		if (message instanceof TextMessage) {
			msgText = ((TextMessage) message).getText(); //message.getBody(String.class);	
		} else if (message instanceof BytesMessage) {
			BytesMessage bytesMsg = (BytesMessage) message;
			bytesMsg.reset(); 
			int msgLength = new Long(bytesMsg.getBodyLength()).intValue();
			byte[] msgBytes = new byte[msgLength];
			bytesMsg.readBytes(msgBytes);
			msgText = new String(msgBytes,UTF_8);				
		} else {
			String reason = String.format("Unsupported JMS msg type '%s' - msg count is %d.", message.getClass().getName(), resp.getFilesWritten());
			resp.setMessage("Messages reader", reason);
			resp.setFilesInError(resp.getFilesInError() + 1);
		}
		return msgText;
	}

	/**
	 * Creates a time-based name, eg: 'myFile_yyyyMMddhhmmssSSS.xml', using pattern: 'myFile_%s.xml' 
	 * @param fileNamePattern
	 * @return
	 */
	private String createName(String fileNamePattern) {
		DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyyMMddhhmmssSSS");
		String now = LocalDateTime.now().format(dtf);
		return createName(fileNamePattern, now);
	}
	
	/**
	 * Creates a name based on the pattern, e.g: 'myFile_01.xml', using pattern: 'myFile_%s.xml'
	 * @param fileNamePattern
	 * @param baseName
	 * @return
	 */
	private String createName(String fileNamePattern, String baseName) {
		String fileName = String.format(fileNamePattern, baseName);
		return fileName;
	}
	
	protected Map<String, String> findUserProperties(Message message) throws JMSException {
		Map<String, String> userProperties = new LinkedHashMap<>();
		@SuppressWarnings("unchecked")
		Enumeration<String> userProps = message.getPropertyNames();
		while (userProps.hasMoreElements()) {
			String propName = userProps.nextElement();
			if (!propName.startsWith("JMS")) {
				String value = (String) message.getObjectProperty(propName);
				userProperties.put(propName, value);
//				System.out.println("Property is name: "+propName+", value is: "+value);
			}			
		}
		
		return userProperties;
	}
	
	@Override
	public void close() throws IOException {
		try {
		if (jmsSession != null) {jmsSession.close();} 
		if (jmsConnection != null) {jmsConnection.close();}
		} catch (JMSException e) {
			// FIXME report in Response
		}	
	}
}
