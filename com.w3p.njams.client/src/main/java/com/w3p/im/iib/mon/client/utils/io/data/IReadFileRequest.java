package com.w3p.im.iib.mon.client.utils.io.data;

public interface IReadFileRequest extends IFileRequest {
	
	/**
	 * Folder path or Queue
	 * @param dest
	 * @return
	 */
	IReadFileRequest setSource(String source);
	
	/**
	 * @return
	 */
	String getSource();
	
	IReadFileRequest setReadBufSize(int readBufSize);
	
	int getReadBufSize();

}
