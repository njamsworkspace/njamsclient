package com.w3p.im.iib.mon.client.config.data;

public class IIBConfig { // extends User {
	
	private static final String LOCAL = "local";
	private String host;
	private String intNode;
	private String connectionType; 
	private int adminPort;
	private User iibUser;
	
	public IIBConfig() {
	}

	public String getConnectionType() {
		return connectionType;
	}
	
	public boolean isBrokerLocal() {
		return connectionType.equals(LOCAL);
	}

	public IIBConfig setConnectionType(String connectionType) {
		this.connectionType = connectionType;
		return this;
	}

	public int getAdminPort() {
		return adminPort;
	}

	public IIBConfig setAdminPort(int adminPort) {
		this.adminPort = adminPort;
		return this;
	}

	public String getHost() {
		return host;
	}
	
	public IIBConfig setHost(String host) {
		this.host = host;
		return this;
	}

	public String getIntNode() {
		return intNode;
	}

	public IIBConfig setIntNode(String intNode) {
		this.intNode = intNode;
		return this;
	}
	
	public User getIibUser() {
		if (iibUser == null) {
			iibUser = new User();
		}
		return iibUser;
	}

	@Override
	public String toString() {
		return "IIBConfig [host=" + host + ", intNode=" + intNode + ", connectionType=" + connectionType
				+ ", adminPort=" + adminPort + "]";
	}	
}
