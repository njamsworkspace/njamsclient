package com.w3p.im.iib.mon.monitor.data.groovy

import groovy.util.XmlSlurper

import org.codehaus.groovy.ast.stmt.SwitchStatement

//import com.ibm.etools.mft.pattern.web.support.extensions.files.MessageFlowNamespaceContextImpl
import com.w3p.im.iib.mon.monitor.data.MonitorEventsRequestForQueues
//import com.w3p.im.iib.mon.monitor.data.MonitorFlowStatsRequest
import com.w3p.im.iib.mon.monitor.data.MonitorEventsRequestForQueues
import com.w3p.im.iib.mon.topology.model.IntegrationServer

import groovy.util.XmlParser


class ParseMonitoringRequest<T extends MonitorEventsRequestForQueues> {
	
//TODO Need to make generic returned object	
	def T parseMonitoringRequest (String xmlString) {
		
		def monitor = new XmlSlurper().parseText(xmlString)
		
//		def requestType = monitor.events != null ? "Events" : "FlowStats"
		
	
//		if  (requestType == "Events") {
			MonitorEventsRequestForQueues request = new MonitorEventsRequestForQueues()
			request.integrationNodeName = monitor.events.integrationNodeName
			request.integrationServerName = monitor.events.integrationServerName
			request.flowName = monitor.events.flowName
			request.tracingEnabled = monitor.events.@tracing.toBoolean()
			return request			
//		} else {
//			MonitorFlowStatsRequest request = new MonitorFlowStatsRequest()
//			return request
//		}
	
	}

}
