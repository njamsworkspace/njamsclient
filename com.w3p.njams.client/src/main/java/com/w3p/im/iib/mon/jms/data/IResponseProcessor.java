package com.w3p.im.iib.mon.jms.data;


import com.w3p.im.iib.mon.exceptions.ApplicationException;



public interface IResponseProcessor {
	
	/**
	 * @param responseAsString
	 * 
	 * This is the callback method for processing messages that arrive in the onMessage method
	 * of the invoked class
	 * @throws ApplicationException 
	 * 
	 */
	public void processMessage(String responseasString) throws ApplicationException;

}
