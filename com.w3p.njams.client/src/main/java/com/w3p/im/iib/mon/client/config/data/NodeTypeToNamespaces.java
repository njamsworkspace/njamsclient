package com.w3p.im.iib.mon.client.config.data;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class NodeTypeToNamespaces {
	
	private final Map<String, List<Namespace>> nodeTypeToNamespaces = new HashMap<>();

	public Map<String, List<Namespace>> getNodeTypeToNamespacesMap () {
		return nodeTypeToNamespaces;
	}

	@Override
	public String toString() {
		return "NodeTypeToNamespaces [nodeTypeToNamespaces=" + nodeTypeToNamespaces + "]";
	}
}
