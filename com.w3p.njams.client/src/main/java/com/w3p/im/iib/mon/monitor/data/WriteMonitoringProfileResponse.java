package com.w3p.im.iib.mon.monitor.data;

import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;

public class WriteMonitoringProfileResponse {
	
	private Map<String, String> messages;
	private boolean reapplyProfile;
	private boolean applyProfile;
	private boolean success = false;
	private String  monitoringProfile;
	
	public WriteMonitoringProfileResponse() { //String monitoringProfile) {
//		this.monitoringProfile = monitoringProfile;
	}
	
//	public String getMonitoringProfile() {
//		return monitoringProfile;
//	}
	public Map<String, String> getMessages() {
		if (messages == null) {
			messages = new LinkedHashMap<>();
		}
		return messages;
	}
	public boolean isSuccess() {
		return success;
	}
	public void setSuccess(boolean success) {
		this.success = success;
	}
	public String getAllMessages() {
		StringBuilder sb = new StringBuilder();
		Set<String> profiles = getMessages().keySet();
		for (String profile : profiles) {
			sb.append(profile).append(": ").append(messages.get(profile));		
		}
		return sb.toString();
	}
	public boolean isReapplyProfile() {
		return reapplyProfile;
	}

	public void setReapplyProfile(boolean reApplyProfile) {
		this.reapplyProfile = reApplyProfile;
	}

	public boolean isApplyProfile() {
		return applyProfile;
	}

	public void setApplyProfile(boolean applyProfile) {
		this.applyProfile = applyProfile;
	}

	public String getMonitoringProfile() {
		return monitoringProfile;
	}

	public void setMonitoringProfile(String monitoringProfile) {
		this.monitoringProfile = monitoringProfile;
	}

	@Override
	public String toString() {
		return ""; //Monitoring Profile="+ monitoringProfile+", messages=" + messages;
	}
}
