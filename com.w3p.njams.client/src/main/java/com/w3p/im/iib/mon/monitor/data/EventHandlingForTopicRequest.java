package com.w3p.im.iib.mon.monitor.data;

import com.im.njams.sdk.Njams;
import com.w3p.im.iib.mon.jms.producers.FlowToProcessModelCache;
import com.w3p.im.iib.mon.jms.service.MqJmsConnectionProperties;
import com.w3p.im.iib.mon.mqtt.service.MqttConnectionProperties;
import com.w3p.im.iib.mon.topology.model.TopologyObject;

public class EventHandlingForTopicRequest extends EventHandlingRequest {
	
	// DEV
	private final Njams njamsClient;
	private final TopologyObject topologyObject;
	private final MqttConnectionProperties mqttConnectionProperties;
	
	private String appName;
	
	private FlowToProcessModelCache flowToProcessModelCache;


	public EventHandlingForTopicRequest(Njams njamsClient, String intNodeName, String intSvrName, TopologyObject topologyObject, 
				MqJmsConnectionProperties mqJmsConnectionProperties, MqttConnectionProperties mqttConnectionProperties) {
		super(intNodeName, intSvrName, mqJmsConnectionProperties);
		this.njamsClient = njamsClient;
		this.topologyObject = topologyObject;
		this.mqttConnectionProperties = mqttConnectionProperties;
	}

	public Njams getNjamsClient() {
		return njamsClient;
	}

	public TopologyObject getTopologyObject() {
		return topologyObject;
	}
	
	public MqttConnectionProperties getMqttConnectionProperties() {
		return mqttConnectionProperties;
	}

	public FlowToProcessModelCache getFlowToProcessModelCache() {
		return flowToProcessModelCache;
	}

	public void setFlowToProcessModelCache(FlowToProcessModelCache flowToProcessModelCache) {
		this.flowToProcessModelCache = flowToProcessModelCache;
	}
}
