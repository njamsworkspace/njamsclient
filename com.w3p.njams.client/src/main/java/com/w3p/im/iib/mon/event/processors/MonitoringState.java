package com.w3p.im.iib.mon.event.processors;

import static com.w3p.im.iib.mon.client.constants.IClientConstants.END_ACTIVITY_NAME;
import static com.w3p.im.iib.mon.client.constants.IClientConstants.GROUP_END_NAME;
import static com.w3p.im.iib.mon.client.constants.IClientConstants.LABEL_NODE;
import static com.w3p.im.iib.mon.client.constants.IClientConstants.START_ACTIVITY_MODEL_ID;

import java.util.ArrayList;
import java.util.List;
import com.im.njams.sdk.logmessage.Activity;
import com.im.njams.sdk.logmessage.Group;
import com.im.njams.sdk.logmessage.Job;
import com.im.njams.sdk.model.ActivityModel;
import com.im.njams.sdk.model.GroupModel;
import com.im.njams.sdk.model.ProcessModel;

import com.w3p.im.iib.mon.monitor.data.MonitoringEvent;
import com.w3p.im.iib.mon.monitor.data.TxnStartMonitoringEvent;

public class MonitoringState {
	
	private Job job;
	private ProcessModel processModel; 
	private String processModelName; // For this state
	private String msgflowProcessModelName; // For the message flow
	private Activity previousActivity;
	private MonitoringEvent previousEvent;
	private Group group;
	private final List<Activity> processedActivities = new ArrayList<>();	
	private final List<MonitoringEvent> processedEvents = new ArrayList<>();
	
	
	public MonitoringState(ProcessModel processModel) {
		this.processModel = processModel;
	}
	public Job getJob() {
		return job;
	}
	public void setJob(Job job) {
		this.job = job;
	}
	public ProcessModel getProcessModel() {
		return processModel;
	}
	public String getProcessModelName() {
		if (processModelName == null) {
			processModelName = processModel.getName();;
		}
		return processModelName;
	}
	public String getMsgflowProcessModelName() {
		if (msgflowProcessModelName == null) {
			msgflowProcessModelName = processModel.getName();
		}
		return msgflowProcessModelName;
	}
	public Activity getPreviousActivity() {
		return previousActivity;
	}

	public MonitoringEvent getPreviousEvent() {
		return previousEvent;
	}

	public Activity getProcessModelActivity(String name) {
		for (ActivityModel activityModel : processModel.getActivityModels()) {
			if (activityModel.getName().equals(name)) {
				Activity activity = job.createActivity(activityModel).build();
				return activity;
			}
		}
		return null; // Not found
	}
	public ActivityModel getStartActivityModel() {
		return processModel.getActivity(START_ACTIVITY_MODEL_ID);
	}
	public Activity getStartActivity() {
		return processedActivities.get(0);
	}
	
	public List<Activity> getProcessedActivities() {
		return processedActivities;
	}
	
	public List<String> getProcessedActivityModelIds() {
		List<String> processedActivityNames = new ArrayList<>();
		for (Activity activity : processedActivities) {
			processedActivityNames.add(activity.getModelId()); //getName());
		}
		return processedActivityNames;
	}
	public Activity getProcessedActivity(String activityId) {
		for (Activity activity : processedActivities) {
			if (activity.getModelId().equals(activityId)) {
				return activity;
			}
		}
		return null;
	}
	public Activity getCompletedPredecessorActivityForCurrentEvent(MonitoringEvent event) {
		// A Label node always has a Start ActivityModel as its predecessor
		if (event.getNodeType().equals(LABEL_NODE)) {
			if (processedActivities.get(0).getModelId().equals(START_ACTIVITY_MODEL_ID)) {
				return processedActivities.get(0);
			}
		}
		
		// Process completed activities in reverse order on the basis that the one required will be more recent
		int processedActivityCount = processedActivities.size() - 1;
		for (int i = processedActivityCount; i >= 0; i--) {
			ActivityModel am = processModel.getActivity(processedActivities.get(i).getModelId());  //processedActivity.getModelId());
			if (am != null){
				List<ActivityModel> successorActivities = am.getSuccessors();
				for (ActivityModel successorActivityModel : successorActivities) {
					if (successorActivityModel.getName().equals(event.getLocalNodeLabel())) {
						return processedActivities.get(i);
					}
				}
			} 
		}

		return null; // Have to return something - a completed predecessor should always exist.		
	}
	public MonitoringEvent getCompletedPredecessorEventForCurrentEvent(Activity predecessorActivity) {
		int predecessorIndex = processedActivities.indexOf(predecessorActivity);
		MonitoringEvent predecessorEvent = processedEvents.get(predecessorIndex);
		return predecessorEvent;
	}

	public boolean doesActivityHaveEndSuccessor(Activity activity) {
		ActivityModel activityModel =  processModel.getActivity(activity.getModelId());
		boolean hasEndSuccessor = false;
		if (activityModel != null) {
			hasEndSuccessor =	activityModel.getSuccessors().stream()  //containsKey("End")) {
					.anyMatch(am -> am.getName().equals(END_ACTIVITY_NAME));
		}	

		return hasEndSuccessor;
	}
	
//	public boolean doesThisActivityHaveSuccessor(Activity thisActivity, Activity targetActivity) {
//		ActivityModel thisActivityModel =  processModel.getActivity(thisActivity.getModelId());
//		ActivityModel targetActivityModel =  processModel.getActivity(targetActivity.getModelId());
//		boolean targetIsSuccessor = false;
//		if (thisActivityModel != null) {
//			targetIsSuccessor =	thisActivityModel.getSuccessors().stream()  //containsKey("activity name")) {
//					.anyMatch(am -> am.getName().equals(targetActivityModel.getName()));
//		}	
//
//		return targetIsSuccessor;
//	}
	
	public boolean doesGroupHaveEndSuccessor(String groupModelId) {
		GroupModel gm = processModel.getGroup(groupModelId);
		boolean hasEndSuccessor = gm.getSuccessors().stream().anyMatch(sam -> sam.getName().equals(END_ACTIVITY_NAME));		
		
		return hasEndSuccessor;		
	}
	
	public List<MonitoringEvent> getProcessedEvents() {
		return processedEvents;
	}
	/**
	 * Set when processing errors reported by IIB 
	 * @param currentActivity
	 * @param currentEvent
	 */
	public void addPrevious(Activity currentActivity, MonitoringEvent currentEvent) {
		this.previousActivity = currentActivity;
		this.previousEvent = currentEvent;
	}

	public void setCurrentGroup(Group group) {
		this.group = group;		
	}
	public Group getCurrentGroup() {
		return group;
	}
	
	public void addCurrent(Activity currentActivity, MonitoringEvent currentEvent) {
		// History or processed Events and Activities
		processedActivities.add(currentActivity);
		processedEvents.add(currentEvent);
		// The most recent Event and Activity - it switches current to be previous for processing the next Event  
		this.previousActivity = currentActivity;
		this.previousEvent = currentEvent;
		// For events from an MQ node
		setJobAttributesFromEvent(currentEvent);
	}
	
	/**
	 *Set Job's Attributes and CorrelationLogId from MQ's MsgId and CorrelId
	 *
	 * @param currentEvent
	 */
	private void setJobAttributesFromEvent(MonitoringEvent currentEvent) {
		//	The first logic is for an MQ Reply node (which means that the msgflow's input node is MQ)
		//  By default, IIB creates a new MsgId value for the output message, and moves the MsgId value of the incoming msg to CorrelId
		if (currentEvent instanceof TxnStartMonitoringEvent) {
			if (currentEvent.getMsgId() != null && currentEvent.getCorrelId() != null) {			
//				job.addAttribute("MsgId", currentEvent.getMsgId());
//				job.setCorrelationLogId(currentEvent.getMsgId());
//				job.addAttribute("CorrelId", currentEvent.getCorrelId());
				job.setParentLogId(currentEvent.getCorrelId());
			} else if (currentEvent.getMsgId() != null){
				// Otherwise, do not update JobCorrelationId if it has already been set (from the Mon Event's GlobalTransactionId
				if (job.getCorrelationLogId().isEmpty()) {
//					job.addAttribute("MsgId", currentEvent.getMsgId());
//					job.setCorrelationLogId(currentEvent.getMsgId());
				} else if (currentEvent.getMsgId().equals(job.getCorrelationLogId())) {
//					job.addAttribute("MsgId", currentEvent.getMsgId()); // Show Msgid in the results.
				}
			} 
//			job.setCorrelationLogId(currentEvent.getGlobalTransactionId()); // FIXME this should handle MsgId and CorrelId if present
			if (currentEvent.getApplicationData() != null) {
				currentEvent.getApplicationData()
				.forEach(
					(k, v) -> {job.addAttribute(k, v);
					});
			}
		}
	}
	public MonitoringEvent getLastProcessedEvent() {
		if (processedEvents.size() > 0) {
			return processedEvents.get(processedEvents.size()-1);
		} 
		return processedEvents.get(0);
	}
	
	public Activity getLastProcessedActivity() {
		if (processedActivities.size() > 0) {
			return processedActivities.get(processedActivities.size()-1);
		} 
		return processedActivities.get(0);
	}
	
	public boolean doesCurrentActivityInGroupHaveEndSuccessor(Activity activity) {
		ActivityModel activityModel =  processModel.getActivity(activity.getModelId());
		boolean hasEndSuccessor = activityModel.getSuccessors().stream()
				.anyMatch(am -> am.getName().equals(GROUP_END_NAME));

		return hasEndSuccessor;
	}

	public boolean isCurrentEventInSameProcessModelAsPrevious(MonitoringEvent currentEvent) {
		return currentEvent.getProcessModelName().equals(previousEvent.getProcessModelName());
	}
	public void clear() {
		job = null;
		processModel = null;
		processModelName = null;
		msgflowProcessModelName = null;
		previousActivity = null;
		previousEvent = null;
		group = null;
		processedActivities.clear();
		processedEvents.clear();
	}
	@Override
	public String toString() {
		return "MonitoringState [processModel name=" + processModel.getName() + "]";
	}

}
