package com.w3p.im.iib.mon.client.utils.io.jms;

import javax.jms.JMSException;

import com.ibm.msg.client.jms.JmsFactoryFactory;

public class JmsFactoryFactoryWrapper {
	
	/*
	 * This wrapper allows up to mock the static 'JmsFactoryFactory.getInstance()' method
	 */


	public JmsFactoryFactory getFactory(String providerName) throws JMSException {
		return JmsFactoryFactory.getInstance(providerName);
	}

}
