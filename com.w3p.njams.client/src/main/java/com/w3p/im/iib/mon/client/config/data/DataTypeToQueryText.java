package com.w3p.im.iib.mon.client.config.data;

import java.util.Optional;

public class DataTypeToQueryText {
	
	private final String name;
	private final String xpath;
	private final Namespaces namespaces;
	
	public DataTypeToQueryText(String name, String xpath, Namespaces namespaces) {
		this.name = name;
		this.xpath = xpath;
		this.namespaces = namespaces;
	}

	public String getName() {
		return name;
	}

	public String getXpath() {
		return xpath;
	}

	public String getUri(final String prefix) {
		Optional<Namespace> opt =  namespaces.getNamespaces().stream().findFirst().filter(ns -> ns.getPrefix().equals(prefix)); 
		if (opt.isPresent()) {
			Namespace namespace = opt.get(); 			
			return namespace.getUri();	
		}
		return null;
	}

	@Override
	public String toString() {
		return "DataTypeToQueryText [name=" + name + ", xpath=" + xpath + ", namespaces=" + namespaces + "]";
	}	
}
