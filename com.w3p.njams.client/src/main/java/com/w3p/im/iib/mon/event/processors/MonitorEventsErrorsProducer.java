package com.w3p.im.iib.mon.event.processors;

import java.io.Closeable;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.jms.BytesMessage;
import javax.jms.Connection;
import javax.jms.JMSException;
import javax.naming.Context;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.w3p.im.iib.mon.jms.data.IMessageProducer;
import com.w3p.im.iib.mon.jms.data.IMessagingService;
import com.w3p.im.iib.mon.jms.producers.MsgProducer;
import com.w3p.im.iib.mon.jms.service.MessagingServiceFactory;
import com.w3p.im.iib.mon.monitor.data.MonitorEventsRequestForQueues;
import com.w3p.im.iib.mon.monitor.data.MonitoringEvent;

public class MonitorEventsErrorsProducer  extends MsgProducer implements IMessageProducer, Closeable {
	private static final Logger logger = LoggerFactory.getLogger(MonitorEventsErrorsProducer.class);
	
	private final MonitorEventsRequestForQueues request;	
	
	
	
	public MonitorEventsErrorsProducer(MonitorEventsRequestForQueues request) {
		this.request = request;
	}

	@Override
	public void createMessageProducer(Context context, Connection connection, String dest, boolean isTransacted) throws JMSException {
		super.createMessageProducer(context, connection, dest, isTransacted);
	}

	public void sendEventMessageSet(List<String> eventsAsXML, List<MonitoringEvent> monEvents, String dest) throws JMSException {
		for (int i = 0; i < eventsAsXML.size(); i++) {
			sendMessageToProblemsQueue(eventsAsXML.get(i), monEvents.get(i), dest);			
		}
	}

	public void sendMessageToProblemsQueue(String eventAsXML, MonitoringEvent monEvent, String dest) throws JMSException {
		Map<String, String> msgProperties = new HashMap<>();
		msgProperties.put("localtxnId", monEvent.getLocalTransactionId());
		msgProperties.put("globaltxnId", monEvent.getGlobalTransactionId());
		msgProperties.put("messageFlow", monEvent.getBaseMessageFlow());
		msgProperties.put("creationTime", monEvent.getCreationTime().toString());
		
		BytesMessage msg = createBytesMessage(eventAsXML);
		msg = (BytesMessage)setMessageProperties(msg, msgProperties);
		
		jmsProducer.send(msg);
		logger.info("Sent");
	}

	@Override
	public void close() throws IOException {
		try {
			if (session != null) {session.close();} 
			if (connection != null) {connection.close();}
		} catch (JMSException e) {
			// FIXME report in Response
		}
	}
}
