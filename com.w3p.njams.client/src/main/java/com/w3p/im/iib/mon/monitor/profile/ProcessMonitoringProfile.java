package com.w3p.im.iib.mon.monitor.profile;

import static com.w3p.im.iib.mon.client.constants.IClientConstants.UTF_8;

import java.io.BufferedReader;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.w3p.api.config.proxy.BrokerProxy;
import com.w3p.api.config.proxy.ConfigManagerProxyLoggedException;
import com.w3p.api.config.proxy.ConfigManagerProxyPropertyNotInitializedException;
import com.w3p.api.config.proxy.ConfigurableService;
import com.w3p.api.config.proxy.MessageFlowProxy;
import com.w3p.im.iib.mon.monitor.data.ApplyProfileRequest;
import com.w3p.im.iib.mon.monitor.data.ProcessMonitoringProfileRequest;
import com.w3p.im.iib.mon.monitor.data.ProcessMonitoringProfileResponse;


public class ProcessMonitoringProfile {
	private static final Logger logger = LoggerFactory.getLogger(ProcessMonitoringProfile.class);
	private static final String MONITORING_PROFILES = "MonitoringProfiles";	// For Config Services
	
	private ProcessMonitoringProfileRequest request;
	
	
	public ProcessMonitoringProfile() {
		// for use in PROD mode to invoke only appyProfile 				
	}
	
	public ProcessMonitoringProfile(ProcessMonitoringProfileRequest request) {
		this.request = request;
	}

	/**
	 * This method is invoked only in DEV  mode.
	 * It invokes the #applyProfile() method to complete the processing
	 * @return
	 * @throws IOException
	 */
	public ProcessMonitoringProfileResponse writeProfileIfNewOrChanged() throws IOException {
		String monProfileDir = request.getMonProfileDir();
		String intNodeName = request.getIntNodeName();
		Path monProfileDirForIntSvr = Paths.get(monProfileDir, intNodeName, request.getIntegrationServer(), request.getApplication());
		Files.createDirectories(monProfileDirForIntSvr);
		Path monProfilePath = Paths.get(monProfileDirForIntSvr.toString(), request.getMonitoringProfileFile());
		
		ApplyProfileRequest apr = new ApplyProfileRequest(request.getIntNodeProxy(), request.getProfileAsXml(),	request.getMessageFlowModel());
			apr.setTimeToWait(request.getTimeToWait())
			.setApplyProfileIfUnchanged(request.isApplyProfileIfUnchanged())
			.setProfileHasChanged(false)
			.setIntNodeName(request.getIntNodeName())
			.setMessageFlowProxy(request.getMessageFlowProxy())
			.setMonProfileDir(monProfileDir);
		
		boolean profileExists = Files.exists(monProfilePath);			 
		if (!profileExists) {
			Files.write(Paths.get(monProfilePath.toString()), request.getProfileAsXml().getBytes(), StandardOpenOption.CREATE);
			request.setApplyProfileIfUnchanged(true); // se this to ensure a new Profile is written
		} else if (request.isApplyProfileIfUnchanged()) {
			Files.write(Paths.get(monProfilePath.toString()), request.getProfileAsXml().getBytes(), StandardOpenOption.TRUNCATE_EXISTING, StandardOpenOption.CREATE);
		} else {
			// Read existing profile
			BufferedReader profileRdr = Files.newBufferedReader(monProfilePath, Charset.forName(UTF_8));
			StringBuilder sbCurrentProfile = new StringBuilder();
			String currentLine = null;
		      while((currentLine = profileRdr.readLine()) != null){  //while there is content on the current line
		        sbCurrentProfile.append(currentLine);
		      }
		      int oldProfilehash = sbCurrentProfile.toString().hashCode();
		      int newProfilehash = request.getProfileAsXml().hashCode();  
			if (newProfilehash != oldProfilehash) { // If profile is unchanvged, the hash values will be the same within a running process
				Files.write(Paths.get(monProfilePath.toString()), request.getProfileAsXml().getBytes(), StandardOpenOption.TRUNCATE_EXISTING, StandardOpenOption.CREATE);
				apr.setProfileHasChanged(true);
				logger.debug("Monitoring Profile '{}' has changed - old hashcode = {}, new hashcode = {}.", request.getProfileAsXml(), oldProfilehash, newProfilehash);
			}
		}
		
		return applyProfile(apr);
	}

	/**
	 * This method has to be able to be invoked independently when applying an existng profile in PROD  mode.
	 * Therefore, it must not depend on any values set by the #writeProfileIfNewOrChanged() method. 
	 * @param request
	 * @return
	 */
	public ProcessMonitoringProfileResponse applyProfile(ApplyProfileRequest request) {
		int timeToWaitMs = request.getTimeToWait()*1000; // set to  msec		
		BrokerProxy intNodeProxy = request.getIntNodeProxy();		
		String profileAsXml = request.getProfileAsXml();
		
		boolean applyProfile = false;
		boolean applyProfileIfUnchanged = request.isApplyProfileIfUnchanged();
		boolean profileHasChanged = request.isProfileHasChanged();
		
		ProcessMonitoringProfileResponse response = new ProcessMonitoringProfileResponse(profileAsXml);		
		// The following actions are 'nice to have' when re-applying a monitoring profile
		if (applyProfileIfUnchanged || profileHasChanged) {
			logger.info("*** Monitoring profile for '{}' will be applied ***", request.getMessageFlowName());
			// Pause monitoring
			if (modifyMessageFlowProperty(intNodeProxy, request.getMessageFlowProxy(), "This/monitoring", "inactive", timeToWaitMs)) {
				response.getMessages().put("\npauseMonitoring", "was successful");
				// Remove Monitoring Profile from the Message Flow
				if (modifyMessageFlowProperty(intNodeProxy, request.getMessageFlowProxy(), "This/monitoringProfile", "", timeToWaitMs)) {
					response.getMessages().put("\nremoveMonitoringProfile", "was successful");
				} else {
					response.getMessages().put("\nremoveMonitoringProfile", "failed, but will continue");
				}
			} else {
				response.getMessages().put("\npauseMonitoring", "failed, but will continue");
			}
			applyProfile = true;			
		}
 
		if (applyProfile) {			/* Does the <app-name|>Policy exist? */
			// Delete the Configurable Service, if it exists
			if (deleteConfigurableService(intNodeProxy, request.getMonitoringProfile(), timeToWaitMs)) {
				response.getMessages().put("\ndeleteConfigurableService", "was successful");
				// Create the Configurable Service
				if (createConfigurableService(intNodeProxy, request.getMonitoringProfile(), timeToWaitMs)) {
					response.getMessages().put("\ncreateConfigurableService", "was successful");
					// Associate Monitoring Profile with the configurable service
					if (modifyConfigurableService(intNodeProxy, request.getMonitoringProfile(), "profileProperties", request.getProfileAsXml(), timeToWaitMs)) {
						response.getMessages().put("\nassociateMonitoringProfileWithConfigurableService", "was successful");
						// Apply Configurable Service to Message Flow
						if (modifyMessageFlowProperty(intNodeProxy, request.getMessageFlowProxy(), "This/monitoringProfile", request.getMonitoringProfile(), timeToWaitMs)) {
							response.getMessages().put("\napplyConfigurableServiceToMessageFlow", "was successful");
							// Activate flow monitoring
							if (modifyMessageFlowProperty(intNodeProxy, request.getMessageFlowProxy(), "This/monitoring", "active", timeToWaitMs)) {
								response.getMessages().put("\nactivateFlowMonitoring", "was successful");
								response.setSuccess(true);
							} else {
								response.getMessages().put("\nactivateFlowMonitoring", "failed");
								response.setSuccess(false);
							}
						} else {
							response.getMessages().put("\napplyConfigurableServiceToMessageFlow", "failed");
							response.setSuccess(false);
						}
					} else {
						response.getMessages().put("\nassociateMonitoringProfileWithConfigurableService", "failed");
						response.setSuccess(false);
					}
				} else {
					response.getMessages().put("\ncreateConfigurableService", "failed");
					response.setSuccess(false);
				}
			} else {
				response.getMessages().put("\ndeleteConfigurableService", "failed");
				response.setSuccess(false);
			}
		}
		
		if (!applyProfile && response.getMessages().isEmpty()) {
			response.getMessages().put("Result:", "Profile already existed unchanged - it was not requested to be forcibly re-applied");
			response.setSuccess(true);
		}
		
		return response;
	}

 	private boolean deleteConfigurableService(BrokerProxy intNodeProxy, String serviceName, int timeToWaitMs) {
		try {
			intNodeProxy.setSynchronous(timeToWaitMs);
			ConfigurableService cs = intNodeProxy.getConfigurableService(MONITORING_PROFILES, serviceName);			
			if (cs != null) {
				cs.delete();			
			}
		} catch (ConfigManagerProxyPropertyNotInitializedException | ConfigManagerProxyLoggedException e) {
			logger.error(e.toString(), e);
			return false;
		} catch (Exception e) {
			e.printStackTrace();
		} 
		finally {
			intNodeProxy.setSynchronous(0);
		}
		return true;
	}

	private boolean createConfigurableService(BrokerProxy intNodeProxy, String serviceName, int timeToWaitMs) {
		try {
			intNodeProxy.setSynchronous(timeToWaitMs);
			intNodeProxy.createConfigurableService(MONITORING_PROFILES, serviceName);
			ConfigurableService	cs = intNodeProxy.getConfigurableService(MONITORING_PROFILES, serviceName);
			if (cs == null) {
				return false; // It failed 
			}
		} catch (ConfigManagerProxyLoggedException | ConfigManagerProxyPropertyNotInitializedException e) {
			logger.error(e.toString(), e);
			return false;
		} finally {
			intNodeProxy.setSynchronous(0);
		}
		return true;
	}
	
	private boolean modifyConfigurableService(BrokerProxy intNodeProxy, String serviceName, String propertyName, String propertyValue, int timeToWaitMs) {
		try {
			intNodeProxy.setSynchronous(timeToWaitMs);
			ConfigurableService cs = intNodeProxy.getConfigurableService(MONITORING_PROFILES, serviceName);
			if (cs != null) {
				cs.setProperty(propertyName, propertyValue);
			} else {
				return false; // It failed 
			}
		} catch (ConfigManagerProxyLoggedException | ConfigManagerProxyPropertyNotInitializedException e) {
			logger.error(e.toString(), e);
			return false;
		} finally {
			intNodeProxy.setSynchronous(0);
		}
		return true;
	}
	
	private boolean modifyMessageFlowProperty(BrokerProxy intNodeProxy, MessageFlowProxy mfp, String propertyName, String propertyValue, int timeToWaitMs) {
		try {
			intNodeProxy.setSynchronous(timeToWaitMs);
			mfp.setRuntimeProperty(propertyName, propertyValue);
			return true;
		} catch (ConfigManagerProxyLoggedException | IllegalArgumentException e) {
			logger.error(e.toString(), e);
			return false;
		} finally {
			intNodeProxy.setSynchronous(0);
		}
	}
}
