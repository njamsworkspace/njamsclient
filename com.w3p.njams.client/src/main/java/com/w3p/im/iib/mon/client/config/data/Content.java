package com.w3p.im.iib.mon.client.config.data;

public class Content {
	
	private String name;
	private String xpath;
	private Namespaces namespaces;
	

	public Content() {
	}

	public Content(String name, String xpath) {
		this.name = name;
		this.xpath = xpath;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getXpath() {
		return xpath;
	}

	public void setXpath(String xpath) {
		this.xpath = xpath;
	}

	public Namespaces getNamespaces() {
		return namespaces;
	}

	public void setNamespaces(Namespaces namespaces) {
		this.namespaces = namespaces;
	}

	@Override
	public String toString() {
		return "Content [name=" + name + ", xpath=" + xpath + ", namespaces=" + namespaces + "]";
	}
}
