package com.w3p.im.iib.mon.client.config.data;

import java.util.ArrayList;
import java.util.List;

public class Namespaces {
	
	private final List<Namespace> namespaces = new ArrayList<>();

	public Namespaces() {
	}
	
	public void addNamespace(Namespace ns) {
		namespaces.add(ns);
	}

	public List<Namespace> getNamespaces() {
		return namespaces;
	}

	@Override
	public String toString() {
		return "Namespaces [namespaces=" + namespaces + "]";
	}
}
