package com.w3p.im.iib.mon.client.config.data;

import java.util.List;

public interface IConfigObject {
	
	boolean isIncludeSchemaName();
	
	String getName();
	
	List<MsgFlow> getMsgFlows();

	List<SubFlow> getSubFlows();
}
