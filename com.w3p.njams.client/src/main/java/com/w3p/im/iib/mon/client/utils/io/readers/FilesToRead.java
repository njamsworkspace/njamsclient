package com.w3p.im.iib.mon.client.utils.io.readers;

import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;

public class FilesToRead {
	
	private final List<Path> filesToBeCopied;

	public FilesToRead() {
		filesToBeCopied = new ArrayList<>();
	}

	public void addFile(Path fileName) {
		filesToBeCopied.add(fileName);
	}

	public List<Path> getFiles() {
		return filesToBeCopied;
	}
}
