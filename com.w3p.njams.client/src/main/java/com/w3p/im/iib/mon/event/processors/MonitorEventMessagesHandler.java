package com.w3p.im.iib.mon.event.processors;

import java.util.concurrent.Executors;

import javax.jms.JMSException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.w3p.im.iib.mon.exceptions.internal.CreateMonitoringEventHandlerException;
import com.w3p.im.iib.mon.exceptions.internal.MessagingException;
import com.w3p.im.iib.mon.jms.consumers.MonitorEventsConsumer;
import com.w3p.im.iib.mon.jms.data.IMessageConsumer;
import com.w3p.im.iib.mon.jms.data.IMessageHandler;
import com.w3p.im.iib.mon.jms.data.IMessageProducer;
import com.w3p.im.iib.mon.jms.data.IMessagingServiceConsumer;
import com.w3p.im.iib.mon.jms.data.IMessagingServiceProducer;
import com.w3p.im.iib.mon.jms.service.MessagingServiceFactory;
import com.w3p.im.iib.mon.jms.service.MqJmsConnectionProperties;
import com.w3p.im.iib.mon.monitor.data.MonitorEventsRequest;
import com.w3p.im.iib.mon.monitor.data.MonitorEventsRequestForQueues;
import com.w3p.im.iib.mon.monitor.data.MonitorEventsRequestForTopics;
import com.w3p.im.iib.mon.mqtt.service.MqttClientConsumer;

public class MonitorEventMessagesHandler implements IMessageHandler {

	private static final Logger logger = LoggerFactory.getLogger(MonitorEventMessagesHandler.class);

	protected MonitorEventsConsumer monitorEventsConsumer;
	protected IMessageProducer monitorEventsErrorsProducer;
	private final MonitorEventsRequest request;
	private final MqJmsConnectionProperties jmsConnectionProperties;

	private final boolean useMQTT; // For MQTT event topic subscriptions
	private final boolean useTopic; // For event topic subscriptions (DEV mode)
	private final boolean useQueue; // For event messages from queue (PRO mode)

	private MqttClientConsumer mqttClientConsumer;


	public MonitorEventMessagesHandler(MonitorEventsRequest request) {
		this.request = request;
		this.jmsConnectionProperties = request.getMqJmsConnectionProperties();
		this.useMQTT = request.isUseMQTT();
		this.useQueue = request.isUseQueue();
		this.useTopic = request.isUseTopic();		
	}

	@Override
	public void handleMessagesForEventsSource(final String eventsProducer) throws CreateMonitoringEventHandlerException {		
		try {
			logger.debug("#handleMessagesForTopic() - Request received: \n   Events source = '{}'", eventsProducer);
			if (useMQTT) {
				MonitorEventsRequestForTopics topicRequest = (MonitorEventsRequestForTopics)request;
				// Create the  MQTT client service to process messages for subscribed topics
				if (mqttClientConsumer == null) {
					logger.debug("#handleMessagesForTopic(): Creating a new MqttClientConsumer for topic '{}': ", eventsProducer);						
					mqttClientConsumer = new MqttClientConsumer(topicRequest);
				}
				Executors.newSingleThreadExecutor().execute(new Runnable() {			
					@Override
					public void run() {
						try {
							// Create the  MQTT client service to process messages for subscribed topics
							if (mqttClientConsumer == null) {
								logger.debug("#handleMessagesForTopic(): Creating a new MqttClientConsumer for topic '{}': ", eventsProducer);						
								mqttClientConsumer = new MqttClientConsumer(topicRequest);
							}
							mqttClientConsumer.consumeMsgsFromDestination(eventsProducer);
						} catch (Exception e) {
							// We can't throw this error from the thread to be caught by the launching thread. Just log it.
							logger.error("JMS error when creating MQTT topic consumer for Monitoring Events. Cause is: '{}'.", e.toString(), e);
						}
					}
				});
			} else if (useTopic){
				MonitorEventsRequestForTopics topicRequest = (MonitorEventsRequestForTopics)request;
				// Create the JMS service to process messages for subscribed topics
				if (monitorEventsConsumer == null) {
					logger.debug("#handleMessagesForTopic(): Creating a new MonitorEventsConsumer for Events source '{}': ", eventsProducer);						
					monitorEventsConsumer = new MonitorEventsConsumer(topicRequest);
				}
				Executors.newSingleThreadExecutor().execute(new Runnable() {	// Can use Callable to get a return and throw exceptions - see https://www.baeldung.com/thread-pool-java-and-guava		
					@Override
					public void run() {
						try {
							IMessagingServiceConsumer topicMessagingConsumer = (IMessagingServiceConsumer)MessagingServiceFactory
									.createMessagingServiceForConsumer(topicRequest.getEventsSource(), jmsConnectionProperties); 
							topicMessagingConsumer.addMessageConsumer(eventsProducer, monitorEventsConsumer);
						} catch (Exception e) {
							// We can't throw this error from the thread to be caught by the launching thread. Just log it.
							logger.error("JMS error when creating JMS topic consumer for Monitoring Events. Cause is: '{}'.", e.toString(), e);
						}
					}
				});
			} else if (useQueue) {
				MonitorEventsRequestForQueues queueRequest = (MonitorEventsRequestForQueues)request;
				String problemsQueue = queueRequest.getIibEventsProblemsQueue(); 
				// Create the JMS service to write Event messages to a local queue when an error occurs in processing them
				if (monitorEventsErrorsProducer == null) {
					logger.debug("#handleMessagesForTopic(): Creating a new MonitorEventsErrorsProducer to write to '{}': ", problemsQueue);
					monitorEventsErrorsProducer = new MonitorEventsErrorsProducer(queueRequest);					
				}
				IMessagingServiceProducer queueMessagingProducer = (IMessagingServiceProducer)MessagingServiceFactory
						.createMessagingServiceForProducer(problemsQueue, jmsConnectionProperties);
				queueMessagingProducer.addMessageProducer(problemsQueue, monitorEventsErrorsProducer);

				// Create the JMS service to process messages from a local queue for Events source
				if (monitorEventsConsumer == null) {
					logger.debug("#handleMessagesForTopic(): Creating a new MonitorEventsConsumer for Events source '{}': ", eventsProducer);						
					monitorEventsConsumer = new MonitorEventsConsumer(queueRequest);
				}
				monitorEventsConsumer.setMessagingService((IMessagingServiceProducer)MessagingServiceFactory.getMessagingService(problemsQueue));				
				Executors.newSingleThreadExecutor().execute(new Runnable() {			
					@Override
					public void run() {
						try {
							IMessagingServiceConsumer queueMessagingConsumer = (IMessagingServiceConsumer)MessagingServiceFactory
									.createMessagingServiceForConsumer(eventsProducer, jmsConnectionProperties);
							queueMessagingConsumer.addMessageConsumer(eventsProducer, monitorEventsConsumer);
							logger.info("\n\n*+*+*+*\n   The nJAMS Client 4 IIB has started and will process monitoring event messages from MQ '{}'.\n*+*+*+*\n", eventsProducer);
						} catch (Exception e) { // MessagingException | JMSException e) {
							// We can't throw this error from the thread to be caught by the launching thread. Just log it.
							logger.error("JMS error when creating MQ queue consumer for Monitoring Events. Cause is: '{}'.", e.toString(), e);
						}
					}
				});	
			}
		} catch (Exception e) {
			throw new CreateMonitoringEventHandlerException("Unhandled exception when creating handlers for Monitoring Events. Cause is: ''{0}''.", e.toString(), e);
			// FIXME close this thread.
		}
	}

	/* Kept for future reference
	 * 
	// For use of Future and Callable, see: https://stackoverflow.com/questions/6546193/how-to-catch-an-exception-from-a-thread
 			ExecutorService executor = null;
 			executor = Executors.newSingleThreadExecutor();
			Future<Void> future = executor.submit(new Callable<Void>() {
				@Override
				public Void call() throws MessagingException {
					IMessagingService IIBMessagingService = MessagingServiceFactory.
							createMessagingService(request.getIntegrationNodeName(), jmsConnectionProperties.getMqJmsConnectionFactory(), 
									jmsConnectionProperties.getMqJmsInitialContextFactory(), jmsConnectionProperties.getMqJmsProviderUrl()); 
					IIBMessagingService.addMessageConsumer(topic, monitorEventsConsumer); // allows > 1 consumer for a service
					return null;
				}
			});
			future.get();
		} catch (Exception e) {
			MessagingServiceFactory.getMessagingService(request.getIntegrationNodeName()).removeMessageConsumer(topic);
			if (e.getClass().isAssignableFrom(ExecutionException.class)) {
				if (e.getCause().getClass().isAssignableFrom(MessagingException.class)) {
					// Something has gone wrong on the thread - log it and close the thread.
					// re-throw as a CreateMonitoringEventHandlerException as that's what was happening
					throw new CreateMonitoringEventHandlerException("JMS error when creating handlers for Monitoring Events. Cause is: ''{0}''.", e.toString(), e);
				} else  {
					// An unhandled exception
					throw new CreateMonitoringEventHandlerException("Unhandled exception when creating handlers for Monitoring Events. Cause is: ''{0}''.", e.toString(), e);					
				}
			}
			throw new CreateMonitoringEventHandlerException("Unhandled exception when creating handlers for Monitoring Events. Cause is: ''{0}''.", e.toString(), e);
		} finally {
			executor.shutdown();
		}
	 *
	 */
}
