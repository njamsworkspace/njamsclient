package com.w3p.im.iib.mon.topology.model;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.StringUtils;

import com.w3p.api.config.proxy.ConfigManagerProxyPropertyNotInitializedException;
import com.w3p.api.config.proxy.ExecutionGroupProxy;


public class IntegrationServer extends AbstractTopology{
	
	private final ExecutionGroupProxy is;
	private final List<Application> applications = new ArrayList<>();
	private final List<RestApi> restApis = new ArrayList<>();
	private final List<Service> services = new ArrayList<>();
	private final List<SharedLibrary> sharedLibraries = new ArrayList<>();

	private boolean includeSvrName;
	

	
	public IntegrationServer(ExecutionGroupProxy is) {
		this.is = is;
		try {
			super.name = is.getName();
		} catch (ConfigManagerProxyPropertyNotInitializedException e) {
			super.name = null;
			e.printStackTrace(); // FIXME unlikey to occur but must be handled correctly
		}
	}
	
	public ExecutionGroupProxy getIntegrationServer() {
		return is;
	}

	@Override
	public <T extends AbstractTopology> void addTopologyItem(T topologyItem) {		
		if (topologyItem instanceof MessageFlow) {
			messageFlows.add((MessageFlow) topologyItem);
		} else if (topologyItem instanceof SubFlow) {
			subFlows.add((SubFlow) topologyItem);
		} else if (topologyItem instanceof Application) {
			applications.add((Application) topologyItem);
		} else if (topologyItem instanceof RestApi) {
			restApis.add((RestApi) topologyItem);
		} else if (topologyItem instanceof Service) {
			services.add((Service) topologyItem);
		} else if (topologyItem instanceof SharedLibrary) {
			sharedLibraries.add((SharedLibrary) topologyItem);
		} else if (topologyItem instanceof StaticLibrary) {
			staticLibraries.add((StaticLibrary) topologyItem);
		} 
	}
	
	@Override
	public boolean isRunning() {
		try {
			return is.isRunning();
		} catch (ConfigManagerProxyPropertyNotInitializedException e) {
			e.printStackTrace();
			return false;
		}
	}

	public List<Application> getApplications() {
		return applications;
	}

	public List<Service> getServices() {
		return services;
	}

	public List<RestApi> getRestApis() {
		return restApis;
	}

	public List<SharedLibrary> getSharedLibraries() {
		return sharedLibraries;
	}

	public SubFlow getActualSubflow(FlowNode node, boolean includeSchemaName) {
		SubFlow target = null;
		String sfPath = node.getSubflowPath(includeSchemaName).toString();
		String sfPathToUse = StringUtils.substringAfter(sfPath, ">");
		String[] sfPathParts =  sfPathToUse.split(">");
		
		String staticLib = null;
		String parent = null;     // TODO change to 'deplooyable'
		String sfRealName = null;
		if (sfPathParts.length == 3) {
			// This subflow is in a Static Lib
			parent = sfPathParts[0];
			staticLib =  sfPathParts[1];
			sfRealName =  sfPathParts[2]; // Underlying (actual) subflow
		} else {
			parent =  sfPathParts[0];
			sfRealName =  sfPathParts[1]; // Underlying (actual) subflow
		}
		
		for (Application app : getApplications()) {
			if (app.getName().equals(parent)) {
				for (SubFlow sf : app.getSubFlows()) {
					if (sf.getName().equals(sfRealName)) {
						target = sf;
						return target;
					}
				}
			}
			for (StaticLibrary stl : app.getStaticLibraries()) {
				if (stl.getName().equals(staticLib)) {
					for (SubFlow sf : stl.getSubFlows()) {
						if (sf.getName().equals(sfRealName)) {
							target = sf;
							return target;
						}
					}
				}
			}
		}
		
		for (RestApi api : getRestApis()) {
			if (api.getName().equals(parent)) {
				for (SubFlow sf : api.getSubFlows()) {
					if (sf.getName().equals(sfRealName)) {
						target = sf;
						return target;
					}
				}
			}
		}
		
		for (Service service : getServices()) {
			if (service.getName().equals(parent)) {
				for (SubFlow sf : service.getSubFlows()) {
					if (sf.getName().equals(sfRealName)) {
						target = sf;
						return target;
					}
				}
			}
		}
		
		for (SharedLibrary shl : getSharedLibraries()) {
			if (shl.getName().equals(parent)) {
				for (SubFlow sf : shl.getSubFlows()) {
					if (sf.getName().equals(sfRealName)) {
						target = sf;
						return target;
					}
				}
			}
		}
		
		return target;
	}
	
	public boolean isIncludeSvrName() {
		return includeSvrName;
	}

	public void setIncludeSvrName(boolean includeSvrName) {
		this.includeSvrName = includeSvrName;
	}

	@Override
	public String toString() {
		return "IntegrationServer [name=" + name + "]";
	}
}
