package com.w3p.im.iib.mon.client.utils.io.readers;

import java.util.Set;

public interface IContent {

	void addContent(String fileName, String content);

	String getContent(String fileName);

	int getSize();

	Set<String> getNames();

}