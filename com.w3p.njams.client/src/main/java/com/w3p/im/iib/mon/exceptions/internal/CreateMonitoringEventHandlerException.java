package com.w3p.im.iib.mon.exceptions.internal;

import com.w3p.im.iib.mon.exceptions.InternalException;

public class CreateMonitoringEventHandlerException extends InternalException {


	private static final long serialVersionUID = 1L;

	public CreateMonitoringEventHandlerException() {
		super();
	}

	public CreateMonitoringEventHandlerException(String msg, Object... values) {
		super(msg, values);
	}
}
