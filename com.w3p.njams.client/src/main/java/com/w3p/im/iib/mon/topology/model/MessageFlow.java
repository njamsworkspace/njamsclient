package com.w3p.im.iib.mon.topology.model;

import com.w3p.api.config.proxy.ConfigManagerProxyPropertyNotInitializedException;
import com.w3p.api.config.proxy.MessageFlowProxy;

public class MessageFlow extends Flow {
	
	private boolean forceApplyMonitoringProfile;

	
	public MessageFlow(MessageFlowProxy mfp) {
		super(mfp);
	}

	public MessageFlowProxy getMessageFlowProxy() {
		return (MessageFlowProxy) super.fp; 
	}

	public boolean isForceApplyMonitoringProfile() {
		return forceApplyMonitoringProfile;
	}

	public void setForceApplyMonitoringProfile(boolean forceApplyMonitoringProfile) {
		this.forceApplyMonitoringProfile = forceApplyMonitoringProfile;
	}

	@Override
	public boolean isRunning() {
		try {
			return ((MessageFlowProxy)fp).isRunning();
		} catch (ConfigManagerProxyPropertyNotInitializedException e) {
			e.printStackTrace();
			return false;
		}
	}

	@Override
	public String toString() {
		return "MessageFlow [name=" + flowName + "]";
	}
}
