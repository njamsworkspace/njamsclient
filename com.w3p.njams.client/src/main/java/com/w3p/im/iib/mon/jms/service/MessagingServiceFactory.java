package com.w3p.im.iib.mon.jms.service;

import java.util.HashMap;
import java.util.Map;

import com.w3p.im.iib.mon.exceptions.internal.MessagingException;
import com.w3p.im.iib.mon.jms.data.IMessagingService;
import com.w3p.im.iib.mon.jms.data.IMessagingServiceConsumer;
import com.w3p.im.iib.mon.jms.data.IMessagingServiceProducer;

public class MessagingServiceFactory {
	
	private static final Map<String, IMessagingService> messagingServices = new HashMap<>();

	/*
	 * The MessagingService holds running services in a Map.
	 * It also establishes the JMS JNDI properties, when the service's class is created
	 */

	public static synchronized IMessagingService createMessagingServiceForConsumer(String name, MqJmsConnectionProperties connectionProperties) { // String connFactory, String initalContextFactory, String providerUrl) {
		if (messagingServices.get(name) == null) {
			IMessagingServiceConsumer ms = new MessagingServiceConsumer(connectionProperties);
			messagingServices.put(name, ms);			
		}				
		return getMessagingService(name);
	}

	public static synchronized IMessagingService createMessagingServiceForProducer(String name, MqJmsConnectionProperties connectionProperties) { // String connFactory, String initalContextFactory, String providerUrl) {
		if (messagingServices.get(name) == null) {
			IMessagingServiceProducer ms = new MessagingServiceProducer(connectionProperties);
			messagingServices.put(name, ms);			
		}				
		return getMessagingService(name);
	}


	public static synchronized IMessagingService getMessagingService(String name) {
		return messagingServices.get(name);
	}

	public static synchronized IMessagingService removeMessagingService(String name) throws MessagingException {
		getMessagingService(name).close();
		return messagingServices.remove(name);
	}

	
	/**
	 * This is for use if and when msgConsumer class names are provided externally
	 * 
	 * @param destination
	 * @param messageConsumerClassName
	 * @throws MessagingException
	 */
//	private static void createConsumerOfService(String destination,	String messageConsumerClassName) throws MessagingException {
//		
//		IMessageHandler requestProcessor = null;
//		try {
//			Class<?> clazz = Class.forName(messageConsumerClassName);
//			requestProcessor = (IMessageHandler) clazz.newInstance();
//		} catch (ClassNotFoundException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
//		catch (IllegalAccessException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		} catch (InstantiationException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
//	}
	
	public void close() {
		for (String name : messagingServices.keySet()) {
			messagingServices.get(name).close();
		}
		messagingServices.clear();		
	}

}
