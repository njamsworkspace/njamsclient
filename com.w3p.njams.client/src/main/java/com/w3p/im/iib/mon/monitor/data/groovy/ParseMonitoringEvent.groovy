package com.w3p.im.iib.mon.monitor.data.groovy

import java.math.RoundingMode
import java.text.SimpleDateFormat
import java.time.LocalDateTime
import java.util.concurrent.atomic.AtomicBoolean
import java.util.concurrent.atomic.AtomicInteger

import org.codehaus.groovy.ast.stmt.SwitchStatement
import org.codehaus.groovy.runtime.XmlGroovyMethods
import com.ibm.mb.connector.discovery.model.descriptor.ResultDescriptorGroup
import com.w3p.im.iib.mon.event.processors.MessageEvents
import com.w3p.im.iib.mon.monitor.data.TxnEndMonitoringEvent
import com.w3p.im.iib.mon.monitor.data.TxnStartMonitoringEvent
import com.w3p.im.iib.mon.monitor.data.ErrorMonitoringEvent
import com.w3p.im.iib.mon.monitor.data.MonitoringEvent

import groovy.transform.ToString
import groovy.util.slurpersupport.NodeChildren
import groovy.util.slurpersupport.NodeChild
import groovy.util.slurpersupport.Node
import groovy.xml.StreamingMarkupBuilder
import groovy.xml.XmlUtil


class ParseMonitoringEvent {
	
	private final SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSX")
	
	def MonitoringEvent parse (String eventXML) {
		
		MonitoringEvent monitoringEvent = null //new  MonitoringEvent(); 
		
		def wmbevent = new XmlSlurper(false, true).parseText(eventXML).declareNamespace('wmb':'http://www.ibm.com/xmlns/prod/websphere/messagebroker/6.1.0/monitoring/event')
		
		def eventPointData = wmbevent.'wmb:eventPointData'
		def eventData = eventPointData.'wmb:eventData'
		
		// Create sub-class of MonitoringEvent according to its type
		String eventType = eventData.'wmb:eventIdentity'.@'wmb:eventName'
		if (eventType.endsWith(MessageEvents.TRANSACTION_START)) {
			monitoringEvent = new TxnStartMonitoringEvent()
		} else if (eventType.endsWith(MessageEvents.TRANSACTION_END)) {
			monitoringEvent = new TxnEndMonitoringEvent()
		} else if (eventType.endsWith(MessageEvents.CATCH_TERMINAL)
			|| eventType.endsWith(MessageEvents.FAILURE_TERMINAL)
			|| eventType.endsWith(MessageEvents.FAULT_TERMINAL)
			|| eventType.endsWith(MessageEvents.TRANSACTION_ROLLBACK)) {
			monitoringEvent = new ErrorMonitoringEvent()
		} else {
			monitoringEvent = new MonitoringEvent()
		}
		
		monitoringEvent.eventName = eventData.'wmb:eventIdentity'.@'wmb:eventName'
		String creationTime = eventData.'wmb:eventSequence'.@'wmb:creationTime'  // "YYY-MM-DDThh:mm:ss.nnnnnnZ"
		String creationTimeNoZone = creationTime.take(creationTime.length() - 1) // Lose the trailing Z 
		monitoringEvent.creationTime = LocalDateTime.parse(creationTimeNoZone)  //format.parse(timeMillis).getTime()
		monitoringEvent.counter = eventData.'wmb:eventSequence'.@'wmb:counter'.toString() as Integer
		
		monitoringEvent.localTransactionId = eventData.'wmb:eventCorrelation'.@'wmb:localTransactionId'
		monitoringEvent.globalTransactionId = eventData.'wmb:eventCorrelation'.@'wmb:globalTransactionId'
		
		def messageFlowData = eventPointData.'wmb:messageFlowData'		
		monitoringEvent.intSvrName = messageFlowData.'wmb:executionGroup'.@'wmb:name'
		monitoringEvent.messageFlow = messageFlowData.'wmb:messageFlow'.@'wmb:name'
		monitoringEvent.uniqueflowName = messageFlowData.'wmb:messageFlow'.@'wmb:uniqueFlowName'
		monitoringEvent.nodeLabel = messageFlowData.'wmb:node'.@'wmb:nodeLabel'
		monitoringEvent.nodeType = messageFlowData.'wmb:node'.@'wmb:nodeType'

		NodeChildren simpleContent = wmbevent.'wmb:applicationData'?.'wmb:simpleContent'
		def appDataMap = [:] // Map to hold any applicationData
		simpleContent?.each { NodeChild child ->
			if (child.@'wmb:name'.text() == 'MsgId') {
				monitoringEvent.msgId = child.@'wmb:value'.text()
			} else if (child.@'wmb:name'.text() == "CorrelId") {
				monitoringEvent.correlId = child.@'wmb:value'.text()
			} 
//			else { // If MsgId or correlId is in AppData - leave it there
				if (child.@'wmb:name') {
					appDataMap.(child.@'wmb:name'.text()) = child.@'wmb:value'.text()
				}
//			}
		}
		if (!appDataMap.isEmpty()) {
			monitoringEvent.applicationData =  appDataMap
		}

		NodeChildren complexContent = wmbevent.'wmb:applicationData'?.'wmb:complexContent'
		if (complexContent.@'wmb:elementName' == "ExceptionList") {
			complexContent.'ExceptionList'.getAt(0).each { it ->
				String el = new StreamingMarkupBuilder().bindNode(it) as String // No <xml version...>
				def elf = new XmlParser().parseText(el) // Formats xml
				def stw = new StringWriter()
				new XmlNodePrinter(new PrintWriter(stw)).print(elf)
				monitoringEvent.exceptionList = "${'<![CDATA['}${stw.toString()}${']]>'}"
			}
		}

		monitoringEvent.bodyIn =  wmbevent.'wmb:bitstreamData'?.'wmb:bitstream'
		
		return monitoringEvent
	}

}
