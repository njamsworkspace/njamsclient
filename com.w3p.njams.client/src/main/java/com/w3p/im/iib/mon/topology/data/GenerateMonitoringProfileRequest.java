package com.w3p.im.iib.mon.topology.data;

import com.w3p.api.config.proxy.BrokerProxy;
import com.w3p.im.iib.mon.client.config.data.IntegrationServer;
import com.w3p.im.iib.mon.topology.model.TopologyObject;

public class GenerateMonitoringProfileRequest {
	
	private final BrokerProxy integrationNodeProxy;
	private final String intSvrName;
	private TopologyObject topology;
	
	private int timeToWait; // Wait-time in seconds for sync calls to remote broker for applying Monitoring Profiles
	private String intNodeName;
	private String monProfileDir;
	private IntegrationServer integrationServer;
	
	private boolean applyProfileIfUnchanged; // 'true' = generate profile and apply it even if it has not changed (compared to current file)
	
	
	public GenerateMonitoringProfileRequest(BrokerProxy integrationNodeProxy, String intSvrName, TopologyObject topology) { //, MonitoredObject<?> monitoringScope) {
		this.integrationNodeProxy = integrationNodeProxy;
		this.topology = topology;
		this.intSvrName = intSvrName;
	}

	public int getTimeToWait() {
		return timeToWait;
	}

	public GenerateMonitoringProfileRequest setTimeToWait(int timeToWait) {
		this.timeToWait = timeToWait;
		return this;
	}

	public BrokerProxy getIntegrationNodeProxy() {
		return integrationNodeProxy;
	}

	public String getIntSvrName() {
		return intSvrName;
	}

	public boolean isApplyProfileIfUnchanged() {
		return applyProfileIfUnchanged;
	}

	public GenerateMonitoringProfileRequest setApplyProfileIfUnchanged(boolean applyProfileIfChanged) {
		this.applyProfileIfUnchanged = applyProfileIfChanged;
		return this;
	}

	public String getIntNodeName() {
		return intNodeName;
	}

	public GenerateMonitoringProfileRequest setIntNodeName(String intNodeName) {
		this.intNodeName = intNodeName;
		return this;
	}

	public String getMonProfileDir() {
		return monProfileDir;
	}

	public GenerateMonitoringProfileRequest setMonProfileDir(String monProfileDir) {
		this.monProfileDir = monProfileDir;
		return this;
	}

	public TopologyObject getTopology() {
		return topology;
	}

	public IntegrationServer getIntegrationServer() {
		return integrationServer;
	}

	public GenerateMonitoringProfileRequest setIntegrationServer(IntegrationServer intSvr) {
		this.integrationServer = intSvr;
		return this;
	}
}
