package com.w3p.im.iib.mon.client.model.data;

import java.io.Serializable;
import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;


public class ActivityModelData implements Serializable {
	
	private static final long serialVersionUID = 1L;
	
	private final ProcessModelData processModelData;
    private GroupModelData parent;

    // fromActivityModelId -> TransitionModel
    private final Map<String, TransitionModelData> incoming = new LinkedHashMap<>();
    // toActivityModelId -> TransitionModel
    private final Map<String, TransitionModelData> outgoing = new LinkedHashMap<>();
   
    // this will be used for the svg rendering
    private int x;
    private int y;

    private boolean starter;

    private String id;
    private String name;
    private String type;
    private String config;
    private String mapping;


	public ActivityModelData(ProcessModelData processModelData) {
		this.processModelData = processModelData;
	}

	public ActivityModelData(ProcessModelData processModelData, String id, String name, String type) {
		this.processModelData = processModelData;
		this.id = id;
		this.name = name;
		this.type = type;
	}

	public ProcessModelData getProcessModelData() {
		return processModelData;
	}

	public GroupModelData getParent() {
		return parent;
	}

	public void setParent(GroupModelData parent) {
		this.parent = parent;
		parent.addChildActivity(this);
	}

	public int getX() {
		return x;
	}

	public void setX(int x) {
		this.x = x;
	}

	public int getY() {
		return y;
	}

	public void setY(int y) {
		this.y = y;
	}

	public boolean isStarter() {
		return starter;
	}

	public void setStarter(boolean starter) {
		this.starter = starter;
		if (starter) {
			if (parent != null) {
				parent.addStartActivity(this);
			} else {
				processModelData.addStartActivity(this);
			}
		}
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getConfig() {
		return config;
	}

	public void setConfig(String config) {
		this.config = config;
	}

	public String getMapping() {
		return mapping;
	}

	public void setMapping(String mapping) {
		this.mapping = mapping;
	}
	
	public void addIncomingTransition(final TransitionModelData transitionModel) {
		String fromId = transitionModel.getFromActivity().getId();
		incoming.put(fromId, transitionModel);
	}

    public List<ActivityModelData> getPredecessors() {
        return Collections.unmodifiableList(incoming.values().stream().map(t -> t.getFromActivity())
                .collect(Collectors.toList()));
    }
    
    public void addOutgoingTransition(TransitionModelData transitionModel) {
    	String toId = transitionModel.getToActivity().getId();
    	outgoing.put(toId, transitionModel);
    }
    public List<ActivityModelData> getSuccessors() {
        return Collections.unmodifiableList(outgoing.values().stream().map(t -> t.getToActivity())
                .collect(Collectors.toList()));
    }
    
    public final boolean isGroup() {
        return GroupModelData.class.isAssignableFrom(this.getClass());
    }

	@Override
	public String toString() {
		return "ActivityModelData [id=" + id + "]";
	}

}
