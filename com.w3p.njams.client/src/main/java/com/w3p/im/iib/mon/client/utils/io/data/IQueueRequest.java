package com.w3p.im.iib.mon.client.utils.io.data;

public interface IQueueRequest extends IRequest {
	
	public IQueueRequest setConnectionData(ConnectionData connData);
	
	public ConnectionData getConnectionData();

}
