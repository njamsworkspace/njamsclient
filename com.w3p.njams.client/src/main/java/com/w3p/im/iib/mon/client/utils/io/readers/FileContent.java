package com.w3p.im.iib.mon.client.utils.io.readers;

import java.util.Hashtable;
import java.util.Set;

public class FileContent implements IContent {
	
	private final Hashtable<String, String> contents = new Hashtable<String, String>(); 


	public FileContent() {
	}

	@Override
	public void addContent(String name, String content) {
		contents.put(name, content);
	}

	@Override
	public String getContent(String name) {
		return contents.get(name);
	}
	
	@Override
	public int getSize() {
		return contents.size();
	}
	
	@Override
	public Set<String> getNames() {
		return contents.keySet();
	}
	
	/**
	 * Only for jUnit testing
	 * @return
	 */
	protected String getContentOfFirstItem() {
		return contents.values().iterator().next();
	}
}
