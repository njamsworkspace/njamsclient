package com.w3p.im.iib.mon.client.model.data;

import static java.util.stream.Collectors.toList;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import com.im.njams.sdk.common.Path;

public class ProcessModelData implements Serializable{
	
	private static final long serialVersionUID = 1L;
	
	private final PathData path;
	private final Map<String, ActivityModelData> activityModels = new LinkedHashMap<>();
	private final Map<String, ActivityModelData> startActivityModels = new LinkedHashMap<>();
	private final Map<String, TransitionModelData> transitionModels = new LinkedHashMap<>();
	private final Map<String, Object> properties = new LinkedHashMap<>();

    private boolean starter;
    private String svg;

	public ProcessModelData(Path path) {
		this.path = new PathData(path.getParts());
	}
	
    /**
     * Called by GroupModelData#createChildSubProcess
     * @param subProcessModelId
     * @param subProcessName
     * @param subProcessType
     * @return
     */
    public SubProcessActivityModelData createSubProcessModelData(String subProcessModelId, String subProcessName, String subProcessType) {
    	SubProcessActivityModelData spamd = new SubProcessActivityModelData(this);
    	spamd.setId(subProcessModelId);
    	spamd.setName(subProcessName);
    	spamd.setType(subProcessType);
    	addActivityModel(spamd);
    	return spamd;
    }

	
	public void addActivityModel(ActivityModelData amd) {
		activityModels.put(amd.getId(), amd);
//		if (amd.getType().equals("startType")) {
//			addStartActivity(amd);
//		}
	}
	
	public ActivityModelData getActivityModel(String id) {
		return activityModels.get(id);		
	}

	public List<ActivityModelData> getActivityModels() {
		return Collections.unmodifiableList(new ArrayList<>(activityModels.values()));
	}

	public void addGroupModel(GroupModelData gmd) {
		activityModels.put(gmd.getId(), gmd);
	}
	
    public List<GroupModelData> getGroupModels() {
        return activityModels.values().stream()
                .filter(am -> GroupModelData.class.isAssignableFrom(am.getClass())).map(GroupModelData.class::cast)
                .collect(toList());
    }
	public Path getPath() {
		return new Path(path.getParts());
	}
	
    public void addStartActivity(ActivityModelData activity) {
        if (!activity.isStarter()) {
            activity.setStarter(true);
        }
        startActivityModels.put(activity.getId(), activity);
    }
    
    public List<ActivityModelData> getStartActivities() {
        return Collections.unmodifiableList(new ArrayList<>(startActivityModels.values()));
    }

    public void addTransition(TransitionModelData tm) {
    	transitionModels.put(tm.getId(), tm);
    }
    
    public List<TransitionModelData> getTransitionModels() {
        return Collections.unmodifiableList(new ArrayList<>(transitionModels.values()));
    }
    
    public TransitionModelData getTransitionModel(String transitionModelId) {
        return transitionModels.get(transitionModelId);
    }
    
    public String getName() {
    	// is the last element in the PathData
        return path.getObjectName();
    }
    
	public boolean isStarter() {
		return starter;
	}

	public void setStarter(boolean starter) {
		this.starter = starter;
	}

	public String getSvg() {
		return svg;
	}

	public void setSvg(String svg) {
		this.svg = svg;
	}	
	
    public Set<String> getPropertyNames() {
		return properties.keySet();
	}

	public Object getProperty(String key) {
        return properties.get(key);
    }

    public boolean hasProperty(String key) {
        return properties.containsKey(key);
    }

    public void setProperty(String key, Object value) {
        properties.put(key, value);
    }

	@Override
	public String toString() {
		return "ProcessModelData [path=" + path + "]";
	}

}
