package com.w3p.im.iib.mon.client.utils.io.writers;

import com.w3p.im.iib.mon.client.utils.io.data.Response;

public interface IWriter {

	void write(String name, String content, Response resp);


}