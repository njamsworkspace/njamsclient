package com.w3p.im.iib.mon.client.utils.serialize;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

import com.w3p.im.iib.mon.client.model.data.ProcessModelData;

public class AbstractSerializer {


	protected void write(Serializable ser, String folder, String file) throws Exception {
		Path folderPath = Paths.get(folder);
		if ((Files.notExists(folderPath))) {
			Files.createDirectories(folderPath);
		}
		File dest = new File(folderPath.toString(), file);
		try (FileOutputStream fileOutputStream = new FileOutputStream(dest);
				ObjectOutputStream objectOutputStream = new ObjectOutputStream(fileOutputStream);) {
			objectOutputStream.writeObject(ser);
			objectOutputStream.flush();
		} catch (FileNotFoundException e ) {
			throw new Exception (e.toString(), e); // TODO Create specific exceptions
		} catch (IOException e) {
			throw new Exception (e.toString(), e);
		}			
	}

	protected Object read(File serialized) throws Exception {
		Object original = null;
		try (FileInputStream fileInputStream = new FileInputStream(serialized);
				ObjectInputStream objectInputStream = new ObjectInputStream(fileInputStream);) {
			original = objectInputStream.readObject();			
		} catch (Exception e) {
			throw new Exception(e.toString(), e);
		}
		return original;
	}

}