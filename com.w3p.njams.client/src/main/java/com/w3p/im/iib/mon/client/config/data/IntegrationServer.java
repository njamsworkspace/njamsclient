package com.w3p.im.iib.mon.client.config.data;

import java.util.ArrayList;
import java.util.List;

public class IntegrationServer {
	
	private final String name;
	private final boolean includeName; // include Int Server name in monitoring displays
	public boolean isIncludeSchemaName() {
		return includeSchemaName;
	}

	private final boolean includeSchemaName; // To make unique concatenated names 
	private List<Application> applications = new ArrayList<>();
	private List<SharedLibrary> sharedLibs = new ArrayList<>();
	private List<RestApi> restAPIs = new ArrayList<>();
	private List<Service> services = new ArrayList<>();

	
	public IntegrationServer(String name, boolean includeName, boolean includeSchemaName) {
		this.name = name;
		this.includeName = includeName;
		this.includeSchemaName = includeSchemaName;
	}


	public String getName() {
		return name;
	}
	public boolean isIncludeName() {
		return includeName;
	}
	public IDeployableObject getApplication(String name) {
		for (IDeployableObject application : applications) {
			if (application.getName().equals("*") && applications.size()==1) {
				return applications.get(0);
			} else if (application.getName().equals(name)) {
				return application;
			}
		}
		return null;
	}
	public List<Application> getApplications() {
		return applications;
	}
	public void setApplications(List<Application> applications) {
		this.applications = applications;
	}
	public List<SharedLibrary> getSharedLibs() {
		return sharedLibs;
	}

	public void setSharedLibs(List<SharedLibrary> sharedLibs) {
		this.sharedLibs = sharedLibs;
	}

	public List<RestApi> getRestAPIs() {
		return restAPIs;
	}

	public void setRestAPIs(List<RestApi> restAPIs) {
		this.restAPIs = restAPIs;
	}

	public RestApi getRestAPI(String name) {
		for (RestApi restAPI : restAPIs) {
			if (restAPI.getName().equals("*") && restAPIs.size()==1) {
				return restAPIs.get(0);
			} else if (restAPI.getName().equals(name)) {
				return restAPI;
			}
		}
		return null;
	}
	
	public List<Service> getServices() {
		return services;
	}

	public void setServices(List<Service> services) {
		this.services = services;
	}

	public Service getService(String name) {
		for (Service service : services) {
			if (service.getName().equals("*") && services.size()==1) {
				return services.get(0);
			} else if (service.getName().equals(name)) {
				return service;
			}
		}
		return null;
	}

	@Override
	public String toString() {
		return "IntegrationServer [name=" + name + "]";
	}
}
