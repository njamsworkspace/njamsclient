package com.w3p.im.iib.mon.client.utils.io.data;

import java.util.Map;

public interface IWriteQueueRequest extends IQueueRequest {
	
	/**
	 * Default is BYTES
	 * @return type of message to create: byte{} or text
	 */
	String getMsgType();
	
	IWriteQueueRequest setMsgType(String msgType);
	
	IWriteQueueRequest addMsgProperty(String name, String value);
	
	String getMsgProperty(String name);

	Map<String, String> getMsgProperties();

}
