package com.w3p.im.iib.mon.monitor.data.groovy

import static com.w3p.im.iib.mon.client.constants.IClientConstants.UTF_8;

import com.w3p.im.iib.mon.monitor.data.MonitoringEventSummary

import groovy.xml.MarkupBuilder
import groovy.xml.StreamingMarkupBuilder


class CreateMonitoringEventSummary {

	// Some Event timestamps come with microseconds - use <string>[..27] to truncate

	String createAsXML (MonitoringEventSummary mes) {

		def builder = new StreamingMarkupBuilder()
		builder.encoding = UTF_8
		def evSummary = {
			mkp.xmlDeclaration()
			summary {
				integrationNode(mes.integrationNode)
				integrationServer(mes.integrationServer)
				messageFlow(mes.messageFlow)
				correlationId(mes.correlationId)
				startTime(mes.start)
				endTime(mes.end)
				duration(new Date().parse("yyyy-MM-dd'T'HH:mm:ss.SSSSSSX", mes.end).getTime() - new Date().parse("yyyy-MM-dd'T'HH:mm:ss.SSSSSSX", mes.start).getTime())
				executionPath{
					mes.executionPath.each { nodeData ->
						node(name:nodeData.nodeLabel) {
							payloadOut(nodeData.payloadOut)
							applicationData(nodeData.applicationData)
						}
					}
				}
			}
		}
		
		String summaryAsXML = builder.bind(evSummary)
		return summaryAsXML
	}

}
