package com.w3p.im.iib.mon.topology;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.List;
import java.util.Properties;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.w3p.api.config.proxy.AdministeredObject;
import com.w3p.api.config.proxy.ApplicationProxy;
import com.w3p.api.config.proxy.BrokerProxy;
import com.w3p.api.config.proxy.ConfigManagerProxyLoggedException;
import com.w3p.api.config.proxy.ConfigManagerProxyPropertyNotInitializedException;
import com.w3p.api.config.proxy.ExecutionGroupProxy;
import com.w3p.api.config.proxy.FlowProxy;
import com.w3p.api.config.proxy.MessageFlowProxy;
import com.w3p.api.config.proxy.MessageFlowProxy.Node;
import com.w3p.api.config.proxy.MessageFlowProxy.NodeConnection;
import com.w3p.api.config.proxy.RestApiProxy;
import com.w3p.api.config.proxy.ServiceProxy;
import com.w3p.api.config.proxy.SharedLibraryProxy;
import com.w3p.api.config.proxy.StaticLibraryProxy;
import com.w3p.api.config.proxy.SubFlowProxy;
import com.w3p.im.iib.mon.exceptions.internal.IIBTopologyCreationException;
import com.w3p.im.iib.mon.monitor.data.MonitoredObject;
import com.w3p.im.iib.mon.topology.data.TopologyRequest;
import com.w3p.im.iib.mon.topology.data.TopologyResponse;
import com.w3p.im.iib.mon.topology.model.AbstractTopology;
import com.w3p.im.iib.mon.topology.model.Application;
import com.w3p.im.iib.mon.topology.model.FlowNode;
import com.w3p.im.iib.mon.topology.model.FlowNodeConnection;
import com.w3p.im.iib.mon.topology.model.IntegrationServer;
import com.w3p.im.iib.mon.topology.model.MessageFlow;
import com.w3p.im.iib.mon.topology.model.RestApi;
import com.w3p.im.iib.mon.topology.model.Service;
import com.w3p.im.iib.mon.topology.model.SharedLibrary;
import com.w3p.im.iib.mon.topology.model.StaticLibrary;
import com.w3p.im.iib.mon.topology.model.SubFlow;
import com.w3p.im.iib.mon.topology.model.TopologyObject;

public class TopologyCreator {	
	private static final Logger logger = LoggerFactory.getLogger(TopologyCreator.class);

	
	public TopologyResponse createTopology(TopologyRequest request) throws IIBTopologyCreationException {

		TopologyResponse topologyResponse = new TopologyResponse();
		BrokerProxy intNodeProxy = request.getIntegrationNode();
		TopologyObject topology = new TopologyObject(intNodeProxy);		
		String intSvrName = request.getIntSvrName();
		
		IntegrationServer is = null;
		try {
			ExecutionGroupProxy intSvrProxy =  intNodeProxy.getExecutionGroupByName(intSvrName);
			if (intSvrProxy == null) {
				logger.error("Integration Server '{}' was not found - monitoring abandoned", intSvrName);
				topologyResponse.setMessage("Integration Server '"+intSvrName+"' was not found - monitoring abandoned");
				topologyResponse.setSuccess(false);
				return topologyResponse;
			}
			
			is = new IntegrationServer(intSvrProxy);
			@SuppressWarnings("unchecked")
			MonitoredObject<MonitoredObject<?>> monitoredIntNode = (MonitoredObject<MonitoredObject<?>>) request.getMonitoringScope(); // Int Node

			getTree(is, intSvrProxy,  monitoredIntNode.getMonitoredChildObjectByName(intSvrProxy.getClass(), intSvrProxy.getName())); // this method is called recursively
			topology.addTopologyItem(is);
			topologyResponse.setTopology(topology);
			if (topology.isSomethingToMonitor(intSvrName)) {
				topologyResponse.setMessage(String.format("Topology was produced for Int Svr: '%s'", intSvrName));					
			} else {
				topologyResponse.setMessage(String.format("No active Message Flows were found for Int Svr: '%s' - nothing will be monitored", intSvrName));
			}
			topologyResponse.setSuccess(topology.isSomethingToMonitor(intSvrName));
			logger.debug(topologyResponse.getMessage());
		} catch (ConfigManagerProxyPropertyNotInitializedException e) {
			throw new IIBTopologyCreationException("Error when using the IIB IntegrationAPI. Exception details: ''{0}''.", e.toString(), e);
		} catch ( Exception e) {
			throw new IIBTopologyCreationException("Unhandled error when using the IIB IntegrationAPI. Exception details: ''{0}''.", e.toString(), e);
		}
		
		return topologyResponse;
	}

	/**
	 * This method is called recursively down the tree of AdministeredObjects (ExecutionGroup/Application/Libraries/MessageFlows/SubFlows
	 * 	The recursion ends when a MessageFlow or a SubFlow is encountered. The topology of these is created, before returning up the tree.
	 * 
	 * @param topologyItem
	 * @param ao
	 * @param mo
	 * @throws ConfigManagerProxyPropertyNotInitializedException 
	 */
	protected <T extends AbstractTopology, M extends MonitoredObject<?>> void getTree(T topologyItem, AdministeredObject ao, M mo) throws ConfigManagerProxyPropertyNotInitializedException {
		Properties running = new Properties();

		if (ao.getClass().isAssignableFrom(ExecutionGroupProxy.class)) {
			// Applications in Int Svr
			processChildren(topologyItem, ao, mo, running, "Applications");
			// RestAPIs in Int Svr
			processChildren(topologyItem, ao, mo, null, "RestApis");
			// Services (SOAP) in Int Svr
			processChildren(topologyItem, ao, mo, null, "Services");
			// Shared Libraries in Int Svr
			processChildren(topologyItem, ao, mo, null, "SharedLibraries");
			
		} else if (ao.getClass().isAssignableFrom(ApplicationProxy.class)) {
			// MessageFlows in Application
			processChildren(topologyItem, ao, mo, running, "MessageFlows");
			// SubFlows in Application
			processChildren(topologyItem, ao, mo, null, "SubFlows");	
			// Static Libraries in Application (note: Shared Libraries in an App, will be references to SharedLibs in the Int Svr - don't duplicate)
			processChildren(topologyItem, ao, mo, null, "StaticLibraries");

		} else if (ao.getClass().isAssignableFrom(RestApiProxy.class)) {
			// MessageFlows in Rest API  
			processChildren(topologyItem, ao, mo, running, "MessageFlows");
			// SubFlows in Rest API
			processChildren(topologyItem, ao, mo, null, "SubFlows");
			// Static Libraries in RestApi
			processChildren(topologyItem, ao, mo, null, "StaticLibraries");

		} else if (ao.getClass().isAssignableFrom(ServiceProxy.class)) {
			// MessageFlows in Service  
			processChildren(topologyItem, ao, mo, running, "MessageFlows");
			// SubFlows in Service
			processChildren(topologyItem, ao, mo, null, "SubFlows");
			// Static Libraries in Service
			processChildren(topologyItem, ao, mo, null, "StaticLibraries");

		} else if (ao.getClass().isAssignableFrom(SharedLibraryProxy.class)) {
			// SubFlowsFlows in Shared Library  
			processChildren(topologyItem, ao, mo, null, "SubFlows");
			// MessageFlows in Shared Library
			processChildren(topologyItem, ao, mo, running, "MessageFlows");	
			
		} else if (ao.getClass().isAssignableFrom(StaticLibraryProxy.class)) {
			// SubFlows in Static Library  
			processChildren(topologyItem, ao, mo, null, "SubFlows");
			// MsgFlows in Static Library
			processChildren(topologyItem, ao, mo, running, "MessageFlows");			
		}
	}

	private <T extends AbstractTopology, M extends MonitoredObject<?>> void processChildren(T topologyItem, AdministeredObject ao, M mo, Properties props, String childType)
			throws ConfigManagerProxyPropertyNotInitializedException {
		
		Enumeration<?> children = null;		
		try {
			Method getChildren = ao.getClass().getMethod("get"+childType, Properties.class);
			children = (Enumeration<?>) getChildren.invoke(ao, props);
		} catch (NoSuchMethodException | SecurityException | IllegalAccessException | IllegalArgumentException | InvocationTargetException e) {
			logger.error(e.toString(), e);
		}		

		topologyItem.setIncludeSchemaName(mo.isIncludeSchemaName());
		if (children != null) {
			while (children.hasMoreElements()) {
				AdministeredObject childProxy = (AdministeredObject) children.nextElement();
				// Static Libs are a fixed part of the their parent and have to be monitored
				if (
					mo.isSomethingToMonitor(childProxy.getClass(), childProxy.getName())) {
					if (childProxy.getClass().isAssignableFrom(MessageFlowProxy.class)) {
						createMessageFlow(topologyItem, childProxy, mo);	// End of recursion
					} else if (childProxy.getClass().isAssignableFrom(SubFlowProxy.class)) {
						createSubFlow(topologyItem, childProxy, mo);	// End of recursion
					} else {
						T type = addTopologyItem(topologyItem, childProxy);
						getTree(type, childProxy, mo.getMonitoredChildObjectByName(childProxy.getClass(), childProxy.getName()));
					}
				}
			}
		}
	}

	private <T extends AbstractTopology, M extends MonitoredObject<?>> void createMessageFlow(T topologyItem, AdministeredObject childProxy, M mo)
			throws ConfigManagerProxyPropertyNotInitializedException {

		MessageFlowProxy mfp = (MessageFlowProxy)childProxy;
		MessageFlow mf = new MessageFlow(mfp); 
		mf.setIncludeSchemaName(mo.isIncludeSchemaName());
		MonitoredObject<?> monFlow = mo.getMonitoredChildObjectByName(childProxy.getClass(), childProxy.getName());
		if (monFlow == null) {
			 monFlow = mo.getMonitoredChildObjectByName(childProxy.getClass(), "*");
		}
		mf.setIncludePayload(monFlow == null ? false : monFlow.isIncludePayload());
		mf.setLabelNodeStartsGroup(monFlow == null ? false : monFlow.isLabelNodeStartsGroup());
		logger.debug("createMessageFlow(): Creating Message Flow'{}' to be monitored", mf.getName());
		topologyItem.addTopologyItem(mf);
		List<FlowNode> nodeList = getFlowNodes(mfp);
		mf.setFlowNodes(nodeList);
		//TODO should auto-activation be a global parameter?
		
		try {
			String monState = mfp.getRuntimeProperty("This/monitoring");
			if (monState != null && monState.equals("active")) {
				String monProf = mfp.getRuntimeProperty("This/monitoringProfile");
				if (monProf == null) {
				// Ensure monitoring is inactive and force recreation of Profile, just in case it exists but not used
				mfp.setRuntimeProperty("This/monitoring", "inactive");
				mf.setForceApplyMonitoringProfile(true); // Ensure profile is re-applied
				logger.warn("  *** Msgflow: {}, has no Monitoring Profile assigned to it - force it to be recreated. Monitoring now inactive", mfp.getName());
				}
			}
		} catch (ConfigManagerProxyLoggedException | IllegalArgumentException e) {
			logger.error(e.toString(), e); 
		}
	}

	private <T extends AbstractTopology, M extends MonitoredObject<?>> void createSubFlow(T topologyItem, AdministeredObject childProxy, M mo)
			throws ConfigManagerProxyPropertyNotInitializedException {
		
		SubFlowProxy sfp = (SubFlowProxy)childProxy;
		SubFlow sf = new SubFlow(sfp);
		sf.setIncludeSchemaName(mo.isIncludeSchemaName());
		MonitoredObject<?> monFlow = mo.getMonitoredChildObjectByName(childProxy.getClass(), childProxy.getName());
		if (monFlow == null) {
			 monFlow = mo.getMonitoredChildObjectByName(childProxy.getClass(), "*");
		}
		sf.setIncludePayload(monFlow == null ? false : monFlow.isIncludePayload());
		sf.setLabelNodeStartsGroup(monFlow == null ? false : monFlow.isLabelNodeStartsGroup());
		logger.debug("createSubFlow: Creating SubFlow '{}' to be monitored", sf.getName());
		topologyItem.addTopologyItem(sf);
		List<FlowNode> nodeList = getFlowNodes(sfp);
		sf.setFlowNodes(nodeList);
	}

	private List<FlowNode> getFlowNodes(FlowProxy flowProxy) throws ConfigManagerProxyPropertyNotInitializedException {
		
		List<FlowNode> flowNodes = new ArrayList<>();
		Enumeration<NodeConnection> connections = null; 
		if (flowProxy instanceof MessageFlowProxy) {
			connections = ((MessageFlowProxy)flowProxy).getNodeConnections();	
		} else {
			connections = ((SubFlowProxy)flowProxy).getNodeConnections();
		}

		List<NodeConnection> connectionsList = new ArrayList<>();		
		while (connections.hasMoreElements()) {
			MessageFlowProxy.NodeConnection connection = (MessageFlowProxy.NodeConnection) connections.nextElement();
			connectionsList.add(connection);
		}

		// A Publication node (ComIbmPublication) in the flow diagram is represented by the node and a doppelganger, in the FlowProxy.
		// There is a connection between them from the (internal) 'response' terminal to the 'in' of the doppelganger.
		// Handle by removing the connection
		boolean pubSubNodeIncluded = false;
		Enumeration<Node> nodeProxies = null;
		if (flowProxy instanceof MessageFlowProxy) {
			nodeProxies = ((MessageFlowProxy)flowProxy).getNodes();	
		} else {
			nodeProxies = ((SubFlowProxy)flowProxy).getNodes(); // FIXME subflows
		}	

		while (nodeProxies.hasMoreElements()) {
			MessageFlowProxy.Node node = (MessageFlowProxy.Node) nodeProxies.nextElement();
			FlowNode flowNode = new FlowNode(node);
			// We need to create the InputNode in order to identify it's target as the Input node in TopologyProducerForNjams
			if (node.getType().equals("ComIbmPublication")) { //TODO change to ! and resequence as the ! condition more common
				if (!pubSubNodeIncluded) {
					List<FlowNodeConnection> targetNodes = getFlowNodeConnections(node.getName(), connectionsList);
					// Check for a self-connection - i.e targetNode = sourceNode (e.g FileOutput node EoD->EoD connection)
					handleSelfConections(targetNodes, flowNode, flowProxy.getName());
					flowNode.setFlowNodeConnections(targetNodes);
					flowNodes.add(flowNode);
					pubSubNodeIncluded = true;
				}
			} else {
				List<FlowNodeConnection> targetNodes = getFlowNodeConnections(node.getName(), connectionsList);
				// Check for a self-connection - i.e targetNode = sourceNode (e.g FileOutput node EoD->EoD connection)
				handleSelfConections(targetNodes, flowNode, flowProxy.getName());
				flowNode.setFlowNodeConnections(targetNodes);
				flowNodes.add(flowNode);
			}
		}
		return flowNodes;
	}

	private void handleSelfConections(List<FlowNodeConnection> targetNodes, FlowNode flowNode, String flowName) {
		FlowNodeConnection selfRefConnection = null;
		for (FlowNodeConnection fnc : targetNodes) {
			if (fnc.getSourceNode().equals(fnc.getTargetNode())) {
				// Remove this connection. The recursive use of getTree() to create the nJAMS topology, goes into a loop if left in
				selfRefConnection = fnc;
			}
			
		}
		// Can't remove the list entry while iterating
		if (selfRefConnection != null) {
			targetNodes.remove(selfRefConnection);
			if (flowNode.getType().equals("ComIbmPublication")) {
				// This connection is between the 'real' node and it's doppelganger - so we can't refer to 
				logger.warn("Found a Publication node with connection to its 'doppelganger' node. Removed the connection."
						+ "\n >> Flow: '{}', Node: '{}', Source terminal: '{}', Target terminal '{}'",
						flowName, flowNode.getName(), selfRefConnection.getSourceOutTerminal(), selfRefConnection.getTargetInTerminal());
			} else {
			logger.warn("Found a node with a recursive connection. Removed the connection.\n >> Flow: '{}', Node: '{}', Source terminal: '{}', Target terminal '{}'",
					flowName, flowNode.getName(),  selfRefConnection.getSourceOutTerminal(), selfRefConnection.getTargetInTerminal());
			}			
		}
	}

	private List<FlowNodeConnection> getFlowNodeConnections(String nodeName, List<NodeConnection> connectionsList) {
		List<FlowNodeConnection> targetNodes = new ArrayList<>();
		for (NodeConnection nc : connectionsList) {
			String sourceNodeName = nc.getSourceNode().getName();
			if (sourceNodeName.equals(nodeName)) { //startsWith(nodeName)) {
				FlowNodeConnection fnc = new FlowNodeConnection(nc.getSourceNode().getName(), nc.getSourceOutputTerminal(), 
						nc.getTargetNode().getName(), nc.getTargetInputTerminal());
				targetNodes.add(fnc);
			}
		}
		return targetNodes;
	}
	
	@SuppressWarnings("unchecked")
	protected <T extends AbstractTopology> T addTopologyItem(T topologyItem, AdministeredObject childProxy) {
		// Use of isAssignableFrom is safer than instanceof
		// Used because RestApiProxt is a subclass of ApplicationProxy, and instanceof treats a ResttApiProxy as an ApplicationProxy
		T item = null; 
		if (childProxy.getClass().isAssignableFrom(ApplicationProxy.class)) {
			item = (T) new Application((ApplicationProxy)childProxy);
			logger.debug("addTopologyItem(): Processing Appplication'{}' for flows to be monitored", item.getName());
		} else 	if (childProxy.getClass().isAssignableFrom(RestApiProxy.class)) {
			item = (T)new RestApi((RestApiProxy)childProxy);
			logger.debug("addTopologyItem(): Processing RestApi'{}' for flows to be monitored", item.getName());
		} else 	if (childProxy.getClass().isAssignableFrom(ServiceProxy.class)) {
			item = (T)new Service((ServiceProxy)childProxy);
			logger.debug("addTopologyItem(): Processing Service'{}' for flows to be monitored", item.getName());
		} else 	if (childProxy.getClass().isAssignableFrom(SharedLibraryProxy.class)) {
			item = (T)new SharedLibrary((SharedLibraryProxy)childProxy);
			logger.debug("addTopologyItem(): Processing Library'{}' for flows to be monitored", item.getName());
		} else 	if (childProxy.getClass().isAssignableFrom(StaticLibraryProxy.class)) {
			item = (T)new StaticLibrary((StaticLibraryProxy)childProxy);
			logger.debug("addTopologyItem(): Processing StaticLibrary'{}' for flows to be monitored", item.getName());
		} else 	if (childProxy.getClass().isAssignableFrom(ExecutionGroupProxy.class)) {
			item = (T)new IntegrationServer((ExecutionGroupProxy) childProxy);
			logger.debug("addTopologyItem(): Processing Integration Server'{}' for flows to be monitored", item.getName());
		} 
		
		topologyItem.addTopologyItem(item);
	
		return item;
	}
}
