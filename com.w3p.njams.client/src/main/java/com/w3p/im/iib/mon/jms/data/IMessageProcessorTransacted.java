/**
 * 
 */
package com.w3p.im.iib.mon.jms.data;

import com.w3p.im.iib.mon.exceptions.internal.MessagingException;

/**
 * @author jormerod
 *
 */
public interface IMessageProcessorTransacted extends IMessageConsumer {
	
	public void commit() throws MessagingException;
	
	public void rollback();
}
