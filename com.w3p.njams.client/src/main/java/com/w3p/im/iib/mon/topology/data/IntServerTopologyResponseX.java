package com.w3p.im.iib.mon.topology.data;

import com.w3p.im.iib.mon.topology.model.TopologyObject;

public class IntServerTopologyResponseX {
	
	private boolean success;
	private int retCode;
	private String message;
	private TopologyObject topology;

	
	public IntServerTopologyResponseX() {
		success = true;
		message = "";
	}
	
	public String getIntNodeName() {
		return topology.getName();
	}
	
	public boolean isSuccess() {
		return success;
	}

	public void setSuccess(boolean success) {
		this.success = success;
	}

	public int getRetCode() {
		return retCode;
	}

	public void setRetCode(int retCode) {
		this.retCode = retCode;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public TopologyObject getTopology() {		
		return topology;
	}


	public void setTopology(TopologyObject topology) {
		this.topology = topology;
	}

	@Override
	public String toString() {
		return "TopologyResponse [intNodeName=" + getIntNodeName() + "]";
	}

}
