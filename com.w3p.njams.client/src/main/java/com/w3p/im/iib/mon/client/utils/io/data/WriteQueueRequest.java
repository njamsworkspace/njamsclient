package com.w3p.im.iib.mon.client.utils.io.data;

import static com.w3p.im.iib.mon.client.constants.IClientConstants.BYTES;

import java.util.HashMap;
import java.util.Map;
	
public class WriteQueueRequest extends QueueRequest implements IWriteQueueRequest {
	
	private final Map<String, String> msgProperties = new HashMap<>(); // Message Properties
	private String msgType; // 'bytes' or 'text'
	
		
	public WriteQueueRequest() {
	}
	
	/**
	 * BYTES is default msgType
	 */
	@Override
	public String getMsgType() {
		if (msgType == null) {
			msgType = BYTES;
		}
		return msgType;
	}

	@Override
	public IWriteQueueRequest setMsgType(String msgType) {
		this.msgType = msgType;
		return this;
	}

	@Override
	public IWriteQueueRequest addMsgProperty(String name, String value) {
		msgProperties.put(name, value);
		return this;
	}

	@Override
	public String getMsgProperty(String name) {
		return msgProperties.get(name);
	}
	
	@Override
	public Map<String, String> getMsgProperties() {
		return msgProperties;
	}
	

}
