package com.w3p.im.iib.mon.event.processors;

import static com.w3p.im.iib.mon.client.constants.IClientConstants.*;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Hashtable;
import java.util.List;
import java.util.Optional;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.im.njams.sdk.common.Path;
import com.im.njams.sdk.model.ActivityModel;
import com.im.njams.sdk.model.ProcessModel;
import com.im.njams.sdk.model.SubProcessActivityModel;


public class FlowToProcessModelCacheRun implements Serializable {
	
	private static final long serialVersionUID = 1L;

	private static final Logger logger = LoggerFactory.getLogger(FlowToProcessModelCacheRun.class);
	
	private final Hashtable<Path, ProcessModel> allProcessModels = new Hashtable<>();	
	private final Hashtable<String, ProcessModel> flowToProcessModel = new Hashtable<>(); 
	private final Hashtable<String, Path> flowToPath = new Hashtable<>();   // msg/subFlow | ProcessModel name : PM Path
	private final Hashtable<String, String> flowToName = new Hashtable<>(); // msg/sub Flow name : PM Name


	/**
	 * Used for <b>subflows</b> where the ProcessModel is not available at creation time.
	 * <br>It will found on first use at runtime and cached.
	 * @param flowName
	 * @param processPathParts
	 */
	public void addPathForFlow(String flowName, List<String> processPathParts) {
		flowToPath.put(flowName, new Path(processPathParts));
		flowToName.put(flowName, processPathParts.get(processPathParts.size()-1));
	}

	/**
	 * Used for <b>msgflows</b> where the ProcessModel is available at creation time.
	 * @param flowName is either: Msgflow or Msgflow.Subflow
	 * @param pm
	 */
	public void addPathForFlow(String flowName, ProcessModel pm) {
		flowToProcessModel.put(flowName, pm);
	}

	/**
	 * For <b>subfkows</b>, this will return null on first use - then cache it using #addProcessModel() 
	 * @param flowName
	 * @return
	 */
	public ProcessModel getProcessModelForFlow(String flowName) {
		return flowToProcessModel.get(flowName);
	}
	
	private synchronized void addProcessModel(String flowName, ProcessModel pm) {
		flowToProcessModel.put(flowName, pm);
		flowToPath.computeIfAbsent(flowName, k -> pm.getPath());
	}
	
	public Path getPathForFlow(String flowName) {
		return flowToPath.get(flowName);
	}
	
	public String getProcessModelNameForFlow(String flowName) {
		return flowToName.get(flowName);
	}
	
	public String getRealSubflowName(String flowName) {
		Optional<String> realSubflowKey = flowToName.keySet().stream().filter(k -> k.endsWith(flowName)).findFirst();
		return realSubflowKey.isPresent() ? flowToName.get(realSubflowKey.get()) : null;
	}
	
	/**
	 * Replace any ProcessModels with a null value with the process model from all PMs in nJAMS client.<br>
	 * This is limited to a two-part key: msgflow.subflow and subflow.subflow.
	 *  
	 * @param allPMs
	 * @param clientPath
	 */
	public void completeTheCache(Collection<ProcessModel> allPMs, Path clientPath) {
		List<String> clientPathParts = clientPath.getParts();		
		// This, in effect, recreates the Map of Process Models in the client. But, on occasion, that fails to find a PM whose key (Path) is present. This is safer.   
		for (ProcessModel pm : allPMs) {
			allProcessModels.put(pm.getPath(), pm);
		}
		
		Set<String> cachedFlowNames = flowToPath.keySet(); //flowToProcessModel.keySet();
		for (String cachedFlowName : cachedFlowNames) { // Resolve all null PMs in the cache
			Path relativePath = flowToPath.get(cachedFlowName);
			Path fullPath = clientPath.add(relativePath);
			ProcessModel cachedPM = allProcessModels.get(fullPath); // relativePath); //cachedFlowName); // flowToProcessModel 
			if (cachedPM == null) {
				List<String> fullPathParts = new ArrayList<>(clientPathParts);
				fullPathParts.addAll(relativePath.getParts());
				fullPath = new Path(fullPathParts);
				ProcessModel targetPM = allProcessModels.get(fullPath);
				if (targetPM != null) {
					addProcessModel(cachedFlowName, targetPM); //flowToProcessModel.put(cachedFlowName, targetPM);
					addPathForFlow(cachedFlowName, targetPM);
				}
			} else {
				addPathForFlow(cachedFlowName, cachedPM);
			}
		}
	}
	
	public ProcessModel findProcessModel(String flowName) {
		return findProcessModel(flowName, null);		
	}
	
	public ProcessModel findProcessModel(String flowName, String parentFlowName) {	
		String searchFlowName = parentFlowName == null ? flowName : parentFlowName+DOT+flowName;
		ProcessModel pm =  getProcessModelForFlow(searchFlowName);
		if (pm != null) {
			return pm;
		}

		logger.warn("ProcessModel for '{}' '{}' not found in FlowToProcessModelCache.", parentFlowName, flowName);	
		synchronized (searchFlowName) {

			// This searchFlowName must have 3 or more parts. It's the last 2 parts that are relevant
			List<String> searchNameParts = Arrays.asList(searchFlowName.split(REGEX_DOT));
			int searchNamePartsSize = searchNameParts.size();
			if (searchNamePartsSize > 1) {
				String flowNameKey = searchNameParts.get(searchNamePartsSize - 2)+DOT+searchNameParts.get(searchNamePartsSize - 1);
				ProcessModel requiredPM = getProcessModelForFlow(flowNameKey);
				if (requiredPM == null) {
					requiredPM = searchForProcessModel(searchFlowName);				
				} 
				addProcessModel(searchFlowName, requiredPM); // Assumed to be found!!
//				setSubprocessModels();
				logger.info("Final Process Model for '{}' '{}' added to FlowToProcessModelCache", flowNameKey, requiredPM.getName());
				return requiredPM;
			} else {
				 throw new RuntimeException("Process model for flow not found: flow name = "+searchFlowName); // FIXME do something better if possible - failure to include required msg/sub flows
			}

		}
	}
	
	private synchronized ProcessModel searchForProcessModel(String searchFlowName) {		
		List<String> searchNameParts = Arrays.asList(searchFlowName.split(REGEX_DOT));
		int searchNamePartsCount = searchNameParts.size();
//		String msgflowName = searchNameParts.get(0);
		// Get the last 2 components of the search names
		String searchFlowName1 = searchNameParts.get(searchNamePartsCount - 2);
		String searchFlowName2 = searchNameParts.get(searchNamePartsCount - 1);
		
		// Subflow names could be local aliases - so translate to real name using msgflowName.subflowAlias
//		String searchFlowAlias = searchFlowName1+DOT+searchFlowName2; //msgflowName+DOT+searchFlowName2;
		String searchFlowRealName = getRealSubflowName(searchFlowName2); //getProcessModelNameForFlow(searchFlowAlias1);
//		String searchFlowNameToUse = searchFlowName1+DOT+searchFlowRealName1;
		
		ProcessModel requiredPM = getProcessModelForFlow(searchFlowRealName);
		if (requiredPM != null) {
			return requiredPM;
		}

		logger.error("Final Process Model for '{}' was not found - null retuirned", searchFlowName);
		return requiredPM;
	}

	public void setSubprocessModels() {		// This sets the actual sub-proc in the proc model, so that cloud server works
		for (String cachedFlowName : flowToProcessModel.keySet()) { // Cached PMs
			ProcessModel cachedPM = flowToProcessModel.get(cachedFlowName); 
			if (cachedPM != null) {
				for (ActivityModel cachedAM : cachedPM.getActivityModels()) {
					if (cachedAM instanceof SubProcessActivityModel) {
						// Get the process model using Path
						// 	e.g >W3P_NJAMS_DEMO>appTesting>JOT420-16>HTTPInputApplication>SF_HTTPInput>
						Path subProcessPath = ((SubProcessActivityModel)cachedAM).getSubProcessPath();
						ProcessModel sfpm = allProcessModels.get(subProcessPath);
						if (sfpm != null) {
							((SubProcessActivityModel)cachedAM).setSubProcess(sfpm);
						}
					}
				}
			} 				
		}
	}

	public boolean isEmpty() {
		return flowToPath.isEmpty();
	}
}
