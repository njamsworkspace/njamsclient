package com.w3p.im.iib.mon.monitor.profile.groovy

import static com.w3p.im.iib.mon.client.constants.IClientConstants.UTF_8;

import java.util.List

import org.slf4j.Logger
import org.slf4j.LoggerFactory

import com.w3p.im.iib.mon.client.config.data.ApplicationDataQuery
import com.w3p.im.iib.mon.client.config.data.Content
import com.w3p.im.iib.mon.client.config.data.DataTypeToQueryText
import com.w3p.im.iib.mon.client.config.data.GlobalCorrelationId
import com.w3p.im.iib.mon.topology.model.Application
import com.w3p.im.iib.mon.topology.model.Flow
import com.w3p.im.iib.mon.topology.model.FlowNode
import com.w3p.im.iib.mon.topology.model.FlowNodeConnection
import com.w3p.im.iib.mon.topology.model.IntegrationServer
import com.w3p.im.iib.mon.topology.model.MessageFlow
import com.w3p.im.iib.mon.topology.model.SubFlow
import groovy.xml.MarkupBuilder
import groovy.xml.StreamingMarkupBuilder

class CreateMonitoringProfile {
	final Logger logger = LoggerFactory.getLogger(CreateMonitoringProfile.class)
//	logger.trace("")
// For use of 'delegate' see: https://stackoverflow.com/questions/10880220/how-to-recursively-add-a-child-inside-streamingmarkupbuilder
	/*
	 * When input is JSON, for xpath use: '$Root/JSON/Data/xpath for data' [JSON is converted to XML]
	 * When input id XML,  for xpath use: '$Root/XMLNSC/xpath-for-data'
	 * When input is SOAP, start xpath after <Body>
	 */
	def globalCorrelationId
	def nodeTypeToCorrelationIdsMap	 // Can be null
//	Sample
//		 = [
//		'ComIbmMQInputNode':'$Root/MQMD/MsgId',
//		'ComIbmSOAPInputNode':'$Root/HTTPInputHeader/MsgId' or '$LocalEnvironment/Destination/HTTP/RequestIdentifier',
//		'ComIbmWSInputNode':'$Root/HTTPInputHeader/MsgId'
//		]
	def nodeTypeToNamespacesMap // Can be null
//	 Sample
//		 = [
//		'ComIbmMQInputNode':[],
//		'ComIbmSOAPInputNode':[],
//		'ComIbmWSInputNode':[]
//		]
	/*
	 * applicationDataQuery
	 */
	def applicationDataQuery
	def applicationDataQueryMap
//	Sample
//	 = [
//		'ComIbmMQGetNode':[
//			'MsgId':'$Root/MQMD/MsgId'],
//		'ComIbmMQOutputNode':[
//			'MsgId':'$LocalEnvironment/WrittenDestination/MQ/DestinationData[1]/msgId'],
//		'ComIbmMQReply':[
//			'MsgId':'$LocalEnvironment/WrittenDestination/MQ/DestinationData[1]/msgId',
//			'CorrelId':'$LocalEnvironment/WrittenDestination/MQ/DestinationData[1]/correlId']
//		]
		
	boolean includePayloadBody = true
		
	String createForFlow (IntegrationServer is, MessageFlow mf, GlobalCorrelationId gci, ApplicationDataQuery adq) {
		logger.debug("CreateMonitoringProfile # createForFlow() - enter")
		applicationDataQuery = adq
		nodeTypeToCorrelationIdsMap = gci?.nodeTypeToCorrelationIds?.nodeTypeToCorrelationIds
		nodeTypeToNamespacesMap = gci?.nodeTypeToNamespaces?.nodeTypeToNamespacesMap
		
		String builtProfile = null
		try	{
			def builder = new StreamingMarkupBuilder()
			builder.encoding = UTF_8
			def profile = {
				mkp.declareNamespace(p: "http://www.ibm.com/xmlns/prod/websphere/messagebroker/6.1.0.3/monitoring/profile")
				'p:monitoringProfile' ('p:version':'2.0') {
					processNodes.delegate = delegate
					processNodes(mf, null, is)
				}
			}

			builtProfile = builder.bind(profile)
			return builtProfile
		} catch (all) {
			all.printStackTrace()
			throw all
		}
		logger.trace("CreateMonitoringProfile # createForFlow() - exit")
	}

	// Define main Closure
	def processNodes = { Flow flow, String sfName, IntegrationServer is ->
		includePayloadBody = flow.isIncludePayload()
		flow.flowNodes.each { node ->
			createEventsource.delegate = delegate
			if (node.type != 'InputNode' && (node.type.endsWith('InputNode') || node.type == 'ComIbmTimeoutNotificationNode')) {
				
				createEventsource(node, sfName, 'Start', nodeTypeToCorrelationIdsMap?."$node.type")
				createEventsource(node, sfName, 'End', null)
				node.flowNodeConnections.each {FlowNodeConnection fnc ->
					if (fnc.sourceOutTerminal.toLowerCase().contains('catch')) {
						createEventsource(node, sfName, 'Catch', null)
					} else if (fnc.sourceOutTerminal.toLowerCase().contains('failure')) {
						createEventsource(node, sfName, 'Failure', null)
					} else if (fnc.sourceOutTerminal.toLowerCase().contains('fault')) {
						createEventsource(node, sfName, 'Fault', null)
					} else if (fnc.sourceOutTerminal.toLowerCase().contains('timeout')) {
						createEventsource(node, sfName, 'Timeout', null)
					}
				}
			} else if (node.type == 'SubFlowNode') { // FIXME need to pass the uuid Basic property to identify the subflow's parent
				SubFlow sf = is.getActualSubflow(node, flow.isIncludeSchemaName()) 
				// is.findSubflow(node.getSubflowFileName(flow.isIncludeSchemaName())) //is.findSubflowUsingURIProperty(node, flow.isIncludeSchemaName()) 
				if (sf) {
					sf.setNameInFlow(node.name)  //TODO includes the schema name //TODO have a null check & find out how to log errors in Groovy
					String nodeName = node.name
					if (flow.class.isAssignableFrom(SubFlow.class)) {
						//					nodeName = flow.getNameInFlow() +'.'+ node.name   // append parent's name (as defined in its subflow node), if parent is a subflow.
						nodeName = sfName +'.'+ node.name
					}
					processNodes(sf, nodeName, is) // node.name is the name used in the msgflow
				} else {
					logger.error("Subflow '{}' was not found - probably not included in the startup config file", sf.getName() ) 
				}
			} else if (node.type == 'ComIbmLabelNode') {
				createEventsource(node, sfName, 'Out', null)
			} else if (node.type.startsWith("ComIbmMQ") && node.type != "ComIbmMQHeaderNode") {
				createEventsource(node, sfName, 'In', null)
				createEventsource(node, sfName, 'Out', null)
			} else if (node.type != 'OutputNode' && node.type != 'InputNode' ) {
				node.flowNodeConnections.each {FlowNodeConnection fnc ->
					if (fnc.sourceOutTerminal.toLowerCase().contains('catch')) {
						createEventsource(node, sfName, 'Catch', null)
					} else if (fnc.sourceOutTerminal.toLowerCase().contains('failure')) {
						createEventsource(node, sfName, 'Failure', null)
					} else if (fnc.sourceOutTerminal.toLowerCase().contains('fault')) {
						createEventsource(node, sfName, 'Fault', null)
					} else if (fnc.sourceOutTerminal == 'noMessage') {
						createEventsource(node, sfName, 'noMessage', null)
					} else if (fnc.sourceOutTerminal == 'noMatch') {
						createEventsource(node, sfName, 'noMatch', null)
					} else if (fnc.sourceOutTerminal.toLowerCase().contains('control')) {
						createEventsource(node, sfName, 'Control', null)
					} else if (fnc.sourceOutTerminal.toLowerCase().contains('timeout')) {
						createEventsource(node, sfName, 'Timeout', null)
					} else if (fnc.sourceOutTerminal.toLowerCase().contains('unknown')) {
						createEventsource(node, sfName, 'Unknown', null)
					} else if (fnc.sourceOutTerminal.toLowerCase().contains('alternate')) {
						createEventsource(node, sfName, 'Alternate', null)
					}
				}
				createEventsource(node, sfName, 'In', null)
			}
		}
	}

	// Define inner Closure
	def createEventsource = { FlowNode node, String sfName, String terminalType, globalCorrelId ->
		String eventSourceAddress = null
		String eventName = null
		String uow = null
		String sourceAddress = sfName == null ? node.name : sfName+'.'+node.name
		if (terminalType == 'In') {
			eventSourceAddress = sourceAddress+'.terminal.in'
			eventName = node.name+'.InTerminal'
			uow = 'independent'
		} else if (terminalType == 'Out') {
			eventSourceAddress = sourceAddress+'.terminal.out'
			eventName = node.name+'.OutTerminal'
			uow = 'independent'
		} else if (terminalType == 'Start' || terminalType == 'End' || terminalType == 'Rollback') {
			eventSourceAddress = sourceAddress+'.transaction.'+terminalType
			eventName = node.name+'.Transaction'+terminalType
			uow = terminalType == 'Start' || terminalType == 'Rollback' ? 'independent' : 'none'
		} else if (terminalType == 'Catch') {
			eventSourceAddress = sourceAddress+'.terminal.catch'
			eventName = node.name+'.CatchTerminal'
			uow = 'independent'
		} else if (terminalType == 'Failure') {
			eventSourceAddress = sourceAddress+'.terminal.failure'
			eventName = node.name+'.FailureTerminal'
			uow = 'independent'
		} else if (terminalType == 'Fault') {
			eventSourceAddress = sourceAddress+'.terminal.fault'
			eventName = node.name+'.FaultTerminal'
			uow = 'independent'
		} else if (terminalType == 'Timeout') {
			eventSourceAddress = sourceAddress+'.terminal.timeout'
			if (node.name == "HTTP Input") {
				eventName = node.name+'.HTTP TimeoutTerminal'
			} else {
				eventName = node.name+'.TimeoutTerminal'
			}
			uow = 'independent'
		} else if (terminalType == 'noMessage') {
			eventSourceAddress = sourceAddress+'.terminal.noMessage'
			eventName = node.name+'.No MessageTerminal'
			uow = 'independent'
		} else if (terminalType == 'noMatch') {
			eventSourceAddress = sourceAddress+'.terminal.noMatch'
			eventName = node.name+'.No matchTerminal'
			uow = 'independent'
		} else if (terminalType == 'Unknown') {							// Aggregate Control node
			eventSourceAddress = sourceAddress+'.terminal.unknown'
			eventName = node.name+'.UnknownTerminal'
			uow = 'independent'
		} else if (terminalType == 'Alternate') { 						// Java Compute node
			eventSourceAddress = sourceAddress+'.terminal.alternate'
			eventName = node.name+'.AlternateTerminal'
			uow = 'independent'
		}
		
		'p:eventSource' ('p:eventSourceAddress':eventSourceAddress, 'p:enabled':'true') {
			'p:eventPointDataQuery' {
				'p:eventIdentity' {
					'p:eventName' ('p:literal':eventName)
				}
				'p:eventCorrelation' {
					'p:localTransactionId' ('p:sourceOfId':'automatic')
					'p:parentTransactionId' ('p:sourceOfId':'automatic')
					if (globalCorrelId) {
						if (nodeTypeToCorrelationIdsMap?."$node.type") {
							'p:globalTransactionId' ('p:queryText':nodeTypeToCorrelationIdsMap."$node.type", 'p:sourceOfId':'query') {
								nodeTypeToNamespacesMap?.each {nodeType, nsList -> 
									nsList.each {ns ->
										'p:prefixMapping' ('p:prefix':ns.prefix, 'p:URI':ns.uri)
									}
								}
							}
						} else {
							'p:globalTransactionId' ('p:queryText':globalCorrelId, 'p:sourceOfId':'query')
						}
					} else {
						'p:globalTransactionId' ('p:sourceOfId':'automatic')
					}
				}
				'p:eventFilter' ('p:queryText':'true()')
				'p:eventUOW' ('p:unitOfWork':uow)
			}
			'p:applicationDataQuery' {
				if (terminalType == 'Start' && (node.type == 'ComIbmMQInputNode' || node.type == 'ComIbmWSInputNode') || node.type == 'ComIbmSOAPInputNode') {
					Content content = applicationDataQuery?.getQueryContent("$node.type")
					if (content) {						
						'p:simpleContent' ('p:dataType':'string', 'p:name':content.name) {
							'p:valueQuery' ('p:queryText':content.xpath) {
								content.namespaces?.namespaces.each { ns ->
									'p:prefixMapping' ('p:prefix':ns.prefix, 'p:URI':ns.uri)
								}
							}
						}
					}
				} else if (terminalType == 'Out' && (node.type == 'ComIbmMQInputNode' || node.type == 'ComIbmWSInputNode' || node.type == 'ComIbmSOAPInputNode')) {
					Content content = applicationDataQuery?.getQueryContent("$node.type")
					if (content) {						
						'p:simpleContent' ('p:dataType':'string', 'p:name':content.name) {
							'p:valueQuery' ('p:queryText':content.xpath) {
								content.namespaces?.namespaces.each { ns ->
									'p:prefixMapping' ('p:prefix':ns.prefix, 'p:URI':ns.uri)
								}
							}
						}
					}
				}  else if (terminalType == 'Out' && node.type != 'ComIbmMQGetNode') {
					Content content = applicationDataQuery?.getQueryContent("$node.type")
					if (content) {	
						'p:simpleContent' ('p:dataType':'string', 'p:name':content.name) {
							'p:valueQuery' ('p:queryText':content.xpath) {
								content.namespaces.namespaces.each { ns ->
									'p:prefixMapping' ('prefix':ns.prefix, 'URI':ns.uri)
								}
							}
						}
					}
				} else if (terminalType == 'In' && node.type == 'ComIbmMQOutputNode') {
					Content content = applicationDataQuery?.getQueryContent("$node.type")
					if (content) {						
						'p:simpleContent' ('p:dataType':'string', 'p:name':content.name) {
							'p:valueQuery' ('p:queryText':content.xpath) {
								content.namespaces?.namespaces.each { ns ->
									'p:prefixMapping' ('p:prefix':ns.prefix, 'p:URI':ns.uri)
								}
							}
						}
					}
				} else if (terminalType == 'In' && node.type == 'ComIbmMQGetNode') {
					Content content = applicationDataQuery?.getQueryContent("$node.type")
					if (content) {						
						'p:simpleContent' ('p:dataType':'string', 'p:name':content.name) {
							'p:valueQuery' ('p:queryText':content.xpath) {
								content.namespaces?.namespaces.each { ns ->
									'p:prefixMapping' ('p:prefix':ns.prefix, 'p:URI':ns.uri)
								}
							}
						}
					}
				} else if (terminalType == 'Catch') {
					'p:complexContent' {
						'p:payloadQuery' ('p:queryText':'$ExceptionList')
					}
				} else if (terminalType == 'Rollback') {
					'p:complexContent' {
						'p:payloadQuery' ('p:queryText':'$ExceptionList')
					}
				} else if (terminalType == 'Failure') {
					'p:complexContent' {
						'p:payloadQuery' ('p:queryText':'$ExceptionList')
					}
				} else if (terminalType == 'Fault') {
					'p:complexContent' {
						'p:payloadQuery' ('p:queryText':'$ExceptionList')
					}
				} else if (terminalType == 'noMessage') {
					'p:complexContent' {
						'p:payloadQuery' ('p:queryText':'$ExceptionList')
					}
				}
			}
			if (includePayloadBody && (terminalType != 'Out' || node.type == 'ComIbmLabelNode')) { // Exclude payload  for: 'out' teminals and Label nodes
				'p:bitstreamDataQuery' ('p:bitstreamContent':'body', 'p:encoding':'base64Binary')
			}
		}
	}
}
