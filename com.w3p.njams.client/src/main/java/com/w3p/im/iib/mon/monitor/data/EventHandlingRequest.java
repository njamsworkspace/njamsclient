package com.w3p.im.iib.mon.monitor.data;

import java.util.HashMap;
import java.util.Map;

import com.im.njams.sdk.Njams;
import com.w3p.im.iib.mon.jms.producers.FlowToProcessModelCache;
import com.w3p.im.iib.mon.jms.service.MqJmsConnectionProperties;

public class EventHandlingRequest {	
	
	protected final Map<String, Njams> njamsClients = new HashMap<>(); // k=intSvrName, v=njamsClient for intSvr
	protected final Map<String, FlowToProcessModelCache> flowToProcessModelCaches = new HashMap<>();; // k=intSvrName, v=FlowToProcessModelCache for intSvr
	protected final String intNodeName;
	protected final String intSvrName;
	private boolean includeIntSvrName;
	
	protected final MqJmsConnectionProperties mqJmsConnectionProperties; // DEV & PROD		
	protected String eventsSource; // DEV & PROD
	protected String iibEventsProblemsQueue; // PROD
	protected int consumerThreads; // PROD
	
	
	public EventHandlingRequest(String intNodeName, String intSvrName, MqJmsConnectionProperties mqJmsConnectionProperties) { //Njams njamsClient, 
		this.intNodeName = intNodeName;
		this.intSvrName = intSvrName;
		this.mqJmsConnectionProperties = mqJmsConnectionProperties;
	}
	
	public void addNjamsClient(String intSvrName, Njams njamsClient) {
		this.njamsClients.put(intSvrName, njamsClient);
	}
	
	public Map<String, Njams> getNjamsClients() {
		return njamsClients;
	}

	public void addFlowToProcessModelCache(String intSvrName, FlowToProcessModelCache flowToProcessModelCache) {
		this.flowToProcessModelCaches.put(intSvrName, flowToProcessModelCache);
	}

	public Map<String, FlowToProcessModelCache> getFlowToProcessModelCaches() {
		return flowToProcessModelCaches;
	}


	public String getIntNodeName() {
		return intNodeName;
	}


	public String getIntSvrName() {
		return intSvrName;
	}

	public boolean isIncludeIntSvrName() {
		return includeIntSvrName;
	}
	
	public MqJmsConnectionProperties getMqJmsConnectionProperties() {
		return mqJmsConnectionProperties;
	}

	public String getEventsSource() {
		return eventsSource;
	}

	public void setEventsSource(String eventsSource) {
		this.eventsSource = eventsSource;
	}

	public String getIibEventsProblemsQueue() {
		return iibEventsProblemsQueue;
	}


	public void setIibEventsProblemsQueue(String iibEventsProblemsQueue) {
		this.iibEventsProblemsQueue = iibEventsProblemsQueue;
	}


	public int getConsumerThreads() {
		return consumerThreads;
	}


	public void setConsumerThreads(int consumerThreads) {
		this.consumerThreads = consumerThreads;
	}
}
