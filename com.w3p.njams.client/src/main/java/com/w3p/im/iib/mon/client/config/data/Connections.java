package com.w3p.im.iib.mon.client.config.data;

public class Connections {
	
	private String iibEventsSource;
	private String iibEventsProblemsQueue;
	private IbmMqConfig ibmMqConfig;
	private MqttConfig mqttConfig;
	private NjamsServer serverConfig;
	private IIBConfig iibConfig;
	
	
	public String getIibEventsSource() {
		return iibEventsSource;
	}
	public void setIibEventsSource(String iibEventSource) {
		this.iibEventsSource = iibEventSource;
	}
	public String getIibEventsProblemsQueue() {
		return iibEventsProblemsQueue;
	}
	public void setIibEventsProblemsQueue (String iibEventsProblemsQueue) {
		this.iibEventsProblemsQueue = iibEventsProblemsQueue;
	}
	public IbmMqConfig getIbmMqConfig() {
		return ibmMqConfig;
	}
	public void setIbmMqConfig(IbmMqConfig mqConfig) {
		this.ibmMqConfig = mqConfig;
	}
	public MqttConfig getMqttConfig() {
		return mqttConfig;
	}
	public void setMqttConfig(MqttConfig mqttConfig) {
		this.mqttConfig = mqttConfig;
	}
	public NjamsServer getServerConfig() {
		return serverConfig;
	}
	public void setServerConfig(NjamsServer serverConfig) {
		this.serverConfig = serverConfig;
	}
	public IIBConfig getIibConfig() {
		return iibConfig;
	}
	public void setIibConfig(IIBConfig iibConfig) {
		this.iibConfig = iibConfig;
	}
	@Override
	public String toString() {
		return "Connections [iibEventsSource=" + iibEventsSource + "]";
	}
}
