package com.w3p.im.iib.mon.resources;

import static com.w3p.im.iib.mon.client.constants.IClientConstants.UTF_8;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.text.MessageFormat;
import java.util.Enumeration;
import java.util.MissingResourceException;
import java.util.Properties;

import org.apache.commons.lang3.StringUtils;




public class ResourceHandler {
	
    /**
     * This is the ResourcesHandler which supplies
     * translatable strings
     */
    private static ResourceHandler nlsHandler = null;

   /**
     * The bundle of resources managed by this handler instance
     */
//    private ResourceBundle resources;
    private Properties clientProps;

    /**
     * Cache of the resources contained in the bundle
     */
    private Properties resourcesCache = null;
	
    private static final String PROPERTIES_FOLDER = "propertiesFolder";
	private static final String PROPERTIES_FILE_NAME = "propertiesFileName";

	/**
     * This constant describes the name of the bundle from which
     * translatable strings should be obtained. The naming rules for
     * Java resource bundles apply.
     */
    private final static String NJAMS_RESOURCE_FILENAME = System.getProperty(PROPERTIES_FILE_NAME); //"njams.properties";
    private final static String NJAMS_RESOURCE_FOLDER = System.getProperty(PROPERTIES_FOLDER);
    
    // Connecting to IIB
    // Client version
    public static final String CLIENT_VERSION = "client.version";
    public static final String CLIENT_SLEEP_INTERVAL = "client.sleep.interval";
    public static final String ACTIVEMQ_JMS_PROVIDER_URL = "activemq.jms.provider.url";
    public static final String INT_NODE_CONN_LOCAL = "int.node.conn.local";
    public static final String INT_NODE_IPADDRESS = "int.node.ipaddress";
    public static final String INT_NODE_PORT = "int.node.port";
    public static final String INT_NODE_QMGR = "int.node.qmgr";
    public static final String INT_NODE_NAME = "int.node.name";
    // Monitoring scope & Topology
    public static final String INT_SERVER_NAMES = "int.server.names";
    // MessageFlows
    public static final String INT_SERVER_MONITORING_NAMES =  "int.server.monitoring.names";
    public static final String APPLICATION_MONITORING_NAMES =  "application.monitoring.names";
    public static final String RESTAPI_MONITORING_NAMES =  "restapi.monitoring.names";
    public static final String SERVICE_MONITORING_NAMES =  "service.monitoring.subflow.names";
    public static final String STATIC_LIBRARY_MONITORING_NAMES =  "static.library.monitoring.names";
    public static final String SHARED_LIBRARY_MONITORING_NAMES =  "shared.library.monitoring.names";
    // SubFlows
    public static final String INT_SERVER_MONITORING_SUBFLOW_NAMES =  "int.server.monitoring.subflow.names";
    public static final String APPLICATION_MONITORING_SUBFLOW_NAMES =  "application.monitoring.subflow.names";
    public static final String RESTAPI_MONITORING_SUBFLOW_NAMES =  "restapi.monitoring.subflow.names";
    public static final String SERVICE_MONITORING_SUBFLOW_NAMES =  "service.monitoring.subflow.names";
    public static final String STATIC_LIBRARY_MONITORING_SUBFLOW_NAMES =  "static.library.monitoring.subflow.names";
    public static final String SHARED_LIBRARY_MONITORING_SUBFLOW_NAMES =  "shared.library.monitoring.subflow.names";
    
    public static final String MONITOR_SUBFLOWS = "monitor.subflows"; // 'false' is intended for v9 monitoring, because subflow topology is not avaialble

    // Control the qualified Message Flow name in njams server
    public static final String INCLUDE_INT_SERVER_NAME = "include.int.server.name";
    public static final String INCLUDE_MSGFLOW_SCHEMA = "include.msgflow.schema";
    // JMS
    public static final String IBM_MQ_JMS_PROVIDER_URL = "ibm.mq.jms.provider.url";
    public static final String IBM_MQ_JMS_INIT_CONTEXT_FAC = "ibm.mq.jms.init.context.fac";
    public static final String IBM_MQ_JMS_CONN_FAC = "ibm.mq.jms.conn.fac";
    // File locations
 	public static final String IBM_IIB_BIN_DIR = "ibm.iib.bin.dir";
 	// Control Monitoring Profile generation and application
 	public static final String IIB_MON_PROFILE_DIR = "iib.mon.profile.dir";
 	public static final String IIB_MON_PROFILE_APPLY_IF_CHANGED = "iib.mon.profile.apply.if.changed";
 	// nJAMS properties
 	public static final String NJAMS_APP_TYPE = "njams.app.type";
    
	  /**
     * Creates a resource handler that uses the supplied filename
     * for obtaining resources.
     * @param bundleName filename of the resource to use. The resource
     * file is loaded using Java's resource bundle loader, which means that
     * a locale specific bundle will be attempted to be loaded before
     * a general one.
     */
    private ResourceHandler(String bundleName) {
        try {
        	String propsFile = NJAMS_RESOURCE_FOLDER+File.separator+bundleName; 
        	clientProps = new Properties();
        	try (InputStreamReader in = new InputStreamReader(new FileInputStream(propsFile), UTF_8)){
        		clientProps.load(in);
			} catch (Exception e) {
				e.printStackTrace();
			}
            cacheAllResources();
        } catch (MissingResourceException ex) {
            System.err.println("Warning: Can't find nJAMS properties '"+bundleName+"'");
            clientProps = null;
		}
    }

    /**
     * Requests a resource from the bundle that contains localisable strings
     * @param keyName constant used to identify the requested resource
     * @param defaultValue Value to return if the resource could not
     * be found
     * @return String containing the value of the requested resource,
     * or an empty string if the resource could not be found.
     */
    public synchronized static String getNLSResource(String keyName) {
        if (nlsHandler == null) {
            nlsHandler = new ResourceHandler(NJAMS_RESOURCE_FILENAME);
        }
        return nlsHandler.getResource(keyName, "");
    }

    /**
     * Requests a resource from the bundle that contains localisable strings
     * and substitutes any %n characters with the appropriate string
     * from the supplied array.
     * @param keyName constant used to identify the requested resource
     * @param defaultValue Value to return if the resource could not
     * be found
     * @return String containing the value of the requested resource,
     * or an empty string if the resource could not be found.
     */
    public synchronized static String getNLSResource(String keyName, String[] substitutions) {
        return MessageFormat.format(getNLSResource(keyName), (Object[])substitutions);
    }

    /**
     * Requests a resource from the current bundle
     * @param keyName Key describing the resource that is sought
     * @param defaultValue Value to return if the resource could not
     * be found
     * @return String containing the value of the requested resource,
     * or defaultValue if the resource could not be found.
     */
    private String getResource(String keyName, String defaultValue) {
        String retVal = null;
        if (resourcesCache != null) {
            String temp = resourcesCache.getProperty(keyName);
            // Look for a single EnvVar and resolve
            if (StringUtils.countMatches(temp, "%") == 2) {
            	String envVar = StringUtils.substringBetween(temp, "%");
            	String sysVar = System.getenv(envVar);
            	sysVar = StringUtils.strip(sysVar, "\"");
            	retVal = StringUtils.replace(temp, "%"+envVar+"%", sysVar);            	
            } else {
            	retVal = temp;
            }
        }
        if ((retVal == null) && (clientProps != null)) {
            try {
                retVal = clientProps.getProperty(keyName);
            } catch (MissingResourceException ex) {
                // ignore - the default will be used
            }
        }

        if (retVal == null) {
            retVal = defaultValue;
        }
        return retVal;
    }

    /**
     * Places all of the resources inside the current bundle
     * and adds them into a hashtable in memory.
     */
    private void cacheAllResources() {
        if (resourcesCache == null) {
            resourcesCache = new Properties();
        }
        if (clientProps != null) {
                Enumeration<Object> e = clientProps.keys();
                while (e.hasMoreElements()) {
                    String key = (String)e.nextElement();
                    String value = clientProps.getProperty(key);
                    resourcesCache.put(key,value);
                }
        }
    }

}
