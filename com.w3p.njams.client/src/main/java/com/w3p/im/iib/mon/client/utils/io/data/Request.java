package com.w3p.im.iib.mon.client.utils.io.data;

import static com.w3p.im.iib.mon.client.constants.IClientConstants.DEFAULT_NAME_PATTERN;

import java.util.Hashtable;
import java.util.Optional;

public class Request implements IRequest {
	
	private String content;
	private String fileNamePattern; // For filtering input files and naming output files.
	private final Hashtable<String, Object> options = new Hashtable<>(30); // Message Properties


	@Override
	public IRequest setContent(String content) {
		this.content = content;
		return this;
	}

	@Override
	public String getContent() {
		return content;
	}
	
	public String getNamePattern() {
		return fileNamePattern == null ? DEFAULT_NAME_PATTERN : 
			fileNamePattern.isEmpty() ? DEFAULT_NAME_PATTERN : fileNamePattern;
	}
	
	public IRequest setNamePattern(String fileNamePattern) {
		this.fileNamePattern = fileNamePattern;				
		return this;
	}

	@Override
	public IRequest addOption(String key, Object value) {
		options.put(key, value);		
		return this;
	}
	
	@Override
	public Optional<Object> getOption(String key) {
		if (options.containsKey(key)) {
			return Optional.of(options.get(key));
		}
		return Optional.empty();
	}
	

}
