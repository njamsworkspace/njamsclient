package com.w3p.im.iib.mon.client.utils.io.jms;

import javax.jms.Connection;
import javax.jms.Destination;
import javax.jms.JMSException;
import javax.jms.Session;

import com.ibm.msg.client.jms.JmsConnectionFactory;
import com.ibm.msg.client.jms.JmsFactoryFactory;
import com.ibm.msg.client.wmq.WMQConstants;
import com.w3p.im.iib.mon.client.utils.io.data.ConnectionData;

public abstract class JmsConnector {
	
//	protected JMSContext context; // = session + connection
	protected Session jmsSession;
	protected Connection jmsConnection;
	protected Destination jmsDest;
	

	public JmsConnector() {
	}
	
	public void init(ConnectionData connData) throws JMSException {
		// Use of 'wrapper' allows us to provide a mock for JMSFactoryFactory.getInstance()
		JmsFactoryFactoryWrapper jffw = new JmsFactoryFactoryWrapper();
		init(connData, jffw);
	}
	
	public void init(ConnectionData connData, JmsFactoryFactoryWrapper jffw) throws JMSException {		
		JmsConnectionFactory connFac = createJmsConnectionFactory(jffw);
		configureConnFactory(connFac, connData);
		jmsConnection = createJMSConnection(connFac, connData); //connFac.createConnection();			
		jmsSession = createJMSSession(jmsConnection); //jmsConnection.createSession(transacted, Session.AUTO_ACKNOWLEDGE);
		jmsDest = createJMSDestination(jmsSession, connData);
	}

	protected JmsConnectionFactory createJmsConnectionFactory(JmsFactoryFactoryWrapper jffw) throws JMSException {
		JmsConnectionFactory connFac =  null;
		JmsFactoryFactory jff = jffw.getFactory(WMQConstants.WMQ_PROVIDER);
		connFac = jff.createConnectionFactory();

		return connFac;
	}

	protected void configureConnFactory(JmsConnectionFactory connFac, ConnectionData connData) throws JMSException {

		connFac.setStringProperty(WMQConstants.WMQ_HOST_NAME, connData.getHost());
		connFac.setIntProperty(WMQConstants.WMQ_PORT, connData.getPort());
		connFac.setStringProperty(WMQConstants.WMQ_CHANNEL, connData.getServerChan());
		connFac.setIntProperty(WMQConstants.WMQ_CONNECTION_MODE, WMQConstants.WMQ_CM_CLIENT);
		connFac.setStringProperty(WMQConstants.WMQ_QUEUE_MANAGER, connData.getQmgr());
		connFac.setStringProperty(WMQConstants.WMQ_APPLICATIONNAME, "nJAMS Client for IIB"); // FIXME  required?			
		connFac.setBooleanProperty(WMQConstants.USER_AUTHENTICATION_MQCSP, false);
		if (connData.getUserid() != null) {
			connFac.setBooleanProperty(WMQConstants.USER_AUTHENTICATION_MQCSP, true);
			connFac.setStringProperty(WMQConstants.USERID, connData.getUserid());
			connFac.setStringProperty(WMQConstants.PASSWORD, connData.getPassword());
		}
	}
	
	protected Connection createJMSConnection(JmsConnectionFactory connFac, ConnectionData connData) throws JMSException {
		Connection jmsConnection = null; 
		connFac.createConnection();
		if (connData.getUserid() == null) {
			jmsConnection = connFac.createConnection();
		} else {
			jmsConnection = connFac.createConnection( connData.getUserid(),  connData.getPassword());
		}

		return jmsConnection;
	}
	
	protected Session createJMSSession(Connection jmsConnection) throws JMSException{
		boolean transacted = false; // FIXME pass as parameter 
		Session jmsSession = jmsConnection.createSession(transacted, Session.AUTO_ACKNOWLEDGE);

		return jmsSession;
	}
	
	protected Destination createJMSDestination(Session jmsSession, ConnectionData connData) throws JMSException {
		Destination jmsDest = null;
		jmsDest = jmsSession.createQueue(String.format("queue:///%s?WMQConstants.WMQ_PERSISTENCE = %d", connData.getQueue(), WMQConstants.WMQ_PER_QDEF)); // FIXME externalise  only for writing

		return jmsDest;	
	}

	/**
	 * For use in jUnit testing
	 * @return
	 */
	public Destination getJmsDest() {
		return jmsDest;
	}

}
