package com.w3p.im.iib.mon.client.config.data;

import java.util.List;

public interface IRunnableObjectX extends IConfigObject {  //IDeployableObject {

//	boolean isIncludeSchemaName();
//
//	String getName();

	void setGlobalCorrelationId(GlobalCorrelationId globalCorrelationId);

	GlobalCorrelationId getGlobalCorrelationId(String msgFlowName);

	void setApplicationDataQuery(ApplicationDataQuery applicationDataQuery);

	ApplicationDataQuery getApplicationDataQuery(String msgFlowName);

//	List<MsgFlow> getMsgFlows();
//
//	List<SubFlow> getSubFlows();

	List<StaticLibrary> getStaticLibs();

}