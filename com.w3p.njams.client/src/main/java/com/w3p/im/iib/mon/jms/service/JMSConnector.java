package com.w3p.im.iib.mon.jms.service;

import static com.w3p.im.iib.mon.client.constants.IClientConstants.QUEUE;

import javax.jms.Connection;
import javax.jms.Destination;
import javax.jms.JMSException;
import javax.jms.MessageConsumer;
import javax.jms.MessageProducer;
import javax.jms.Session;
import javax.naming.Context;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


public abstract class JMSConnector {
	private static final Logger logger = LoggerFactory.getLogger(JMSConnector.class);
	
	protected Context context; // JNDI context
	// JMS Connection is thread-safe and can be shared - unlike session which must be single-threaded
	protected Connection connection; 
	protected Session session; // JMS Session for receiving or sending.
	
	protected boolean isTransacted;
	
		
	/**
	 * @param jndiDestName
	 * @return
	 * @throws JMSException 
	 */
	protected Destination getDestination(String jndiDestName) throws JMSException {
		if (jndiDestName.startsWith(QUEUE)) {
			return session.createQueue(jndiDestName);	
		} else {
			return  session.createTopic(jndiDestName);
		}		
	}
	
	/**
	 * @param connection
	 * @param transacted
	 * @throws JMSException 
	 */
	protected void createSession(Connection connection, boolean transacted) throws JMSException {
		logger.trace("start");
		session = connection.createSession(transacted, Session.AUTO_ACKNOWLEDGE); 
		logger.trace("end");
	}
	
	/**
	 * @param connection
	 * @param jmsDest
	 * @return
	 * @throws JMSException 
	 */
	protected MessageConsumer createJmsConsumer(Connection connection, Destination dest) throws JMSException {
		logger.trace("start");
		MessageConsumer consumer = session.createConsumer(dest);
		logger.trace("end");
		return consumer;
	}
	
	/**
	 * @param connection
	 * @param jmsDest
	 * @return
	 * @throws JMSException 
	 */
	protected MessageProducer createJmsProducer(Connection connection, Destination dest) throws JMSException {
		logger.trace("start");
		MessageProducer producer = session.createProducer(dest);		
		logger.trace("end");
		return producer;
	}
	

	/**
	 * @param session
	 * @throws JMSException 
	 */
	protected void commit() throws JMSException {
		logger.trace("start");
		if (session != null && isTransacted) {			
			session.commit();
			session.close();
		}
		logger.trace("end");
	}

	/**
	 * @param session
	 * @throws JMSException 
	 */
	protected void rollback() throws JMSException {
		logger.trace("start");
		if (session != null && isTransacted) {
			session.rollback();
		}		
		logger.trace("end");
	}
}
