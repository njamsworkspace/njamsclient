package com.w3p.im.iib.mon.monitor.data;

public class MonitoredSubFlow extends MonitoredObject<MonitoredObject<?>> {

	public MonitoredSubFlow(String name) {
		super(name);
	}
	
	@Override
	public String toString() {
		return "Monitored SubFlow [name=" + name + "]";
	}
}
