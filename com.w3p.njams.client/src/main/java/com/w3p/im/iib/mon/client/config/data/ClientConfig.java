package com.w3p.im.iib.mon.client.config.data;

public class ClientConfig {
	
	private String clientHome;
	private String type; // IIB version, e.g 'IIB-10' or 'ACE-11'
	private String clientMode;
	private Connections connections;
	private Monitoring  monitoring;
	
	
	public ClientConfig() {
	}

	public String getClientHome() {
		return clientHome;
	}
	public ClientConfig setClientHome(String clientHome) {
		this.clientHome = clientHome;
		return this;
	}

	public Connections getConnections() {
		return connections;
	}
	public ClientConfig setConnections(Connections connections) {
		this.connections = connections;
		return this;
	}
	public Monitoring getMonitoring() {
		return monitoring;
	}
	public ClientConfig setMonitoring(Monitoring monitoring) {
		this.monitoring = monitoring;
		return this;
	}
	public String getType() {
		return type;
	}
	public ClientConfig setType(String type) {
		this.type = type;
		return this;
	}
	public String getClientMode() {
		return clientMode;
	}
	public ClientConfig setClientMode(String clientMode) {
		this.clientMode = clientMode;
		return this;
	}

	@Override
	public String toString() {
		return "ClientConfig [type=" + type + ", clientMode=" + clientMode + ", connections=" + connections
				+ ", monitoring=" + monitoring + "]";
	}
}
