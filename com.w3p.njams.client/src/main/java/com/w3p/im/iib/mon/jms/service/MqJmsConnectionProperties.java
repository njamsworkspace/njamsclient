package com.w3p.im.iib.mon.jms.service;

import com.w3p.im.iib.mon.client.config.data.User;

public class MqJmsConnectionProperties {
	
	private String mqJmsProviderUrl;
	private String mqJmsInitialContextFactory;
	private String mqJmsConnectionFactory;
	private User mqJmsUser;
	private String jmsClientId;
	
	
	public String getMqJmsProviderUrl() {
		return mqJmsProviderUrl;
	}

	public MqJmsConnectionProperties setMqJmsProviderUrl(String mqJmsProviderUrl) {
		this.mqJmsProviderUrl = mqJmsProviderUrl;
		return this;
	}

	public String getMqJmsInitialContextFactory() {
		return mqJmsInitialContextFactory;
	}

	public MqJmsConnectionProperties setMqJmsInitialContextFactory(String mqJmsInitialContextFactory) {
		this.mqJmsInitialContextFactory = mqJmsInitialContextFactory;
		return this;
	}

	public String getMqJmsConnectionFactory() {
		return mqJmsConnectionFactory;
	}

	public MqJmsConnectionProperties setMqJmsConnectionFactory(String mqJmsConnectionFactory) {
		this.mqJmsConnectionFactory = mqJmsConnectionFactory;
		return this;
	}

	public User getMqJmsUser() {
		return mqJmsUser;
	}

	public MqJmsConnectionProperties setMqJmsUser(User mqJmsUser) {
		this.mqJmsUser = mqJmsUser;
		return this;
	}

	public String getJmsClientId() {
		return jmsClientId;
	}

	public MqJmsConnectionProperties setJmsClientId(String jmsClientId) {
		this.jmsClientId = jmsClientId;
		return this;
	}
}
