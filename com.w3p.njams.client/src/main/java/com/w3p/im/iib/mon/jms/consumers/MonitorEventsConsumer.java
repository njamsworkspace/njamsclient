package com.w3p.im.iib.mon.jms.consumers;

import java.util.Hashtable;
import java.util.Map;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.LinkedBlockingDeque;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

import javax.jms.Connection;
import javax.jms.JMSException;
import javax.jms.Message;
import javax.naming.Context;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.im.njams.sdk.Njams;
import com.w3p.im.iib.mon.event.processors.EventSet;
import com.w3p.im.iib.mon.event.processors.MessageEvents;
import com.w3p.im.iib.mon.event.processors.MonitorEventsProcessor;
import com.w3p.im.iib.mon.exceptions.monitoring.MonitoringEventParserException;
import com.w3p.im.iib.mon.exceptions.monitoring.MonitoringException;
import com.w3p.im.iib.mon.jms.data.IMessageConsumer;
import com.w3p.im.iib.mon.jms.data.IMessageProducer;
import com.w3p.im.iib.mon.jms.data.IMessagingServiceProducer;
import com.w3p.im.iib.mon.jms.producers.FlowToProcessModelCache;
import com.w3p.im.iib.mon.monitor.data.MonitorEventsRequest;
import com.w3p.im.iib.mon.monitor.data.MonitorEventsRequestForQueues;
import com.w3p.im.iib.mon.monitor.data.MonitorEventsRequestForTopics;
import com.w3p.im.iib.mon.monitor.data.MonitoringEvent;
import com.w3p.im.iib.mon.monitor.data.groovy.ParseMonitoringEvent;

public class MonitorEventsConsumer extends MsgConsumer implements IMessageConsumer {
	
	private static final Logger logger = LoggerFactory.getLogger(MonitorEventsConsumer.class);

	private final MonitorEventsRequest request;
	protected String intSvrName;	// DEV
	protected Njams njamsClient;  // DEV
	protected FlowToProcessModelCache flowToProcessModelCache;
	
	protected Map<String, Njams> njamsClients;                               // PROD k=intSvrName, v=njamsClient for intSvr
	protected Map<String, FlowToProcessModelCache> flowToProcessModelCaches; // PROD k=intSvrName, v=FlowToProcessModelCache for intSvr
	
	// See: https://www.baeldung.com/java-hash-table
	private final Hashtable<String, EventSet> eventSets = new Hashtable<>(50); // localTxnId, EventSet  Note: is synchronized	
	private final Hashtable<String, MonitorEventsProcessor> eventProcessors = new Hashtable<>();
	
	private IMessagingServiceProducer messagingServiceProducer;
	private IMessageProducer monitorEventsErrorsProducer;
	
	protected ExecutorService consumeEventsExecutor;
	protected ExecutorService  processEventsForMsgflow;

	
	public MonitorEventsConsumer(MonitorEventsRequest request) {
		this.request = request;
		int consumerThreads = request.getConsumerThreads();
		this.consumeEventsExecutor = new ThreadPoolExecutor(consumerThreads, consumerThreads, 1000, TimeUnit.MILLISECONDS, 
				new LinkedBlockingDeque<Runnable>()) {}; 			
		if (request instanceof MonitorEventsRequestForQueues) {
			MonitorEventsRequestForQueues reuestForQueues = (MonitorEventsRequestForQueues)request;
			this.njamsClients = reuestForQueues.getNjamsClients();
			this.flowToProcessModelCaches = reuestForQueues.getFlowToProcessModelCaches();
		} else if (request instanceof MonitorEventsRequestForTopics) {
			MonitorEventsRequestForTopics requestForTopics = (MonitorEventsRequestForTopics)request;
			this.intSvrName = requestForTopics.getIntegrationServerName();
			this.njamsClient = requestForTopics.getNjamsClient();
			this.flowToProcessModelCache = requestForTopics.getFlowToProcessModelCache();
		}
	}

	public void setMessagingService(IMessagingServiceProducer messagingService) {
		this.messagingServiceProducer = messagingService;
		if (messagingServiceProducer != null) { 
			monitorEventsErrorsProducer = messagingServiceProducer.getMessageProducer();
		}
	}
	
	@Override
	public void consumeMsgsFromDestination(Context context, Connection connection, String dest, boolean isTransacted) throws JMSException {
		super.consumeMsgsFromDestination(context, connection, dest, isTransacted);
		jmsConsumer.setMessageListener(this); 
		logger.trace("entry");	
		connection.start(); // Start listening
	}

	@Override
	public void onMessage(Message jmsMsg) {
		try {
			// Note: jmsCorrelationId is constant for a msg set, but not = to event's localCorrelationid
			String eventAsXML = extractMessageAsString(jmsMsg);
			String localTxnId = StringUtils.substringBetween(eventAsXML, "wmb:localTransactionId=\"", "\" wmb:parentTransactionId");
			
			// If there is no entry for the localTxnId, a new EventSet is created and returned.
			// If there is an existing entry for the key, the EventSet is returned.
			final EventSet eventSet = eventSets.computeIfAbsent(localTxnId, k -> new EventSet(k));
			if (eventSet.isNew()) {
				eventSet.registerHandler(this); // The sendEventsToNjams() method will be called back when all mon events have been received
			}			
			eventSet.addEvent(eventAsXML);
		} catch (Exception e) {
			// Could be anything - don't throw an exception to avoid breaking the MQTT cient
			logger.error(e.toString(), e);
			e.printStackTrace();
		} 		
	}

	/**
	 *  Callback from EventSet when all events have been received for the localTxnId
	 * 
	 * @param localTxnId
	 * @param eventSet
	 */
	public void sendEventsToNjams(final String localTxnId, final EventSet eventSet) {
		
		consumeEventsExecutor.execute(new Runnable() {		
			@Override
			public void run() {
				ParseMonitoringEvent eventParser = new ParseMonitoringEvent();
				try {					
					MessageEvents messageEvents = createEventsFromMessageFlow(localTxnId, eventSet, eventParser);
					messageEvents.processEvents();
					String intSvrName = messageEvents.getFirstEvent().getIntSvrName();
					String threadId = Long.toString(Thread.currentThread().getId());
					String intSvrThreadKey = intSvrName.concat(threadId);
					
					MonitorEventsProcessor monitorEventsProcessor = null;
					if (eventProcessors.containsKey(intSvrThreadKey)) {
						logger.info(" Re-using MonitorEventsProcessor for int svr thread key '{}'.", intSvrThreadKey); // FIXME restore to 'debug'
						monitorEventsProcessor = eventProcessors.get(intSvrThreadKey);						
					} else {
						logger.info("Creating new MonitorEventsProcessor for int svr thread key  '{}'.", intSvrThreadKey);
						Njams client = null;
						FlowToProcessModelCache cache = null;
						if (njamsClients != null) {
							client = njamsClients.get(intSvrName);
							cache = flowToProcessModelCaches.get(intSvrName);
						} else {
							client = njamsClient;
							cache = flowToProcessModelCache;
						}
						monitorEventsProcessor = new MonitorEventsProcessor(client, cache, request);
						eventProcessors.put(intSvrThreadKey, monitorEventsProcessor);
					}
					
					monitorEventsProcessor.sendLogMessageToNjams(messageEvents);
					// Start over again				
					messageEvents.clearEvents();
					messageEvents = null;
				} catch (MonitoringException e )  {
					// Error parsing a monitoring event, or processing the event set.
					logger.error(e.toString(), e);
					e.printStackTrace();
				} catch (Exception e) {
					// We can't throw this error from the thread to be caught by the launching thread. Just log it.
					// FIXME add a callback to sync method outside this run()
					logger.error("Error when processing Monitoring Events. Cause is: '{}'.", e.toString(), e);
					e.printStackTrace();
				}
			}

			private MessageEvents createEventsFromMessageFlow(final String localTxnId, final EventSet eventSet, ParseMonitoringEvent eventParser) {
				MessageEvents messageEvents = new MessageEvents();
				try {
					// Invoke the event processor
					for (String eventAsXML : eventSet.getMonEvents()) {
						MonitoringEvent monitoringEvent = processMessage(eventAsXML, eventParser);
						messageEvents.addEvent(monitoringEvent);
					}
				} catch (MonitoringEventParserException e) {
					// Error parsing a monitoring event
					logger.error(e.toString(), e);
				} catch (Exception e) {
					// Error parsing a monitoring event
					logger.error(e.toString(), e);
					e.printStackTrace();
				}
				
				// We have read the last message of the set & no more event messages for subsequent msgflows - process the event set
				logger.debug("readEventsMessages(): read last msg of event set - processing the set");
				try {
					eventSet.clear(); // Remove references to used objects	
				} catch (Exception e) {
					System.err.println("Error when clearing the EventSet: "+ e.toString());
					e.printStackTrace();
				}
				
				synchronized (eventSets) {
					eventSets.remove(localTxnId);
				}
				return messageEvents;
			}

			/**
				 * @param eventAsXML
			 * @param eventParser 
				 * @return MonitoringEvent
				 * @throws MonitoringEventParserException
				 */
				private MonitoringEvent processMessage(String eventAsXML, ParseMonitoringEvent eventParser) throws MonitoringEventParserException {
					logger.trace("#processMessage() - Message received: \n  Text = {}", eventAsXML);
					try {
						MonitoringEvent event = eventParser.parse(eventAsXML);
						logger.debug("Local txn id: {}", event.getLocalTransactionId());
						return event;			
					} catch (Exception e) {
						throw new  MonitoringEventParserException(String.format("Error parsing a Monitoring Event. The message set will be skipped. Cause: '%s'.\n  Message = '%s''. ",
								e.toString(), eventAsXML), e);
					}
				}
		});

	}
}
