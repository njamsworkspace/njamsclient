package com.w3p.im.iib.mon.exceptions.internal;

import com.w3p.im.iib.mon.exceptions.InternalException;

public class MessagingException extends InternalException {


	private static final long serialVersionUID = 1L;

	public MessagingException() {
		super();
	}

	public MessagingException(String msg, Object... values) {
		super(msg, values);
	}
}
