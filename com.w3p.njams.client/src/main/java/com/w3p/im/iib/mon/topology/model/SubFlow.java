package com.w3p.im.iib.mon.topology.model;

import com.w3p.api.config.proxy.SubFlowProxy;

//import com.ibm.broker.config.proxy.SubFlowProxy;

public class SubFlow extends Flow {
	

	public SubFlow(SubFlowProxy sfp) {
		super(sfp);

	}
	
	public SubFlowProxy getSubFlowProxy() {
		return (SubFlowProxy)fp; 
	}

	@Override
	public String toString() {
		return "SubFlow [name=" + flowName + "]";
	}
}
