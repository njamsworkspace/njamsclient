package com.w3p.im.iib.mon.exceptions;

import java.text.MessageFormat;
import java.util.Arrays;
import java.util.Date;


/**
 * @author JohnO
 *
 * Top-level exception for all checkable exceptions that are created
 * 
 */
public abstract class NjamsException extends Exception {

	private static final long serialVersionUID = 1L;

	protected Throwable cause; // The exception that caused this one to be thrown
	protected String errorID; // Unique id of originating error - assigned to subsequently thrown errors
	protected String msg;

	/**
	 * 
	 */
	protected NjamsException() {
		super();
		errorID = createErrorId();
	}


	protected String createErrorId() {
		return String.format("\n Error Id: %s", String.valueOf(new Date().getTime()));
	}


	protected void formatMsg(String message, Object... values) {
		if (values != null && values.length > 0) {
			int argsLength = values.length;
			if (Throwable.class.isAssignableFrom(values[argsLength-1].getClass())) {
				cause = (Throwable)values[argsLength-1];
			}
			if (argsLength > 1) {
				Object[] vals = Arrays.copyOfRange(values, 0, argsLength);
				msg = MessageFormat.format(message, vals);
			} else {
				msg = MessageFormat.format(message, values);
			}
		} else {
			msg = message;
		}
	}

	/**
	 * @return
	 */
	public Throwable getCause() {
		return cause;
	}


//	@Override
//	public String getMessage() {
//		return cause != null ? cause.getMessage() : super.getMessage();
//	}

	/**
	 * @return
	 */
	public String getErrorID() {
		return errorID;
	}

	/**
	 * @return the String that has been provided to this exception instance
	 */
	public String getMsg() {
		return msg;
	}

	@Override
	public String toString() {
		return "NjamsException [cause=" + cause + ", errorID=" + errorID + ", msg=" + msg + "]";
	}
}
