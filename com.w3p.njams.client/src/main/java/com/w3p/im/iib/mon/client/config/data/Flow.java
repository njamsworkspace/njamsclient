package com.w3p.im.iib.mon.client.config.data;

public class Flow {

	protected final String name;
	protected final boolean includePayload;
	protected final boolean labelNodeStartsGroup;
	
	public Flow(String name, boolean includePayload, boolean labelNodeStartsGroup) {
		this.name = name;
		this.labelNodeStartsGroup = labelNodeStartsGroup;
		this.includePayload = includePayload;
	}

	public String getName() {
		return name;
	}
	public boolean isLabelNodeStartsGroup() {
		return labelNodeStartsGroup;
	}

	public boolean isIncludePayload() {
		return includePayload;
	}

}
