package com.w3p.im.iib.mon.topology.model;

import static com.w3p.im.iib.mon.client.constants.IClientConstants.SUBFLOW_URI_SHARED_LIB;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.StringUtils;

import com.im.njams.sdk.common.Path;
import com.w3p.api.config.proxy.MessageFlowProxy;
import com.w3p.api.config.proxy.MessageFlowProxy.Node;


public class FlowNode {
	
	private static final String PUB_SUB_NODE = "ComIbmPSServiceNode";
	protected final static String DOT = ".";
	protected final static String GT = ">";
	protected final MessageFlowProxy.Node node;
	
	protected final List<FlowNodeConnection> flowNodeConnections = new ArrayList<>();
	protected String inTerminal;
	protected List<String> outTerminals;
	protected final String name;

	
	public FlowNode(Node node) {
		this.node = node;
		// Handle 'Publish' nodes to remove the '.Publish' name from the end => Events do not contain the '.publish' in their Node label
		// NOTE: this found from testing with IIB 9 Pager sample, msgflow SurfWatch.
		if (node.getType().equals(PUB_SUB_NODE) ){
			this.name = node.getName().substring(0, node.getName().indexOf(DOT));
		} else {
			this.name = node.getName();
		}
	}

	public String getName() {
		return this.name;
	}
	
	public String getType() {
		return node.getType();
	}

	public Path getSubflowPath(boolean includeSchemaNames) {
		// The 'parent.subflow-name', when this node is a SubFlow
		// Example of URI: /apiv1/executiongroups/njamsTesting/restapis/EmployeeService_REST/subflows/[schema.]getEmployee
		// Target name: EmployeeService_REST.getEmployee
		// Example of IIB URI: /apiv1/executiongroups/njamsTesting/sharedlibraries/PetsAtHomeLibrary/subflows/PaH_SubFlowLib.SF_IIB_Input_Basic
		// Target name: PetsAtHomeLibrary[.PaH_SubFlowLib].SF_IIB_Input_Basic
		// Example of ACE URI in App:  /apiv2/servers/njamsDemo/applications/DemoApp/subflows/SF_HTTPInput 
		// In a shared library  :  /apiv2/servers/njamsDemo/shared-libraries/HTTPInputLibrary/subflows/Subflows.SF_Ttrace
		// In a STATIC library  :  /apiv2/servers/njamsDev/applications/TestStaticLibApp/libraries/TestStaticLib/subflows/SubflowTestStaticLib   NOTE: has extra layer
		Path subflowPath = null;
		
		if (node.getType().equals("SubFlowNode")) {
			String subflowURI = node.getProperties().getProperty("subflowURI");
			boolean isInStaticLib = subflowURI.contains("libraries") && !subflowURI.contains(SUBFLOW_URI_SHARED_LIB);
			
			String subflowName = StringUtils.substringAfterLast(subflowURI, "/");
			// Remove any Broker Schema values for the Flow
			if (!includeSchemaNames && StringUtils.contains(subflowName, DOT)) {
				subflowName = StringUtils.substringAfterLast(subflowName, DOT);
			}			
			
			String staticLibName = null;
			String deployableName = null;
			String preSubflows = StringUtils.substringBefore(subflowURI, "/subflows"); 
			if (isInStaticLib) {  // preSubflows = // /apiv2/servers/njamsDev/applications/TestStaticLibApp/libraries/TestStaticLib
				staticLibName = StringUtils.substringAfter(preSubflows, "/libraries/");
				String preDeployableName = StringUtils.substringBefore(preSubflows, "/libraries"); // /apiv2/servers/njamsDev/applications/TestStaticLibApp
				deployableName = StringUtils.substringAfterLast(preDeployableName, "/");
				subflowPath =  new Path(deployableName, staticLibName, subflowName);
			} else {  // preSubflows =  /apiv2/servers/njamsDemo/applications/DemoApp
				deployableName = StringUtils.substringAfterLast(preSubflows, "/");
				subflowPath = new Path(deployableName, subflowName); // If subflow is in the same deployable as the flow, return: local-sfname>real-sfname
			}			
		}		
		return subflowPath;
	}

	public List<FlowNodeConnection> getFlowNodeConnections() {
		return flowNodeConnections;
	}
	
	public void setFlowNodeConnections(List<FlowNodeConnection> flowNodeConnections) {
		this.flowNodeConnections.clear();
		this.flowNodeConnections.addAll(flowNodeConnections);
	}

	public String getInTerminal() {
		return inTerminal;
	}

	public void setInTerminal(String inTerminal) {
		this.inTerminal = inTerminal;
	}

	public List<String> getOutTerminals() {
		return outTerminals;
	}

	public void setOutTerminals(List<String> outTerminals) {
		this.outTerminals.clear();
		this.outTerminals.addAll(outTerminals);
	}

	public int getX() {
		return (int)node.getLocation().getX();
	}

	public int getY() {
		return (int)node.getLocation().getY();
	}

	public boolean isRootNode() {
		return flowNodeConnections.size() > 1;
	}

	@Override
	public String toString() {
		return "FlowNode [name=" + getName() + ", type=" + getType() + "]"; //", sfFileName=" + getSubflowFileName() + ", sfName=" + getSubflowPath(false) +"]";
	}
}
