package com.w3p.im.iib.mon.monitor.profile;

import java.io.IOException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.w3p.api.config.proxy.BrokerProxy;
import com.w3p.im.iib.mon.client.config.data.ApplicationDataQuery;
import com.w3p.im.iib.mon.client.config.data.GlobalCorrelationId;
import com.w3p.im.iib.mon.client.config.data.IDeployableObject;
import com.w3p.im.iib.mon.exceptions.internal.IIBTopologyCreationException;
import com.w3p.im.iib.mon.monitor.data.ProcessMonitoringProfileRequest;
import com.w3p.im.iib.mon.monitor.data.ProcessMonitoringProfileResponse;
import com.w3p.im.iib.mon.monitor.profile.groovy.CreateMonitoringProfile;
import com.w3p.im.iib.mon.topology.data.GenerateMonitoringProfileRequest;
import com.w3p.im.iib.mon.topology.data.GenerateMonitoringProfileResponse;
import com.w3p.im.iib.mon.topology.model.Application;
import com.w3p.im.iib.mon.topology.model.Deployable;
import com.w3p.im.iib.mon.topology.model.IntegrationServer;
import com.w3p.im.iib.mon.topology.model.MessageFlow;
import com.w3p.im.iib.mon.topology.model.RestApi;
import com.w3p.im.iib.mon.topology.model.Service;

public class GenerateMonitoringProfiles {
	private static final Logger logger = LoggerFactory.getLogger(GenerateMonitoringProfiles.class);

	private final GenerateMonitoringProfileRequest request;
	
	public GenerateMonitoringProfiles(GenerateMonitoringProfileRequest request) {
		this.request = request;
	}
	
	public GenerateMonitoringProfileResponse generateProfile() throws IIBTopologyCreationException {
		
		GenerateMonitoringProfileResponse response = new GenerateMonitoringProfileResponse();
		
		BrokerProxy intNodeProxy = request.getIntegrationNodeProxy();
		com.w3p.im.iib.mon.client.config.data.IntegrationServer configIntSvr = request.getIntegrationServer();
		String intSvrName = 	configIntSvr.getName();		
		IntegrationServer topologyIntSvr = request.getTopology().getIntegrationServer(intSvrName);
		
		try {
		//  When 'isApplyProfileIfUnchanged' is true, this forces the profile to be generated and applied, even if it hasn't changed.
			for (Application app : topologyIntSvr.getApplications()) {
				IDeployableObject configRun = configIntSvr.getApplication(app.getName()); // topologyApp  configApp				
				for (MessageFlow topologyMsgFlow : app.getMessageFlows()) {
					create(response, intNodeProxy, intSvrName, topologyIntSvr, app, configRun,	topologyMsgFlow);
				}
			}

			for (RestApi api : topologyIntSvr.getRestApis()) {
				IDeployableObject configRun = configIntSvr.getRestAPI(api.getName()); // topologyApp  configApp				
				for (MessageFlow topologyMsgFlow : api.getMessageFlows()) {
					create(response, intNodeProxy, intSvrName, topologyIntSvr, api, configRun,	topologyMsgFlow);
				}
			}
			
			for (Service service : topologyIntSvr.getServices()) {
				IDeployableObject configRun = configIntSvr.getService(service.getName()); // topologyApp  configApp				
				for (MessageFlow topologyMsgFlow : service.getMessageFlows()) {
					create(response, intNodeProxy, intSvrName, topologyIntSvr, service, configRun,	topologyMsgFlow);
				}
			}
		} catch (Exception e) {
			response.setSuccess(false);
			e.printStackTrace(); // FIXME add error handling
		}
		
		return response;
		
	}

	private void create(GenerateMonitoringProfileResponse response, BrokerProxy intNodeProxy, String intSvrName,
			IntegrationServer topologyIntSvr, Deployable runnable, IDeployableObject configRun,	MessageFlow topologyMsgFlow) throws IOException {
		
		GlobalCorrelationId gciToUse = configRun.getGlobalCorrelationId(topologyMsgFlow.getName()); //getMsgFlowCorrelations(topologyMsgFlow.getName());
		ApplicationDataQuery adqTouse = configRun.getApplicationDataQuery(topologyMsgFlow.getName());
		// Create the mon profile for the msgflow
		String monitoringProfileasXml =  new CreateMonitoringProfile().createForFlow(topologyIntSvr, topologyMsgFlow, gciToUse, adqTouse); // correlationsToUse);
		logger.info("Created Monitoring Profile for message flow '{}'", topologyMsgFlow.getName());							
		
		// Now, apply the monitoring profile, if it is new or has changed
		ProcessMonitoringProfileRequest pmpr = new ProcessMonitoringProfileRequest(monitoringProfileasXml, topologyIntSvr.getName(), runnable.getName(), topologyMsgFlow);
		pmpr.setTimeToWait(request.getTimeToWait())
			.setIntNodeName(request.getIntNodeName())
			.setMonProfileDir(request.getMonProfileDir())
			.setApplyProfileIfUnchanged(request.isApplyProfileIfUnchanged())
			.setIntNodeProxy(intNodeProxy);

		ProcessMonitoringProfile processMonitoringProfile = new ProcessMonitoringProfile(pmpr);
		ProcessMonitoringProfileResponse processMonitoringProfileResponse = processMonitoringProfile.writeProfileIfNewOrChanged();
		
		// Check response							
		if (processMonitoringProfileResponse.isSuccess()) {
			logger.info("Apply Monitoring Profile succeeded: {}", processMonitoringProfileResponse.getAllMessages());			
		} else {
			logger.error("Apply Monitoring Profile failed for: {}", processMonitoringProfileResponse.getAllMessages());
		}
		if (processMonitoringProfileResponse.getAllMessages().isEmpty()) {
			logger.info("Response messages from applying flow monitoring to '{}':\n{}", topologyMsgFlow.getNameWithSchema(), processMonitoringProfileResponse.getAllMessages());
			response.getMonProfileResponses().put(intSvrName, processMonitoringProfileResponse.getAllMessages());
		}
	}
}
