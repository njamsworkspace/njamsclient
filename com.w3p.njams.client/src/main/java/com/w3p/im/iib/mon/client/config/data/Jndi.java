package com.w3p.im.iib.mon.client.config.data;

public class Jndi {
	
	private User user;
	public User getUser() {
		return user;
	}
	public void setUser(User user) {
		this.user = user;
	}
	@Override
	public String toString() {
		return "Jndi [user=" + user + "]";
	}
}
