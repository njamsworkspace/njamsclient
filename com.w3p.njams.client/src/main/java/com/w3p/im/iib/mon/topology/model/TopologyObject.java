package com.w3p.im.iib.mon.topology.model;

import java.util.ArrayList;
import java.util.List;

import com.w3p.api.config.proxy.BrokerProxy;
import com.w3p.api.config.proxy.ConfigManagerProxyPropertyNotInitializedException;


public class TopologyObject  extends AbstractTopology {
	
	private final BrokerProxy in;
	private final List<IntegrationServer> integrationServers = new ArrayList<>();

	public TopologyObject(BrokerProxy in) {
		this.in = in;
		try {
			super.name = in.getName();
		} catch (ConfigManagerProxyPropertyNotInitializedException e) {
			super.name = null;
			e.printStackTrace();
		}
	}

	public BrokerProxy getIntegrationNode() {
		return in;
	}

	@Override
	public <T extends AbstractTopology> void addTopologyItem(T is) {
		integrationServers.add((IntegrationServer)is);
	}
	
	@Override
	public boolean isRunning() {
		try {
			return in.isRunning();
		} catch (ConfigManagerProxyPropertyNotInitializedException e) {
			e.printStackTrace();
			return false;
		}
	}

	public String getIntegrationNodeName() {
		return getName();
	}

	public IntegrationServer getIntegrationServer(String intSvrName) {
		for (IntegrationServer intSvr : integrationServers) {
			if (intSvr.getName().equals(intSvrName)) {
				return intSvr;
			}
		}
		return null;
	}
	
	public boolean isSomethingToMonitor(String intSvrName) {
		IntegrationServer is = getIntegrationServer(intSvrName);
		for (Application app : is.getApplications()) {
			if (app.getMessageFlows().size() > 0) {
				return true;
			}
			if (app.getSubFlows().size() > 0) {
				return true;
			}
		}
		for (RestApi api : is.getRestApis()) {
			if (api.getMessageFlows().size() > 0) {
				return true;
			}
			if (api.getSubFlows().size() > 0) {
				return true;
			}
		}
		
		for (Service service : is.getServices()) {
			if (service.getMessageFlows().size() > 0) {
				return true;
			}
			if (service.getSubFlows().size() > 0) {
				return true;
			}
		}
		
		return false;
	}
	
	@Override
	public String toString() {
		return "TopologyObject [integrationNodeName=" + name + ", integrationServers="
				+ integrationServers + "]";
	}	
}
