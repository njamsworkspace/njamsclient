package com.w3p.im.iib.mon.exceptions.internal;

import com.w3p.im.iib.mon.exceptions.InternalException;

public class ClientProblemException extends InternalException {


	private static final long serialVersionUID = 1L;

	public ClientProblemException() {
		super();
	}

	public ClientProblemException(String msg, Object... values) {
		super(msg, values);
	}
}
