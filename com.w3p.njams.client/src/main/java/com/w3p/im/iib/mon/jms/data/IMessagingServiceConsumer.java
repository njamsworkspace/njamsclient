package com.w3p.im.iib.mon.jms.data;

import com.w3p.im.iib.mon.jms.service.MessagingServiceFactory;

/**
 * @author John Ormerod
 *
 */
public interface IMessagingServiceConsumer extends IMessagingService {
	
	public void addMessageConsumer(String dest, IMessageConsumer msgConsumer) throws Exception;
	
	public void addMessageConsumer(String dest, IMessageConsumer msgConsumer, boolean isTransacted) throws Exception;
	
	public IMessageConsumer getMessageConsumer();
	
	void setMessagingServiceFactory(MessagingServiceFactory messagingServiceFactory);
	
	MessagingServiceFactory getMessagingServiceFactory();
}
