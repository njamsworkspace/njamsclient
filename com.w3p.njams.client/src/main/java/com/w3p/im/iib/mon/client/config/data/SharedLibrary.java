package com.w3p.im.iib.mon.client.config.data;

public class SharedLibrary extends Library {

	public SharedLibrary(String name, boolean includeSchemaName) {
		super(name, includeSchemaName);
	}

	@Override
	public String toString() {
		return "SharedLibrary [name=" + getName() + "]";
	}

}
