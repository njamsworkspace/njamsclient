package com.w3p.im.iib.mon.client.utils.io.writers;

import java.io.Closeable;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.w3p.im.iib.mon.client.utils.io.data.IWriteFileRequest;
import com.w3p.im.iib.mon.client.utils.io.data.Response;

public class FileWriter implements IFileWriter, Closeable {
	private static final Logger log = LoggerFactory.getLogger(FileWriter.class);

	protected Path folderPath;
	protected String fileNamePattern;
	protected boolean replaceFilesIfExist;

	public FileWriter() {
	}

	@Override
	public void init(IWriteFileRequest req, Response resp) {
		IWriteFileRequest wfreq = (IWriteFileRequest)req;		
		String dest = wfreq.getDestination();
		this.fileNamePattern = wfreq.getNamePattern();

		try {
			// For reference:  kind of Elvis operator - null-safe
			//			Optional<Object> o = Optional.ofNullable(req.getOption(CREATE_FOLDER_IF_NOT_EXISTS));
			//			boolean createFolderIfNotExists = (o.isPresent() ? (Boolean)o.get() : false);
			
			boolean createFolderIfNotExists = wfreq.isCreateFolderIfNotExists();
			boolean clearFolderIfNotEmpty = wfreq.isClearFolderIfNotEmpty();
			this.replaceFilesIfExist = wfreq.isReplaceFilesIfExist(); // If a file exists and this is true, the file will be replaced.

			folderPath = Paths.get(dest);
			if (Files.notExists(folderPath)) {
				if (createFolderIfNotExists) {					
					Files.createDirectories(folderPath);
					String infoMsg = String.format("Created folder '%s'.", folderPath.toString());
					log.info(infoMsg);
					resp.setMessage(Response.INFO, infoMsg);
				} else {
					String errMsg = String.format("Folder '%s' does not exist. Pease create it or auto-create it by using the 'createFolderIfNotExists' option.", dest);
					log.error(errMsg);
					resp.setMessage(Response.ERROR, errMsg);
				}
			} else {
				// Check folder is empty				
				if (folderPath.toFile().listFiles().length > 0) {
					if (clearFolderIfNotEmpty) {						
						deleteFilesInTargetFolder(folderPath, resp);
					}else {
						String infoMsg = String.format("Folder '%s' is not empty - new files will be added.", folderPath.toString());
						log.info(infoMsg);
						resp.setMessage(Response.INFO, infoMsg);
					} 
				}
			}
		} catch (IOException e) {
			String reason = String.format("Error initialising the folder '%s' to which files will be writtem." , dest); //, e.toString());
			log.error(reason, e);
			resp.setMessage(Response.ERROR, reason);
		}
	}

	protected void deleteFilesInTargetFolder(Path folderPath, Response resp) throws IOException {
		// See: https://stackoverflow.com/questions/35988192/java-nio-most-concise-recursive-directory-delete
		final List<Path> pathsToDelete = Files.walk(folderPath).sorted(Comparator.reverseOrder()).collect(Collectors.toList());
		for(Path path : pathsToDelete) {
			// Allows exceptions can be caught
			if (path != folderPath) {
				Files.deleteIfExists(path); // Do not delete the target folder
			}
		}
		String infoMsg = String.format("Cleared existing files in target folder '%s'.", folderPath.toString());
		log.info(infoMsg);
		resp.setMessage(Response.INFO, infoMsg);
	}

	@Override
	public void write(String fileName, String content, Response resp) {
		Path filePath = Paths.get(folderPath.toString(), String.format(fileNamePattern,fileName));
		resp.setFilesWritten(resp.getFilesWritten() + 1);
		try {
			boolean fileExists = Files.exists(filePath);
			if (fileExists && replaceFilesIfExist) {
				Files.write(filePath, content.getBytes(), StandardOpenOption.TRUNCATE_EXISTING, StandardOpenOption.CREATE);
				String infoMsg = String.format("Content of file '%s' has been replaced.", filePath.toString());
				log.info(infoMsg);
				resp.setMessage(Response.INFO, infoMsg);
			} else if (fileExists && !replaceFilesIfExist) {
				String warnMsg = String.format("The file '%s' exists. It has not been changed because the option 'replace file if exists' was not set to 'true'.", filePath.toString());
				log.warn(warnMsg);
				resp.setMessage(Response.WARN, warnMsg);
			} else {
				Files.write(filePath, content.getBytes(), StandardOpenOption.CREATE);
				String infoMsg = String.format("File '%s' has been created.", filePath.toString());
				log.info(infoMsg);
				resp.setMessage(Response.INFO, infoMsg);
			}
		} catch (IOException e) {
			String reason = String.format("I/O Error writing the file '%s'." , fileName); //, e.toString());
			log.error(reason, e);
			resp.setMessage(Response.ERROR, reason);
			resp.setFilesInError(resp.getFilesInError() + 1); //FIXME should be 'messages in error' etc
		} catch (Exception e) {
			// Catch any un-handled exception
			String reason = String.format("Unhandled error writing the output file '%s'.", fileName);
			log.error(reason, e);
			resp.setMessage(Response.ERROR, reason);
			resp.setFilesInError(resp.getFilesInError() + 1);
		}
	}

	@Override
	public void close() throws IOException {
		// TODO Auto-generated method stub		
	}

}
