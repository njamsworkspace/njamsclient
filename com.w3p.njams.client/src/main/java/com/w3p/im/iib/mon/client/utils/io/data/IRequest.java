package com.w3p.im.iib.mon.client.utils.io.data;

import java.util.Optional;

public interface IRequest {
	
	/**
	 * Data to be written or data read
	 * @param content
	 * @return
	 */	
	IRequest setContent(String content);
	
	/**
	 * @return
	 */
	String getContent();
	
	/**
	 * For writing - defines the file type using String.format - e.g %s.xml
	 * For reading - defines the search pattern - e.g *.xml
	 * @param fileNamePattern
	 * @return
	 */
	IRequest setNamePattern(String fileNamePattern);
	
	/**
	 * @return
	 */
	String getNamePattern();
	
	/**
	 * Controls the operation. 
	 * Predefined keys are public constants. 
	 * @param key
	 * @param value
	 * @return
	 */
	IRequest addOption(String key, Object value);
	
	/**
	 * Retrieve a specific option.
	 * All values stored as an Object - so have to cast.
	 * @param key
	 * @return
	 */
	Optional<Object> getOption(String key);
}