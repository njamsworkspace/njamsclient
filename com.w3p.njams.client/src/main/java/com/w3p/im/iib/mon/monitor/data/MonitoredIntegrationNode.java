package com.w3p.im.iib.mon.monitor.data;

public class MonitoredIntegrationNode extends MonitoredObject<MonitoredObject<?>> {
	
	public MonitoredIntegrationNode(String name) {
		super(name);
	}
	
	@Override
	public String toString() {
		return "Monitored Integration Node [name=" + name +  "]";
	}
}
