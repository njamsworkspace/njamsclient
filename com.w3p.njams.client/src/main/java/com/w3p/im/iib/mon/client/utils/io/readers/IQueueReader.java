package com.w3p.im.iib.mon.client.utils.io.readers;

import com.w3p.im.iib.mon.client.utils.io.data.IReadQueueRequest;
import com.w3p.im.iib.mon.client.utils.io.data.Response;

public interface IQueueReader extends IReader {

	void init(IReadQueueRequest req, Response resp);

	IContent readAll(IReadQueueRequest req, Response resp);

}