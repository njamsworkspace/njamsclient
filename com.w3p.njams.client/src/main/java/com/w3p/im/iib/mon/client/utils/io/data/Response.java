package com.w3p.im.iib.mon.client.utils.io.data;

import java.util.Optional;

import com.google.common.collect.LinkedHashMultimap;

public class Response {
	
	public static final String INFO = "info";
	public static final String WARN = "warn";
	public static final String ERROR = "error";

	// Map that allows duplicate keys - from Google's Guave. See: https://www.baeldung.com/java-map-duplicate-keys
	private LinkedHashMultimap<String, String> messages = LinkedHashMultimap.create();;

	private String status;
	
	private int filesToBeRead;
	private int filesRead;
	private int filesWritten;
	private int filesDeleted;
	private int filesInError;;
	
	public Response() {
		status = INFO;
	}
	
	/**
	 * Intended only for JUnit
	 * @return
	 */
	protected LinkedHashMultimap<String, String> getMessages() {
		return messages;
	}

	/**
	 * @param key - must be INFO, WARN, ERROR
	 * @param message
	 */
	public void setMessage(String key, String message) {
		getMessages().put(key, message);
		if (key.equals(INFO)) {
			status = (status.equals(WARN) || status.equals(ERROR)) ? status : INFO;
		} else if (key.equals(WARN)) {
			status = status.equals(ERROR) ? status : WARN;
		} else {
			status = ERROR;
		}		
	}

	public boolean isSuccess() {
		return getStatus().equals(INFO);
	}
//	private void setSuccess(boolean success) {
//		this.success = success;
//	}
	public String getStatus() {
		return status;
	}

	public Optional<String> getAllMessages() {
		StringBuilder sb = new StringBuilder();
		buildMessages(sb, getAllMessagesFor(INFO));
		buildMessages(sb, getAllMessagesFor(WARN));
		buildMessages(sb, getAllMessagesFor(ERROR));
		return sb.length() == 0 ? Optional.empty() : Optional.of(sb.toString());
	}

	private void buildMessages(StringBuilder sb, Optional<String> messages) {
		if (messages.isPresent()) {
			sb.append(messages.get());
		}
	}
	public Optional<String> getAllMessagesFor(String status) {
		StringBuilder sb = new StringBuilder();
		for (String message : messages.get(status)) {
			sb.append(status.toUpperCase()).append(": ").append(message).append("\r\n");
		}
		return sb.length() == 0 ? Optional.empty() : Optional.of(sb.toString());
	}
	public int getFilesToBeRead() {
		return filesToBeRead;
	}

	public void setFilesToBeRead(int filesToBeRead) {
		this.filesToBeRead = filesToBeRead;
	}

	public int getFilesRead() {
		return filesRead;
	}

	public void setFilesRead(int filesRead) {
		this.filesRead = filesRead;
	}

	public int getFilesWritten() {
		return filesWritten;
	}

	public void setFilesWritten(int filesWritten) {
		this.filesWritten = filesWritten;
	}

	public int getFilesDeleted() {
		return filesDeleted;
	}

	public void setFilesDeleted(int filesDeleted) {
		this.filesDeleted = filesDeleted;
	}

	public int getFilesInError() {
		return filesInError;
	}

	public void setFilesInError(int filesInError) {
		this.filesInError = filesInError;
	}

	@Override
	public String toString() {
		return String.format("Messages = %s", messages);
	}
}
