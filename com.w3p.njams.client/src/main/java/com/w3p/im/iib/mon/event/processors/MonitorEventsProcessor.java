package com.w3p.im.iib.mon.event.processors;

import static com.w3p.im.iib.mon.client.constants.IClientConstants.DOT;
import static com.w3p.im.iib.mon.client.constants.IClientConstants.END_ACTIVITY_MODEL_ID;
import static com.w3p.im.iib.mon.client.constants.IClientConstants.GROUP_END_MODEL_ID;
import static com.w3p.im.iib.mon.client.constants.IClientConstants.GROUP_TYPE;
import static com.w3p.im.iib.mon.client.constants.IClientConstants.LABEL_NODE;
import static com.w3p.im.iib.mon.client.constants.IClientConstants.REGEX_DOT;
import static com.w3p.im.iib.mon.client.constants.IClientConstants.START_ACTIVITY_MODEL_ID;
import static com.w3p.im.iib.mon.client.constants.IClientConstants.START_ACTIVITY_NAME;
import static com.w3p.im.iib.mon.client.constants.IClientConstants.START_ACTIVITY_TYPE;
import static com.w3p.im.iib.mon.client.constants.IClientConstants.SUB_FLOW_NODE;
import static com.w3p.im.iib.mon.client.constants.IClientConstants.UTF_8;

import java.nio.charset.Charset;
import java.time.Duration;
import java.time.LocalDateTime;
import java.util.Date;
import java.util.Stack;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.exception.ExceptionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.faizsiegeln.njams.messageformat.v4.logmessage.ActivityStatus;
import com.im.njams.sdk.Njams;
import com.im.njams.sdk.common.Path;
import com.im.njams.sdk.logmessage.Activity;
import com.im.njams.sdk.logmessage.Event;
import com.im.njams.sdk.logmessage.EventStatus;
import com.im.njams.sdk.logmessage.Group;
import com.im.njams.sdk.logmessage.GroupImpl;
import com.im.njams.sdk.logmessage.Job;
import com.im.njams.sdk.logmessage.JobStatus;
import com.im.njams.sdk.logmessage.SubProcessActivity;
import com.im.njams.sdk.model.ActivityModel;
import com.im.njams.sdk.model.GroupModel;
import com.im.njams.sdk.model.ProcessModel;
import com.im.njams.sdk.model.SubProcessActivityModel;
import com.w3p.im.iib.mon.jms.data.IMessageProducer;
import com.w3p.im.iib.mon.jms.producers.FlowToProcessModelCache;
import com.w3p.im.iib.mon.monitor.data.ErrorMonitoringEvent;
import com.w3p.im.iib.mon.monitor.data.GroupMonitoringEvent;
import com.w3p.im.iib.mon.monitor.data.MonitorEventsRequest;
import com.w3p.im.iib.mon.monitor.data.MonitoringEvent;
//import com.w3p.im.iib.mon.serializer.Base64Serializer;
import com.w3p.im.iib.mon.monitor.data.SubflowMonitoringEvent;
import com.w3p.im.iib.mon.monitor.data.TxnEndMonitoringEvent;
import com.w3p.im.iib.mon.monitor.data.TxnStartMonitoringEvent;

import jakarta.xml.bind.DatatypeConverter;

public class MonitorEventsProcessor {

	private static final Logger logger = LoggerFactory.getLogger(MonitorEventsProcessor.class);

	private final MonitorEventsRequest request;
	private final Charset UTF8_CHARSET = Charset.forName(UTF_8);
//	private final FlowToProcessModelCache flowToProcessModelCacheTopology;
	private final FlowToProcessModelCacheRun flowToProcessModelCache; // = new FlowToProcessModelCacheRun();
	private final Njams njamsClient;
	
//	private IMessagingServiceProducer messagingServiceProducer;
	private IMessageProducer monitorEventsErrorsProducer;

	private JobStatus jobStatus;
	private boolean catchHasBeenHandled;	
	private Stack<MonitoringState> stateStack = new Stack<>(); // We need to hold the items that vary by Process Model, in a stack

	private	long msgsProcessStart = 0L;
	private long msgsProcessEnd = 0L;


	public MonitorEventsProcessor(Njams njamsClient, FlowToProcessModelCache flowToProcessModelCacheTopology, MonitorEventsRequest request) {
		this.request = request;
		this.njamsClient = njamsClient;
//		this.flowToProcessModelCacheTopology = flowToProcessModelCacheTopology;
		this.flowToProcessModelCache = flowToProcessModelCacheTopology.getFlowToProcessModelCacheRun();
	}


	
	public void setMonitorEventsErrorsProducer(IMessageProducer monitorEventsErrorsProducer) {
		this.monitorEventsErrorsProducer = monitorEventsErrorsProducer;
	}



	public void sendLogMessageToNjams(MessageEvents messageEvents) {
		// Use try/catch to debug any NPEs from nJAMS
		msgsProcessStart = new Date().getTime();
		try {
			if (monitorEventsErrorsProducer != null) { 
				monitorEventsErrorsProducer.createBytesMessage("rhubarb"); // FIXME - TEMP to validate access.}		}
			}
			
			LocalDateTime endTime = null;
			catchHasBeenHandled = false;
			TxnStartMonitoringEvent firstEvent = messageEvents.getFirstEvent();
			Job job = processFirstEvent(messageEvents, firstEvent);

			// Now begin the loop through the published events - from MessageFlows and SubFlows
			for(MonitoringEvent currentEvent : messageEvents.getEvents()) {
				MonitoringState ms = stateStack.peek(); // This is the state at the top of the stack at this point.
				currentEvent.setProcessModel(currentEvent.isFromSubFlow() ? 
						flowToProcessModelCache.findProcessModel(currentEvent.getSubflowName(), currentEvent.getFlowParentName()) : 
							flowToProcessModelCache.findProcessModel(currentEvent.getBaseMessageFlow()));
				if (ms.getPreviousActivity() == null) {
					endTime = reportPreviousActivityIsNull(currentEvent, ms);
				} else {
					if (currentEvent.isSuccessorToPreviousEvent(ms.getPreviousEvent())) {
						processCurrentEventIsSuccessorToPrevious(currentEvent, ms);
					} else if (currentEvent instanceof TxnEndMonitoringEvent) {
						endTime = processEndEvent(currentEvent, ms);
					} else if (currentEvent instanceof ErrorMonitoringEvent) {
						handleErrorEvent(ms, (ErrorMonitoringEvent)currentEvent);
					} else if (!ms.getProcessModelName().equals(currentEvent.getProcessModelName()))  {
						// The current Process Definition is different from the previous one: we have entered or returned from a Subflow (or gone from one subflow directly to another)
						processCurrentEventIsFromDifferentProcessModel(ms, currentEvent);
					} else if (currentEvent.getNodeLabel().equals(ms.getPreviousEvent().getNodeLabel())) {
						// This should be where we have events from the In and Out terminals of an MQ node - extract monitoring data from the Out terminal.
						// Do nothing - values form the current event have already been copied to the previous event						
					} else {
						// We have an event that is not the target of the previous event.
						processCurrentEventNotSuccessorToPrevious(ms, currentEvent);
					}
				}
			}

			if (job != null) {
				job.setEndTime(endTime);
				job.setStatus(jobStatus);
				if (jobStatus == JobStatus.SUCCESS) {
					job.end(true);
				} else {
					job.end(false);
				}
			}
		} catch (Exception e) {
			MonitoringState ms = stateStack.peek();
			MonitoringEvent previousEvent = ms.getPreviousEvent();
			
			e.printStackTrace();
			Throwable cause = null;
			if (e.getCause() != null) {
				cause = e.getCause();
				cause.printStackTrace();
			}
			System.out.println(String.format("Exception thrown while processing monitoring events. Exception text: %s, \n message flow: %s, previous node: %s, localTxnId: %s",
					e.toString(), previousEvent.getBaseMessageFlow(), previousEvent.getNodeLabel(), previousEvent.getLocalTransactionId()));
			if (cause!= null) {
				System.out.println("   Cause is: "+cause.toString());
			}
			logger.error("Exception thrown while processing monitoring events. Exception text: {}. \n message flow: {}, previous node: {} ", 
					e.toString(),  previousEvent.getBaseMessageFlow(), previousEvent.getNodeLabel(), e);
			if (cause!= null) {
				logger.error("   Cause is: "+cause.toString(), cause);
			}
			
			/*
			 * Write out the event message set to the 'errors' queue
			 * 
			 *  1. Get hold of the
			 */
			
			
			
			Job job = ms.getJob();
			String logId = " Error Id: "+String.valueOf(new Date().getTime())+"\n";				
			String stackTrace = ExceptionUtils.getStackTrace(e);
			Activity previousActivity = ms.getPreviousActivity(); // Can return null => job.getActivityByModelId(previousEvent.getLocalNodeLabelAsId());
			Event exception = previousActivity.createEvent(); // .setsetMessage("").setPayload(""); // 
			
			try {
				exception.setMessage("Internal error: exception thrown by Client (version = "+"version X"+
						") after this Activity. Has been logged in the Client with 'Error Id' = "+logId+DOT)
				.setCode("97")
				.setPayload(previousEvent.getBodyIn())
				.setStacktrace(stackTrace)
				.setStatus(EventStatus.ERROR);
			} catch (Exception ex) {
				// Can't do much about this - ignore it for now?
				logger.error("Exception '{}' thrown when handling exception '{}' while processing monitoring events. Can't do more than log it", ex.toString(), e.toString(), ex);
			}
			
			job.setStatus(JobStatus.ERROR);
			job.end(false); //jobStatus);
		} finally {	
			// Ensure no memory leaks from each event-set
//			messageEvents.clearEvents();
			while (stateStack.size() > 0 ) {
				stateStack.peek().clear();
				stateStack.removeElementAt(stateStack.size() - 1);
			}
			stateStack.clear();
		}
	}

	private Job processFirstEvent(MessageEvents messageEvents, TxnStartMonitoringEvent firstEvent) {
		MonitoringState ms = initJob(firstEvent, messageEvents);
		Job job = ms.getJob();
		job.setCorrelationLogId(firstEvent.getGlobalTransactionId()); // If Global Txn Id has been set from data in payload or envt

		Activity startActivity = job.createActivity(ms.getStartActivityModel()).build();
		MonitoringEvent startEvent = createStartEvent(ms.getProcessModel(), firstEvent); 
		ms.addCurrent(startActivity, startEvent);
		Activity currentActivity = null;
		if (firstEvent.isFromSubFlow()) {
			currentActivity = handleTransitionToSubflow(ms, startActivity, startEvent, firstEvent);			
		} else {
			currentActivity = stepToNextActivity(startActivity, startEvent, firstEvent);
			ms.addCurrent(currentActivity, firstEvent);
		}
		try {
			messageEvents.removeEvent(firstEvent); // FIXME need to preserve this for when there is an error
		} catch (Exception e) {
			System.err.println("exception thrown when removing the first event from MessageEvents: "+e.toString());
			e.printStackTrace();			
		}
		
		
		return job;
	}

	protected MonitoringState initJob(TxnStartMonitoringEvent firstEvent, MessageEvents messageEvents) {
		// Note: If first event is from a subflow, njams requires the initial Process Model to be for the Message flow
		setProcessModelForFirstEvent(firstEvent);
		MonitoringState ms = stateStack.peek();

		Job job = ms.getProcessModel().createJob();
		jobStatus = JobStatus.SUCCESS;
		job.setStartTime(firstEvent.getCreationTime()); 
		job.start(); // The Start Activity created, can be accessed using job.getCurrentActivity
		logger.debug("#sendEventToNjams() - creating Job for ProcessModel: '{}'", firstEvent.getProcessModelName());
		ms.setJob(job);
		return ms;
	}

	private MonitoringEvent createStartEvent(ProcessModel startPM, TxnStartMonitoringEvent currentEvent) {
			// This can only be a close approximation to what would have been emitted if Subflow nodes generated monitoring events.
			MonitoringEvent startEvent = new  MonitoringEvent();
			startEvent.setBodyIn(currentEvent.getBodyIn());
			startEvent.setBodyOut(currentEvent.getBodyOut());
			startEvent.setCounter(0); // This isn't used after the events have been assembled.
			startEvent.setCreationTime(currentEvent.getCreationTime()); // We have the actual creation time of first event in subflow

			startEvent.setIntSvrName(currentEvent.getIntSvrName());
			startEvent.setGlobalTransactionId(currentEvent.getGlobalTransactionId());
			startEvent.setIncludeIntSvrName(currentEvent.isIncludeIntSvrName());
			startEvent.setIncludeSchemaName(currentEvent.isIncludeSchemaName());
			startEvent.setLocalTransactionId(currentEvent.getLocalTransactionId());
			startEvent.setMessageFlow(currentEvent.getMessageFlow());
	
			startEvent.setEventName(START_ACTIVITY_NAME+DOT+MessageEvents.IN_TERMINAL);
			// If this is being created by a subflow: nodeLabel = {subflowA}.{subflowB}.{node-name} ==> {subflowA}.{subflowB}.sfName
			startEvent.setNodeLabel(START_ACTIVITY_NAME);
	
			startEvent.setNodeType(START_ACTIVITY_TYPE);
			startEvent.setUniqueflowName(currentEvent.getUniqueflowName());
			startEvent.setProcessModel(startPM); // was ms.
			return startEvent;
		}
	
	private MonitoringEvent createSubProcessStartEvent(ProcessModel pm, String subflowName, SubflowMonitoringEvent subflowEvent, MonitoringEvent currentEvent) {
		// This can only be a close approximation to what would have been emitted if Subflow nodes generated monitoring events.
		MonitoringEvent startEvent = new  MonitoringEvent();
		startEvent.setBodyIn(subflowEvent.getBodyOut());
		startEvent.setBodyOut(currentEvent.getBodyIn());
		startEvent.setCounter(0); // This isn't used after the events have been assembled.
		startEvent.setCreationTime(subflowEvent.getCreationTime());
		startEvent.setIntSvrName(subflowEvent.getIntSvrName());
		startEvent.setMessageFlow(subflowEvent.getMessageFlow());

		startEvent.setEventName(START_ACTIVITY_NAME+DOT+MessageEvents.IN_TERMINAL);
		// If this is being created by a subflow: nodeLabel = {subflowA}.{subflowB}.{node-name} ==> {subflowA}.{subflowB}.sfName => sets subflowName
		if (stateStack.size() == 1) {
			if (subflowEvent.isFromMsgFlow()) {
				startEvent.setNodeLabel(subflowName+DOT+START_ACTIVITY_NAME);
			} else if (currentEvent.isFromSubFlow()) {
				startEvent.setNodeLabel(StringUtils.substringBefore(currentEvent.getNodeLabel(), DOT+currentEvent.getLocalNodeLabel() +DOT+START_ACTIVITY_NAME));
			}
		} else if (currentEvent.getNodeLabel().contains(subflowName)) {
			if (currentEvent.getNodeLabel().endsWith(currentEvent.getSubflowName()+DOT+currentEvent.getLocalNodeLabel())) { // parentSF.thisSF.nodeName 'endsWith' thisSF +'.'+ nodeName
				// Subflow is next one down
				String sfParents = StringUtils.substringBefore(currentEvent.getNodeLabel(), subflowName); // SF_ParentA.SF_ParentB.subflowName.SF_Child.nodeName => SF_ParentA.SF_ParentB.
				String realNodeLabel = sfParents+subflowName+DOT+START_ACTIVITY_NAME;
				// Need to remove the last 2 .String i.e .SF_next.NodeName
				startEvent.setNodeLabel(realNodeLabel);
			} else {
				// Subflow is 2 or more levels down
				int flowLevel = currentEvent.getFlowLevel();
				String nameOfSubflowLevel = currentEvent.getNodeLabel().split(REGEX_DOT)[flowLevel-1 - stateStack.size()];
				startEvent.setNodeLabel(nameOfSubflowLevel+ DOT+START_ACTIVITY_NAME);
			}
		} else {
			logger.error("*** #createSubflowEvent() for subflow '{}': not expecting to end up here - check it out", subflowName);
			startEvent.setNodeLabel(StringUtils.substringBefore(currentEvent.getNodeLabel(), DOT+currentEvent.getLocalNodeLabel()));
		}

		startEvent.setNodeType(SUB_FLOW_NODE);
		startEvent.setUniqueflowName(subflowEvent.getUniqueflowName());
		startEvent.setProcessModel(pm); //currentEvent.getProcessModel()); //ms.getProcessModel());  // From pm's name
		return startEvent;
	}
	

	private void setProcessModelForFirstEvent(TxnStartMonitoringEvent firstEvent) {		
		firstEvent.setProcessModel(firstEvent.isFromSubFlow() ? 
				flowToProcessModelCache.findProcessModel(firstEvent.getSubflowName(), firstEvent.getFlowParentName()) : 
					flowToProcessModelCache.findProcessModel(firstEvent.getBaseMessageFlow()));
		
		ProcessModel pm = flowToProcessModelCache.getProcessModelForFlow(firstEvent.getBaseMessageFlow()); // Returns the msgflow PM if forst event is frfom a subflow
		MonitoringState ms = new MonitoringState(pm); // Initialise the stateStack at msgflow level
		stateStack.add(ms);
	}

	private LocalDateTime reportPreviousActivityIsNull(MonitoringEvent currentEvent, MonitoringState ms) {
		LocalDateTime endTime;
		logger.error("** Previous activity is null.\n>>> Process model:'{}'.\n    Previous event:'{}'. Current event:'{}'. Abandoning reporting for this message",
				ms.getProcessModelName(), ms.getPreviousEvent().toString(), currentEvent.toString());
		jobStatus = JobStatus.ERROR;
		endTime = processEndEvent(currentEvent, ms);
		return endTime;
	}

	private void processStartOfGroup(MonitoringEvent currentEvent, MonitoringState ms) {
		/*
		 * This method handles:
		 * 	- entry to a Group
		 * 	- loop back to start of a Group
		 */
		Activity previousActivity = ms.getPreviousActivity();
		if (ms.isCurrentEventInSameProcessModelAsPrevious(currentEvent)) {
			// Current event is in same Process Model as previous
			processEntryOrIteration(ms, currentEvent, previousActivity);
		} else {
			// Current event is from a different Process Model
			handleReturnToParentFlow(ms, previousActivity,  ms.getPreviousEvent(), currentEvent);
			// Loop round the stack, making a 'transition-to-end' for each subflow level, until we get the process model for the current event. Process as normal.
			Activity previousSubflowActivity = null; 
			for (int i = stateStack.size()-1; i >= 0; i--) { // search up the stack from most recent to oldest 
				previousSubflowActivity = ms.getPreviousActivity();
				while (!ms.getProcessModel().equals(currentEvent.getProcessModel())) {
					handleReturnToParentFlow(ms, previousSubflowActivity,  ms.getPreviousEvent(), currentEvent);
					ms = stateStack.peek();
				}
			}
			processEntryOrIteration(ms, currentEvent, previousSubflowActivity);
		}
	}

	private void processEntryOrIteration(MonitoringState ms, MonitoringEvent currentEvent, Activity previousActivity) {
		// Is previous activity from this Group?
		if (previousActivity.getParent() != null && previousActivity.getParent().getClass().isAssignableFrom(GroupImpl.class)) { // Parent is GroupImpl
			// Previous activity was in a Group - was it the same Group?
			if (ms.getCurrentGroup().getModelId().equals(currentEvent.getGroupModel().getId())) {
				// This is an iteration within a Group
				handleIterationInGroup(ms, currentEvent);
			} else {
				// Previous activity was in a different Group
				handleEndOfGroup(ms, currentEvent); // exit from previousGroup
				handleEntryToGroup(ms, currentEvent);
			}				
		} else {
			// Current event is in same Process Model as previous event - && NOT iteration in same Group
			handleEntryToGroup(ms, currentEvent);
		}
	}

	private void handleEntryToGroup(MonitoringState ms, MonitoringEvent currentEvent) {
		// Create transition from Start to Group - #stepTo() not used when in a msgflow, because we need to add the Group to the processed activites and events
		Activity groupStartActivity = ms.getStartActivity();
		
		// Step to the next activity, which is a group  
		GroupModel groupModel = currentEvent.getGroupModel();
		Group group = groupStartActivity.stepToGroup(groupModel).build();
		ms.setCurrentGroup(group);
		// Step from group startto the Label node
		processFirstEventOfGroup(ms, currentEvent);
	}

	private void processFirstEventOfGroup(MonitoringState ms, MonitoringEvent currentEvent) {
		Group group = ms.getCurrentGroup();
		GroupModel groupModel = currentEvent.getGroupModel();		
		ActivityModel groupStartModel = groupModel.getChildActivities().get(0); // Must always be the first child activity
		
		Activity groupStartActivity = group.createChildActivity(groupStartModel).build();
		GroupMonitoringEvent groupStartEvent = createGroupEvent(ms, groupModel.getName(), ms.getPreviousEvent(), currentEvent);
		ms.addCurrent(groupStartActivity, groupStartEvent);		
		ActivityModel labelNodeActivityModel = currentEvent.getActivityModel();
		
		// Create transition from Group 'start' to Label node
		Activity labelNodeActivity = groupStartActivity.stepTo(labelNodeActivityModel).build();
		ms.addCurrent(labelNodeActivity, currentEvent);
	}

	private void handleIterationInGroup(MonitoringState ms, MonitoringEvent currentEvent) {
		handleEndOfGroup(ms, currentEvent);
		ms.getCurrentGroup().iterate();
		processFirstEventOfGroup(ms, currentEvent);
	}

	private void handleEndOfGroup(MonitoringState ms, MonitoringEvent currentEvent) {
		Activity previousActivity = ms.getPreviousActivity();
		MonitoringEvent previousEvent = ms.getPreviousEvent(); 
		ProcessModel pm = previousEvent.getProcessModel();
		
		if (ms.doesCurrentActivityInGroupHaveEndSuccessor(previousActivity)) {
			Activity groupEndActivity = previousActivity.stepTo(pm.getActivity(GROUP_END_MODEL_ID)).build();
			setActivityTimesAndPayloads(ms.getPreviousActivity(), ms.getPreviousEvent(), currentEvent);

			// Are we leaving the Group or iterating?
			if (currentEvent.getGroupModel() == null) {
				// We are - does the Group itself have an End activity? 
				String groupModelId = ms.getPreviousActivity().getParent().getModelId();				
				if (ms.doesGroupHaveEndSuccessor(groupModelId)) {
					// Make a transition from the Group to End
					ActivityModel endModel = pm.getActivity(END_ACTIVITY_MODEL_ID);
					Activity endActivity = groupEndActivity.getParent().stepTo(endModel).build();
					setActivityTimesAndPayloads(endActivity, previousEvent, currentEvent); // FIXME what should these be?				
				}
			}
		} else {
			// Just set the elapsed time and payloads
			setActivityTimesAndPayloads(previousActivity, previousEvent, currentEvent);
		}
	}

	private Activity createActivity(MonitoringState ms, MonitoringEvent nodeEvent) { //, String nodeLabel) {
		ProcessModel pm = ms.getProcessModel();
		String nodeLabelId = nodeEvent.getLocalNodeLabelAsId();
		ActivityModel activityModel = pm.getActivity(nodeLabelId);
		Job job = ms.getJob();
		Activity currentActivity = job.createActivity(activityModel).build();
		return currentActivity;
		
//		while (activityModel == null) { // NOTE! this can be an endless loop!
//			for (GroupModel gm : pm.getGroupModels()) {
//				if (nodeEvent.getNodeType().equals(GROUP_TYPE)) {
//					if (gm.getName().equals(nodeLabel)) {
//						activityModel = gm;
//						break;
//					}
//				} else {
//					for (ActivityModel activity : gm.getChildActivities()) {
//						if (activity.getName().equals(nodeLabel)) {
//							activityModel = activity;
//							break;
//						}
//					}
//				}
//			}
//		}
	}

	private void processCurrentEventNotSuccessorToPrevious(MonitoringState ms, MonitoringEvent currentEvent) {
		// The two events are in the same Process Model
		// The event is from: a target of a node with multiple targets, or  event is a Label node, or event is first one after leaving (= completing?) a  Group
		Activity predecessorActivity = ms.getCompletedPredecessorActivityForCurrentEvent(currentEvent);
		MonitoringEvent predecessorEvent = ms.getCompletedPredecessorEventForCurrentEvent(predecessorActivity);
	
		if (isCurrentEventAtStartOfGroup(currentEvent)) {
			processStartOfGroup(currentEvent, ms);
		}
		
		// Find predecessor
		else if (currentEvent.isSuccessorToPreviousEvent(predecessorEvent)) {
			Group parent = ms.getPreviousActivity().getParent();  // Can be Group or SubPrtocessActivity (which is a subclass of Group... unfortunately
			if (parent != null &&  parent.getClass().isAssignableFrom(Group.class)) {
				handleEndOfGroup(ms, currentEvent); // End the Group
			}
			// Now create the transition to the currentEvent
			Activity currentActivity = stepToNextActivity(predecessorActivity, predecessorEvent, currentEvent);
			ms.addCurrent(currentActivity, currentEvent);				
		} else {				 
			logger.error("#processCurrentEventNotSuccessorToPrevious: unable to handle the situation. Previous '{}', Current '{}', Predecessor: '{}'. ",
					ms.getPreviousEvent().getLocalNodeLabel(), currentEvent.getLocalNodeLabel(), predecessorEvent.getLocalNodeLabel());
			throw new RuntimeException ("#processCurrentEventNotSuccessorToPrevious: unable to handle the situation.");
		}
	}

	private void processCurrentEventIsFromDifferentProcessModel(MonitoringState ms, MonitoringEvent currentEvent) {
		/*
		 * Case 1: currentEvent is from a (immediate?) subflow
		 * Case 2: currentEvent is from a parent flow
		 * Case 3: current event is from a node in a subflow the is a child flow of the immediate parent flow (if that makes sense!)  
		 */
		Activity previousActivity = ms.getPreviousActivity();
		MonitoringEvent previousEvent = ms.getPreviousEvent();

		// Check if current event is an immediate child of previous event, if not then go down the subflow levels one at q time	
		if (currentEvent.isChildOfSubflow(previousEvent)) {  // Only tests for being from an immediate child subflow
			// Case 1: we are handling transition from a flow to a node in a child subflow 
			handleTransitionToSubflow(ms, previousActivity, previousEvent, currentEvent);
		} else if (previousEvent.isChildOfSubflow(currentEvent)){
			// Case 2: we are handling return from a subflow to a node in a parent subflow
			handleReturnToParentFlow(ms, previousActivity, previousEvent, currentEvent); // NOTE: the previousevent could be down > 1 subflow level
			ms = stateStack.peek();
			while (ms.getProcessModel() != currentEvent.getProcessModel()) {
				// Leave this subflow
				handleReturnToParentFlow(ms, ms.getPreviousActivity(), ms.getPreviousEvent(), currentEvent);
				ms = stateStack.peek();
			}
			handleTransitionToNextNode(ms, currentEvent); 
		} else {
			// Case 3: Find the path to current event
			handleTransitionToSubflowInNextSubflowNode(ms, previousActivity, previousEvent, currentEvent);
		}
	}

	private void handleReturnToParentFlow(MonitoringState ms, Activity previousActivity, MonitoringEvent previousEvent, MonitoringEvent currentEvent) {
		if (ms.doesActivityHaveEndSuccessor(previousActivity)) { // If true - this is last node in a subflow
			stepToEndOfSubflow(ms, previousActivity, previousEvent, currentEvent); // returns Activity endActivity - not used
		}		
		// Subflow complete, restore previous State 
		restoreAfterSubflow(ms, currentEvent); //This will be SubProcessActivity that 'called' the subflow
	}

	/**
	 * The currentevent is from a subflow somewhere in the subflow tree of the next node, which must be a subflow node
	 * 
	 * @param ms
	 * @param previousActivity
	 * @param previousEvent
	 * @param currentEvent
	 * @return
	 */
	private Activity handleTransitionToSubflowInNextSubflowNode(MonitoringState ms, Activity previousActivity, MonitoringEvent previousEvent, MonitoringEvent currentEvent) {
		// TODO Group processing not taken into account.
		Activity currentActivity = null;
		if (isCurrentEventFromChildFlow(ms, previousEvent, currentEvent)) { // This searches processed activities
			throw new RuntimeException("We should never arrive here: #handleTransitionToSubflowInNextSubflowNode() #isCurrentEventFromChildFlow()"); // Go down to a child subflow
//			currentActivity = handleTransitionToSubflow(ms, previousActivity, previousEvent, currentEvent);
		} else {
			// Go up one-level 
			handleReturnToParentFlow(ms, previousActivity, previousEvent, currentEvent);
			ms = stateStack.peek();
			SubProcessActivity subProcessActivity = (SubProcessActivity)ms.getLastProcessedActivity();
			SubflowMonitoringEvent subflowEvent = (SubflowMonitoringEvent)ms.getLastProcessedEvent();
			// Is subProcessActivity in same Proc Model as currentActivity?
			if (subProcessActivity.getModelId().equals(currentEvent.getProcessModelNameAsId())) {
				currentActivity = stepToNextActivity(subProcessActivity, subflowEvent, currentEvent); // currentEvent is a node in this flow
				ms.addCurrent(currentActivity, currentEvent);
			} else {
				currentActivity = handleTransitionToSubflow(ms, subProcessActivity, subflowEvent, currentEvent); // currentEvent is (in) a subflow
			}
		}
		return currentActivity;
	}

	private void handleErrorEvent(MonitoringState ms, ErrorMonitoringEvent currentEvent) throws Exception {
		if (currentEvent.getEventName().endsWith(MessageEvents.CATCH_TERMINAL)) {
			reportCatchEvent(ms, currentEvent);
			catchHasBeenHandled = true;
		} else if (currentEvent.getEventName().endsWith(MessageEvents.TRANSACTION_ROLLBACK)) {					
			reportRollbackEvent(ms, currentEvent);
		} else if (currentEvent.getEventName().endsWith(MessageEvents.FAULT_TERMINAL)) {
			reportFaultEvent(ms, currentEvent);
		} else if (currentEvent.getEventName().endsWith(MessageEvents.FAILURE_TERMINAL)) {
			reportFailureEvent(ms, currentEvent);
		}
		processErrorOrRollbackEvent(ms, currentEvent); // A Compute Node can send failures from its Failure terminal, and is acting like a Try/Catch node
		jobStatus = JobStatus.ERROR;
	}

	private void processErrorOrRollbackEvent(MonitoringState ms, MonitoringEvent currentEvent) {
		Activity previousActivity = ms.getPreviousActivity();
		MonitoringEvent previousEvent = ms.getPreviousEvent();
		setActivityTimesAndPayloads(previousActivity, previousEvent, currentEvent);
		/*
		 * Case 0: The current event is from the node with Catch/Rollback terminal.
		 * Case 1: Current event and the node that issued the catch/rollback are in the same Msg/Sub flow.         
		 * Case 2: Current event (from the node that issued the catch/rollback) is in a parent flow (relative to current event). 
		 *         We need to pop the subflow tree to return to the State containing the current Process Model.
		 * Case 3: Current event (catch/rollback) is in a child flow (relative to the previous event).
		 *         We have to find the Process Model for the current event, create a MonitoringState for it and push to the stack.
		 * Case 4: Both events are from sibling subflows (Case 2, then Case 3):
		 * 		   Pop the subflow tree to return to the msgflow
		 * 		   Find the Process Model for the current event, create a MonitoringState for it and push to the stack.
		 * Find the Activity for the current event and make it the current activity. 
		 * Now any further events from the target node of the catch connection can be processed.          
		 */	
	
		// Case 0 - current and previous events from same node - but different terminals
		if (currentEvent.getLocalNodeLabel().equals(previousEvent.getLocalNodeLabel())) {
				Activity  errorActivity = createActivity(ms, currentEvent); //, currentEvent.getLocalNodeLabel());
				errorActivity.setActivityStatus(ActivityStatus.ERROR);
				ms.addCurrent(errorActivity, currentEvent);
		} else if (ms.getProcessModel().equals(currentEvent.getProcessModel()))  {
			// Case 1
			Activity  catchActivity = createActivity(ms, currentEvent);
			catchActivity.setActivityStatus(ActivityStatus.ERROR);
			ms.addCurrent(catchActivity, currentEvent);
		} else if (previousEvent.isFromSubFlow() && currentEvent.isFromSubFlow()) {						
			if (currentEvent.getProcessModel().equals(previousEvent.getProcessModel())) {
				
			} else if (isCurrentEventFromParentFlow(ms, currentEvent)) {
				// Case 2
				while(!ms.getProcessModelName().equals(currentEvent.getProcessModelName())) {
					ms = restorePreviousState(stateStack);  // This 'pops' the stack. 
				}							
				Activity catchActivity =  ms.getProcessedActivity(currentEvent.getLocalNodeLabelAsId());
				ms.addCurrent(catchActivity, currentEvent);
			} else if (isCurrentEventFromChildFlow(ms, previousEvent, currentEvent)) {
				// Case 3
				// Create an MS for the current event's Process Model and make current
				Path pmPath = flowToProcessModelCache.getPathForFlow(currentEvent.getProcessModelName());
				ProcessModel pmSubflowProcessModel = njamsClient.getProcessModels().stream()
						.filter(pm -> pm.getPath().equals(pmPath)).findFirst().get();				
				MonitoringState msSubflow = createMonitoringStateForSubflow(ms, pmSubflowProcessModel);
				ms =  stateStack.push(msSubflow);
				// Find Activity with name = current event's local node name
				Activity catchActivity = ms.getProcessModelActivity(currentEvent.getLocalNodeLabel());
				ms.addCurrent(catchActivity, currentEvent);
			} else {
				// Case 4
				while(!ms.getMsgflowProcessModelName().equals(ms.getProcessModelName())) {
					ms = restorePreviousState(stateStack);  // This 'pops' the stack. 
				}
				// Copied from Case 3
				Path pmPath = flowToProcessModelCache.getPathForFlow(currentEvent.getProcessModelName());
				ProcessModel pmSubflowProcessModel = njamsClient.getProcessModels().stream()
						.filter(pm -> pm.getPath().equals(pmPath)).findFirst().get();		
				MonitoringState msSubflow = createMonitoringStateForSubflow(ms, pmSubflowProcessModel);
				ms =  stateStack.push(msSubflow);
				// Find Activity with name = current event's local node name
				Activity catchActivity = ms.getProcessModelActivity(currentEvent.getLocalNodeLabel());
				ms.addCurrent(catchActivity, currentEvent);
			}
		} else if (isCurrentEventFromParentFlow(ms, currentEvent)) {
			// Case 2
			while(!ms.getProcessModelName().equals(currentEvent.getProcessModelName())) {
				restoreAfterSubflow(ms, currentEvent);
				ms = stateStack.peek();
			}							
			Activity catchActivity =  ms.getProcessedActivity(currentEvent.getLocalNodeLabel());
			//TODO Test for catch activity = null
			ms.addPrevious(catchActivity, currentEvent);
		} else {
			// Case 3
			// Create an MS for the current event's Process Model and make current
			Path pmPath = flowToProcessModelCache.getPathForFlow(currentEvent.getProcessModelName());
			ProcessModel pmSubflowProcessModel = njamsClient.getProcessModels().stream()
					.filter(pm -> pm.getPath().equals(pmPath)).findFirst().get();
			MonitoringState msSubflow = createMonitoringStateForSubflow(ms, pmSubflowProcessModel);
			ms =  stateStack.push(msSubflow);
			// Find Activity with name = current event's local node name
			Activity catchActivity = ms.getProcessModelActivity(currentEvent.getLocalNodeLabel());
			ms.addCurrent(catchActivity, currentEvent); //TODO This line was in Case 4 (part 2), but not here?
		}
	}

	private LocalDateTime processEndEvent(MonitoringEvent currentEvent, MonitoringState ms) {
		LocalDateTime startTime = ms.getJob().getStartTime();
		LocalDateTime endTime = currentEvent.getCreationTime(); 
		logger.trace("+++ End of processing for this Msg Flow. Name: ''{}'; Start time: {}; End time: {}; Elapsed (msec): {}.", 
				currentEvent.getMessageFlow(), startTime, endTime, Duration.between(startTime, endTime).toMillis());
		msgsProcessEnd = new Date().getTime(); // TODO check this is updated - set here tpo avoid a null vaalue
		logger.trace("+++ Time to process the Events in msec: {}", (msgsProcessEnd - msgsProcessStart));
		Activity previousActivity = ms.getPreviousActivity();			
		MonitoringEvent previousEvent = ms.getPreviousEvent();	
		while (stateStack.size() > 1 ) {
			// Current event must have come from a Subflow
			previousActivity = ms.getLastProcessedActivity();
			previousEvent = ms.getLastProcessedEvent();
			if (!ms.getProcessedActivityModelIds().contains(END_ACTIVITY_MODEL_ID)) {
				handleEndOfEndEvent(ms, previousActivity, previousEvent, currentEvent);
				restoreAfterSubflow(ms, currentEvent); // returns the SubProcessActivity that 'called' the subflow - not used
			} else {
				stateStack.pop();
			}
			ms = stateStack.peek();
			if (stateStack.size() == 1) {
				// We have bubbled up to the Msgflow
				previousActivity = ms.getLastProcessedActivity();
				previousEvent = ms.getLastProcessedEvent();
			}
		}

		handleEndOfEndEvent(ms, previousActivity, previousEvent, currentEvent);

		return endTime;
	}

	private void handleEndOfEndEvent(MonitoringState ms, Activity previousActivity, MonitoringEvent previousEvent, MonitoringEvent currentEvent) {
		if (previousActivity.getParent() != null) {
			if (previousActivity.getParent().getClass().isAssignableFrom(Group.class)) {
				// We are in a Group - must transition to GroupEnd
				Group parentGroup = (Group)previousActivity.getParent();
				handleEndOfGroup(ms,currentEvent); 
				stepToEndOfSubflow(ms, parentGroup, previousEvent, currentEvent); 
			} else if (previousActivity.getParent().getClass().isAssignableFrom(SubProcessActivity.class) || 
					previousActivity.getParent() instanceof SubProcessActivity) {
				stepToEndOfSubflow(ms, previousActivity, previousEvent, currentEvent);
			}
		} else if (ms.doesActivityHaveEndSuccessor(previousActivity)) { 
			stepToEndOfMsgflow(previousActivity, previousEvent, currentEvent);
		}
	}

	private void stepToEndOfMsgflow(Activity previousActivity, MonitoringEvent previousEvent, MonitoringEvent currentEvent) {
		ProcessModel pm = previousEvent.getProcessModel();
		ActivityModel endActivityModel = pm.getActivity(END_ACTIVITY_MODEL_ID);
		previousActivity.stepTo(endActivityModel).build();
		setActivityTimesAndPayloads(previousActivity, previousEvent, currentEvent);
	}

	private Activity processCurrentEventIsSuccessorToPrevious(MonitoringEvent currentEvent, MonitoringState ms) {
		Activity previousActivity = ms.getPreviousActivity();			
		MonitoringEvent previousEvent = ms.getPreviousEvent();	
		Activity currentActivity = stepToNextActivity(previousActivity, previousEvent, currentEvent);
		if (currentActivity != null) {
			ms.addCurrent(currentActivity, currentEvent);
		} else {
			// TODO create a "null" Activity to return. Use 'Optional'???
			jobStatus = JobStatus.ERROR;
			logger.error("$$$$ #handleTransitionToNextNode#processCurrentEventIsSuccessorToPrevious((): null returned from 'stepToNextActivity()'.\n >> previousActivity = '{}', currentEvent = '{}'.", previousActivity.getModelId(), currentEvent.getNodeLabel());
		}
		return currentActivity;
	}

	private boolean isCurrentEventAtStartOfGroup(MonitoringEvent currentEvent) {
		return currentEvent.getNodeType().equals(LABEL_NODE) && currentEvent.isLabelNodeStartsGroup();
	}

	/**
	 * Determine the relative positions of the Catch or Rollback node (current event) and the node that caused the catch/rollback
	 * The ms param is the one that corresponds to the previousEvent, which caused the catch/rollback.<br>
	 * This method determines whether the current (catch/rollback) event is from a 'parent' flow of the previous event.<br><br>
	 * @param ms
	 * @param currentEvent
	 * @return
	 */
	private boolean isCurrentEventFromParentFlow(MonitoringState ms, MonitoringEvent currentEvent) {
		int previousEventIndex = stateStack.indexOf(ms);
		for (int i = 0; i < stateStack.size(); i++) {
			MonitoringState mState = stateStack.get(i);
			if (mState.getProcessModelName().equals(currentEvent.getProcessModelName())) {
				return i < previousEventIndex;
			}
		}
		return false; // currentEventIndex < previousEventIndex; 
	}

	private boolean isCurrentEventFromChildFlow(MonitoringState ms, MonitoringEvent previousEvent, MonitoringEvent currentEvent) {
		for (Activity processedActivity : ms.getProcessedActivities()) {
			if (processedActivity.getClass().isAssignableFrom(SubProcessActivity.class)) {
				String processedActivityName = ms.getProcessModel().getActivity(processedActivity.getModelId()).getName();
				if (currentEvent.getSubflowName().contains(processedActivityName)) {
					return true;
				}
			}
		}
		return false;
	}

	private void handleTransitionToNextNode(MonitoringState ms, MonitoringEvent currentEvent) {
		Activity previousActivity = ms.getPreviousActivity();
		MonitoringEvent previousEvent = ms.getPreviousEvent();
		// Is the current Event a successor of the previousEvent - if a Label node and it starts a Group, this was handled in #processCurrentEventNotSuccessorToPrevious()
		if (previousEvent != null && currentEvent.isSuccessorToPreviousEvent(previousEvent)) {
			processCurrentEventIsSuccessorToPrevious(currentEvent, ms);
		} else if (previousActivity.getModelId().equals(START_ACTIVITY_MODEL_ID) && currentEvent.getNodeType().equals(LABEL_NODE)) { // Added to handle introduction of 'labelNodeStartsGroup = false'
			stepToNextActivity(previousActivity, previousEvent, currentEvent);
		} else {			
			// We have an event that is not the target of the previous event.
			processCurrentEventNotSuccessorToPrevious(ms, currentEvent);
		}
	}

//	private Activity handleExitFromSubflow(MonitoringState ms, Activity previousActivity, MonitoringEvent previousEvent, MonitoringEvent currentEvent) {
//		stepToEndOfSubflow(ms, previousActivity, previousEvent, currentEvent); // returns Activity endActivity - not used
//		// Subflow complete, restore previous State 
//		SubProcessActivity subProcessActivity = restoreAfterSubflow(ms, currentEvent); //This will be SubProcessActivity that 'called' the subflow
//		
//		return subProcessActivity;
//	}

	/**
	 * Finds a subflow in a tree of subflows
	 * 
	 * @param ms
	 * @param previousActivity
	 * @param previousEvent
	 * @param currentEvent
	 * @return
	 */
	private Activity handleTransitionToSubflow(MonitoringState ms, Activity previousActivity, MonitoringEvent previousEvent, MonitoringEvent currentEvent) {
		Activity currentActivity = null;		
		int levelJump = currentEvent.getFlowLevel() - stateStack.size();
		if (levelJump == 1) {
			String subflowName = currentEvent.getSubflowName(); 
			currentActivity = handleEntryToSubflow(ms, subflowName, currentEvent);
			// We are now tracking in the subflow
			ms = stateStack.peek();
			currentActivity = processCurrentEventIsSuccessorToPrevious(currentEvent, ms);
			return currentActivity;
		}
		
		// Find the subflow that created the current event in a tree of subflows
		// 	When the flowLevel for current event is 1 greater than the stack size e.g stack-size = 2 and current event flow-level = 3. 
		// 	Then we can stepToChildSubflow from level 2 to level 3
		while (levelJump > 0) { 
			//  The current event is the first node in a child subflow of the current subflow, and the currentEvent is its first node...
			String[] pathToSubflowParts = currentEvent.getNodeLabel().split(REGEX_DOT); // 1stSubflow.2ndSubflow.3rdSubflow...nodeName
			String subflowName = pathToSubflowParts[2- levelJump]; // e.g if levelJump = 2, then use 1stSubflow, (index = 0); levelJump = 1, use 2ndSubflow (index = 1)

			// Now transition from the current SubflowNode to next level SubflowNode
			currentActivity = handleEntryToSubflow(ms, subflowName, currentEvent); // handleTransitionToChildSubflow
			ms = stateStack.peek(); // Restore previous MonitoringSate
			levelJump = currentEvent.getFlowLevel() - stateStack.size();
			if (levelJump == 0) {
				currentActivity = processCurrentEventIsSuccessorToPrevious(currentEvent, ms); // Process the current event
			}				
		}		

		return currentActivity;
	}

	/**
	 * Get the subProcessStartActivityModell from the Process Model
	 * Create the subProcessStartActivity by creating it as the ChildActivity of the subProcessActivity
	 * Create the subProcessStartEvent
	 * Step to the current event's node/ActivityModel from Start - <b>note</b>: the current event might be from a sub-subflow
	 * 
	 * @param ms (newly created, so is empty)
	 * @param subflowName
	 * @param currentEvent
	 * @return
	 */
	private Activity handleEntryToSubflow(MonitoringState ms, String subflowName, MonitoringEvent currentEvent) {
		// Step 1 - enter the subflow
		SubflowMonitoringEvent	subflowEvent = createSubflowEvent(ms, subflowName, currentEvent);
		
		// Check that the previous event/activity is a predecessor of the subflow node - when predecessor is a Flow Node this might not be the case
		Activity predecessorActivity = ms.getCompletedPredecessorActivityForCurrentEvent(subflowEvent);
		// Is predecessor not the previous activity?
		if (predecessorActivity != ms.getPreviousActivity()) {
			// If not, create the transition from the predecessor to the currentEvent
			MonitoringEvent predecessorEvent = ms.getCompletedPredecessorEventForCurrentEvent(predecessorActivity);
			predecessorEvent.setCreationTime(ms.getPreviousEvent().getCreationTime()); // set it's creation time to that of the previous event
			ms.addCurrent(predecessorActivity, predecessorEvent); // Make it look as though The Flow Node was the previous one
		}
		
		SubProcessActivity subProcessActivity = enterTheSubflow(ms, currentEvent, subflowEvent);
		ms = stateStack.peek();
		
		// Step 2 - start the subflow processing 		
		Activity subProcessStartActivity = startToProcessTheSubflow(ms, currentEvent, subflowEvent, subProcessActivity);
		
		return subProcessStartActivity;
	}

	private SubProcessActivity enterTheSubflow(MonitoringState ms, MonitoringEvent currentEvent, SubflowMonitoringEvent subflowEvent) {
		SubProcessActivityModel subProcessActivityModel = subflowEvent.getSubProcessActivityModel();
		SubProcessActivity subProcessActivity = ms.getPreviousActivity().stepToSubProcess(subProcessActivityModel).build();
		setActivityTimesAndPayloads(ms.getPreviousActivity(), ms.getPreviousEvent(), subflowEvent);
		ms.addCurrent(subProcessActivity, subflowEvent);
		
		ProcessModel subProcessModel = findProcessModelForSubflow(stateStack, subProcessActivityModel.getName(), currentEvent);		
		// Create a new state for the currentEvent's ProcessModel - and push to top of stack
		MonitoringState msSubflow = createMonitoringStateForSubflow(ms, subProcessModel);
		ms = stateStack.push(msSubflow);
		return subProcessActivity;
	}

	private ProcessModel findProcessModelForSubflow(Stack<MonitoringState> stateStack, String subflowName, MonitoringEvent currentEvent) {
		int levelJump = currentEvent.getFlowLevel() - stateStack.size();
		String processFlowPath = null;
		// We want to go down 1 level from current postion in the stack
		if (levelJump == 1) {
			processFlowPath = currentEvent.getProcessFlowPath();
		} else {
			// levelJump > 1
			processFlowPath = currentEvent.getMessageFlow()+DOT+subflowName;
		}
		
		return flowToProcessModelCache.getProcessModelForFlow(processFlowPath);
	}

	private Activity startToProcessTheSubflow(MonitoringState ms, MonitoringEvent currentEvent, SubflowMonitoringEvent subflowEvent, SubProcessActivity subProcessActivity) {
		// We are now tracking the message in the subProcess - begin by creatng a 'start' child activity
		ProcessModel pm = ms.getProcessModel();
		ActivityModel subProcessStartActivityModel = ms.getStartActivityModel();
        Activity subProcessStartActivity = subProcessActivity.createChildActivity(subProcessStartActivityModel).build();
        // Create the corresponding Start Event - the current event holds the local subflow name, whereas the ms holds the actual subflow
        // Note: The subflow name to use depends on where we are in the 'chain' of subflows in the current event's nodeLabel. This method steps down the chain one subflow at a time 
        String subFlowName = null;
        int levelJump = currentEvent.getFlowLevel() - stateStack.size();
        if (levelJump == 0) {
        	// We are in the 'destination' subflow, so use the event's subflow name
        	subFlowName = currentEvent.getSubflowName();
        } else {
        	String[] nodeLabelParts = currentEvent.getNodeLabel().split(REGEX_DOT);
        	subFlowName = nodeLabelParts[levelJump - 1];
        }

        MonitoringEvent subProcessStartEvent = createSubProcessStartEvent(pm, subFlowName, subflowEvent, currentEvent); 
		ms.addCurrent(subProcessStartActivity, subProcessStartEvent);
		return subProcessStartActivity;
	}

	private SubProcessActivity restoreAfterSubflow(MonitoringState ms, MonitoringEvent currentEvent) {
		MonitoringEvent finalSubflowEvent = ms.getPreviousEvent();
		// 'pop' the stack.
		ms = restorePreviousState(stateStack);
		
		SubProcessActivity subProcessActivity = (SubProcessActivity)ms.getPreviousActivity();
		SubflowMonitoringEvent subflowMonitoringEvent = (SubflowMonitoringEvent)ms.getPreviousEvent();
		subflowMonitoringEvent.setBodyOut(finalSubflowEvent.getBodyOut());
		
		if (subProcessActivity.getParent() != null && subProcessActivity.getParent().getClass().isAssignableFrom(Group.class)) {
			// This will create a transition to Group 'end' if oldPreviousActivity is at the end of the Group
			if (ms.doesCurrentActivityInGroupHaveEndSuccessor(subProcessActivity)) {
				// FIXME we need to do something  here - step to end of group??
			}
		}
		if (finalSubflowEvent instanceof SubflowMonitoringEvent) { // The subflow event's end time is that of its last event
			subflowMonitoringEvent.setCreationTime(finalSubflowEvent.getCreationTime());
//		} else {
//			subflowMonitoringEvent.setCreationTime(subflowMonitoringEvent.getCreationTime());
		}
		return subProcessActivity;	
	}

	private MonitoringState restorePreviousState(Stack<MonitoringState> stateStack) {
		MonitoringState ms = stateStack.pop();   // This 'pops' the current stack. 
		ms.clear(); // Clear object rewfs in Lists etc 
		ms = stateStack.peek(); // Restore prvious MonitoringSate
		return ms;
	}

	private MonitoringState createMonitoringStateForSubflow(MonitoringState ms, ProcessModel subProcessModel) {
		Job job = ms.getJob();
		MonitoringState msSubflow = new MonitoringState(subProcessModel);
		msSubflow.setJob(job);
		return msSubflow;
	}

	private SubflowMonitoringEvent createSubflowEvent(MonitoringState ms, String subflowName, MonitoringEvent currentEvent) {
		// The creation time at entry to the subflow will be that of the current event - i.e the first event of the subflow and is set here
		// The creation time at exit from the subflow will be that of the previous event - i.e the last event of the subflow and set externally
		ProcessModel pm = ms.getProcessModel();
		MonitoringEvent previousEvent = ms.getPreviousEvent();
		
		SubflowMonitoringEvent subflowEvent = new  SubflowMonitoringEvent();		
		subflowEvent.setApplicationData(currentEvent.getApplicationData());
		subflowEvent.setBodyIn(previousEvent.getBodyOut());
		// Can only set BodyOut at exit from subflow from previousEvent.getBodyOut() that is the final event in the subflow.
		subflowEvent.setCounter(0); // This isn't used after the events have been assembled.
		subflowEvent.setCreationTime(currentEvent.getCreationTime()); 
		subflowEvent.setIntSvrName(currentEvent.getIntSvrName());
		subflowEvent.setIncludeIntSvrName(currentEvent.isIncludeIntSvrName());
		subflowEvent.setIncludeSchemaName(currentEvent.isIncludeSchemaName());
		subflowEvent.setMessageFlow(currentEvent.getMessageFlow());

		subflowEvent.setEventName(subflowName+DOT+MessageEvents.IN_TERMINAL);
		// If this is being created by a subflow: nodeLabel = {subflowA}.{subflowB}.{node-name} ==> {subflowA}.{subflowB}.sfName
		if (stateStack.size() == 1) {
			if (previousEvent.isFromMsgFlow()) {
				subflowEvent.setNodeLabel(subflowName);
			} else if (currentEvent.isFromSubFlow()) {
				subflowEvent.setNodeLabel(StringUtils.substringBefore(currentEvent.getNodeLabel(), DOT+currentEvent.getLocalNodeLabel()));
			}
		} else if (currentEvent.getNodeLabel().contains(subflowName)) {
			subflowEvent.setNodeLabel(StringUtils.substringBefore(currentEvent.getNodeLabel(), DOT+currentEvent.getLocalNodeLabel()));
		} else {
			logger.error("*** #createSubflowEvent() for subflow '{}': not expecting to end up here - check it out", subflowName);
			subflowEvent.setNodeLabel(StringUtils.substringBefore(currentEvent.getNodeLabel(), DOT+currentEvent.getLocalNodeLabel()));
		}
			
		subflowEvent.setNodeType(SUB_FLOW_NODE);
		subflowEvent.setUniqueflowName(currentEvent.getUniqueflowName());
		subflowEvent.setProcessModel(pm); //ms.getProcessModel());
		return subflowEvent;
	}

	private GroupMonitoringEvent createGroupEvent(MonitoringState ms, String groupName, MonitoringEvent previousEvent, MonitoringEvent currentEvent) {
		// This can only be a close approximation to what would have been emitted if Subflow nodes generated monitoring events.
		GroupMonitoringEvent groupEvent = new  GroupMonitoringEvent();
// TODO set any specific values for a Group Mon Event		
		groupEvent.setApplicationData(currentEvent.getApplicationData());
		groupEvent.setBodyIn(previousEvent.getBodyOut());
		groupEvent.setBodyOut(currentEvent.getBodyIn());
		groupEvent.setCounter(0);
		if (currentEvent.isFromSubFlow()) {
			groupEvent.setCreationTime(currentEvent.getCreationTime()); // We have the actual creation time of first event in subflow
		} else {
			groupEvent.setCreationTime(currentEvent.getCreationTime()); // + previousEvent.getCreationTimeMilliSecs()) / 2); // Best fit is in middle of the times 
		}
		groupEvent.setExceptionList(currentEvent.getExceptionList());
		groupEvent.setIntSvrName(currentEvent.getIntSvrName());
		groupEvent.setGlobalTransactionId(currentEvent.getGlobalTransactionId());
		groupEvent.setIncludeIntSvrName(currentEvent.isIncludeIntSvrName());
		groupEvent.setIncludeSchemaName(currentEvent.isIncludeSchemaName());
		groupEvent.setLocalTransactionId(currentEvent.getLocalTransactionId());
		groupEvent.setMessageFlow(currentEvent.getMessageFlow());
		groupEvent.setEventName(groupName+DOT+MessageEvents.IN_TERMINAL);
		// If this is being created by a subflow: nodeLabel = {subflowA}.{subflowB}.{node-name} ==> {subflowA}.{subflowB}.sfName
		groupEvent.setNodeLabel(previousEvent.isFromSubFlow() ? StringUtils.replace(previousEvent.getNodeLabel(), previousEvent.getLocalNodeLabel(), groupName) : groupName);
		groupEvent.setNodeType(GROUP_TYPE); //GROUP_NODE);
		groupEvent.setUniqueflowName(currentEvent.getUniqueflowName());
		groupEvent.setProcessModel(ms.getProcessModel()); // must be same Proc Model as current  //findProcessDefinitionName(ms, sfName));
		return groupEvent;
	}

	/**
	 * Creates a Transition-2-End for a node with an 'End' successor
	 * 
	 * @param ms
	 * @param previousActivity
	 * @param previousEvent
	 * @param currentEvent
	 * @return
	 */
	private Activity stepToEndOfSubflow(MonitoringState ms, Activity previousActivity, MonitoringEvent previousEvent, MonitoringEvent currentEvent) {
		ProcessModel pm = previousEvent.getProcessModel();
		ActivityModel subProcessActivityEndModel = pm.getActivity(END_ACTIVITY_MODEL_ID);
		Activity subProcessEndActivity = previousActivity.stepTo(subProcessActivityEndModel).build();
		setActivityTimesAndPayloads(previousActivity, previousEvent, currentEvent);
		// Set duration of subProcessEndActivity to 0
		subProcessEndActivity.setExecution(previousActivity.getExecution());
		subProcessEndActivity.setDuration(0);
		subProcessEndActivity.setCpuTime(0);
		
		return subProcessEndActivity;
	}

	protected Activity stepToNextActivity(Activity previousActivity, MonitoringEvent previousEvent, MonitoringEvent currentEvent) {
		
		Activity currentActivity = previousActivity.stepTo(currentEvent.getActivityModel()).build(); //currentActivityModel).build();
		setActivityTimesAndPayloads(previousActivity, previousEvent, currentEvent);
		
		return currentActivity;
	}

	/**
	 * Set correct elapsed times for previous Activity using the time difference between current and previous events
	 * Set the input and output payloads for the previous Activity from the currentEvent
	 * @param previousActivity
	 * @param previousEvent
	 * @param currentEvent
	 */
	protected void setActivityTimesAndPayloads(Activity previousActivity, MonitoringEvent previousEvent, MonitoringEvent currentEvent) {
		setActivityTimes(previousActivity, previousEvent, currentEvent);
		setActivityPayloads(previousActivity, previousEvent, currentEvent);
	}
	
	protected void setActivityTimes(Activity previousActivity, MonitoringEvent previousEvent, MonitoringEvent currentEvent) {
		long duration = currentEvent.getCreationTimeMillis() - previousEvent.getCreationTimeMillis();
		previousActivity.setExecution(previousEvent.getCreationTime()); // Start time
		previousActivity.setDuration(duration < 1 ? 1 : duration );
		previousActivity.setCpuTime(duration);
	}
	
	protected void setActivityPayloads(Activity previousActivity, MonitoringEvent previousEvent, MonitoringEvent currentEvent) {
		// When tracing is active, this sets the payloads for the previous activity - hence the reference to previousEvent 
		if (currentEvent instanceof ErrorMonitoringEvent) { // Event from an 'error' terminal 
			previousActivity.processOutput(null); // The Out terminal will not have any data			
		} else {
			previousActivity.processInput(previousEvent.getBodyIn()); //currentEvent.getBodyIn());
			previousActivity.processOutput(previousEvent.getBodyOut()); //currentEvent.getBodyOut());
		}
		
	}	

	protected void reportFaultEvent(MonitoringState ms, ErrorMonitoringEvent currentEvent) throws Exception {
		logger.debug("SOAP Fault reported by this node '{}'", currentEvent.getNodeLabel());
		MonitoringEvent previousEvent = ms.getPreviousEvent();
		Activity previousActivity = ms.getPreviousActivity();
		Event exception = previousActivity.createEvent(); // This is the node for which the SOAP fault occurred
		
		if (currentEvent.getExceptionList() != null && !currentEvent.getExceptionList().equals("")) {			
			exception.setMessage("SOAP Fault reported by this node, with payload and ExceptionList")
			.setCode("98")
			.setPayload(currentEvent.getBodyIn() != null ? njamsClient.getSerializer(String.class).serialize(previousEvent.getBodyIn()) : "")
			.setStacktrace(currentEvent.getExceptionList())
			.setStatus(EventStatus.ERROR);
		} else {
			// No Exception List
			exception.setMessage("SOAP Fault reported by this node, with payload")
			.setCode("98")
			.setPayload(currentEvent.getBodyIn() != null ? njamsClient.getSerializer(String.class).serialize(previousEvent.getBodyIn()) : "")
			.setStatus(EventStatus.ERROR);
		}
		
		previousActivity.setActivityStatus(ActivityStatus.ERROR);
	}
	
	protected void reportFailureEvent(MonitoringState ms, ErrorMonitoringEvent currentEvent) throws Exception {
		logger.debug("Failure reported by this node '{}'", currentEvent.getNodeLabel());
		MonitoringEvent previousEvent = ms.getPreviousEvent();
		Activity previousActivity = ms.getPreviousActivity();
		Event exception = previousActivity.createEvent(); // This is the node which caused the Failure terminal to produce a message
		
		if (currentEvent.getExceptionList() != null && !currentEvent.getExceptionList().equals("")) {
			exception.setMessage("Failure reported by this node, with payload and ExceptionList")
			.setCode("97")
			.setPayload(currentEvent.getBodyIn() != null ? njamsClient.getSerializer(String.class).serialize(previousEvent.getBodyIn()) : "")
			.setStacktrace(currentEvent.getExceptionList())
			.setStatus(EventStatus.ERROR);			
		} else {
			// No Exception List
			exception.setMessage("Failure reported by this node, with payload")
			.setCode("97")
			.setPayload(currentEvent.getBodyIn() != null ? njamsClient.getSerializer(String.class).serialize(previousEvent.getBodyIn()) : "")
			.setStatus(EventStatus.ERROR);
		}
		
		previousActivity.setActivityStatus(ActivityStatus.ERROR);
	}
	
	protected void reportCatchEvent(MonitoringState ms, ErrorMonitoringEvent currentEvent) throws Exception {
		logger.debug("Caught exception from this node '{}'", currentEvent.getNodeLabel());
		MonitoringEvent previousEvent = ms.getPreviousEvent();
		Activity previousActivity = ms.getPreviousActivity();
		Event exception = previousActivity.createEvent(); // This is the node which caused the Catch terminal to produce a message
		
		if (currentEvent.getExceptionList() != null && !currentEvent.getExceptionList().isEmpty()) {
			exception.setMessage("Caught exception from this node, with payload and ExceptionList")
			.setCode("96")
			.setPayload(currentEvent.getBodyIn() != null ? njamsClient.getSerializer(String.class).serialize(previousEvent.getBodyIn()) : "")
			.setStacktrace(currentEvent.getExceptionList())
			.setStatus(EventStatus.ERROR);
		} else {
			// No Exception List
			exception.setMessage("Caught exception from this node, with payload")
			.setCode("96")
			.setPayload(currentEvent.getBodyIn() != null ? njamsClient.getSerializer(String.class).serialize(previousEvent.getBodyIn()) : "")
			.setStatus(EventStatus.ERROR);
		}
		
		previousActivity.setActivityStatus(ActivityStatus.ERROR);
	}

	protected Job reportRollbackEvent(MonitoringState ms, ErrorMonitoringEvent currentEvent) throws Exception {
		/*
		 * When a rollback occurs:
		 * 	- the rollback event is the final one in the event set (same localTxnId) - there is no TxnEnd
		 *  - if the error occurs in the output node, no event is published, so we can only report the name of the previous node.
		 *  
		 *  - the transaction restarts at the entry node, with a second TxnStart event (new localTxnId), which flows from the 'Failure' terminal
		 *  - Monitoring events are produced for the failure 'leg', including a Txnend event
		 *  - Note: all of the events have the same globlaTxnId (derived from the CorrelationId or first 24 chars of JMS CorrelationId)
		 *    TODO  Thus we could tie the two sets of events together, should it become essential, but we can't be sure that they would follow in sequence in a busy system.
		 *  
		 *  In the case of the rollback occurring in the input node (typically an XML validation failure), the above produces slightly strange events:
		 *   - Only the rollback event is received (counter=2) from the first event set (previousActivity = rollback)
		 *   - Only the TxnEnd event is received (counter=2) from the second set (previousActivity = TxnEnd)       	
		 */

		MonitoringEvent previousEvent = ms.getPreviousEvent();
		logger.debug("Rollback reported by this node '{}'.", previousEvent.getNodeLabel());
		Job job = ms.getJob();
		Activity previousActivity = ms.getPreviousActivity();
		
		Event exception = previousActivity.createEvent(); // This is the node which caused the Catch terminal to produce a message
		if (catchHasBeenHandled) {			
			exception.setMessage("Rollback occurred, details reported in the previous message")
			.setCode("99");			
		} else if (previousEvent != null){
			if (currentEvent.getExceptionList() != null && !currentEvent.getExceptionList().equals("" )) {
				exception.setMessage("Rollback reported by this node, with payload and ExceptionList")
				.setCode("99")
				.setPayload(previousEvent.getBodyIn() != null ? njamsClient.getSerializer(String.class).serialize(previousEvent.getBodyIn()) : "") //TODO make payload in clear optional
				.setStacktrace(currentEvent.getExceptionList())
				.setStatus(EventStatus.ERROR);
			}  else {
				// No Exception List
				exception.setMessage("Rollback reported by this node, with payload")
				.setCode("99")
				.setPayload(previousEvent.getBodyIn() != null ? njamsClient.getSerializer(String.class).serialize(previousEvent.getBodyIn()) : "")
				.setStatus(EventStatus.ERROR);
			}
		}
		
		previousActivity.setActivityStatus(ActivityStatus.ERROR);
		
		return job;
	}

	/**
	 * @param utfInput - IBM MQ puts the first 24 bytes of JMSCorrelationId into $Root/MQMD/CorrelId as Hex UTF-8
	 * @return as ASCII
	 */
	protected String decodeUTF8(String utfInput) {
		try {
			System.out.println("++++++++++ utfInput = "+utfInput);
			return new String(DatatypeConverter.parseHexBinary(StringUtils.strip(utfInput, "0")), UTF8_CHARSET);
		} catch (Exception e) {
			logger.error(e.toString(), e);
		}
		return new String("1111111");
	}

}
