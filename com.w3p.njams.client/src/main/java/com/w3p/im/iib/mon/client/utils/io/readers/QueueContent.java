package com.w3p.im.iib.mon.client.utils.io.readers;

import java.util.Hashtable;
import java.util.Set;

public class QueueContent implements IContent {
	
	private final Hashtable<String, String> contents = new Hashtable<>(); 


	public QueueContent() {
	}

	@Override
	public void addContent(String name, String content) {
		contents.put(name, content);
	}

	@Override
	public String getContent(String name) {
		return contents.get(name);
	}
	
	@Override
	public int getSize() {
		return contents.size();
	}
	
	@Override
	public Set<String> getNames() {
		// TODO Auto-generated method stub
		return null;
	}

	/**
	 * Only for jUnit testing
	 * @return
	 */
	protected String getContentOfFirstItem() {
		return contents.values().iterator().next();
	}
}
