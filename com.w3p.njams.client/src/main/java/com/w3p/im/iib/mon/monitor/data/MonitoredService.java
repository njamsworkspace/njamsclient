package com.w3p.im.iib.mon.monitor.data;

public class MonitoredService extends MonitoredObject<MonitoredObject<?>> {
	
	public MonitoredService(String name) {
		super(name);
	}

	@Override
	public String toString() {
		return "Monitored Service [name=" + name + "]";
	}
}
