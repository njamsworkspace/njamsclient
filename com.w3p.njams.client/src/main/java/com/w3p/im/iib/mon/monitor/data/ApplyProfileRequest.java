package com.w3p.im.iib.mon.monitor.data;

import static com.w3p.im.iib.mon.client.constants.IClientConstants.MON_PROF; 

import com.w3p.api.config.proxy.BrokerProxy;
import com.w3p.api.config.proxy.MessageFlowProxy;
import com.w3p.im.iib.mon.topology.model.MessageFlow;

public class ApplyProfileRequest {
	
	private int timeToWait; // Wait-time in seconds for sync calls to remote broker for applying Monitoring Profiles   
	private boolean applyProfileIfUnchanged; // 'true' = generate profile and apply it even if it has not changed (compared to current file)
	private boolean profileHasChanged;
	private final BrokerProxy intNodeProxy;
	private MessageFlowProxy messageFlowProxy;
	private String intNodeName;
	private String monProfileDir;
	
	private final String profileAsXml;
	private final String monitoringProfile;
	private final String messageFlow;

	
	public ApplyProfileRequest(BrokerProxy intNodeProxy, String profileAsXml, MessageFlow messageFlowModel) {
		this.intNodeProxy = intNodeProxy;
		this.profileAsXml = profileAsXml;
		
		this.messageFlow = messageFlowModel.getNameWithSchema();
		this.messageFlowProxy = messageFlowModel.getMessageFlowProxy();
		this.monitoringProfile = messageFlowModel.isIncludeSchemaName() ? this.messageFlow+MON_PROF : messageFlowModel.getName()+MON_PROF;
	}
	
	public int getTimeToWait() {
		return timeToWait;
	}
	public ApplyProfileRequest setTimeToWait(int timeToWait) {
		this.timeToWait = timeToWait;
		return this;
	}
	public boolean isApplyProfileIfUnchanged() {
		return applyProfileIfUnchanged;
	}
	public ApplyProfileRequest setApplyProfileIfUnchanged(boolean applyProfile) {
		this.applyProfileIfUnchanged = applyProfile;
		return this;
	}

	public String getProfileAsXml() {
		return profileAsXml;
	}
	public String getMonitoringProfile() {
		return monitoringProfile;
	}
	public String getIntNodeName() {
		return intNodeName;
	}
	public ApplyProfileRequest setIntNodeName(String intNodeName) {
		this.intNodeName = intNodeName;
		return this;
	}
	public String getMonProfileDir() {
		return monProfileDir;
	}
	public ApplyProfileRequest setMonProfileDir(String monProfileDir) {
		this.monProfileDir = monProfileDir;
		return this;
	}
	public BrokerProxy getIntNodeProxy() {
		return intNodeProxy;
	}
	public MessageFlowProxy getMessageFlowProxy() {
		return messageFlowProxy;
	}
	public ApplyProfileRequest setMessageFlowProxy(MessageFlowProxy messageFlowProxy) {
		this.messageFlowProxy = messageFlowProxy;
		return this;
	}

	public boolean isProfileHasChanged() {
		return profileHasChanged;
	}

	public ApplyProfileRequest setProfileHasChanged(boolean profileHasChanged) {
		this.profileHasChanged = profileHasChanged;
		return this;
	}

	public String getMessageFlowName() {
		return messageFlow;
	}
}
