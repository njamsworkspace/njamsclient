package com.w3p.im.iib.mon.topology.model;

public class FlowNodeConnection {
	
	public FlowNodeConnection(String sourceNode, String sourceOutTerminal, String targetNode, String targetInTerminal) {
		super();
		this.sourceNode = sourceNode;
		this.sourceOutTerminal = sourceOutTerminal;
		this.targetNode = targetNode;
		this.targetInTerminal = targetInTerminal;
	}
	private String sourceNode;
	private String sourceOutTerminal;
	private String targetNode;
	private String targetInTerminal;
	
	public String getSourceNode() {
		return sourceNode;
	}
	public void setSourceNode(String sourceNode) {
		this.sourceNode = sourceNode;
	}
	public String getSourceOutTerminal() {
		return sourceOutTerminal;
	}
	public void setSourceOutTerminal(String sourceOutTerminal) {
		this.sourceOutTerminal = sourceOutTerminal;
	}
	public String getTargetNode() {
		return targetNode;
	}
	public void setTargetNode(String targetNode) {
		this.targetNode = targetNode;
	}
	public String getTargetInTerminal() {
		return targetInTerminal;
	}
	public void setTargetInTerminal(String targetInTerminal) {
		this.targetInTerminal = targetInTerminal;
	}
	@Override
	public String toString() {
		return "FlowNodeConnection [sourceNode=" + sourceNode + ", sourceOutTerminal=" + sourceOutTerminal
				+ ", targetNode=" + targetNode + ", targetInTerminal=" + targetInTerminal + "]";
	}

	
}
