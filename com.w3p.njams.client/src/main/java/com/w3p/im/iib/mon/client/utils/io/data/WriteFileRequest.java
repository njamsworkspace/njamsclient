package com.w3p.im.iib.mon.client.utils.io.data;

public class WriteFileRequest extends FileRequest implements IWriteFileRequest {
	
	private String dest;
	protected boolean createFolderIfNotExists;
	protected boolean clearFolderIfNotEmpty;
	protected boolean replaceFilesIfExist;

	public WriteFileRequest() {
	}
	
	@Override
	public IWriteFileRequest setDestination(String dest) {
		this.dest = dest;
		return this;
	}

	@Override
	public String getDestination() {
		return dest;
	}

	@Override
	public IWriteFileRequest setCreateFolderIfNotExists(boolean createFolderIfNotExists) {
		this.createFolderIfNotExists = createFolderIfNotExists;
		return this;
	}

	@Override
	public boolean isCreateFolderIfNotExists() {
		return createFolderIfNotExists;
	}

	@Override
	public IWriteFileRequest setClearFolderIfNotEmpty(boolean clearFolderIfNotEmpty) {
		this.clearFolderIfNotEmpty = clearFolderIfNotEmpty;
		return this;
	}

	@Override
	public boolean isClearFolderIfNotEmpty() {
		return clearFolderIfNotEmpty;
	}

	@Override
	public IWriteFileRequest setReplaceFilesIfExist
	(boolean replaceFilesIfExist) {
		this.replaceFilesIfExist = replaceFilesIfExist;
		return this;
	}

	@Override
	public boolean isReplaceFilesIfExist() {
		return replaceFilesIfExist;
	}
}
