package com.w3p.im.iib.mon.exceptions;

/**
 * @author JohnO
 *
 * To change the template for this generated type comment go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
public interface ILogableException {
	
	public String getMessage();
	
	public Throwable getCause();
	
	public boolean isLogged();
	
	abstract void logMessage();
}
