package com.w3p.im.iib.mon.topology.data;

import com.im.njams.sdk.Njams;
import com.w3p.im.iib.mon.topology.model.TopologyObject;

public class TopologyForNjamsRequest {
	
	private final Njams njamsClient;
	
	private final TopologyObject topologyObject;
	private final String intSvrName;
	private boolean includeIntSvrName;
	private String clientHome;
	
	public TopologyForNjamsRequest(Njams njamsClient, TopologyObject topologyObject, String intSvrName) {
		this.njamsClient = njamsClient;
		this.topologyObject = topologyObject;
		this.intSvrName = intSvrName;
	}


	public TopologyObject getTopologyObject() {
		return topologyObject;
	}


	public  String getIntServerName() {
		return intSvrName;
	}
	
	public Njams getNjamsClient() {
		return njamsClient;
	}

	public boolean isIncludeIntSvrName() {
		return includeIntSvrName;
	}

	public TopologyForNjamsRequest setIncludeIntSvrName(boolean includeIntSvrName) {
		this.includeIntSvrName = includeIntSvrName;
		return this;
	}


	public String getClientHome() {
		return clientHome;
	}


	public TopologyForNjamsRequest setClientHome(String clientHome) {
		this.clientHome = clientHome;
		return this;
	}
}
