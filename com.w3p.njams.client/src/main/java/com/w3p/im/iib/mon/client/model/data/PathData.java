package com.w3p.im.iib.mon.client.model.data;

import java.util.List;
import java.util.stream.Collectors;

import org.apache.commons.lang3.StringUtils;

import static java.util.Arrays.asList;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;

public class PathData implements Serializable {

	private static final long serialVersionUID = 1L;
	private final String pathAsString;
	private final List<String> parts;

	public PathData(String pathAsString) {
//		String cleanPathAsString = StringUtils.substringAfter(pathAsString, ">");
		this(Arrays.asList(StringUtils.substringAfter(pathAsString, ">").split(">")));
	}
	
	public PathData(final String... parts) {
		this(asList(parts));
	}

	public PathData(final List<String> parts) {
		this.parts = new ArrayList<>(parts);
		this.pathAsString = ">" + this.parts.stream().collect(Collectors.joining(">")) + ">";
	}

	public String getPathAsString() {
		return pathAsString;
	}

	public List<String> getParts() {
		return parts;
	}
	
    public String getObjectName() {
    	// The Object Name is the last part of the path
        return parts.get(parts.size() - 1);
    }

	@Override
	public String toString() {
		return "PathData [pathAsString=" + pathAsString + "]";
	}

}
