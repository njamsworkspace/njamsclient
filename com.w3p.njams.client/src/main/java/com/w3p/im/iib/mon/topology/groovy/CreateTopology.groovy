package com.w3p.im.iib.mon.topology.groovy

import static com.w3p.im.iib.mon.client.constants.IClientConstants.UTF_8;

import com.w3p.im.iib.mon.topology.model.IntegrationServer
import com.ibm.broker.config.proxy.BrokerProxy
import com.w3p.im.iib.mon.topology.model.FlowNode
import com.w3p.im.iib.mon.topology.model.FlowNodeConnection
import com.w3p.im.iib.mon.topology.model.MessageFlow
import groovy.xml.MarkupBuilder
import groovy.xml.StreamingMarkupBuilder


class CreateTopology {


	String createAsXML (List<IntegrationServer> isl, BrokerProxy intNode) {

		def builder = new StreamingMarkupBuilder()
		builder.encoding = UTF_8
		def topology = {
			mkp.xmlDeclaration()
			topology {
				integrationNode(name:intNode.name, running:intNode.running){
					isl.each { is ->
						integrationServer(name:is.name, running:is.running) {
							is.messageFlows.each { mf ->
								messageFlow(name:mf.name, running:mf.running) {
									mf.flowNodes.each { fn ->
										node(name:fn.name) {
											type(fn.type)
											fn.flowNodeConnections.each { fnc ->
												connection {
													sourceTerminal(fnc.sourceOutTerminal)
													targetNode(name:fnc.targetNode, terminal:fnc.targetInTerminal)
												}
											}
										}
									}
								}
							}
						}
					}
				}
			}
		}

		String topologyAsXML = builder.bind(topology)
		return topologyAsXML
	}

}
