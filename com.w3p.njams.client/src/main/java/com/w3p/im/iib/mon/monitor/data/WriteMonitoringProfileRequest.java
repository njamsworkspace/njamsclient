package com.w3p.im.iib.mon.monitor.data;

import com.ibm.broker.config.proxy.BrokerProxy;
import com.ibm.broker.config.proxy.MessageFlowProxy;
import com.w3p.im.iib.mon.topology.model.MessageFlow;

public class WriteMonitoringProfileRequest {
	
	private static final String MP ="_MonitoringProfile";
	private static final String XML =".xml";
	
//	private int timeToWait; // Wait-time in seconds for sync calls to remote broker for applying Monitoring Profiles   
//	private boolean applyProfileIfChanged; // 'true' = generate profile and apply it, if it has changed (compare with current file)
	private boolean  forceApplyMonitoringProfile = false;
	
//	private BrokerProxy intNodeProxy;
//	private MessageFlowProxy messageFlowProxy;
//	private String intNodeName;
	private String monProfileDir;
	
	private final String profileAsXml;
	private final String monitoringProfile;
	private final String monitoringProfileFile;
	private final String integrationServer;
	private final String application;
	private final String messageFlow;
	
	public WriteMonitoringProfileRequest(String profileAsXml, String integrationServer, String application, MessageFlow messageFlow) {
		this.profileAsXml = profileAsXml;
		this.integrationServer = integrationServer;
		this.application = application;
		this.messageFlow = messageFlow.getNameWithSchema();
		this.monitoringProfile = messageFlow.isIncludeSchemaName() ? this.messageFlow+MP : messageFlow.getName()+MP;
		this.monitoringProfileFile = monitoringProfile+XML;
	}
//	public int getTimeToWait() {
//		return timeToWait;
//	}
//	public WriteMonitoringProfileRequest setTimeToWait(int timeToWait) {
//		this.timeToWait = timeToWait;
//		return this;
//	}
//	public boolean isApplyProfileIfChanged() {
//		return applyProfileIfChanged;
//	}
//	public WriteMonitoringProfileRequest setApplyProfileIfChanged(boolean applyProfile) {
//		this.applyProfileIfChanged = applyProfile;
//		return this;
//	}
	public boolean isForceApplyMonitoringProfile() {
		return forceApplyMonitoringProfile;
	}
	public WriteMonitoringProfileRequest setForceApplyMonitoringProfile(boolean forceApplyMonitoringProfile) {
		this.forceApplyMonitoringProfile = forceApplyMonitoringProfile;
		return this;
	}
	public String getProfileAsXml() {
		return profileAsXml;
	}
	public String getMonitoringProfile() {
		return monitoringProfile;
	}
	public String getMonitoringProfileFile() {
		return monitoringProfileFile;
	}
	public String getIntegrationServer() {
		return integrationServer;
	}
	public String getApplication() {
		return application;
	}
//	public String getMessageFlow() {
//		return messageFlow;
//	}
//	public String getIntNodeName() {
//		return intNodeName;
//	}
//	public WriteMonitoringProfileRequest setIntNodeName(String intNodeName) {
//		this.intNodeName = intNodeName;
//		return this;
//	}
	public String getMonProfileDir() {
		return monProfileDir;
	}
	public WriteMonitoringProfileRequest setMonProfileDir(String monProfileDir) {
		this.monProfileDir = monProfileDir;
		return this;
	}
//	public BrokerProxy getIntNodeProxy() {
//		return intNodeProxy;
//	}
//	public WriteMonitoringProfileRequest setIntNodeProxy(BrokerProxy intNodeProxy) {
//		this.intNodeProxy = intNodeProxy;
//		return this;
//	}
//	public MessageFlowProxy getMessageFlowProxy() {
//		return messageFlowProxy;
//	}
//	public WriteMonitoringProfileRequest setMessageFlowProxy(MessageFlowProxy messageFlowProxy) {
//		this.messageFlowProxy = messageFlowProxy;
//		return this;
//	}
}
