package com.w3p.im.iib.mon.client.utils.io.writers;

import com.w3p.im.iib.mon.client.utils.io.data.IWriteFileRequest;
import com.w3p.im.iib.mon.client.utils.io.data.Response;

public interface IFileWriter extends IWriter {
	
	void init(IWriteFileRequest req, Response resp);

}
